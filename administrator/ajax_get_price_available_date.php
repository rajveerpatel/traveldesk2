<?php
include_once('include.inc.php');

// get Price on catgory change
if(!empty($_POST['cat_id']) && !empty($_POST['trip_slug']) && !empty($_POST['price_act']) && !empty($_POST['selected_date'])){
    $cat_id = $_POST['cat_id'];
    $trip_slug = $_POST['trip_slug'];
    $selected_date = $_POST['selected_date'];
    
    $trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND slug = '$trip_slug' AND trip_category = $cat_id "));
    
    $price = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_styles = 1 AND trip_categories = $cat_id AND trip_id = ".$trip['trip_id']." AND trip_start_date = '".$selected_date."' "));
    
    echo $price['trip_price'];
    
}

// get Trip Stopover price
if(!empty($_POST['available_date']) && !empty($_POST['trip_id']) && !empty($_POST['cat_id']) && !empty($_POST['room_acc'])){
    $cat_id = $_POST['cat_id'];
    $trip_id = $_POST['trip_id'];
    $available_date = date('Y-m-d', strtotime($_POST['available_date']));
    
    $room_acc_price = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE '$available_date' = trip_start_date AND trip_id = $trip_id AND trip_categories = $cat_id "));
    ?>
    <input type="hidden" id="land_single_room" name="land_single_room" value="<?php echo $room_acc_price['land_single_room']; ?>">
    <input type="hidden" id="land_twin_sharing" name="land_twin_sharing" value="<?php echo $room_acc_price['land_twin_sharing']; ?>">
    <input type="hidden" id="land_triple_sharing" name="land_triple_sharing" value="<?php echo $room_acc_price['land_triple_sharing']; ?>">
    <input type="hidden" id="airland_single_room" name="airland_single_room" value="<?php echo $room_acc_price['airland_single_room']; ?>">
    <input type="hidden" id="airland_twin_sharing" name="airland_twin_sharing" value="<?php echo $room_acc_price['airland_twin_sharing']; ?>">
    <input type="hidden" id="airland_triple_sharing" name="airland_triple_sharing" value="<?php echo $room_acc_price['airland_triple_sharing']; ?>">
<?php    
}
?>