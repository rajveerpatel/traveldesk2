<?php
include_once('include.inc.php');

$curr_date = date('Y-m-d');

// get trip id on catgory change
if(!empty($_POST['cat_id']) && !empty($_POST['trip_slug']) && !empty($_POST['get_trip_id'])){
    $cat_id = $_POST['cat_id'];
    $trip_slug = $_POST['trip_slug'];
    
    $trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND slug = '$trip_slug' AND trip_category = $cat_id "));
    
    echo $trip['trip_id'];
    
}

// get trip room price on catgory change
if(!empty($_POST['cat_id']) && !empty($_POST['trip_slug']) && !empty($_POST['room_cost_act'])){
    $cat_id = $_POST['cat_id'];
    $trip_slug = $_POST['trip_slug'];
    $available_date = date('Y-m-d', strtotime($_POST['available_date']));
    
    $trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND slug = '$trip_slug' AND trip_category = $cat_id "));
    
    $trip_id = $trip['trip_id'];
    
    $room_acc_price = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE '$available_date' = trip_start_date AND trip_id = $trip_id AND trip_categories = $cat_id "));
    ?>
    <input type="text" id="land_single_room" name="land_single_room" value="<?php echo $room_acc_price['land_single_room']; ?>">
    <input type="text" id="land_twin_sharing" name="land_twin_sharing" value="<?php echo $room_acc_price['land_twin_sharing']; ?>">
    <input type="text" id="land_triple_sharing" name="land_triple_sharing" value="<?php echo $room_acc_price['land_triple_sharing']; ?>">
    <input type="text" id="airland_single_room" name="airland_single_room" value="<?php echo $room_acc_price['airland_single_room']; ?>">
    <input type="text" id="airland_twin_sharing" name="airland_twin_sharing" value="<?php echo $room_acc_price['airland_twin_sharing']; ?>">
    <input type="text" id="airland_triple_sharing" name="airland_triple_sharing" value="<?php echo $room_acc_price['airland_triple_sharing']; ?>">
<?php    
    
}

// get Price on catgory change
if(!empty($_POST['cat_id']) && !empty($_POST['trip_slug']) && !empty($_POST['price_act'])){
    $cat_id = $_POST['cat_id'];
    $trip_slug = $_POST['trip_slug'];
    
    $trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND slug = '$trip_slug' AND trip_category = $cat_id "));
    
    $price = mysqli_fetch_assoc(mysqli_query($con, "SELECT MIN(land_twin_sharing) as trip_price FROM tbl_trip_prices WHERE trip_categories = $cat_id AND trip_id = ".$trip['trip_id']." "));
    
    echo $price['trip_price'];
    
}

// get available_date on catgory change
if(!empty($_POST['cat_id']) && !empty($_POST['date_act']) && !empty($_POST['prev_date']) && !empty($_POST['price_type'])){
    $cat_id = $_POST['cat_id'];
    $prev_date = date('Y-m-d', strtotime($_POST['prev_date']));
    $price_type = $_POST['price_type'];
    
    if(isset($_POST['trip_slug'])){
        $trip_slug = $_POST['trip_slug'];
        
        $trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND slug =   '$trip_slug' AND trip_category = $cat_id "));
    }
    
    if(isset($_POST['trip_id'])){
        $trip_id = $_POST['trip_id'];
    } else {
        $trip_id = $trip['trip_id'];
    }
    
    $date_qry = mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_styles = 1 AND trip_id = ".$trip_id." AND trip_categories = $cat_id AND '$curr_date' < trip_start_date ");
    while($dateres = mysqli_fetch_assoc($date_qry)){
        if($prev_date==$dateres['trip_start_date']){ $selected = 'selected'; } else { $selected = ''; }
        echo '<option value="'.$dateres['trip_start_date'].'" '.$selected.'>'.date('M d, Y', strtotime($dateres['trip_start_date'])).' &emsp; ($'.$dateres[$price_type.'_twin_sharing'].')'.'</option>';
    }
    
}
?>