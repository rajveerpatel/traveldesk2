<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    $tripid = $_GET['id'];

    if( !empty($_POST['trip_name']) ){
        
        /*if(!empty($_FILES['trip_thumb']['name'])){
            $trip_thumb=uploadfiles($_FILES['trip_thumb']['name'], $_FILES['trip_thumb']['tmp_name'], $_FILES['trip_thumb']['error'], $_FILES['trip_thumb']['size'], PACKAGE_IMG);
        }else{
            $trip_thumb=$_POST['old_thumb'];
        }*/
        
        if(isset($_POST['thumb_name'])){
            $trip_thumb = $_POST['thumb_name'];
        } else {
            $trip_thumb = '';
        }
        
        if(!empty($_POST['trip_themes'])){
            $trip_theme = $_POST['trip_themes'];
        } else {
            $trip_theme = '';
        }
        
        /*if(!empty($_POST['trip_category'])){
            $trip_cat = implode(',', $_POST['trip_category']);
        } else {
            $trip_cat = '';
        }*/
        
        if(!empty($_POST['trip_category'])){
            $trip_cat = $_POST['trip_category'];
        } else {
            $trip_cat = '';
        }
        
        if(!empty($_POST['trip_type'])){
            $trip_type = implode(',', $_POST['trip_type']);
        } else {
            $trip_type = '';
        }
        
        if(!empty($_POST['validity_start_date'])){
            $validity_start_date = date('Y-m-d', strtotime($_POST['validity_start_date']));
        } else {
            $validity_start_date = '';
        }
        
        if(!empty($_POST['validity_end_date'])){
            $validity_end_date = date('Y-m-d', strtotime($_POST['validity_end_date']));
        } else {
            $validity_end_date = '';
        }
        
        if(!empty($_POST['related_trip'])){
            $related_trip = implode(',', $_POST['related_trip']);
        } else {
            $related_trip = '';
        }
        
        /*if(!empty($_POST['trip_provides'])){
            $trip_provides = implode(',', $_POST['trip_provides']);
        } else {
            $trip_provides = '';
        }*/
        
        $tripdays = $_POST['trip_days'] + $_POST['more_days'];
        
        function clean($string) {
           $string = str_replace(' ', '-', $string); 
           $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string); 

           return preg_replace('/-+/', '-', $string); 
        }
        /*$replacecharacters = '/[^A-Za-z0-9\-]/';*/
        $pack_title = clean(trim($_POST['trip_name']));
        $pack_dest = explode(':', $_POST['country_dest']);
        $pack_destination = clean(trim($pack_dest[1]));
        $slug=$pack_title.'-'.$pack_destination.'-'.$tripdays.'-days';
        
        $tsql = "UPDATE tbl_trip_packages SET 
        trip_sku = '".$_POST['trip_sku']."',
        trip_name = '".mysqli_real_escape_string($con, $_POST['trip_name'])."',
        trip_thumb = '".$trip_thumb."',
        slug = '".trim(strtolower($slug))."',
        banner_id = '".$_POST['banner_id']."',
        trip_days = '".$tripdays."',
        parent_dest = '".$_POST['parent_dest']."',
        country_dest = '".$_POST['country_dest']."',
        trip_themes = '".$trip_theme."',
        trip_category = '".$trip_cat."',
        trip_type = '".$trip_type."',
        validity_start_date = '".$validity_start_date."',
        validity_end_date = '".$validity_end_date."',
        short_highlights = '".mysqli_real_escape_string($con, $_POST['short_highlights'])."',
        trip_inclusions = '".mysqli_real_escape_string($con, $_POST['trip_inclusions'])."',
        trip_highlights = '".mysqli_real_escape_string($con, $_POST['trip_highlights'])."',
        trip_map = '".mysqli_real_escape_string($con, $_POST['trip_map'])."',
        pre_trip_hotel = '".$_POST['pre_trip_hotel']."',
        post_trip_hotel = '".$_POST['post_trip_hotel']."',
        related_trip = '".$related_trip."',
        deal = '".$_POST['deal']."',
        original_strike_twin_sharing = '".$_POST['original_strike_twin_sharing']."',
        status = '".$_POST['status']."'
        WHERE trip_id = $tripid ";
        mysqli_query($con, $tsql);
        
        //add itenerary
        if( sizeof( $_POST['itinerary_city'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
            mysqli_query($con, "DELETE FROM tbl_trip_itinerary_day_plan WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['itinerary_city'] as $key => $value ){
                 if( !empty( $_POST['itinerary_city'][$key]  ) ){
                    
                    $isql = "insert into tbl_trip_itineraries set
                    trip_id = '$tripid',
                    cat_id = '$trip_cat',
                    day = '".$_POST['day'][$key]."',
                    hotel = '".mysqli_real_escape_string($con, $_POST['hotel'][$key])."',
                    city_id = '".mysqli_real_escape_string($con, $_POST['itinerary_city'][$key])."' ";
                    mysqli_query($con, $isql);

                    $itinerary_id = mysqli_insert_id($con);
                    
                    if( $_POST['plan_frm_cnt'][$key] > 0 ){
                        $form_cnt = $_POST['plan_frm_cnt'][$key];
                        for( $cnt = 1; $cnt <= $form_cnt; $cnt++ ){
                            if(isset($_POST['day_act_type_'.$i.'_'.$cnt])){
                                $day_plan = $_POST['day_act_type_'.$i.'_'.$cnt];
                            } else {
                                $day_plan = '';
                            }
                            if(isset($_POST['flight_train_no_'.$i.'_'.$cnt])){
                                $flight_train_no = $_POST['flight_train_no_'.$i.'_'.$cnt];
                            } else {
                                $flight_train_no = '';
                            }
                            if(isset($_POST['flight_train_no_'.$i.'_'.$cnt])){
                                $flight_train_no = $_POST['flight_train_no_'.$i.'_'.$cnt];
                            } else {
                                $flight_train_no = '';
                            }
                            if(isset($_POST['departure_time_'.$i.'_'.$cnt])){
                                $departure_time = $_POST['departure_time_'.$i.'_'.$cnt];
                            } else {
                                $departure_time = '';
                            }
                            if(isset($_POST['arrival_time_'.$i.'_'.$cnt])){
                                $arrival_time = $_POST['arrival_time_'.$i.'_'.$cnt];
                            } else {
                                $arrival_time = '';
                            }
                            if(isset($_POST['dep_city_code_'.$i.'_'.$cnt])){
                                $dep_city_code = $_POST['dep_city_code_'.$i.'_'.$cnt];
                            } else {
                                $dep_city_code = '';
                            }
                            if(isset($_POST['arr_city_code_'.$i.'_'.$cnt])){
                                $arr_city_code = $_POST['arr_city_code_'.$i.'_'.$cnt];
                            } else {
                                $arr_city_code = '';
                            }
                            if(isset($_POST['departure_city_'.$i.'_'.$cnt])){
                                $departure_city = $_POST['departure_city_'.$i.'_'.$cnt];
                            } else {
                                $departure_city = '';
                            }
                            if(isset($_POST['arrival_city_'.$i.'_'.$cnt])){
                                $arrival_city = $_POST['arrival_city_'.$i.'_'.$cnt];
                            } else {
                                $arrival_city = '';
                            }
                            if(isset($_POST['departure_date_'.$i.'_'.$cnt])){
                                $departure_date = date('Y-m-d', strtotime($_POST['departure_date_'.$i.'_'.$cnt]));
                            } else {
                                $departure_date = '';
                            }
                            if(isset($_POST['arrival_date_'.$i.'_'.$cnt])){
                                $arrival_date = date('Y-m-d', strtotime($_POST['arrival_date_'.$i.'_'.$cnt]));
                            } else {
                                $arrival_date = '';
                            }
                            if(isset($_POST['duration_'.$i.'_'.$cnt])){
                                $duration = $_POST['duration_'.$i.'_'.$cnt];
                            } else {
                                $duration = '';
                            }
                            if(isset($_POST['transfer_details_'.$i.'_'.$cnt])){
                                $transfer_details = $_POST['transfer_details_'.$i.'_'.$cnt];
                            } else {
                                $transfer_details = '';
                            }
                            if(isset($_POST['transport_details_'.$i.'_'.$cnt])){
                                $transport_details = $_POST['transport_details_'.$i.'_'.$cnt];
                            } else {
                                $transport_details = '';
                            }
                            if(isset($_POST['distance_'.$i.'_'.$cnt])){
                                $distance = $_POST['distance_'.$i.'_'.$cnt];
                            } else {
                                $distance = '';
                            }
                            if(isset($_POST['sightseeing_views_'.$i.'_'.$cnt])){
                                $sightseeing_views = $_POST['sightseeing_views_'.$i.'_'.$cnt];
                            } else {
                                $sightseeing_views = '';
                            }
                            if(isset($_POST['meal_name_'.$i.'_'.$cnt])){
                                $meal_name = $_POST['meal_name_'.$i.'_'.$cnt];
                            } else {
                                $meal_name = '';
                            }
                            
                            echo $plan_q = "INSERT INTO tbl_trip_itinerary_day_plan set
                            trip_id = '$tripid',
                            iti_id = '$itinerary_id',
                            plan_name = '".mysqli_real_escape_string($con, $day_plan)."',
                            flight_train_no = '".mysqli_real_escape_string($con, $flight_train_no)."',
                            departure_time = '".mysqli_real_escape_string($con, $departure_time)."',
                            arrival_time = '".mysqli_real_escape_string($con, $arrival_time)."',
                            departure_city_code = '".mysqli_real_escape_string($con, $dep_city_code)."',
                            arrival_city_code = '".mysqli_real_escape_string($con, $arr_city_code)."',
                            departure_city = '".mysqli_real_escape_string($con, $departure_city)."',
                            arrival_city = '".mysqli_real_escape_string($con, $arrival_city)."',
                            departure_date = '".mysqli_real_escape_string($con, $departure_date)."',
                            arrival_date = '".mysqli_real_escape_string($con, $arrival_date)."',
                            duration = '".mysqli_real_escape_string($con, $duration)."',
                            transfer_details = '".mysqli_real_escape_string($con, $transfer_details)."',
                            transport_details = '".mysqli_real_escape_string($con, $transport_details)."',
                            distance = '".mysqli_real_escape_string($con, $distance)."',
                            sightseeing_views = '".mysqli_real_escape_string($con, $sightseeing_views)."',
                            meal_name = '".mysqli_real_escape_string($con, $meal_name)."' ";
                            mysqli_query($con, $plan_q);
                        }
                    }
                    $i++;
                 }
             }
         }
        
        //add trip prices
        if( sizeof( $_POST['trip_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['trip_start_date'] as $key => $value ){
                 if( !empty( $_POST['trip_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['trip_end_date'][$key])){
                         $trip_end_date = date('Y-m-d', strtotime($_POST['trip_end_date'][$key]));
                     } else {
                         $trip_end_date = '';
                     }
                    
                    $q = "INSERT INTO tbl_trip_prices set 
                    trip_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_start_date'][$key])))."',
                    trip_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_end_date'][$key])))."',
                    trip_categories='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_styles='".mysqli_real_escape_string($con, $_POST['trip_styles'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
        }
        
        //add trip blackout dates
        if( sizeof( $_POST['blackout_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_blackout_dates WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['blackout_start_date'] as $key => $value ){
                 if( !empty( $_POST['blackout_start_date'][$key] ) ){
                     
                     if(!empty($_POST['blackout_end_date'][$key])){
                         $blackout_end_date = date('Y-m-d', strtotime($_POST['blackout_end_date'][$key]));
                     } else {
                         $blackout_end_date = '';
                     }
                    
                    echo $q = "INSERT INTO tbl_trip_blackout_dates set 
                    blackout_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['blackout_start_date'][$key])))."',
                    blackout_end_date='".mysqli_real_escape_string($con, $blackout_end_date)."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_blackout_dates WHERE trip_id = $tripid ");
        }
        
        //add trip purchase dates
        if( sizeof( $_POST['purchase_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_purchase_dates WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['purchase_start_date'] as $key => $value ){
                 if( !empty( $_POST['purchase_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['purchase_end_date'][$key])){
                         $purchase_end_date = date('Y-m-d', strtotime($_POST['purchase_end_date'][$key]));
                     } else {
                         $purchase_end_date = '';
                     }
                    
                    echo $q = "INSERT INTO tbl_trip_purchase_dates set 
                    purchase_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['purchase_start_date'][$key])))."',
                    purchase_end_date='".mysqli_real_escape_string($con, $purchase_end_date)."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else{
            mysqli_query($con, "DELETE FROM tbl_trip_purchase_dates WHERE trip_id = $tripid ");
        }
        
        //add trip purchase dates
        if( sizeof( $_POST['season_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_season_price WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['season_start_date'] as $key => $value ){
                 if( !empty( $_POST['season_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['season_end_date'][$key])){
                         $season_end_date = date('Y-m-d', strtotime($_POST['season_end_date'][$key]));
                     } else {
                         $season_end_date = '';
                     }
                    
                    echo $q = "INSERT INTO tbl_trip_season_price set 
                    season_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['season_start_date'][$key])))."',
                    season_end_date='".mysqli_real_escape_string($con, $season_end_date)."',
                    single_room='".mysqli_real_escape_string($con, $_POST['single_room'][$key])."',
                    twin_sharing='".mysqli_real_escape_string($con, $_POST['twin_sharing'][$key])."',
                    triple_sharing='".mysqli_real_escape_string($con, $_POST['triple_sharing'][$key])."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_season_price WHERE trip_id = $tripid ");
        }
        
        header("location: trip-packages.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = $tripid "));
    
    $cat_id = $row['trip_category'];

?>
      <link rel="stylesheet" href="assets/css/croppie.css">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-center">Edit Trip Package</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <div class="text-right btn00">
            <a class="btn btn03 btn-width-03" href="trip-packages.php">Trip Packages</a>
        </div>
        <p class="space2"></p>        

        <div class="input-form">
            <form name="category" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_name">Trip Name*</label>
                            <input type="text" name="trip_name" id="trip_name" class="form-control form-control01" value="<?php echo $row['trip_name']; ?>" placeholder="Trip Name*" required>
                            <input type="hidden" id="trip_id" value="<?php echo $row['trip_id']; ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_thumb">Trip Image*</label>
                            <input type="file" name="trip_thumb" id="trip_thumb" class="form-control form-control01" placeholder="Trip Thumb*">
                            <div id="uploaded">
                                <input type="hidden" name="thumb_name" id="thumb_name" value="<?php echo $row['trip_thumb']; ?>" >
                                <?php
                                if(!empty($row['trip_thumb'])){
                                    echo '<img src="'.PACKAGE_IMG.$row['trip_thumb'].'" class="img-responsive" width="80">';
                                } 
                                ?>
                            </div>
                            <p class="img_note">Note : Trip Image size should be 690 x 480</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_sku">Trip SKU</label>
                            <input type="text" name="trip_sku" id="trip_sku" class="form-control form-control01" value="<?php echo $row['trip_sku']; ?>">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Banner Slider</label>
                            <select name="banner_id" id="banner_id" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ban = mysqli_query($con, "SELECT * FROM tbl_banners WHERE status = 'Active' and banner_for = 'trip' ");
                                while($banres = mysqli_fetch_assoc($ban)){
                                ?>
                                <option value="<?php echo $banres['pbid']; ?>" <?php if($banres['pbid']==$row['banner_id']){ echo 'selected'; } ?>><?php echo $banres['pagename']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_start_date">Trip Validity Start Date*</label>
                            <input type="text" name="validity_start_date" id="validity_start_date" class="form-control form-control01 date-type" value="<?php if($row['validity_start_date']!='0000-00-00' && $row['validity_start_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($row['validity_start_date'])); } else { echo ''; } ?>" placeholder="Travel Start Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_end_date">Trip Validity End Date*</label>
                            <input type="text" name="validity_end_date" id="validity_end_date" class="form-control form-control01 date-type" value="<?php if($row['validity_end_date']!='0000-00-00' && $row['validity_end_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($row['validity_end_date'])); } else { echo ''; } ?>" placeholder="Travel End Date*" required>
                        </div>
                    </div>
                </div>
                
                <div class="row">            

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Status*</label>
                            <select name="status" class="form-control form-control01" required>
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_type">Trip Type</label>
                            <select name="trip_type[]" id="trip_type" class="form-control form-control01" multiple required>
                                <?php
                                $qry_style = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                while($res_style = mysqli_fetch_assoc($qry_style)){
                                    $style_ids = explode(',', $row['trip_type']);
                                ?>
                                <option value="<?php echo $res_style['style_id']; ?>" <?php if(in_array($res_style['style_id'], $style_ids)){ echo 'selected'; } ?>><?php echo $res_style['trip_style']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Available Dates</p>
                        <div id="price_boxes" class="price_boxes">
                            <?php
                            $pricesql = mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_id = $tripid AND trip_categories = $cat_id ");
                            if(mysqli_num_rows($pricesql) > 0){
                                $s=0;
                                while($priceres = mysqli_fetch_assoc($pricesql)){
                                ++$s;
                            ?>
                            <div id="price_box_<?php echo $s; ?>" class="price_box">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_start_date_<?php echo $s; ?>">Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_start_date[]" id="trip_start_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($priceres['trip_start_date']!='0000-00-00' && $priceres['trip_start_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($priceres['trip_start_date'])); } ?>">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_end_date_<?php echo $s; ?>">End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_end_date[]" id="trip_end_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($priceres['trip_end_date']!='0000-00-00' && $priceres['trip_end_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($priceres['trip_end_date'])); } ?>">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_styles_<?php echo $s; ?>">Trip Type</label>
                                            <div class="hotel-box">
                                                <select name="trip_styles[]" id="trip_styles_<?php echo $s; ?>" class="form-control form-control01">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                                    while($style_res = mysqli_fetch_assoc($style_qry)){
                                                    ?>
                                                    <option value="<?php echo $style_res['style_id']; ?>" <?php if($style_res['style_id']==$priceres['trip_styles']){ echo 'selected'; } ?>><?php echo $style_res['trip_style']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <!--<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_price_<?php echo $s; ?>">Trip Price</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_price[]" id="trip_price_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $priceres['trip_price']; ?>">
                                            </div>   
                                        </div>
                                    </div>-->
                                </div>
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $s; ?>" onclick="remove_image(this.id)" /></div>
                            </div>
                            <?php }
                            } else { ?>
                            <div id="price_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_start_date_1">Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_start_date[]" id="trip_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_end_date_1">End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_end_date[]" id="trip_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_styles_1">Trip Type</label>
                                            <div class="hotel-box">
                                                <select name="trip_styles[]" id="trip_styles_1" class="form-control form-control01">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                                    while($style_res = mysqli_fetch_assoc($style_qry)){
                                                    ?>
                                                    <option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <!--<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_price_1">Trip Price ($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_price[]" id="trip_price_1" class="form-control form-control01" placeholder="Trip Price ($)">
                                            </div>   
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                            <?php } ?>
                            <input type="hidden" id="prices_cnt" value="<?php echo $s; ?>">
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_price" /></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Blackout Dates</p>
                        <div id="blackout_boxes" class="blackout_boxes">
                            <?php
                            $bdsql = mysqli_query($con, "SELECT * FROM tbl_trip_blackout_dates WHERE trip_id = $tripid AND cat_id = $cat_id ");
                            if(mysqli_num_rows($bdsql) > 0){
                                $b=0;
                                while($bdres = mysqli_fetch_assoc($bdsql)){
                                ++$b;
                            ?>
                            <div id="blackout_box_<?php echo $b; ?>" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_start_date_<?php echo $b; ?>">Blackout Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="blackout_start_date[]" id="blackout_start_date_<?php echo $b; ?>" class="form-control form-control01 date-type" value="<?php if($bdres['blackout_start_date']!='0000-00-00' && $bdres['blackout_start_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($bdres['blackout_start_date'])); } ?>" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_end_date_<?php echo $b; ?>">Blackout End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="blackout_end_date[]" id="blackout_end_date_<?php echo $b; ?>" class="form-control form-control01 date-type" value="<?php if($bdres['blackout_end_date']!='0000-00-00' && $bdres['blackout_end_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($bdres['blackout_end_date'])); } ?>" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $b; ?>" onclick="remove_blackout(this.id)" /></div>
                            </div>
                            <?php } } else { ?>
                            <div id="blackout_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_start_date_1">Blackout Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="blackout_start_date[]" id="blackout_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_end_date_1">Blackout End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="blackout_end_date[]" id="blackout_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_blackout_date" /></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Purchase Dates</p>
                        <div id="purchase_boxes" class="purchase_boxes">
                            <?php
                            $bdsql = mysqli_query($con, "SELECT * FROM tbl_trip_purchase_dates WHERE trip_id = $tripid AND cat_id = $cat_id ");
                            if(mysqli_num_rows($bdsql) > 0){
                                $b=0;
                                while($bdres = mysqli_fetch_assoc($bdsql)){
                                ++$b;
                            ?>
                            <div id="purchase_box_<?php echo $b; ?>" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_start_date_<?php echo $b; ?>">Purchase Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="purchase_start_date[]" id="purchase_start_date_<?php echo $b; ?>" class="form-control form-control01 date-type" value="<?php if($bdres['purchase_start_date']!='0000-00-00' && $bdres['purchase_start_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($bdres['purchase_start_date'])); } ?>" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_end_date_<?php echo $b; ?>">Purchase End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="purchase_end_date[]" id="purchase_end_date_<?php echo $b; ?>" class="form-control form-control01 date-type" value="<?php if($bdres['purchase_end_date']!='0000-00-00' && $bdres['purchase_end_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($bdres['purchase_end_date'])); } ?>" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $b; ?>" onclick="remove_purchase(this.id)" /></div>
                            </div>
                            <?php } } else { ?>
                            <div id="purchase_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_start_date_1">Purchase Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="purchase_start_date[]" id="purchase_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_end_date_1">Purchase End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="purchase_end_date[]" id="purchase_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_purchase_date" /></div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Map*</label>
                            <textarea cols="80" id="trip_map" class="form-control form-control01 map-area" name="trip_map" rows="3"><?php echo $row['trip_map']; ?></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_dest">Parent Destination*</label>
                            <select name="parent_dest" id="parent_dest" class="form-control form-control01" onchange="return select_dest(this.value);" required>
                                <option value="">Select Parent Destination*</option>
                                <?php
                                $qry_pd = mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE status = 'active' ");
                                while($res_pd = mysqli_fetch_assoc($qry_pd)){
                                ?>
                                <option value="<?php echo $res_pd['parent_dest_id']; ?>" <?php if($res_pd['parent_dest_id']==$row['parent_dest']){ echo 'selected'; } ?>><?php echo $res_pd['parent_destination']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_dest">Country Destination*</label>
                            <select name="country_dest" id="country_dest" class="form-control form-control01" required>
                                <option value="">Select Country Destination*</option>
                                <?php
                                if(!empty($row['parent_dest'])){
                                    $destid = explode(':', $row['country_dest']);
                                    $destsql = mysqli_query($con, "SELECT * FROM tbl_destinations WHERE parent_dest = '".$row['parent_dest']."' ");
                                    while($destres = mysqli_fetch_assoc($destsql)){ ?>
                                        <option value="<?php echo $destres['dest_id'].':'.$destres['destination']; ?>" <?php if($destid[0]==$destres['dest_id']){ echo 'selected'; } ?>><?php echo $destres['destination']; ?></option>';
                                    <?php } } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_category">Select Trip Category*</label>
                            <select name="trip_category" id="trip_category" class="form-control form-control01" required>
                                <option value="">Please Select</option>
                                <?php
                                $qry_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'active' ORDER BY order_no, order_no=0 ");
                                while($res_cat = mysqli_fetch_assoc($qry_cat)){
                                    $cat_id = $row['trip_category'];
                                ?>
                                <option value="<?php echo $res_cat['cat_id']; ?>" <?php if($res_cat['cat_id']==$cat_id){ echo 'selected'; } ?>><?php echo $res_cat['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_theme">Select Trip Themes*</label>
                            <select name="trip_themes" class="form-control form-control01" id="trip_theme" required>
                                <option value="">Please Select</option>
                            <?php
                                $qry_type = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'active' ");
                                while($res_type = mysqli_fetch_assoc($qry_type)){
                                    $types_id = explode(',', $row['trip_themes']);
                            ?>
                                <option value="<?php echo $res_type['trip_type_id']; ?>" <?php if(in_array($res_type['trip_type_id'], $types_id)){ echo 'selected'; } ?>><?php echo $res_type['trip_type']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_inclusions">Trip Inclusions</label>
                            <textarea id="trip_inclusions" name="trip_inclusions" rows="10"><?php echo $row['trip_inclusions']; ?></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'trip_inclusions' );</script>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_highlights">Trip Highlights</label>
                            <textarea id="trip_highlights" name="trip_highlights" rows="10"><?php echo $row['trip_highlights']; ?></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'trip_highlights' );</script>
                        </div>
                    </div>
                
                <?php if(!empty($row['trip_days'])){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">Trip Days</label>
                            <?php if($row['trip_days'] < 10){ $zero = '0'; } else { $zero = ''; } ?>
                            <input type="text" class="form-control form-control01" id="trip_days" name="trip_days" value="<?php if($row['trip_days'] > 1 ){ echo $zero.$row['trip_days'].' Days'; } else { echo $zero.$row['trip_days'].' Day'; } ?>" readonly>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
                <?php
                $itinerary = mysqli_query($con, "SELECT * FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
                if(mysqli_num_rows($itinerary) > 0){ ?>
                    <div class="row">
                        <div class="col-md-12"><p class="head11">Itinerary Days</p></div>
                        <?php
                        $i=0;
                        $day_cnt = mysqli_num_rows($itinerary);
                        while($result = mysqli_fetch_assoc($itinerary)){
                            ++$i;
                            if($i==1){ $first_city = $result['city_id']; }
                            $last_city = $result['city_id'];
                        ?>
                        <div class="itinerary_box clearfix">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="user_login_label">DAY <?php if($i < 10){ echo '0'.$i; } else { echo $i; } ?></label>
                                </div>
                            </div>
                            
                            <input type="hidden" name="day[]" value="<?php echo $i; ?>" >
                            
                            <!--<div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="itenerary_title[]" class="form-control form-control01" value="<?php echo $result['itenerary_title']; ?>" placeholder="Itinerary Title">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="file" name="itinerary_image[]" class="form-control form-control01">
                                    <input type="hidden" name="old_itinerary_image[]" value="<?php echo $result['itinerary_image']; ?>">
                                    <?php
                                    if(!empty($result['itinerary_image'])){
                                        echo '<img src="'.ITINERARY_IMG.$result['itinerary_image'].'" class="img-responsive" width="80">';
                                    }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-<?php echo $i; ?>" placeholder="Itinerary Description"><?php echo $result['itenerary_description']; ?></textarea>
                                </div>
                            </div>-->
                            
                            <div class="col-md-12">
                                <div class="form-group city_box">
                                    <select name="itinerary_city[]" id="itinerary_city-<?php echo $i; ?>" class="cty_drpdwn form-control form-control01">
                                        <option value="">Select Itinerary City</option>
                                        <?php
                                        $qcity = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CountryId = '".$destid[0]."' AND CityStatus = '1' ");
                                        while($rcity = mysqli_fetch_assoc($qcity)){ ?>
                                        <option value="<?php echo $rcity['CityId']; ?>" <?php if($rcity['CityId']==$result['city_id']){ echo 'selected'; } ?>><?php echo $rcity['CityName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <p class="img_note">Note : If searched city doesn't have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="activity_boxes_<?php echo $i; ?>" id="activity_boxes_<?php echo $i; ?>">
                                            <?php
                                            $plan_sql = mysqli_query($con, "SELECT * FROM tbl_trip_itinerary_day_plan WHERE trip_id = $tripid AND iti_id = '".$result['itid']."' ORDER BY departure_time ASC ");
                                            if(mysqli_num_rows($plan_sql) > 0){
                                                $fl = 0;
                                                $at = 0;
                                                $tr = 0;
                                                $ss = 0;
                                                $ml = 0;
                                                $p=0;
                                                while($plan_res = mysqli_fetch_assoc($plan_sql)){
                                                ++$p;
                                                if(!empty($plan_res['plan_name']) && $plan_res['plan_name']=='flight_train_details'){
                                                    $fl++;
                                            ?>
                                            <div id="flight_train_box_<?php echo $i.'_'.$p; ?>" class="price_box flight_train_details">
                                                <div class="row">
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="flight_train_no_<?php echo $i.'_'.$p; ?>">Flight/Train No.</label>
                                                            <div class="hotel-box"><input type="text" name="flight_train_no_<?php echo $i.'_'.$p; ?>" id="flight_train_no_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['flight_train_no']; ?>"><input type="hidden" name="day_act_type_<?php echo $i.'_'.$p; ?>" value="flight_train_details"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="departure_time_<?php echo $i.'_'.$p; ?>">Departure Time</label>
                                                            <div class="hotel-box">
                                                                <select name="departure_time_<?php echo $i.'_'.$p; ?>" id="departure_time_<?php echo $i.'_'.$p; ?>" class="form-control form-control01">
                                                                    <option value="">Please Select</option>
                                                                    <?php 
                                                                    $time = 6; 
                                                                    for($t=1; $t<32; $t++){ 
                                                                        if($t%2!=0){ 
                                                                            if($time < 10){ $dep_time = '0'.$time.':00'; } else { $dep_time = $time.':00'; }
                                                                    ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $dep_time = '0'.$time.':30'; } else { $dep_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                    <?php $time++; } } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="arrival_time_<?php echo $i.'_'.$p; ?>">Arrival Time</label>
                                                            <div class="hotel-box">
                                                                <select name="arrival_time_<?php echo $i.'_'.$p; ?>" id="arrival_time_<?php echo $i.'_'.$p; ?>" class="form-control form-control01">
                                                                    <option value="">Please Select</option>
                                                                    <?php 
                                                                    $time = 6; 
                                                                    for($t=1; $t<32; $t++){ 
                                                                        if($t%2!=0){ 
                                                                            if($time < 10){ $arrival_time = '0'.$time.':00'; } else { $arrival_time = $time.':00'; }
                                                                    ?>
                                                                        <option value="<?php echo $arrival_time ?>" <?php if($plan_res['arrival_time']==$arrival_time){ echo 'selected'; } ?>><?php echo $arrival_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $arrival_time = '0'.$time.':30'; } else { $arrival_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $arrival_time; ?>" <?php if($plan_res['arrival_time']==$arrival_time){ echo 'selected'; } ?>><?php echo $arrival_time; ?></option>
                                                                    <?php $time++; } } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="dep_city_code_<?php echo $i.'_'.$p; ?>">Departure City Code</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="dep_city_code_<?php echo $i.'_'.$p; ?>" id="dep_city_code_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['departure_city_code']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="arr_city_code_<?php echo $i.'_'.$p; ?>">Arrival City Code:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="arr_city_code_<?php echo $i.'_'.$p; ?>" id="arr_city_code_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['arrival_city_code']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="departure_city_<?php echo $i.'_'.$p; ?>">Departure City:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="departure_city_<?php echo $i.'_'.$p; ?>" id="departure_city_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['departure_city']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="arrival_city_<?php echo $i.'_'.$p; ?>">Arrival City:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="arrival_city_<?php echo $i.'_'.$p; ?>" id="arrival_city_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['arrival_city']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="departure_date_<?php echo $i.'_'.$p; ?>">Departure Date:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="departure_date_<?php echo $i.'_'.$p; ?>" id="departure_date_<?php echo $i.'_'.$p; ?>" class="form-control form-control01 date-type" value="<?php if($plan_res['departure_date']!='0000-00-00' && $plan_res['departure_date']!='1970-01-01'){ echo $plan_res['departure_date']; } ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="arrival_date_<?php echo $i.'_'.$p; ?>">Arrival Date:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="arrival_date_<?php echo $i.'_'.$p; ?>" id="arrival_date_<?php echo $i.'_'.$p; ?>" class="form-control form-control01 date-type" value="<?php if($plan_res['arrival_date']!='0000-00-00' && $plan_res['arrival_date']!='1970-01-01'){ echo $plan_res['arrival_date']; } ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-2-5">
                                                        <div class="form-group">
                                                            <label class="user_login_label" for="duration_<?php echo $i.'_'.$p; ?>">Duration:</label>
                                                            <div class="hotel-box">
                                                                <input type="text" name="duration_<?php echo $i.'_'.$p; ?>" id="duration_<?php echo $i.'_'.$p; ?>" class="form-control form-control01" value="<?php echo $plan_res['duration']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $i.'_'.$p; ?>" onclick="remove_flight_train(this.id)" /></div>
                                            </div>
                                            <?php 
                                                } elseif(!empty($plan_res['plan_name']) && $plan_res['plan_name']=='airport_station_transfer'){ 
                                                $at++;
                                                ?>
                                                <div id="transfer_box_<?php echo $at.'_'.$i; ?>" class="price_box flight_train_details">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="transfer_details_<?php echo $at.'_'.$i; ?>">Airport/Station Transfer Detail.</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="transfer_details_<?php echo $i.'_'.$p; ?>" id="transfer_details_<?php echo $at.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['transfer_details']; ?>">
                                                                    <input type="hidden" name="day_act_type_<?php echo $i.'_'.$p; ?>" value="airport_station_transfer">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="departure_time_<?php echo $at.'_'.$i; ?>">Departure Time</label>
                                                                <div class="hotel-box">
                                                                    <select name="departure_time_<?php echo $i.'_'.$p; ?>" id="departure_time_<?php echo $at.'_'.$i; ?>" class="form-control form-control01">
                                                                        <option value="">Please Select</option>
                                                                        <?php 
                                                                        $time = 6; 
                                                                        for($t=1; $t<32; $t++){ 
                                                                            if($t%2!=0){ 
                                                                                if($time < 10){ $dep_time = '0'.$time.':00'; } else { $dep_time = $time.':00'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $dep_time = '0'.$time.':30'; } else { $dep_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php $time++; } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $at.'_'.$i; ?>" onclick="remove_transfer(this.id)" /></div>
                                                </div>
                                                <?php
                                                } elseif(!empty($plan_res['plan_name']) && $plan_res['plan_name']=='transport'){ 
                                                $tr++;
                                                ?>
                                                <div id="city_journey_box_<?php echo $tr.'_'.$i; ?>" class="price_box flight_train_details">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="transport_details_<?php echo $tr.'_'.$i; ?>">Transport Detail.</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="transport_details_<?php echo $i.'_'.$p; ?>" id="transport_details_<?php echo $tr.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['transport_details']; ?>">
                                                                    <input type="hidden" name="day_act_type_<?php echo $i.'_'.$p; ?>" value="transport">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="departure_time_<?php echo $tr.'_'.$i; ?>">Departure Time</label>
                                                                <div class="hotel-box">
                                                                    <select name="departure_time_<?php echo $i.'_'.$p; ?>" id="departure_time_<?php echo $tr.'_'.$i; ?>" class="form-control form-control01">
                                                                        <option value="">Please Select</option>
                                                                        <?php 
                                                                        $time = 6; 
                                                                        for($t=1; $t<32; $t++){ 
                                                                            if($t%2!=0){ 
                                                                                if($time < 10){ $dep_time = '0'.$time.':00'; } else { $dep_time = $time.':00'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $dep_time = '0'.$time.':30'; } else { $dep_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php $time++; } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="distance_<?php echo $tr.'_'.$i; ?>">Distance</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="distance_<?php echo $i.'_'.$p; ?>" id="distance_<?php echo $tr.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['distance']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="duration_<?php echo $tr.'_'.$i; ?>">Duration</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="duration_<?php echo $i.'_'.$p; ?>" id="duration_<?php echo $tr.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['duration']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $tr.'_'.$i; ?>" onclick="remove_city_journey(this.id)" /></div>
                                                </div>
                                                <?php
                                                } elseif(!empty($plan_res['plan_name']) && $plan_res['plan_name']=='sightseeing'){ 
                                                $ss++;
                                                ?>
                                                <div id="sightseeing_box_<?php echo $ss.'_'.$i; ?>" class="price_box flight_train_details">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="sightseeing_views_<?php echo $ss.'_'.$i; ?>">Sightseeing View.</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="sightseeing_views_<?php echo $i.'_'.$p; ?>" id="sightseeing_views_<?php echo $ss.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['sightseeing_views']; ?>">
                                                                    <input type="hidden" name="day_act_type_<?php echo $i.'_'.$p; ?>" value="sightseeing">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="departure_time_<?php echo $ss.'_'.$i; ?>">Start Time</label>
                                                                <div class="hotel-box">
                                                                    <select name="departure_time_<?php echo $i.'_'.$p; ?>" id="departure_time_<?php echo $ss.'_'.$i; ?>" class="form-control form-control01">
                                                                        <option value="">Please Select</option>
                                                                        <?php 
                                                                        $time = 6; 
                                                                        for($t=1; $t<32; $t++){ 
                                                                            if($t%2!=0){ 
                                                                                if($time < 10){ $dep_time = '0'.$time.':00'; } else { $dep_time = $time.':00'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $dep_time = '0'.$time.':30'; } else { $dep_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php $time++; } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="duration_<?php echo $ss.'_'.$i; ?>">Duration</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="duration_<?php echo $i.'_'.$p; ?>" id="duration_<?php echo $ss.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['duration']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $ss.'_'.$i; ?>" onclick="remove_sightseeing(this.id)" /></div>
                                                </div>
                                                <?php
                                                } elseif(!empty($plan_res['plan_name']) && $plan_res['plan_name']=='meal'){ 
                                                $ml++;
                                                ?>
                                                <div id="meal_box_<?php echo $ml.'_'.$i; ?>" class="price_box flight_train_details">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="meal_name_<?php echo $ml.'_'.$i; ?>">Meal Name.</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="meal_name_<?php echo $i.'_'.$p; ?>" id="meal_name_<?php echo $ml.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['meal_name']; ?>">
                                                                    <input type="hidden" name="day_act_type_<?php echo $i.'_'.$p; ?>" value="meal">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="departure_time_<?php echo $ml.'_'.$i; ?>">Start Time</label>
                                                                <div class="hotel-box">
                                                                    <select name="departure_time_<?php echo $i.'_'.$p; ?>" id="departure_time_<?php echo $ml.'_'.$i; ?>" class="form-control form-control01">
                                                                        <?php 
                                                                        $time = 6; 
                                                                        for($t=1; $t<32; $t++){ 
                                                                            if($t%2!=0){ 
                                                                                if($time < 10){ $dep_time = '0'.$time.':00'; } else { $dep_time = $time.':00'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php } else { 
                                                                            if($time < 10){ $dep_time = '0'.$time.':30'; } else { $dep_time = $time.':30'; }
                                                                        ?>
                                                                        <option value="<?php echo $dep_time; ?>" <?php if($plan_res['departure_time']==$dep_time){ echo 'selected'; } ?>><?php echo $dep_time; ?></option>
                                                                        <?php $time++; } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="user_login_label" for="duration_<?php echo $ml.'_'.$i; ?>">Duration</label>
                                                                <div class="hotel-box">
                                                                    <input type="text" name="duration_<?php echo $i.'_'.$p; ?>" id="duration_<?php echo $ml.'_'.$i; ?>" class="form-control form-control01" value="<?php echo $plan_res['duration']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $ml.'_'.$i; ?>" onclick="remove_meal(this.id)" /></div>
                                                </div>
                                                <?php
                                                } 
                                                }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if($i==1 || $i==$day_cnt){ ?>
                                    <div class="col-md-3 col-2-5 optional_<?php echo $i; ?>">
                                        <div class="form-group">
                                            <div class="user_login_label">Flight/Train Details <img src="assets/images/plus_icon.png" id="<?php echo $i; ?>" class="plus_icon22 add_filght" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5 optional_<?php echo $i; ?>">
                                        <div class="form-group">
                                            <div class="user_login_label">Airport/Station Transfer <img src="assets/images/plus_icon.png" id="<?php echo $i; ?>" class="plus_icon22 add_hotel_transfer" /></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <div class="user_login_label">Transport <img src="assets/images/plus_icon.png" id="<?php echo $i; ?>" class="plus_icon22 add_transport" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <div class="user_login_label">Sightseeing <img src="assets/images/plus_icon.png" id="<?php echo $i; ?>" class="plus_icon22 add_day_sightseeing" /></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <div class="user_login_label">Meal <img src="assets/images/plus_icon.png" id="<?php echo $i; ?>" class="plus_icon22 add_day_meal" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="plan_frm_cnt[]" id="plan_frm_cnt_<?php echo $i; ?>" value="<?php echo $p; ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select name="hotel[]" id="hotel-<?php echo $i; ?>" class="hotel_dropdown form-control form-control01">
                                            <option value="">Select Hotel</option>
                                            <?php 
                                            if( $result['cat_id'] > 0 ){
                                                $cat_clause = ' AND category_id = '.$result['cat_id'];
                                            }
                                            if( $result['city_id'] > 0 ){
                                                $city_clause = ' AND city_id = '.$result['city_id'];
                                            }
                                            $qhotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status= 'Active' $city_clause $cat_clause ");
                                            while($rhotel = mysqli_fetch_assoc($qhotel)){
                                            ?>
                                            <option value="<?php echo $rhotel['hotel_id']; ?>" <?php if($rhotel['hotel_id']=$result['hotel']){ echo 'selected'; } ?>><?php echo $rhotel['hotel_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="more_days" class="form-control form-control01" onchange="fnitenerary(this.value)">
                                <option value="">Add More Itineraries</option>
                            <?php
                                $count = 30 - $row['trip_days'];
                                for($i=1; $i<=$count; $i++){
                                    if( $i < 10 ){
                                        $i = '0'.$i;
                                    }
                            ?>
                                <option value="<?php echo $i; ?>" ><?php echo $i; if($i < 2){ echo ' Day'; } else { echo ' Days'; } ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="itinerary"></div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Pre-Trip Hotel</label>
                            <select name="pre_trip_hotel" id="pre_trip_hotel" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $q_pre_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= '".$first_city."' AND category_id = '".$row['trip_category']."' ");
                                while($pre_hotel = mysqli_fetch_assoc($q_pre_hotel)){
                                ?>
                                <option value="<?php echo $pre_hotel['hotel_id']; ?>" <?php if($pre_hotel['hotel_id']==$row['pre_trip_hotel']){ echo 'selected'; } ?>><?php echo $pre_hotel['hotel_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Post-Trip Hotel</label>
                            <select name="post_trip_hotel" id="post_trip_hotel" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $q_post_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= '".$last_city."' AND category_id = '".$row['trip_category']."' ");
                                while($post_hotel = mysqli_fetch_assoc($q_post_hotel)){
                                ?>
                                <option value="<?php echo $post_hotel['hotel_id']; ?>" <?php if($post_hotel['hotel_id']==$row['post_trip_hotel']){ echo 'selected'; } ?>><?php echo $post_hotel['hotel_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">Related Trip</label>
                            <select name="related_trip[]" id="related_trip" class="form-control form-control01" multiple>
                                <?php
                                $related_sql = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND trip_id!= '".$row['trip_id']."' AND trip_category = '".$row['trip_category']."' AND country_dest like '%".$row['country_dest']."%' ");
                                while($related = mysqli_fetch_assoc($related_sql)){
                                    $related_trips = explode(',', $row['related_trip']);
                                ?>
                                <option value="<?php echo $related['trip_id']; ?>" <?php if(in_array($related['trip_id'], $related_trips)){ echo 'selected'; } ?>><?php echo $related['trip_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Trip Occupancy Prices According to Season</p>
                        <div id="season_boxes" class="season_boxes">
                            <?php
                            $spsql = mysqli_query($con, "SELECT * FROM tbl_trip_season_price WHERE trip_id = $tripid AND cat_id = $cat_id ");
                            if(mysqli_num_rows($spsql) > 0){
                                $s=0;
                                while($spres = mysqli_fetch_assoc($spsql)){
                                ++$s;
                            ?>
                            <div id="season_box_<?php echo $s; ?>" class="price_box">
                                <div class="row">
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="season_start_date_<?php echo $s; ?>">Season Start</label>
                                            <div class="hotel-box">
                                                <input type="text" name="season_start_date[]" id="season_start_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($spres['season_start_date']!='0000-00-00' && $spres['season_start_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($spres['season_start_date'])); } ?>" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="season_end_date_<?php echo $s; ?>">Season End</label>
                                            <div class="hotel-box">
                                                <input type="text" name="season_end_date[]" id="season_end_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($spres['season_end_date']!='0000-00-00' && $spres['season_end_date']!='1970-01-01'){ echo date('d-m-Y', strtotime($spres['season_end_date'])); } ?>" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="single_room_<?php echo $s; ?>">Single Room($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="single_room[]" id="single_room_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $spres['single_room']; ?>" placeholder="Single Room($)">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="twin_sharing_<?php echo $s; ?>">Twin Sharing($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="twin_sharing[]" id="twin_sharing_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $spres['twin_sharing']; ?>" placeholder="Twin Sharing($)">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="triple_sharing_<?php echo $s; ?>">Triple Sharing($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="triple_sharing[]" id="triple_sharing_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $spres['triple_sharing']; ?>" placeholder="Triple Sharing($)">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $s; ?>" onclick="remove_season(this.id)" /></div>
                            </div>
                            <?php } } else { ?>
                            <div id="season_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="season_start_date_1">Season Start</label>
                                            <div class="hotel-box">
                                                <input type="text" name="season_start_date[]" id="season_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="season_end_date_1">Season End</label>
                                            <div class="hotel-box">
                                                <input type="text" name="season_end_date[]" id="season_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="single_room_1">Single Room($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="single_room[]" id="single_room_1" class="form-control form-control01" placeholder="Single Room($)">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="twin_sharing_1">Twin Sharing($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="twin_sharing[]" id="twin_sharing_1" class="form-control form-control01" placeholder="Twin Sharing($)">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2-5">
                                        <div class="form-group">
                                            <label class="user_login_label" for="triple_sharing_1">Triple Sharing($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="triple_sharing[]" id="triple_sharing_1" class="form-control form-control01" placeholder="Triple Sharing($)">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_season" /></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="user_login_label">Make a Deal: <input type="checkbox" name="deal" id="deal" value="Yes" <?php if($row['deal']=="Yes"){ echo 'checked'; } ?>> Yes</label>
                        </div>
                    </div>
                    <div class="col-md-4 deal_box" <?php if($row['deal']==""){ echo 'style="display: none;"'; } ?>>
                        <div class="form-group">
                            <p class="ftl_txt02">Above price will be deals price and original price twin sharing will be struck off</p>
                        </div>
                    </div>
                    <div class="col-md-3 deal_box" <?php if($row['deal']==""){ echo 'style="display: none;"'; } ?>>
                        <div class="form-group">
                            <input type="text" name="original_strike_twin_sharing" id="original_strike_twin_sharing" class="form-control form-control01" value="<?php echo $row['original_strike_twin_sharing']; ?>" placeholder="Original Twin Sharing Cost($)">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="short_highlights">Trip Short Highlight*</label>
                            <textarea id="short_highlights" name="short_highlights" class="form-control form-control01" rows="3" placeholder="eg: Highlight 1 | Highlight 2 | Highlight 3"><?php echo $row['short_highlights']; ?></textarea>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <button name="submit" class="btn btn03 btn-width-03">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<div id="ImgCrop" class="modal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Crop Image</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-12 text-center">
						  <div id="image" style="width:250px; margin-top:20px"></div>
  					</div>
				</div>
      		</div>
      		<div class="modal-footer">
                <button class="btn btn-success crop_image">Crop & Upload Image</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>

<?php 
    include('elements/footer.php');
?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
      
    <script src="assets/fullcalendar/lib/main.min.js"></script>
    <script src="assets/js/calendar_edit.js"></script>
      
    <script src="assets/js/croppie.js"></script>
      
    <script src="assets/select2/select2.min.js"></script>

    <!-- Multi Select JS -->
    <script src="assets/js/bootstrap-multiselect.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $('#deal').click(function(){
            if($(this).prop('checked')==true){
                $('.deal_box').css('display', 'block');
            } else {
                $('.deal_box').css('display', 'none');
            }
        });
    </script>

    <script>
    // Crop Trip Thumb Image
    $(document).ready(function(){

        var image_crop = $('#image').croppie({
        enableExif: true,
        viewport: {
          width:690,
          height:480,
        },
        boundary:{
          width:690,
          height:480
        }
      });

      $('#trip_thumb').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
          image_crop.croppie('bind', {
            url: event.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
        $('#ImgCrop').modal('show');
      });



      $('.crop_image').click(function(event){
        image_crop.croppie('result', {
          type: 'canvas',
          size: 'viewport'
        }).then(function(response){
          var img_folder = 'trip_package';
          $.ajax({
            url:'croppie.php',
            type: "POST",
            data:{image: response, img_folder: img_folder},
            success:function(data)
            {
              $('#ImgCrop').modal('hide');
              $('#uploaded').empty();
              var html = '<input type="hidden" name="thumb_name" id="thumb_name" value="'+data+'"><img src="../uploads/'+img_folder+'/'+data+'" width="80">'; 
              $('#uploaded').html(html);
              
            }
          });
        })
      });

    });  
    </script>

    <script>
        /*$(function () {
            $('#trip_theme').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Types*'
            });
        });*/
        
        /*$(function () {
            $('#trip_category').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Categories*'
            });
        });*/
        
        $(function () {
            $('#related_trip').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Search trip...',
                templates: {
                    filter: '<div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div>',
                },
                maxHeight: 300,
                nonSelectedText: 'Select Related Trip'
            });
        });
        
        $(function () {
            $('#trip_type').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Type*'
            });
        });
    </script>

    <script>
    function select_dest(parent_id){
        $.ajax({
            url: 'get-destination.php',
            type: 'POST',
            data: { parentid:parent_id },
            success: function(response){

                if(response){
                    $('#country_dest').empty();
                    $('#country_dest').append(response);
                }
            }
          });
    }
    </script>
<?php
$catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
while($catres = mysqli_fetch_assoc($catqry)){
    $catoption .= '<option value="'.$catres['cat_id'].'">'.$catres['category_name'].'</option>';
}
?>

    <script>
    function fnitenerary(val){
        var dest = $("#country_dest").val();
        dest = dest.split(':');
        var dest_id = dest[0];
        
        var daynum = parseInt($("#trip_days").val())
        var cnt = parseInt(val);
        
        var total_cnt = parseInt(daynum)+parseInt(cnt);
        itinerary = '';
        //alert(cnt);
        itinerary='<div class="col-md-12"><p class="head11">Itinerary Days</p></div>';
        for( i=1; i<=cnt; ++i){
        if(daynum>0){
            daynum = parseInt(daynum)+parseInt(1);
        } else {
            daynum = 1;
        }
        num = daynum;
        if( daynum < 10 ){ daynum = '0'+daynum; } else { daynum = daynum; }
        itinerary+='<div class="itinerary_box clearfix"><div class="col-md-12"><div class="form-group"><label class="user_login_label">DAY '+daynum+'</label></div></div><input type="hidden" name="day[]" value="'+daynum+'" ><div class="col-md-12"><div class="form-group city_box"><select name="itinerary_city[]" id="itinerary_city-'+num+'" class="city_drpdwn form-control form-control01"><option value="">Select Itinerary City</option></select><p class="img_note">Note : If searched city doesn\'t have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p></div></div><div class="col-md-12"><div class="row"><div class="col-md-12"><div class="activity_boxes_'+num+'" id="activity_boxes_'+num+'"></div></div></div><div class="row"><div class="col-md-3 col-2-5 optional_'+num+'"><div class="form-group"><div class="user_login_label">Flight/Train Details <img src="assets/images/plus_icon.png" id="'+num+'" class="plus_icon22 add_filght_train" /></div></div></div><div class="col-md-3 col-2-5 optional_'+num+'"><div class="form-group"><div class="user_login_label">Airport/Station Transfer <img src="assets/images/plus_icon.png" id="'+num+'" class="plus_icon22 add_transfer" /></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><div class="user_login_label">Transport <img src="assets/images/plus_icon.png" id="'+num+'" class="plus_icon22 add_city_journey" /></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><div class="user_login_label">Sightseeing <img src="assets/images/plus_icon.png" id="'+num+'" class="plus_icon22 add_sightseeing" /></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><div class="user_login_label">Meal <img src="assets/images/plus_icon.png" id="'+num+'" class="plus_icon22 add_meal" /></div></div></div></div><input type="hidden" name="plan_frm_cnt[]" id="plan_frm_cnt_'+num+'"><div class="row"><div class="col-md-12"><div class="form-group"><select name="hotel[]" id="hotel-'+num+'" class="hotel_dropdown form-control form-control01"><option value="">Select Hotel</option></select></div></div></div></div></div>';
        }       
        $("#itinerary").html(itinerary);
        
        var iti_num = 1;
        $('.itinerary_box').each(function(){
            if(iti_num==1 || iti_num==total_cnt){
                $('.itinerary_box > .col-md-12 > .row > .optional_'+iti_num).css('display', 'block');
                $("#activity_boxes_"+iti_num+" > div[id^='flight_train_box_']").css('display', 'block');
                $("#activity_boxes_"+iti_num+" > div[id^='transfer_box_']").css('display', 'block');
            } else {
                $('.itinerary_box > .col-md-12 > .row > .optional_'+iti_num).remove();
                $("#activity_boxes_"+iti_num+" > div[id^='flight_train_box_']").remove();
                $("#activity_boxes_"+iti_num+" > div[id^='transfer_box_']").remove();
            }
            iti_num++;
        });
        
        //Display cities according to selected destination
        $.ajax({
            url: 'ajax_get_city.php',
            type: 'post',
            data: { country: dest_id },
            
            success: function(response){
                $('.city_drpdwn').empty();
                $('.city_drpdwn').append(response);
                $('.city_drpdwn').select2().on("select2:select", function (e) {
                    var city_element = $(e.currentTarget);
                    var serial_id = city_element.attr('id');
                    var city_id = city_element.val();
                    
                    serial_id = serial_id.split('-');
                    serial_no = serial_id[1];
                    
                    var catid = $('#trip_category option:selected').val();

                    //get hotels of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        async: false,
                        type: 'post',
                        data: { hotel_city_id: city_id, hotel_cat_id: catid },

                        success: function(response){
                            $('#hotel-'+serial_no).empty();
                            $('#hotel-'+serial_no).append(response);
                        }
                    });

                    //get activities of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        type: 'post',
                        data: { activity_city_id: city_id },

                        success: function(response){
                            $('[id^="activities-'+serial_no+'"]').multiselect();
                            var options = jQuery.parseJSON(response);
                            $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                        }
                    });
                    
                    
                    //get first and last city and display its hotels in pre trip hotel dropdown
                    var day_length = $('select[name="itinerary_city[]"]').length;
                    alert(day_length);
                    alert(serial_no);
                    for(var i = 1; i <= day_length; i++){
                        if(i==1 && serial_no==i){
                            var first_city = $('#itinerary_city-'+i).val();

                            //get first city and display its hotels in pre trip hotel dropdown    
                            $.ajax({
                                url: 'ajax_get_dynamic_data.php',
                                type: 'post',
                                data: { first_city: first_city, cat_id: catid },

                                success: function(response){
                                    $('#pre_trip_hotel').empty();
                                    $('#pre_trip_hotel').append(response);
                                }
                            });
                        } else if(serial_no==day_length){
                            var last_city = $('#itinerary_city-'+i).val();

                            //get last city and display its hotels in post trip hotel dropdown
                            $.ajax({
                                url: 'ajax_get_dynamic_data.php',
                                type: 'post',
                                data: { last_city: last_city, cat_id: catid },

                                success: function(response){
                                    $('#post_trip_hotel').empty();
                                    $('#post_trip_hotel').append(response);
                                }
                            });
                        }
                    }
                });
            }
        });
        
        //If the flight/train add icon clicked form inputs regarding flight/train is populated
        $(".add_filght_train").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='flight_train_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            if(id==0 && (num==1 || num==total_cnt)){
                id++;
                sr_no++;
                var append_data = '<div id="flight_train_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="flight_train_no_'+id+'_'+num+'">Flight/Train No.</label><div class="hotel-box"><input type="text" name="flight_train_no_'+num+'_'+sr_no+'" id="flight_train_no_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="flight_train_details"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_time_'+id+'_'+num+'">Arrival Time</label><div class="hotel-box"><select name="arrival_time_'+num+'_'+sr_no+'" id="arrival_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="dep_city_code_'+id+'_'+num+'">Departure City Code</label><div class="hotel-box"><input type="text" name="dep_city_code_'+num+'_'+sr_no+'" id="dep_city_code_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arr_city_code_'+id+'_'+num+'">Arrival City Code:</label><div class="hotel-box"><input type="text" name="arr_city_code_'+num+'_'+sr_no+'" id="arr_city_code_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_city_'+id+'_'+num+'">Departure City:</label><div class="hotel-box"><input type="text" name="departure_city_'+num+'_'+sr_no+'" id="departure_city_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_city_'+id+'_'+num+'">Arrival City:</label><div class="hotel-box"><input type="text" name="arrival_city_'+num+'_'+sr_no+'" id="arrival_city_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_date_'+id+'_'+num+'">Departure Date:</label><div class="hotel-box"><input type="text" name="departure_date_'+num+'_'+sr_no+'" id="departure_date_'+id+'_'+num+'" class="form-control form-control01 date-type"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_date_'+id+'_'+num+'">Arrival Date:</label><div class="hotel-box"><input type="text" name="arrival_date_'+num+'_'+sr_no+'" id="arrival_date_'+id+'_'+num+'" class="form-control form-control01 date-type"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration:</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_flight_train(this.id)" /></div></div>';
                $("#activity_boxes_"+num).append(append_data); //append new text box in main div
                $("#flight_train_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
                $("#plan_frm_cnt_"+num).val(sr_no);
            } else {
                alert("Flight/Train details can be added first and last day of itinerary");
            }
            $( ".date-type" ).datepicker({ dateFormat: 'dd-mm-yy' });
        });
        
        //If the Airport/Station Transfer add icon clicked form inputs regarding flight/train is populated
        $(".add_transfer").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='transfer_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            if(id==0 && (num==1 || num==total_cnt)){
                sr_no++;
                id++;
                var append_data = '<div id="transfer_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="transfer_details_'+id+'_'+num+'">Airport/Station Transfer Detail.</label><div class="hotel-box"><input type="text" name="transfer_details_'+num+'_'+sr_no+'" id="transfer_details_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="airport_station_transfer"></div></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_transfer(this.id)" /></div></div>';
                $("#activity_boxes_"+num).append(append_data); //append new text box in main div
                $("#transfer_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
                $("#plan_frm_cnt_"+num).val(sr_no);
            } else {
                alert("Airport/Station Transfer can be added first and last day of itinerary");
            }
        }); 
        
        //If the City Journey icon clicked form inputs regarding city journey is populated
        $(".add_city_journey").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='city_journey_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="city_journey_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="transport_details_'+id+'_'+num+'">Transport Detail.</label><div class="hotel-box"><input type="text" name="transport_details_'+num+'_'+sr_no+'" id="transport_details_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="transport"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="distance_'+id+'_'+num+'">Distance</label><div class="hotel-box"><input type="text" name="distance_'+num+'_'+sr_no+'" id="distance_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-2"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_city_journey(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#city_journey_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        }); 
        
        //If the Sightseeing icon clicked form inputs regarding Sightseeing is populated
        $(".add_sightseeing").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='sightseeing_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="sightseeing_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="sightseeing_views_'+id+'_'+num+'">Sightseeing View.</label><div class="hotel-box"><input type="text" name="sightseeing_views_'+num+'_'+sr_no+'" id="sightseeing_views_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="sightseeing"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Start Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_sightseeing(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#sightseeing_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        }); 
        
        //If the Meal icon clicked form inputs regarding Meal is populated
        $(".add_meal").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='meal_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="meal_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="meal_name_'+id+'_'+num+'">Meal Name.</label><div class="hotel-box"><input type="text" name="meal_name_'+num+'_'+sr_no+'" id="meal_name_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="meal"></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Start Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_meal(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#meal_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        });
        
        //$('.cat_drpdwn').append('<?php echo $catoption; ?>');
        
        //CKEDITOR.replaceAll( 'itinerary_detail' );
        
        $(function () {
            $('.activity_drpdwn').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Activities'
            });
        });
        
        /*$('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });*/
    }
    </script>

    <script>        
        $('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });     
        
        
        $('.cty_drpdwn').select2().on("select2:select", function (e) {
            var city_element = $(e.currentTarget);
            var serial_id = city_element.attr('id');
            var city_id = city_element.val();
            serial_id = serial_id.split('-');
            serial_no = serial_id[1];
                    
            var catid = $('#trip_category option:selected').val();
            
            //get hotels of selected city
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                async: false,
                type: 'post',
                data: { hotel_city_id: city_id, hotel_cat_id: catid },

                success: function(response){
                    $('#hotel-'+serial_no).empty();
                    $('#hotel-'+serial_no).append(response);
                }
            });

            //get activities of selected city
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { activity_city_id: city_id },

                success: function(response){
                    $('[id^="activities-'+serial_no+'"]').multiselect();
                    var options = jQuery.parseJSON(response);
                    $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                }
            });
                    
                    
            //get first and last city and display its hotels in pre trip hotel dropdown
            var day_length = $('select[name="itinerary_city[]"]').length;
            for(var i = 1; i <= day_length; i++){
                if(i==1 && serial_no==i){
                    var first_city = $('#itinerary_city-'+i).val();
                    
                    //get first city and display its hotels in pre trip hotel dropdown    
                    $.ajax({
                        url: 'ajax_get_dynamic_data.php',
                        type: 'post',
                        data: { first_city: first_city, cat_id: catid },

                        success: function(response){
                            $('#pre_trip_hotel').empty();
                            $('#pre_trip_hotel').append(response);
                        }
                    });
                } else if(serial_no==day_length){
                    var last_city = $('#itinerary_city-'+i).val();
                    
                    //get last city and display its hotels in post trip hotel dropdown
                    $.ajax({
                        url: 'ajax_get_dynamic_data.php',
                        type: 'post',
                        data: { last_city: last_city, cat_id: catid },

                        success: function(response){
                            $('#post_trip_hotel').empty();
                            $('#post_trip_hotel').append(response);
                        }
                    });
                }
            }

        });
    </script>

    <script>
    //Display cities according to selected destination
    $(document).ready( function() {
        $('#country_dest').on('change', function(){
            var dest = $(this).val();
            var dest = dest.split(':');
            var dest_id = dest[0];
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country: dest_id },

                success: function(response){
                    $('.city_drpdwn').empty();
                    $('.city_drpdwn').append(response);
                    $('.city_drpdwn').select2();
                    
                    $('.cty_drpdwn').empty();
                    $('.cty_drpdwn').append(response);
                    $('.cty_drpdwn').select2();
                    
                    $('.hotel_dropdown').empty();
                    $('.hotel_dropdown').append('<option value="">Select Hotel</option>');
                    
                    /*$('.activity_drpdwn option').remove();
                    $('.activity_drpdwn').multiselect('rebuild');
                    $('.activity_drpdwn').multiselect('refresh');*/
                }
            });
        });
    } );
    </script>

    <script>
    //Get Trip Stopovers and Extensions according to selected category
    $('#trip_category').change(function(){
        var cat_id  = $(this).val();
        var dest = $('#country_dest option:selected').val();
        var dest = dest.split(':');
        var destination = dest[1];
        var trip_id = <?php echo $tripid; ?>;
        var action = 'all_trip';
        
        //related trip
        $.ajax({
            url: 'ajax_get_dynamic_data.php',
            type: 'post',
            data: { cat_id: cat_id, trip_id: trip_id, destination: destination, action: action },

            success: function(response){
                $('[id^="related_trip"]').multiselect();
                var options = jQuery.parseJSON(response);
                $('[id^="related_trip"]').multiselect('dataprovider', options);
            }
        });
        
        //fetch all hotels according to category and city
        var i = 1;
        var citynum = $('.city_box').length;
        $(".city_box > select").each(function(){
            var city_id = $(this).val();
            var city_box = $(this).attr('id');
            var city_box = city_box.split('-');
            var num = city_box[1];
            
            $.ajax({
                type: 'POST',
                url: 'ajax_get_hotel_activity.php',
                data: {hotel_city_id: city_id, hotel_cat_id: cat_id},
                
                success: function(response){
                    $('#hotel-'+num).empty();
                    $('#hotel-'+num).append(response);
                }
            });
            
            //pre-trip hotel
            if(i==1){
                $.ajax({
                    type: 'POST',
                    url: 'ajax_get_hotel_activity.php',
                    data: {hotel_city_id: city_id, hotel_cat_id: cat_id},

                    success: function(response){
                        $('#pre_trip_hotel').empty();
                        $('#pre_trip_hotel').append(response);
                    }
                });
            }
            
            //pre-trip hotel
            if(i==citynum){
                $.ajax({
                    type: 'POST',
                    url: 'ajax_get_hotel_activity.php',
                    data: {hotel_city_id: city_id, hotel_cat_id: cat_id},

                    success: function(response){
                        $('#post_trip_hotel').empty();
                        $('#post_trip_hotel').append(response);
                    }
                });
            }
            
            i++;
        });
    });
    </script>
      
    <script type="text/javascript">//If the flight/train add icon clicked form inputs regarding flight/train is populated
        
        //If the flight/train add icon clicked form inputs regarding flight/train is populated
        $(".add_filght").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='flight_train_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            var total_cnt = $("div[id^='activity_boxes_']").length;
            
            if(id==0 && (num==1 || num==total_cnt)){
                id++;
                sr_no++;
                var append_data = '<div id="flight_train_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="flight_train_no_'+id+'_'+num+'">Flight/Train No.</label><div class="hotel-box"><input type="text" name="flight_train_no_'+num+'_'+sr_no+'" id="flight_train_no_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="flight_train_details"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_time_'+id+'_'+num+'">Arrival Time</label><div class="hotel-box"><select name="arrival_time_'+num+'_'+sr_no+'" id="arrival_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="dep_city_code_'+id+'_'+num+'">Departure City Code</label><div class="hotel-box"><input type="text" name="dep_city_code_'+num+'_'+sr_no+'" id="dep_city_code_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arr_city_code_'+id+'_'+num+'">Arrival City Code:</label><div class="hotel-box"><input type="text" name="arr_city_code_'+num+'_'+sr_no+'" id="arr_city_code_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_city_'+id+'_'+num+'">Departure City:</label><div class="hotel-box"><input type="text" name="departure_city_'+num+'_'+sr_no+'" id="departure_city_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_city_'+id+'_'+num+'">Arrival City:</label><div class="hotel-box"><input type="text" name="arrival_city_'+num+'_'+sr_no+'" id="arrival_city_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_date_'+id+'_'+num+'">Departure Date:</label><div class="hotel-box"><input type="text" name="departure_date_'+num+'_'+sr_no+'" id="departure_date_'+id+'_'+num+'" class="form-control form-control01 date-type"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_date_'+id+'_'+num+'">Arrival Date:</label><div class="hotel-box"><input type="text" name="arrival_date_'+num+'_'+sr_no+'" id="arrival_date_'+id+'_'+num+'" class="form-control form-control01 date-type"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration:</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_flight_train(this.id)" /></div></div>';
                $("#activity_boxes_"+num).append(append_data); //append new text box in main div
                $("#flight_train_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
                $("#plan_frm_cnt_"+num).val(sr_no);
            } else {
                alert("Flight/Train details can be added first and last day of itinerary");
            }
            $( ".date-type" ).datepicker({ dateFormat: 'dd-mm-yy' });
        });
        
        //If the Airport/Station Transfer add icon clicked form inputs regarding flight/train is populated
        $(".add_hotel_transfer").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='transfer_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            var total_cnt = $("div[id^='activity_boxes_']").length;
            
            if(id==0 && (num==1 || num==total_cnt)){
                sr_no++;
                id++;
                var append_data = '<div id="transfer_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="transfer_details_'+id+'_'+num+'">Airport/Station Transfer Detail.</label><div class="hotel-box"><input type="text" name="transfer_details_'+num+'_'+sr_no+'" id="transfer_details_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="airport_station_transfer"></div></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_transfer(this.id)" /></div></div>';
                $("#activity_boxes_"+num).append(append_data); //append new text box in main div
                $("#transfer_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
                $("#plan_frm_cnt_"+num).val(sr_no);
            } else {
                alert("Airport/Station Transfer can be added first and last day of itinerary");
            }
        }); 
        
        //If the City Journey icon clicked form inputs regarding city journey is populated
        $(".add_transport").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='city_journey_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="city_journey_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="transport_details_'+id+'_'+num+'">Transport Detail.</label><div class="hotel-box"><input type="text" name="transport_details_'+num+'_'+sr_no+'" id="transport_details_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="transport"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="distance_'+id+'_'+num+'">Distance</label><div class="hotel-box"><input type="text" name="distance_'+num+'_'+sr_no+'" id="distance_'+id+'_'+num+'" class="form-control form-control01"></div></div></div><div class="col-md-2"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_city_journey(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#city_journey_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        }); 
        
        //If the Sightseeing icon clicked form inputs regarding Sightseeing is populated
        $(".add_day_sightseeing").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='sightseeing_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="sightseeing_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="sightseeing_views_'+id+'_'+num+'">Sightseeing View.</label><div class="hotel-box"><input type="text" name="sightseeing_views_'+num+'_'+sr_no+'" id="sightseeing_views_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="sightseeing"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Start Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_sightseeing(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#sightseeing_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        }); 
        
        //If the Meal icon clicked form inputs regarding Meal is populated
        $(".add_day_meal").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            var num = $(this).attr("id");
            var id = $("#activity_boxes_"+num+" > div[id^='meal_box_']").length;
            var sr_no = $("#activity_boxes_"+num+" > .flight_train_details").length;
            
            sr_no++;
            id++;
            var append_data = '<div id="meal_box_'+id+'_'+num+'" class="price_box flight_train_details"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="meal_name_'+id+'_'+num+'">Meal Name.</label><div class="hotel-box"><input type="text" name="meal_name_'+num+'_'+sr_no+'" id="meal_name_'+id+'_'+num+'" class="form-control form-control01"><input type="hidden" name="day_act_type_'+num+'_'+sr_no+'" value="meal"></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Start Time</label><div class="hotel-box"><select name="departure_time_'+num+'_'+sr_no+'" id="departure_time_'+id+'_'+num+'" class="form-control form-control01"><option value="">Please Select</option><?php $time = 6; for($t=1; $t<32; $t++){ if($t%2!=0){ ?><option value="<?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?>"><?php if($time < 10){ echo '0'.$time.':00'; } else { echo $time.':00'; } ?></option><?php } else { ?><option value="<?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?>"><?php if($time < 10){ echo '0'.$time.':30'; } else { echo $time.':30'; } ?></option><?php $time++; } } ?></select></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration</label><div class="hotel-box"><input type="text" name="duration_'+num+'_'+sr_no+'" id="duration_'+id+'_'+num+'" class="form-control form-control01"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'_'+num+'" onclick="remove_meal(this.id)" /></div></div>';
            $("#activity_boxes_"+num).append(append_data); //append new text box in main div
            $("#meal_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down
                
            $("#plan_frm_cnt_"+num).val(sr_no);
            /*} else {
                alert("City Journey only 10 allowed");
            }*/
        });
    </script>

    <script>
    //If the cross icon was clicked flight/train box remove
    function remove_flight_train(num){
        $("#flight_train_box_"+num).css('background','tomato');
        $("#flight_train_box_"+num).fadeOut(800,function(){
           $("#flight_train_box_"+num).remove();
        });
    }
        
    //If the cross icon was clicked airport/station transfer remove
    function remove_transfer(num){
        $("#transfer_box_"+num).css('background','tomato');
        $("#transfer_box_"+num).fadeOut(800,function(){
           $("#transfer_box_"+num).remove();
        });
    }
        
    //If the cross icon was clicked city journey remove
    function remove_city_journey(num){
        $("#city_journey_box_"+num).css('background','tomato');
        $("#city_journey_box_"+num).fadeOut(800,function(){
           $("#city_journey_box_"+num).remove();
        });
    }
        
    //If the cross icon was clicked sightseeing remove
    function remove_sightseeing(num){
        $("#sightseeing_box_"+num).css('background','tomato');
        $("#sightseeing_box_"+num).fadeOut(800,function(){
           $("#sightseeing_box_"+num).remove();
        });
    }
        
    //If the cross icon was clicked meal remove
    function remove_meal(num){
        $("#meal_box_"+num).css('background','tomato');
        $("#meal_box_"+num).fadeOut(800,function(){
           $("#meal_box_"+num).remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var id = 1;
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_price").on('click',function(){
            var lastid = $("#price_boxes > .price_box > .remove-box img:last").attr('id');
            
            if(typeof lastid == "undefined"){
                id = 1;
            } else {
                id = lastid;
            }
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div id="price_box_'+id+'" class="price_box"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="trip_start_date_'+id+'">Start Date</label><div class="hotel-box"><input type="text" name="trip_start_date[]" id="trip_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="trip_end_date_'+id+'">End Date</label><div class="hotel-box"><input type="text" name="trip_end_date[]" id="trip_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label" for="trip_styles_'+id+'">Trip Type</label><div class="hotel-box"><select name="trip_styles[]" id="trip_styles_'+id+'" class="form-control form-control01"><option value="">Please Select</option> <?php $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' "); while($style_res = mysqli_fetch_assoc($style_qry)){ ?><option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option><?php } ?></select></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div>';
            $("#price_boxes").append(append_data); //append new text box in main div
            $("#price_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
    });

    //If the cross icon was clicked
    function remove_image(pricenum){
        $("#price_box_"+pricenum).css('background','tomato');
        $("#price_box_"+pricenum).fadeOut(800,function(){
           $("#price_box_"+pricenum).remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var max = 20,append_data;

        //If the add icon is clicked blackout date start and end date is populate
        $(".add_blackout_date").on('click',function(){
            var lastid = $("#blackout_boxes > .price_box > .remove-box img:last").attr('id');
            
            if(typeof lastid == "undefined"){
                id = 1;
            } else {
                id = lastid;
            }
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div id="blackout_box_'+id+'" class="price_box"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="blackout_start_date_'+id+'">Blackout Start Date</label><div class="hotel-box"><input type="text" name="blackout_start_date[]" id="blackout_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="blackout_end_date_'+id+'">Blackout End Date</label><div class="hotel-box"><input type="text" name="blackout_end_date[]" id="blackout_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_blackout(this.id)" /></div></div>';
            $("#blackout_boxes").append(append_data); //append new text box in main div
            $("#blackout_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
    });

    //If the cross icon was clicked
    function remove_blackout(num){
        $("#blackout_box_"+num).css('background','tomato');
        $("#blackout_box_"+num).fadeOut(800,function(){
           $("#blackout_box_"+num).remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var max = 20,append_data;

        //If the add icon is clicked purchase date start and end date is populate
        $(".add_purchase_date").on('click',function(){
            var lastid = $("#purchase_boxes > .price_box > .remove-box img:last").attr('id');
            
            if(typeof lastid == "undefined"){
                id = 1;
            } else {
                id = lastid;
            }
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div id="purchase_box_'+id+'" class="price_box"><div class="row"><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="purchase_start_date_'+id+'">Purchase Start Date</label><div class="hotel-box"><input type="text" name="purchase_start_date[]" id="purchase_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label" for="purchase_end_date_'+id+'">Purchase End Date</label><div class="hotel-box"><input type="text" name="purchase_end_date[]" id="purchase_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_purchase(this.id)" /></div></div>';
            $("#purchase_boxes").append(append_data); //append new text box in main div
            $("#purchase_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
    });

    //If the cross icon was clicked
    function remove_purchase(num){
        $("#purchase_box_"+num).css('background','tomato');
        $("#purchase_box_"+num).fadeOut(800,function(){
           $("#purchase_box_"+num).remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){

        //If the add icon is clicked purchase date start and end date is populate
        $(".add_season").on('click',function(){
            var lastid = $("#season_boxes > .price_box > .remove-box > img:last").attr('id');
            
            if(typeof lastid == "undefined"){
                id = 1;
            } else {
                id = lastid;
            }
            
            var max = 5,append_data;
            
            if($("div[id^='season_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
                id++;
                var append_data = '<div id="season_box_'+id+'" class="price_box"><div class="row"><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="season_start_date_'+id+'">Season Start</label><div class="hotel-box"><input type="text" name="season_start_date[]" id="season_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="season_end_date_'+id+'">Season End</label><div class="hotel-box"><input type="text" name="season_end_date[]" id="season_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="single_room_'+id+'">Single Room($)</label><div class="hotel-box"><input type="text" name="single_room[]" id="single_room_'+id+'" class="form-control form-control01" placeholder="Single Room($)"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="twin_sharing_'+id+'">Twin Sharing($)</label><div class="hotel-box"><input type="text" name="twin_sharing[]" id="twin_sharing_'+id+'" class="form-control form-control01" placeholder="Twin Sharing($)"></div></div></div><div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="triple_sharing_'+id+'">Triple Sharing($)</label><div class="hotel-box"><input type="text" name="triple_sharing[]" id="triple_sharing_'+id+'" class="form-control form-control01" placeholder="Triple Sharing($)"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_season(this.id)" /></div></div>';
                $("#season_boxes").append(append_data); //append new text box in main div
                $("#season_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

                $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            } else {
                alert("Maximum 4 seasons are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_season(num){
        $("#season_box_"+num).css('background','tomato');
        $("#season_box_"+num).fadeOut(800,function(){
           $("#season_box_"+num).remove();
        });
    }
    </script>

    <script>
        $('.cty_drpdwn').select2();
        $('.act_drpdwn').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            nonSelectedText: 'Select Activities'
        });
        CKEDITOR.replaceAll( 'itinerary_detail' );
        CKEDITOR.replaceAll( 'inclusions' );
        CKEDITOR.replaceAll( 'exclusions' );
    </script>

    <script>
    $( function() {
        $( ".date-type" ).datepicker({ dateFormat: 'dd-mm-yy' });
    });
    </script>

  </body>
</html>
