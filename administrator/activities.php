<?php 
    include('include.inc.php');
    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-delete', $_SESSION['AccessRights'])){
            $del_display = "";
        } else {
            $del_display = "display: none;";
        }

        //Add Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('activities-add', $_SESSION['AccessRights'])){
            $add_display = "";
        } else {
            $add_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('activities-edit', $_SESSION['AccessRights'])){
            $edit_display = "";
        } else {
            $edit_display = "display: none;";
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Activities - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Activities List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Activities List</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                                <p align="right"><a class="btn btn-primary text-white" href="activities-add.php" role="button">Add Activity</a></p>
                                
                                <div class="table-responsive m-t-20">
                                    <table id="config-table" class="table display table-striped no-wrap m-t-20">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Activity Name</th>
                                                <th>Cost</th>
                                                <th>Duration</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                       
                        $query=mysqli_query($con, "SELECT h.*, ct.CityName, cat.CountryName FROM tbl_activities h
                        LEFT JOIN tbl_cities ct ON ct.CityId=h.city_id
                        LEFT JOIN tbl_countries cat ON cat.CountryId=h.country_id
                        ORDER BY h.country_id, h.city_id ");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                while( $row=mysqli_fetch_assoc($query) ){
                                ++$i;
                                ?>
                                            <tr>
                                            <td><?php echo $i;?></td>
                             <td><?php echo $row['activity_name']; ?></td>
                             <td><?php if(!empty($row['activity_cost'])){ echo '$'.$row['activity_cost']; } ?></td>
                             <td><?php echo $row['duration']; ?></td>
                             <td><?php echo $row['CountryName']; ?></td>
                             <td><?php echo $row['CityName']; ?></td>
                             <td id="status_<?php echo $row['act_id']; ?>"><?php echo $row['status'];?></td>
                             <td><div class="btn-group"><a class="btn bg-red change_status" id="<?php echo $row['act_id'];?>" title="Active"><i class="fa fa-check"></i></a> <a class="btn bg-red change_status" id="<?php echo $row['act_id'];?>" title="Inactive"><i class="fa fa-ban"></i></a> <a class="btn bg-navy" style="<?php echo $edit_display; ?>" href="activities-edit.php?id=<?php echo $row['act_id'];?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" style="<?php echo $del_display; ?>" id="<?php echo $row['act_id'];?>" title="Delete"><i class="fa fa-trash"></i></a> </div></td>
                                
                            </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script>
   
$(document).ready(function(){

    // Delete 
    $('.delete').click(function(){
       var el = this;

       // Delete id
       var deleteid = $(this).attr('id');

       var confirmalert = confirm("Are you sure want to delete?");
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'delete-record.php',
            type: 'POST',
            data: { hotel_id:deleteid },
            success: function(response){

            if(response == 1){
            // Remove row from HTML Table
            $(el).closest('tr').css('background','tomato');
            $(el).closest('tr').fadeOut(800,function(){
               $(this).remove();
            });
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
    

// Update Status    
$(document).ready(function(){

    // Delete 
    $('.change_status').click(function(){
       var el = this;

       // Delete id
       var statusid = $(this).attr('id');
       var status_title = $(this).attr('title');

       if( $(this).attr('title')=='Active' ){
            var confirmalert = confirm("Are you sure want to Active?");
       } else if( $(this).attr('title')=='Inactive' ){
            var confirmalert = confirm("Are you sure want to Inactive?");
       }
        
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'status-update.php',
            type: 'POST',
            data: { hotel_id:statusid, hotel_title:status_title },
            success: function(response){

            if(response){
                $('#status_'+statusid).css('border-bottom','2px solid green');
                setTimeout(function () {
                    //alert(response);
                    $('#status_'+statusid).empty();
                    $('#status_'+statusid).html(response);
                    $('#status_'+statusid).css('border-bottom', 'none');
                },300);
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
    
</script>
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 100,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary me-1');
        });

    </script>
</body>

</html>