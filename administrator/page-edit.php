<?php 
    include('include.inc.php');

    $pageid = $_GET['id'];

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('pages', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }
    
    if( !empty($_POST['pagename']) ){
        
        if(!empty($_FILES['content_img1']['name'])){
            $content_img1=uploadfiles($_FILES['content_img1']['name'], $_FILES['content_img1']['tmp_name'], $_FILES['content_img1']['error'], $_FILES['content_img1']['size'], PAGE_CONTENT_IMG);
        }else{
            $content_img1=$_POST['old_img1'];
        }
        
        if(!empty($_FILES['content_img2']['name'])){
            $content_img2=uploadfiles($_FILES['content_img2']['name'], $_FILES['content_img2']['tmp_name'], $_FILES['content_img2']['error'], $_FILES['content_img2']['size'], PAGE_CONTENT_IMG);
        }else{
            $content_img2=$_POST['old_img2'];
        }
        
        $first_con = str_replace("&",'&amp;',$_POST['content_first']);
        $second_con = str_replace("&",'&amp;',$_POST['content_second']);
                
        echo $qry = "UPDATE tbl_static_pages SET 
        pagename = '".$_POST['pagename']."',
        pageurl = '".$_POST['pageurl']."',
        content_img1 = '".$content_img1."',
        content_img2 = '".$content_img2."',
        content_first = '".mysqli_real_escape_string($con, $first_con)."',
        content_second = '".mysqli_real_escape_string($con, $second_con)."',
        bannerid = '".$_POST['bannerid']."'
        WHERE pageid='".$pageid."' ";
        mysqli_query($con, $qry);
        
        header("location: pages.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_static_pages WHERE pageid=$pageid "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Static Page - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Static Page</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="pages.php">Static Page List</a></li>
                            </ol>
                           
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="role_add" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="pagename">Page Name*</label>
                            <input type="text" name="pagename" id="pagename" class="form-control form-control01" value="<?php echo $row['pagename']; ?>" placeholder="Page Name*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="pageurl">Page URL*</label>
                            <input type="text" name="pageurl" id="pageurl" class="form-control form-control01" value="<?php echo $row['pageurl']; ?>" placeholder="Page URL*" required>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="content_img1">Content Image 1</label>
                            <input type="file" name="content_img1" id="content_img1" class="form-control form-control01">
                            <input type="hidden" name="old_img1" value="<?php echo $row['content_img1']; ?>">
                            <?php
                            if(!empty($row['content_img1'])){
                                $content_img1 = PAGE_CONTENT_IMG.$row['content_img1'];
                                if(file_exists($content_img1)){
                                    echo '<img src="'.$content_img1.'" width="200" class="img-responsive">';
                                }
                            }
                            ?>
                            <p class="img_note">Note : Content Image 1 size should be 969 x 302</p>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="content_first"><?php if($row['pageurl']==HOME_URL.'contact-us/'){ echo 'Map Iframe'; } else { echo 'Content 1'; } ?></label>
                            <textarea cols="80" class="form-control textarea-a map-area" id="content_first" name="content_first" rows="10"><?php echo htmlentities($row['content_first']); ?></textarea>
				            <?php if($row['pageurl']!=HOME_URL.'contact-us/'){ echo '<script type="text/javascript">CKEDITOR.replace( "content_first" );</script>'; } ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="content_img2">Content Image 2</label>
                            <input type="file" name="content_img2" id="content_img2" class="form-control input-a">
                            <input type="hidden" name="old_img2" value="<?php echo $row['content_img2']; ?>">
                            <?php
                            if(!empty($row['content_img2'])){
                                $content_img2 = PAGE_CONTENT_IMG.$row['content_img2'];
                                if(file_exists($content_img2)){
                                    echo '<img src="'.$content_img2.'" width="200" class="img-responsive">';
                                }
                            }
                            ?>
                            <p class="img_note">Note : Content Image 2 size should be 969 x 302</p>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="content_second">Content 2</label>
                            <textarea cols="80" id="content_second" name="content_second" rows="10"><?php echo $row['content_second']; ?></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'content_second' );</script>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="bannerid">Select Banner for page</label>
                            <select class="form-control form-control01" name="bannerid" id="bannerid">
                                <option value="">Select Banner for page</option>
                                <?php
                                $qbanner = mysqli_query($con, "SELECT * FROM tbl_banners_page WHERE status='Active' ORDER BY pagename");
                                while($rbanner = mysqli_fetch_assoc($qbanner)){ ?>
                                <option value="<?php echo $rbanner['pbid']; ?>" <?php if($rbanner['pbid']==$row['bannerid']){ echo 'selected'; } ?>><?php echo $rbanner['pagename']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    
                    
                     
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script>
    $(document).ready(function(){
		$("#btnRight").click(function (e) {
            var selectedOpts = $("#lstBox1 option:selected");
            if (selectedOpts.length == 0) {
              alert("Nothing to move.");
              e.preventDefault();
            }
            $("#lstBox2").append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
        $("#btnLeft").click(function (e) {
            var selectedOpts = $("#lstBox2 option:selected");
            if (selectedOpts.length == 0) {
              alert("Nothing to move.");
              e.preventDefault();
            }
            $("#lstBox1").append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
        
        $('.btn.btn03').click(function(){
            $("select#lstBox1 option").prop("selected", "selected");
            $("select#lstBox2 option").prop("selected", "selected");
        });
	});      
    </script>
    
</body>

</html>