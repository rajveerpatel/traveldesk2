<?php
include('includes/config.inc.php');

//Update Status Parent Destination Record
$id = 0;
$title = '';
if(!empty($_POST['parent_dest_id'])){
    $id = $_POST['parent_dest_id'];
    $title = $_POST['dest_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_parent_destinations SET status = 'Active' WHERE parent_dest_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_parent_destinations WHERE parent_dest_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_parent_destinations SET status = 'Inactive' WHERE parent_dest_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_parent_destinations WHERE parent_dest_id = $id "));
        echo $res['status'];
    }
}

//Update Status Country Destination Record
if(!empty($_POST['dest_id'])){
    $id = $_POST['dest_id'];
    $title = $_POST['dest_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_destinations SET status = 'Active' WHERE dest_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_destinations WHERE dest_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_destinations SET status = 'Inactive' WHERE dest_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_destinations WHERE dest_id = $id "));
        echo $res['status'];
    }
}

//Update Status vehicle Record
if(!empty($_POST['vehicle_id'])){
    $id = $_POST['vehicle_id'];
    $title = $_POST['vehicle_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tblvehicle SET status = 'Active' WHERE vehicleid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tblvehicle WHERE vehicleid = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tblvehicle SET status = 'Inactive' WHERE vehicleid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tblvehicle WHERE vehicleid = $id "));
        echo $res['status'];
    }
}


//Update Status Trip Category Record
if(!empty($_POST['cat_id'])){
    $id = $_POST['cat_id'];
    $title = $_POST['cat_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_trip_categories SET status = 'Active' WHERE cat_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_categories WHERE cat_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_trip_categories SET status = 'Inactive' WHERE cat_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_categories WHERE cat_id = $id "));
        echo $res['status'];
    }
}

//Update Status Trip Type Record
if(!empty($_POST['trip_type_id'])){
    $id = $_POST['trip_type_id'];
    $title = $_POST['trip_type_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_trip_types SET status = 'Active' WHERE trip_type_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_types WHERE trip_type_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_trip_types SET status = 'Inactive' WHERE trip_type_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_types WHERE trip_type_id = $id "));
        echo $res['status'];
    }
}

//Update Status Trip Package Record
if(!empty($_POST['trip_id'])){
    $id = $_POST['trip_id'];
    $title = $_POST['trip_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_trip_packages SET status = 'Active' WHERE trip_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_packages WHERE trip_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_trip_packages SET status = 'Inactive' WHERE trip_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_packages WHERE trip_id = $id "));
        echo $res['status'];
    }
}

//Update Status Page Banner Record
if(!empty($_POST['pbid'])){
    $id = $_POST['pbid'];
    $title = $_POST['banner_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_banners_page SET status = 'Active' WHERE pbid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_banners_page WHERE pbid = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_banners_page SET status = 'Inactive' WHERE pbid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_banners_page WHERE pbid = $id "));
        echo $res['status'];
    }
}

//Update Status Admin Record
if(!empty($_POST['adminid'])){
    $id = $_POST['adminid'];
    $title = $_POST['admin_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_admin SET status = 'Active' WHERE adminid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_admin WHERE adminid = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_admin SET status = 'Inactive' WHERE adminid = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_admin WHERE adminid = $id "));
        echo $res['status'];
    }
}

//Update Status Departure City Record
if(!empty($_POST['city_id'])){
    $id = $_POST['city_id'];
    $title = $_POST['city_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_departure_cities SET status = 'Active' WHERE city_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_departure_cities WHERE city_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_departure_cities SET status = 'Inactive' WHERE city_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_departure_cities WHERE city_id = $id "));
        echo $res['status'];
    }
}

//Update Status Hotel Record
if(!empty($_POST['hotel_id'])){
    $id = $_POST['hotel_id'];
    $title = $_POST['hotel_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_hotels SET status = 'Active' WHERE hotel_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_hotels WHERE hotel_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_hotels SET status = 'Inactive' WHERE hotel_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_hotels WHERE hotel_id = $id "));
        echo $res['status'];
    }
}

//Update Status Activity Record
if(!empty($_POST['act_id'])){
    $id = $_POST['act_id'];
    $title = $_POST['act_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_activities SET status = 'Active' WHERE act_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_activities WHERE act_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_activities SET status = 'Inactive' WHERE act_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_activities WHERE act_id = $id "));
        echo $res['status'];
    }
}

//Update Status Extensions Record
if(!empty($_POST['ext_id'])){
    $id = $_POST['ext_id'];
    $title = $_POST['ext_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_extension SET status = 'Active' WHERE ext_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_extension WHERE ext_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_extension SET status = 'Inactive' WHERE ext_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_extension WHERE ext_id = $id "));
        echo $res['status'];
    }
}

//Update Status Trip Style Record
if(!empty($_POST['style_id'])){
    $id = $_POST['style_id'];
    $title = $_POST['style_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_trip_style SET status = 'Active' WHERE style_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_style WHERE style_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_trip_style SET status = 'Inactive' WHERE style_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_trip_style WHERE style_id = $id "));
        echo $res['status'];
    }
}

//Update Status Room Type Record
if(!empty($_POST['room_type_id'])){
    $id = $_POST['room_type_id'];
    $title = $_POST['room_type_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_room_type SET status = 'Active' WHERE room_type_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_room_type WHERE room_type_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_room_type SET status = 'Inactive' WHERE room_type_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_room_type WHERE room_type_id = $id "));
        echo $res['status'];
    }
}

//Update Status Agent Commission Record
if(!empty($_POST['com_id'])){
    $id = $_POST['com_id'];
    $title = $_POST['com_title'];
    
    if($title=='Active'){
        mysqli_query($con, "UPDATE tbl_agent_commision_slab SET status = 'Active' WHERE com_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_agent_commision_slab WHERE com_id = $id "));
        echo $res['status'];
    } elseif($title=='Inactive'){
        mysqli_query($con, "UPDATE tbl_agent_commision_slab SET status = 'Inactive' WHERE com_id = $id ");
        $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT status FROM tbl_agent_commision_slab WHERE com_id = $id "));
        echo $res['status'];
    }
}
?>