<?php 
ini_set("error_reporting", E_ALL & ~E_NOTICE);

$page = basename($_SERVER['PHP_SELF']);

if($_SESSION['AdminRole'] > 0){
    $r_access = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_roles WHERE role_id='".$_SESSION['AdminRole']."' "));
    $_SESSION['UserRole'] = $r_access['role_name'];
    $_SESSION['AccessRights'] = explode(',',$r_access['access_rights']);
}
?>
<div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                       <li> <a class="has-arrow waves-effect waves-dark" href="dashboard.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard <span class="badge rounded-pill bg-cyan ms-auto">3</span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="admin-list.php">Admin   List </a></li>
                                <li><a href="admin-roles.php">Admin Roles</a></li>
                                <li><a href="role-actions.php">Roles Actions</a></li>
                                
                            </ul>
                        </li>
                        <li class="nav-small-cap">--- PACKAGE MANAGEMENT</li>
                        <li> <a href="trip-categories.php" ><i class="mdi mdi-book-open"></i><span class="hide-menu">Trip Categories</span></a></li>
						<li> <a href="trip-types.php" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Trip Types</span></a></li>
                        <li> <a href="trip-styles.php" aria-expanded="false"><i class="mdi mdi-book-plus"></i><span class="hide-menu">Trip Styles</span></a></li>
                        <li> <a href="destinations.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Destinations</span></a></li>
                        <li> <a href="parent-destinations.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Parent Destinations</span></a></li>
                        <li> <a href="hotels.php" aria-expanded="false"><i class="fas fa-building"></i><span class="hide-menu">Hotels</span></a></li>
                        <li> <a href="country.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Country List</span></a></li>
                        <li> <a href="city.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">City List</span></a></li>
                        <li> <a href="activities.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Activity List</span></a></li>
                        <li> <a href="meals.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Meals List</span></a></li>
                        <li> <a href="vehicletype.php" aria-expanded="false"><i class="fas fa-map-signs"></i><span class="hide-menu">Vehicle Type List</span></a></li>
                        
                        <li> <a href="room-type.php" aria-expanded="false"><i class="mdi mdi-hotel"></i><span class="hide-menu">Hotels Rooms</span></a></li>
                        <li> <a href="trip-packages.php" aria-expanded="false"><i class="mdi mdi-package"></i><span class="hide-menu">Trip Packages</span></a></li>
						<li> <a href="departure-cities.php" aria-expanded="false"><i class="mdi mdi-city"></i><span class="hide-menu">Departure Cities</span></a></li>
                        <li class="nav-small-cap">--- USERS & AGENTS MANAGEMENT</li>
                        <li> <a href="agents.php" aria-expanded="false"><i class="fas fa-users"></i><span class="hide-menu">Agents</span></a></li>
						<li> <a href="agent-commission.php" aria-expanded="false"><i class="fas fa-donate"></i><span class="hide-menu">Agent Commission</span></a></li>
						<li> <a href="trip-enquiries.php " aria-expanded="false"><i class="fab fa-tripadvisor"></i><span class="hide-menu">Trip Enquiries</span></a></li>
						<li> <a href="user-management.php" aria-expanded="false"><i class="mdi mdi-account-multiple-plus"></i><span class="hide-menu">User Management</span></a></li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-view-other"></i><span class="hide-menu">Other Section <span class="badge rounded-pill bg-cyan ms-auto">3</span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="banners.php">Banner</a></li>
                                <li><a href="pages.php">Static Pages</a></li>
                              </ul>
                        </li>
						<li> <a href="tailor-made-enquiries.php" aria-expanded="false"><i class="mdi mdi-camera-iris"></i><span class="hide-menu">Tailor Made</span></a></li>
						<li> <a href="book-air-plus.php" aria-expanded="false"><i class="mdi mdi-airplane-takeoff"></i><span class="hide-menu">Book Air Plus</span></a></li>
						<li> <a href="contact-enquiries.php" aria-expanded="false"><i class="mdi mdi-contacts"></i><span class="hide-menu">Contact Enquiries</span></a></li>
						
                        
                        
                        
                        
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>