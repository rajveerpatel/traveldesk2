<footer class="footer">
    <div class="container">
        <div class="copy-right">
            <span class="copy-right-text">Copyright © <?php echo date('Y'); ?> Travdek. All Rights Reserved.</span>
            <span class="pipe">|</span>
            <p class="copy-right-link"><a href="mailto:support@travdek.com">support@travdek.com</a></p> 
        </div>
    </div>
</footer>