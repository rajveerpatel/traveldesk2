<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    if( !empty($_POST['trip_name']) ){
        
        if(!empty($_FILES['trip_thumb']['name'])){
            $trip_thumb=uploadfiles($_FILES['trip_thumb']['name'], $_FILES['trip_thumb']['tmp_name'], $_FILES['trip_thumb']['error'], $_FILES['trip_thumb']['size'], PACKAGE_IMG);
        }else{
            $trip_thumb='';
        }
        
        if(!empty($_POST['trip_themes'])){
            $trip_theme = implode(',', $_POST['trip_themes']);
        } else {
            $trip_theme = '';
        }
        
        /*if(!empty($_POST['trip_category'])){
            $trip_cat = implode(',', $_POST['trip_category']);
        } else {
            $trip_cat = '';
        }*/
        
        if(!empty($_POST['trip_category'])){
            $trip_cat = $_POST['trip_category'];
        } else {
            $trip_cat = '';
        }
        
        if(!empty($_POST['trip_type'])){
            $trip_type = implode(',', $_POST['trip_type']);
        } else {
            $trip_type = '';
        }
        
        if(!empty($_POST['validity_start_date'])){
            $validity_start_date = date('Y-m-d', strtotime($_POST['validity_start_date']));
        } else {
            $validity_start_date = '';
        }
        
        if(!empty($_POST['validity_end_date'])){
            $validity_end_date = date('Y-m-d', strtotime($_POST['validity_end_date']));
        } else {
            $validity_end_date = '';
        }
        
        /*if(!empty($_POST['trip_provides'])){
            $trip_provides = implode(',', $_POST['trip_provides']);
        } else {
            $trip_provides = '';
        }*/
        
        function clean($string) {
           $string = str_replace(' ', '-', $string); 
           $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string); 

           return preg_replace('/-+/', '-', $string); 
        }
        /*$replacecharacters = '/[^A-Za-z0-9\-]/';*/
        $pack_title = clean(trim($_POST['trip_name']));
        $pack_dest = explode(':', $_POST['country_dest']);
        $pack_destination = clean(trim($pack_dest[1]));
        $duration = clean(trim($_POST['trip_days']));
        $slug=$pack_title.'-'.$pack_destination.'-'.$duration.'-days';

        /*$qslug = mysqli_query($con, "select slug from tbl_trip_packages ");
        if(mysqli_num_rows($qslug) > 0){
            for($i=1; $i<=30; $i++){
                while($rslug = mysqli_fetch_assoc($qslug)){
                    $slug_arr[] = $rslug['slug'];
                }
                if($i!=1){ $slugend = '-'.$i; } else { $slugend = ''; }
                $new_slug = strtolower($slug.$slugend);
                if(!in_array($new_slug, $slug_arr)){
                    $slug= $new_slug;
                    break;
                }
            }
        }*/
        
        echo $tsql = "INSERT INTO tbl_trip_packages SET 
        trip_sku = '".$_POST['trip_sku']."',
        trip_name = '".mysqli_real_escape_string($con, $_POST['trip_name'])."',
        trip_thumb = '".$trip_thumb."',
        banner_id = '".$_POST['banner_id']."',
        slug = '".trim(strtolower($slug))."',
        trip_days = '".$_POST['trip_days']."',
        parent_dest = '".$_POST['parent_dest']."',
        country_dest = '".$_POST['country_dest']."',
        trip_themes = '".$trip_theme."',
        trip_category = '".$trip_cat."',
        trip_type = '".$trip_type."',
        validity_start_date = '".$validity_start_date."',
        validity_end_date = '".$validity_end_date."',
        short_highlights = '".mysqli_real_escape_string($con, $_POST['short_highlights'])."',
        trip_highlights_inclusions = '".mysqli_real_escape_string($con, $_POST['trip_highlights_inclusions'])."',
        single_room = '".mysqli_real_escape_string($con, $_POST['single_room'])."',
        twin_sharing = '".mysqli_real_escape_string($con, $_POST['twin_sharing'])."',
        triple_sharing = '".mysqli_real_escape_string($con, $_POST['triple_sharing'])."',
        quad_sharing = '".mysqli_real_escape_string($con, $_POST['quad_sharing'])."',
        trip_map = '".mysqli_real_escape_string($con, $_POST['trip_map'])."',
        trip_stopover = '".$_POST['trip_stopover']."',
        trip_extension = '".$_POST['trip_extension']."',
        check_ext_st = '".$_POST['check_ext_st']."',
        status = '".$_POST['status']."',
        postip = '".$_SERVER['REMOTE_ADDR']."',
        postdate=NOW() ";
        mysqli_query($con, $tsql);
        
        $tripid = mysqli_insert_id($con);
        
        //add itenerary
        if( sizeof( $_POST['itenerary_title'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['itenerary_title'] as $key => $value ){
                 if( !empty( $_POST['itenerary_title'][$key]  ) ){
                    
                    if(!empty($_FILES['itinerary_image']['name'][$key])){
                        $itinerary_image=uploadfiles($_FILES['itinerary_image']['name'][$key], $_FILES['itinerary_image']['tmp_name'][$key], $_FILES['itinerary_image']['error'][$key], $_FILES['itinerary_image']['size'][$key], ITINERARY_IMG);
                    }else{
                        $itinerary_image=$_POST['old_itinerary_image'][$key];
                    }
                    
                    $activities = $_POST["activities$i"];
                    if(!empty($activities)){
                        echo $activity_ids = implode(',', $activities);
                    } else {
                        echo $activity_ids = '';
                    }
                    
                    $meal = $_POST['meal'.$i];
                    if(!empty($meal)){
                        echo $meals = implode(',', $meal);
                    } else {
                        echo $meals = '';
                    }
                    
                    $isql = "insert into tbl_trip_itineraries set
                    trip_id = '$tripid',
                    cat_id = '$trip_cat',
                    day = '".$_POST['day'][$key]."',
                    itenerary_title = '".mysqli_real_escape_string($con, $_POST['itenerary_title'][$key])."',
                    itenerary_description = '".mysqli_real_escape_string($con, $_POST['itenerary_description'][$key])."',
                    itinerary_image = '".mysqli_real_escape_string($con, $itinerary_image)."',
                    hotel = '".mysqli_real_escape_string($con, $_POST['hotel'][$key])."',
                    activities = '".mysqli_real_escape_string($con, $activity_ids)."',
                    meal = '".mysqli_real_escape_string($con, $meals)."',
                    city_id = '".mysqli_real_escape_string($con, $_POST['itinerary_city'][$key])."' ";
                    mysqli_query($con, $isql);
                     
                    $itinerary_id = mysqli_insert_id($con);
                     
                    $i++;
                 }
             }
         }
        
        //add trip prices
        if( sizeof( $_POST['trip_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['trip_start_date'] as $key => $value ){
                 if( !empty( $_POST['trip_start_date'][$key]  ) ){
                    
                    $q = "INSERT INTO tbl_trip_prices set 
                    trip_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_start_date'][$key])))."',
                    trip_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_end_date'][$key])))."',
                    trip_categories='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_styles='".mysqli_real_escape_string($con, $_POST['trip_styles'][$key])."',
                    trip_price='".mysqli_real_escape_string($con, $_POST['trip_price'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
        header("location: trip-packages.php");
    }
?>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-center">Add Trip Package</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <div class="btn00 text-right">
            <a class="btn btn03 btn-width-03" href="trip-packages.php">Trip Packages</a>
        </div>
        <p class="space2"></p>       

        <div class="input-form">
            <form name="category" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_name">Trip Name*</label>
                            <input type="text" name="trip_name" id="trip_name" class="form-control form-control01" placeholder="Trip Name*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_thumb">Trip Image*</label>
                            <input type="file" name="trip_thumb" id="trip_thumb" class="form-control form-control01" placeholder="Trip Thumb*" required>
                            <p class="img_note">Note : Trip Image size should be 400 x 279</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_sku">Trip SKU</label>
                            <input type="text" name="trip_sku" id="trip_sku" class="form-control form-control01">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_style">Trip Type</label>
                            <select name="trip_type[]" id="trip_type" class="form-control form-control01" multiple required>
                                <?php
                                $qry_style = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                while($res_style = mysqli_fetch_assoc($qry_style)){
                                    $style_ids = explode(',', $row['trip_style']);
                                ?>
                                <option value="<?php echo $res_style['style_id']; ?>" <?php if(in_array($res_style['style_id'], $style_ids)){ echo 'selected'; } ?>><?php echo $res_style['trip_style']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!--<div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="luxury_price">Price in Calendar($)</label>
                            <div id="calendar"></div>
                            <input type="hidden" id="tomorrow_date" value="<?php echo date('Y-m-d', strtotime('+1 day')); ?>">
                        </div>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Trip Price (trip type and date wise)</p>
                        <div id="price_boxes" class="price_boxes">
                            <div id="price_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_start_date_1">Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_start_date[]" id="trip_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_end_date_1">End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_end_date[]" id="trip_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_styles_1">Trip Type</label>
                                            <div class="hotel-box">
                                                <select name="trip_styles[]" id="trip_styles_1" class="form-control form-control01">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                                    while($style_res = mysqli_fetch_assoc($style_qry)){
                                                    ?>
                                                    <option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_price_1">Trip Price ($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_price[]" id="trip_price_1" class="form-control form-control01" placeholder="Trip Price ($)">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_price" /></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_start_date">Trip Validity Start Date*</label>
                            <input type="text" name="validity_start_date" id="validity_start_date" class="form-control form-control01 date-type" placeholder="Trip Validity Start Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_end_date">Trip Validity End Date*</label>
                            <input type="text" name="validity_end_date" id="validity_end_date" class="form-control form-control01 date-type"  placeholder="Trip Validity End Date*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_dest">Select Parent Destination*</label>
                            <select name="parent_dest" id="parent_dest" class="form-control form-control01" onchange="return select_dest(this.value);" required>
                                <option value="">Select Parent Destination*</option>
                                <?php
                                $qry_pd = mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE status = 'active' ");
                                while($res_pd = mysqli_fetch_assoc($qry_pd)){
                                ?>
                                <option value="<?php echo $res_pd['parent_dest_id']; ?>"><?php echo $res_pd['parent_destination']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_dest">Select Country Destination*</label>
                            <select name="country_dest" id="country_dest" class="form-control form-control01" required>
                                <option value="">Select Country Destination*</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_category">Select Trip Categories*</label>
                            <select name="trip_category" id="trip_category" class="form-control form-control01" required>
                                <?php
                                $qry_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'active' ORDER BY order_no, order_no=0 ");
                                while($res_cat = mysqli_fetch_assoc($qry_cat)){
                                ?>
                                <option value="<?php echo $res_cat['cat_id']; ?>"><?php echo $res_cat['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_type">Select Trip Themes*</label>
                            <select name="trip_themes[]" class="form-control form-control01" id="trip_themes" multiple required>
                            <?php
                                $qry_type = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'active' ");
                                while($res_type = mysqli_fetch_assoc($qry_type)){
                            ?>
                                <option value="<?php echo $res_type['trip_type_id']; ?>"><?php echo $res_type['trip_type']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_days">Select Trip Days*</label>
                            <select name="trip_days" class="form-control form-control01" id="trip_days" onchange="fnitenerary(this.value)" required>
                                <option value="">Select Trip Days*</option>
                            <?php
                                for($i=1; $i<=30; $i++){
                                    if( $i < 10 ){
                                        $i = '0'.$i;
                                    }
                            ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; if($i < 2){ echo ' Day'; } else { echo ' Days'; } ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                    
                <div class="row" id="itinerary"></div>
                    
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="short_highlights">Trip Short Highlight*</label>
                            <textarea id="short_highlights" name="short_highlights" class="form-control form-control01" rows="3"></textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_highlights_inclusions">Trip Highlights & Inclusions</label>
                            <textarea id="trip_highlights_inclusions" name="trip_highlights_inclusions" rows="10"></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'trip_highlights_inclusions' );</script>
                        </div>
                    </div>
                            
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="single_room">Single Room</label>
                                    <input type="number" min="0" name="single_room" id="single_room" class="form-control form-control01">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="twin_sharing">Twin Sharing</label>
                                    <input type="number" min="0" name="twin_sharing" id="twin_sharing" class="form-control form-control01">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="triple_sharing">Triple Sharing</label>
                                    <input type="number" min="0" name="triple_sharing" id="triple_sharing" class="form-control form-control01">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="quad_sharing">Quad Sharing</label>
                                    <input type="number" min="0" name="quad_sharing" id="quad_sharing" class="form-control form-control01">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Map*</label>
                            <textarea cols="80" id="trip_map" class="form-control form-control01 map-area" name="trip_map" rows="3" required></textarea>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">This Trip as Stopover or Extension: <input type="checkbox" name="check_ext_st" id="check_ext_st" value="Yes"> Yes</label>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="stop_ext">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Trip Stopover</label>
                            <select name="trip_stopover" id="trip_stopover" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ts = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' ");
                                while($ts_res = mysqli_fetch_assoc($ts)){
                                ?>
                                <option value="<?php echo $ts_res['trip_id']; ?>"><?php echo $ts_res['trip_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Trip Extension</label>
                            <select name="trip_extension" id="trip_extension" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ts = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' ");
                                while($ts_res = mysqli_fetch_assoc($ts)){
                                ?>
                                <option value="<?php echo $ts_res['trip_id']; ?>"><?php echo $ts_res['trip_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Banner Slider</label>
                            <select name="banner_id" id="banner_id" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ban = mysqli_query($con, "SELECT * FROM tbl_banners WHERE status = 'Active' and banner_for = 'trip' ");
                                while($banres = mysqli_fetch_assoc($ban)){
                                ?>
                                <option value="<?php echo $banres['pbid']; ?>"><?php echo $banres['pagename']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Status*</label>
                            <select name="status" class="form-control form-control01" required>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <input name="submit" type="submit" value="Submit" class="btn btn03 btn-width-03">
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<?php 
    include('elements/footer.php');
?>

<div class="trdek-pp">  
    <!-- Modal HTML -->
    <div id="calendar_price" class="modal fade">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title inline-img-t6">Add Trip Price</h4>
                </div>
                <div class="modal-body">
					<div class="space2"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="user_login_label" id="trip_date_show"></div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_price">Trip Price($)*</label>
                                <input type="text" class="form-control form-control01" id="trip_price" placeholder="Trip Price($)*" >
                            </div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_cat_price">Trip Category*</label>
                                <select class="form-control form-control01" id="trip_cat_price" >
                                    <option value="">Please Select</option>
                                    <?php
                                    $q_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                    while($r_cat = mysqli_fetch_assoc($q_cat)){
                                    ?>
                                    <option value="<?php echo $r_cat['cat_id']; ?>"><?php echo $r_cat['category_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="modal-footer btn00">
                    <button type="button" id="save_btn" class="btn btn03 btn-width-03" data-dismiss="modal">Save changes</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
</div> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
      
    <script src="assets/fullcalendar/lib/main.min.js"></script>
    <script src="assets/js/calendar.js"></script>
      
    <script src="assets/select2/select2.min.js"></script>

    <!-- Multi Select JS -->
    <script src="assets/js/bootstrap-multiselect.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $('#check_ext_st').click(function(){
            if($(this).prop('checked')==true){
                $('#trip_stopover').prop('disabled', 'disabled');
                $('#trip_extension').prop('disabled', 'disabled');
                $('#stop_ext').css('display', 'none');
            } else {
                $('#stop_ext').css('display', 'block');
                $('#trip_stopover').removeAttr('disabled');
                $('#trip_extension').removeAttr('disabled');
            }
        });
    </script>

    <script>
        $(function () {
            $('#trip_type').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Types*'
            });
        });
        
        /*$(function () {
            $('#trip_category').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Categories*'
            });
        });*/
        
        $(function () {
            $('#trip_themes').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Styles*'
            });
        });
    </script>

    <script>
    function select_dest(parent_id){
        $.ajax({
            url: 'get-destination.php',
            type: 'POST',
            data: { parentid:parent_id },
            success: function(response){

                if(response){
                    $('#country_dest').empty();
                    $('#country_dest').append(response);
                }
            }
          });
    }
    </script>
<?php
$catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
while($catres = mysqli_fetch_assoc($catqry)){
    $catoption .= '<option value="'.$catres['cat_id'].'">'.$catres['category_name'].'</option>';
}
?>
    <script>
    function fnitenerary(val){
        var dest = $("#country_dest").val();
        dest = dest.split(':');
        var dest_id = dest[0];
        
        var cnt = parseInt(val);
        itinerary='<div class="col-md-12"><p class="head11">Itinerary Days</p></div>';
        for( i=1; i<=cnt; ++i){
            
        itinerary+='<div class="itinerary_box"><div class="col-md-12"><div class="form-group"><label class="user_login_label">DAY '+i+'</label></div></div><div class="col-md-6"><div class="form-group"><input type="hidden" name="day[]" value="'+i+'" ><input type="text" name="itenerary_title[]" class="form-control form-control01" placeholder="Itinerary Title"></div></div><div class="col-md-6"><div class="form-group"><input type="file" name="itinerary_image[]" class="form-control form-control01"></div></div><div class="col-md-12"><div class="form-group"><textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-'+i+'" placeholder="Itinerary Description"></textarea></div></div><div class="col-md-12"><div class="form-group city_box"><select name="itinerary_city[]" id="itinerary_city-'+i+'" class="city_drpdwn form-control form-control01"><option value="">Select Itinerary City</option></select><p class="img_note">Note : If searched city doesn\'t have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p></div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><div class="form-group"><select name="hotel[]" id="hotel-'+i+'" class="hotel_dropdown form-control form-control01"><option value="">Select Hotel</option></select></div></div><div class="col-md-4"><div class="form-group"><select name="activities'+i+'[]" id="activities-'+i+'" class="activity_drpdwn form-control form-control01" multiple></select></div></div><div class="col-md- 4"><div class="form-group"><label class="user_login_label">Meal:</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+i+'[]" id="breakfast-'+i+'" value="Breakfast"> <label class="user_login_label" for="breakfast-'+i+'">Breakfast</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+i+'[]" id="lunch-'+i+'" value="Lunch"> <label class="user_login_label" for="lunch-'+i+'">Lunch</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+i+'[]" id="dinner-'+i+'" value="Dinner"> <label class="user_login_label" for="dinner-'+i+'">Dinner</label></div></div></div></div></div>';
        }       
        $("#itinerary").html(itinerary);
        
        //Display cities according to selected destination
        $.ajax({
            url: 'ajax_get_city.php',
            type: 'post',
            data: { country: dest_id },
            
            success: function(response){
                $('.city_drpdwn').empty();
                $('.city_drpdwn').append(response);
                $('.city_drpdwn').select2().on("select2:select", function (e) {
                    var city_element = $(e.currentTarget);
                    var serial_id = city_element.attr('id');
                    var city_id = city_element.val();
                    
                    serial_id = serial_id.split('-');
                    serial_no = serial_id[1];
                    
                    var catid = $('#trip_category option:selected').val();

                    //get hotels of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        async: false,
                        type: 'post',
                        data: { hotel_city_id: city_id, hotel_cat_id: catid },

                        success: function(response){
                            $('#hotel-'+serial_no).empty();
                            $('#hotel-'+serial_no).append(response);
                        }
                    });

                    //get activities of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        type: 'post',
                        data: { activity_city_id: city_id },

                        success: function(response){
                            $('[id^="activities-'+serial_no+'"]').multiselect();
                            var options = jQuery.parseJSON(response);
                            $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                        }
                    });
                });
            }
        });
        
        //$('.cat_drpdwn').append('<?php echo $catoption; ?>');
        
        //CKEDITOR.replaceAll( 'itinerary_detail' );
        
        $(function () {
            $('.activity_drpdwn').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Activities'
            });
        });
        
        /*$('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });*/
    }
    </script>

    <script>
    //Display cities according to selected destination
    $(document).ready( function() {
        $('#country_dest').on('change', function(){
            var dest = $(this).val();
            var dest = dest.split(':');
            var dest_id = dest[0];
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country: dest_id },

                success: function(response){
                    $('.city_drpdwn').empty();
                    $('.city_drpdwn').append(response);
                    $('.city_drpdwn').select2();
                    
                    $('.hotel_drpdwn').empty();
                    $('.hotel_drpdwn').append('<option value="">Select Hotel</option>');
                    
                    /*$('.activity_drpdwn option').remove();
                    $('.activity_drpdwn').multiselect('rebuild');
                    $('.activity_drpdwn').multiselect('refresh');*/
                }
            });
        });
    } );
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var id = 1;
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_summary").on('click',function(){
        if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="col-md-6"><div id="box_'+id+'"><div class="form-group"><label class="user_login_label">Trip Summary '+id+':</label><div class="hotel-box"><input type="text" name="summary_text[]" id="summary_text_'+id+'" class="form-control  form-control01"></div> <div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div></div></div>';
            $("#summary_boxes > .row").append(append_data); //append new text box in main div
            $("#box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 20 summary text are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_image(sumnum){
        $("#box_"+sumnum).parent().css('background','tomato');
        $("#box_"+sumnum).parent().fadeOut(800,function(){
           $("#box_"+sumnum).parent().remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var id = 1;
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_price").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div id="price_box_'+id+'" class="price_box"><div class="row"><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_start_date_'+id+'">Start Date</label><div class="hotel-box"><input type="text" name="trip_start_date[]" id="trip_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_end_date_'+id+'">End Date</label><div class="hotel-box"><input type="text" name="trip_end_date[]" id="trip_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_styles_'+id+'">Trip Type</label><div class="hotel-box"><select name="trip_styles[]" id="trip_styles_'+id+'" class="form-control form-control01"><option value="">Please Select</option> <?php $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' "); while($style_res = mysqli_fetch_assoc($style_qry)){ ?><option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option><?php } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_price_'+id+'">Trip Price ($)</label><div class="hotel-box"><input type="text" name="trip_price[]" id="trip_price_'+id+'" class="form-control form-control01" placeholder="Trip Price ($)"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div>';
            $("#price_boxes").append(append_data); //append new text box in main div
            $("#price_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
    });

    //If the cross icon was clicked
    function remove_image(pricenum){
        $("#price_box_"+pricenum).css('background','tomato');
        $("#price_box_"+pricenum).fadeOut(800,function(){
           $("#price_box_"+pricenum).remove();
        });
    }
    </script>

    <script>
        CKEDITOR.replaceAll( 'inclusions' );
        CKEDITOR.replaceAll( 'exclusions' );
    </script>

    <script>
    $( function() {
        $( ".date-type" ).datepicker({ dateFormat: 'dd-mm-yy' });
    });
    </script>

  </body>
</html>