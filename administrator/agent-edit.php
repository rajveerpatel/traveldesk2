<?php 
    include('include.inc.php');

    $id = $_GET['agentid'];

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('agents', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

   if( !empty($_POST['agent_name']) && !empty($_POST['agent_phone']) && !empty($_POST['agent_email']) ){
        
        if($_POST['location_supp1']){
            $location_supp1=$_POST['location_supp1'];
        }else{
            $location_supp1='';
        }
          if($_POST['location_supp2']){
            $location_supp2=$_POST['location_supp2'];
        }else{
            $location_supp2='';
        }
          if($_POST['location_supp3']){
            $location_supp3=$_POST['location_supp3'];
        }else{
            $location_supp3='';
        }
        if($_POST['location_supp4']){
            $location_supp4=$_POST['location_supp4'];
        }else{
            $location_supp4='';
        }
        if(isset($_POST['location_supp5'])){
            $location_supp5=$_POST['location_supp5'];
        }else{
            $location_supp5='';
        }
         $query="UPDATE tbl_agent_details SET
                        agent_name='".$_POST['agent_name']."',
                        agent_country_code='".$_POST['agent_country_code']."',
                        agent_phone='".$_POST['agent_phone']."',
                        agent_email='".$_POST['agent_email']."',
                        agency_name='".$_POST['agency_name']."',
                        agency_phone='".$_POST['agency_phone']."',
                        password='".$_POST['password']."',
                        agency_email='".$_POST['agency_email']."',
                        agency_website='".$_POST['agency_website']."',
                        agency_address='".$_POST['agency_address']."',
                        agency_city='".$_POST['agency_city']."',
                        agency_province='".$_POST['agency_province']."',
                        agency_zip='".$_POST['agency_zip']."',
                        agency_country='".$_POST['agency_country']."',
                        location_supp1='".$location_supp1."',
                        location_supp2='".$location_supp2."',
                        location_supp3='".$location_supp3."',
                        location_supp4='".$location_supp4."',
                        location_supp5='".$location_supp5."',
                        location_supp_other='".$_POST['location_supp_other']."',
                        postdate=NOW(),
                        postip='".$_SERVER['REMOTE_ADDR']."',
                        status='".$_POST['status']."'
                        WHERE agentid= $id
                     ";
        mysqli_query($con, $query);
    
        if(sizeof($_POST['industry_company'])){
            foreach( $_POST['industry_company'] as $key=>$value){
                if( !empty($_POST['industry_company'][$key])){
                        $queryref="UPDATE tbl_agent_industry_reference SET
                                            industry_company='".$_POST['industry_company'][$key]."',
                                            industry_name='".$_POST['industry_name'][$key]."',
                                            industry_designation='".$_POST['industry_designation'][$key]."',
                                            industry_email='".$_POST['industry_email'][$key]."',
                                            industry_phone='".$_POST['industry_phone'][$key]."'
                                            WHERE refid=$key
                                            ";
                     mysqli_query($con, $queryref);
                }
            }
        } 
  
}

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_agent_details WHERE agentid= $id "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Agent Details - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Agent Details</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="agents.php">Registered Agents</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Travel Advisior Name*</label>
										<input class="form-control form-control01" name="agent_name" value="<?php if(isset($row['agent_name'])){echo $row['agent_name'];}?>"  required="#">
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Travel Advisior Cell Number*</label>
										<input class="form-control form-control01" type="tel"  name="agent_phone" value="<?php if(isset($row['agent_phone'])){echo $row['agent_phone'];}?>"   required="#">
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Travel Advisior Email*</label>
										<input class="form-control form-control01" type="email" name="agent_email" value="<?php if(isset($row['agent_email'])){echo $row['agent_email'];}?>"   required="#">
									</div>
								</div>
                                <p class="py-2 mb-0"></p>
							<p class="head09">AGENCY INFORMATION</p>
							<hr class="redline-full">
							<p class="py-2 mb-0"></p>
                            <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Agency Name*</label>
										<input class="form-control form-control01" name="agency_name"  value="<?php if(isset($row['agency_name'])){echo $row['agency_name'];}?>"   required="#">
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Agency Phone*</label>
										<input class="form-control form-control01" name="agency_phone"  value="<?php if(isset($row['agency_phone'])){echo $row['agency_phone'];}?>"   >
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Agency Email*</label>
										<input class="form-control form-control01"  name="agency_email" value="<?php if(isset($row['agency_email'])){echo $row['agency_email'];}?>"   >
									</div>
								</div>

                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Agency Website*</label>
										<input class="form-control form-control01" name="agency_website" value="<?php if(isset($row['agency_website'])){echo $row['agency_website'];}?>"  >
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Agency Address*</label>
										<input class="form-control form-control01" name="agency_address" value="<?php if(isset($row['agency_address'])){echo $row['agency_address'];}?>"    required="#">
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">City*</label>
										<input class="form-control form-control01" name="agency_city"  value="<?php if(isset($row['agency_city'])){echo $row['agency_city'];}?>"   required="#">
									</div>
								</div>

                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">State/Province*</label>
										<input class="form-control form-control01" name="agency_province" value="<?php if(isset($row['agency_province'])){echo $row['agency_province'];}?>"    required="#">
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Zip/Postal Code*</label>
										<input class="form-control form-control01" name="agency_zip" value="<?php if(isset($row['agency_zip'])){echo $row['agency_zip'];}?>"    required="#">
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="user_login_label">Country*</label>
										<input class="form-control form-control01" name="agency_country" value="<?php if(isset($row['agency_country'])){echo $row['agency_country'];}?>"    required="#">
									</div>
								</div>
                                <p class="head09">SUPPLEMENTARY INFORMATION</p>
						<hr class="redline-full">
						<p class="py-3 mb-0"></p>
                        <div class="col-lg-3">
										<div class="form-group">
											<div class="checkbox small">
												<input class="check-a" name="location_supp1" id="location_supp1" type="checkbox" value="Asia" <?php if(isset($row['location_supp1']) && $row['location_supp1']=="Asia"){echo 'checked';}?> >
												<label class="label-b" for="pt1">Asia</label>
                                                
											</div>
										</div>
									</div>
                            <div class="col-lg-3">
										<div class="form-group">
											<div class="checkbox small">
												<input class="check-a" name="location_supp2" id="location_supp2" type="checkbox" value="Domestic US" <?php if(isset($row['location_supp2']) && $row['location_supp2']=="Domestic US"){echo 'checked';}?>>
												<label class="label-b" for="pt1">Domestic US</label>
											</div>
										</div>
									</div>
                            <div class="col-lg-3">
										<div class="form-group">
											<div class="checkbox small">
												<input class="check-a" name="location_supp3"  id="location_supp3" type="checkbox" value="India Subcontinent" <?php if(isset($row['location_supp3']) && $row['location_supp3']=="India Subcontinent"){echo 'checked';}?>>
												<label class="label-b" for="pt1">India Subcontinent</label>
											</div>
										</div>
									</div>
                            <div class="col-lg-3">
										<div class="form-group">
											<div class="checkbox small">
												<input class="check-a" name="location_supp4" id="location_supp4" type="checkbox" value="Middle East" <?php if(isset($row['location_supp4']) && $row['location_supp4']=="Middle East"){echo 'checked';}?>>
												<label class="label-b" for="pt1">Middle East</label>
                                                
											</div>
										</div>
									</div>

                                    <div class="col-lg-1">
										<div class="form-group">
											<div class="checkbox small">
												<input class="check-a" name="location_supp5"  id="location_supp5" type="checkbox" value="Other" <?php if(isset($row['location_supp5']) && $row['location_supp5']=="Other"){echo 'checked';}?>>
												<label class="label-b" for="pt1">Other</label>
                                              
											</div>
										</div>
									</div>
                            <div class="col-lg-4">
                                                <input class="form-control form-control01" placeholder="Please Specify" name="location_supp_other" value="<?php if(isset($row['location_supp_other'])){echo $row['location_supp_other'];}?>" >
									</div>
                                    
                                    

                                    <?php
                        $query=mysqli_query($con, "SELECT * FROM tbl_agent_industry_reference WHERE agentid=$id");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                while( $rowind=mysqli_fetch_assoc($query) ){ ?>
                        <p class="head09">INDUSTRY REFERENCE <?php echo ++$i;?></p>
						<hr class="redline-full">
						<p class="py-3 mb-0"></p>


                        <div class="col-lg-4">
									<div class="form-group">
										<label class="user_login_label">Company Name*</label>
										<input class="form-control form-control01" name="industry_company[<?php echo $rowind['refid'];?>]" value="<?php if(isset($rowind['industry_company'])){echo $rowind['industry_company'];}?>" >
									</div>
								</div>
                                <div class="col-lg-4">
									<div class="form-group">
										<label class="user_login_label">Name (First & Last)*</label>
										<input class="form-control form-control01" name="industry_name[<?php echo $rowind['refid'];?>]" value="<?php if(isset($rowind['industry_name'])){echo $rowind['industry_name'];}?>" >
									</div>
								</div>
                                <div class="col-lg-4">
									<div class="form-group">
										<label class="user_login_label">Designation*</label>
										<input class="form-control form-control01" name="industry_designation[<?php echo $rowind['refid'];?>]" value="<?php if(isset($rowind['industry_designation'])){echo $rowind['industry_designation'];}?>" >
									</div>
								</div>


                                <div class="col-lg-4">
									<div class="form-group">
										<label class="user_login_label">Email*</label>
										<input class="form-control form-control01" type="email" name="industry_email[<?php echo $rowind['refid'];?>]" value="<?php if(isset($rowind['industry_email'])){echo $rowind['industry_email'];}?>"  >
									</div>
								</div>
                                <div class="col-lg-4">
									<div class="form-group">
										<label class="user_login_label">Phone Number*</label>
										<input class="form-control form-control01" type="tel" name="industry_phone[<?php echo $rowind['refid'];?>]" value="<?php if(isset($rowind['industry_phone'])){echo $rowind['industry_phone'];}?>" >
									</div>
								</div>

                                <?php }
                            }
                ?>
                    
                    
                                      
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="status">Status*</label>
                            <select id="status" name="status"class="form-control form-control01" >
                                               <option value="Pending" <?php if($row['status']=="Pending"){echo 'selected';}?> >Pending</option>
                                                <option value="Active" <?php if($row['status']=="Active"){echo 'selected';}?>>Active</option>
                                             <option value="Inactive" <?php if($row['status']=="Inactive"){echo 'selected';}?> >Inactive</option>
                                        </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script>
    $( function() {
        $( "#travel_date" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
    } );
    </script>

    <script>
        $(document).ready(function(){            
            $('.remove-optional').on('click', function(){
                
                var confirmalert = confirm("Are you sure want to remove?");
                if (confirmalert == true) {
                    var text_id = $(this).attr('id');
                    var text_id = text_id.split('-');

                    var opt_cost = $(this).attr('title');
                    var display_price = $('#total_trip_cost').val();

                    var input_id = text_id[0]+'-'+text_id[1];
                    
                    var total_passenger = $('#total_passenger').val();
                    
                    var opt_div = $(this).parent().parent().parent().parent().attr('id');
                    $(this).parent().remove();
                    $('#'+input_id).remove();

                    if($('#'+opt_div+' .opt_num').length==0){
                        $('#'+opt_div).remove();
                    }

                    var opt_num = $('.opt_num').length;
                    if(opt_num==0){
                        //$('.opt_cnt').text('None of optional service left');
                        $('#optional_box').remove();
                    }
                    
                    if(opt_cost!=''){
                        if(total_passenger!='' || total_passenger!=0){
                            var opt_price = parseInt(total_passenger) * parseInt(opt_cost);
                            var total_price = parseInt(display_price) - parseInt(opt_price);
                        }

                        $('#total_trip_cost').val(total_price);
                    }
                }
            });
        });
    </script>

    <script>
        $(document).ready(function(){            
            $('.remove-extension').on('click', function(){
                
                var confirmalert = confirm("Are you sure want to remove?");
                if (confirmalert == true) {
                    var text_id = $(this).attr('id');
                    var text_id = text_id.split('-');
                    
                    var extension_cost = $(this).attr('title');
                    var display_price = $('#total_trip_cost').val();

                    $('#'+text_id[0]).remove();

                    $('#'+text_id[0]+'_trip_id').val(0);
                    $('#'+text_id[0]+'_trip_cat_id').val(0);
                    
                    if(extension_cost!=''){
                        
                        var total_price = parseInt(display_price) - parseInt(extension_cost);

                        $('#total_trip_cost').val(total_price);

                    }
                }
            });
        });
    </script>
</body>

</html>