<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('admin-roles', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $id = $_GET['id'];

    if( !empty($_POST['role_name']) ){
        
        $access_rights = implode(",",$_POST["access_rights"]);
        
        $qry = "UPDATE tbl_roles SET 
        role_name = '".mysqli_real_escape_string($con, $_POST['role_name'])."',
        access_rights='".$access_rights."',
        status = '".$_POST['status']."'
        WHERE role_id = $id ";
        mysqli_query($con, $qry);
        
        header("location: admin-roles.php");
    }

    $r_role = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_roles WHERE role_id=$id "));
    $access = explode(',',$r_role['access_rights']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Role - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Role</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="admin-roles.php">Admin Role</a></li>
                            </ol>
                           
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="role_add" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                            <label class="form-floating mb-3" for="role_name">Role Name*</label>
                            <input type="text" name="role_name" id="role_name" class="form-control form-control01" value="<?php echo $r_role['role_name']; ?>" placeholder="Role Name*" required>
                    </div>
                  </div>
                    <div class="col-md-12">
                    <div class="form-group">
                            <div class="subject-info-box-1">
                              <label class="form-floating mb-3">Function Name: </label>
                              <select name="action_name[]" class="form-control form-control01" id='lstBox1' multiple>
                              <?php 
                                  $qfunction = mysqli_query($con, "SELECT * FROM tbl_role_actions WHERE status = 'Active' ");
                                  while($rfunction = mysqli_fetch_assoc($qfunction)){
                                    if(!in_array($rfunction['action_value'], $access)){
                                ?>
                                <option value="<?php echo $rfunction['action_value']; ?>"><?php echo $rfunction['action_display_name']; ?></option>
                                <?php }
                                  } 
                                ?>
                              </select>
                            </div>
                            <div class="subject-info-arrows text-center">
                              <input type='button' id='btnRight' value='>' class="btn btn-default" /><br />
                              <input type='button' id='btnLeft' value='<' class="btn btn-default" /><br />
                            </div>
                            <div class="subject-info-box-2">
                              <label class="form-floating mb-3">Select Access Rights: </label>
                              <select name="access_rights[]" class="form-control form-control01" id='lstBox2' multiple>
                                <?php 
                                $qfunction = mysqli_query($con, "SELECT * FROM tbl_role_actions WHERE status = 'Active' ");
                                while($rfunction = mysqli_fetch_assoc($qfunction)){
                                    if(in_array($rfunction['action_value'], $access)){
                                ?>
                                <option value="<?php echo $rfunction['action_value']; ?>"><?php echo $rfunction['action_display_name']; ?></option>
                                <?php }
                                  } 
                                ?>
                              </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                    <div class="form-group">
                            <label class="form-floating mb-3" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-control01">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                     <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script>
    $(document).ready(function(){
		$("#btnRight").click(function (e) {
            var selectedOpts = $("#lstBox1 option:selected");
            if (selectedOpts.length == 0) {
              alert("Nothing to move.");
              e.preventDefault();
            }
            $("#lstBox2").append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
        $("#btnLeft").click(function (e) {
            var selectedOpts = $("#lstBox2 option:selected");
            if (selectedOpts.length == 0) {
              alert("Nothing to move.");
              e.preventDefault();
            }
            $("#lstBox1").append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
        
        $('.btn.btn03').click(function(){
            $("select#lstBox1 option").prop("selected", "selected");
            $("select#lstBox2 option").prop("selected", "selected");
        });
	});      
    </script>
    
</body>

</html>