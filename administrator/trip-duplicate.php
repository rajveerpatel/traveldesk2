<?php include('include.inc.php');

//FETCH A SELECTED RECORD FOR DUPLICATION
$trip_id=$_GET['id'];
$trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id=$trip_id"));
$trip_name = $trip['trip_name']." Duplicate";
$trip_sku = $trip['trip_sku'];
$trip_thumb = $trip['trip_thumb'];
$slug = $trip['slug'];
$validity_start_date = date('Y-m-d', strtotime($trip['validity_start_date']));
$validity_end_date = date('Y-m-d', strtotime($trip['validity_end_date']));
$trip_days = $trip['trip_days'];
$area = $trip['area'];
$sub_area = $trip['sub_area'];
$parent_dest = $trip['parent_dest'];
$country_dest = $trip['country_dest'];
$trip_themes = $trip['trip_themes'];
$trip_category = $trip['trip_category'];
$trip_type = $trip['trip_type'];
$short_highlights = $trip['short_highlights'];
$trip_inclusions = $trip['trip_inclusions'];
$trip_highlights = $trip['trip_highlights'];
$related_trip = $trip['related_trip'];
$trip_map = $trip['trip_map'];
$pre_trip_hotel = $trip['pre_trip_hotel'];
$post_trip_hotel = $trip['post_trip_hotel'];
$banner_id = $trip['banner_id'];
$deal = $trip['deal'];
$original_strike_twin_sharing = $trip['original_strike_twin_sharing'];


//INSERT A DUPLICATE RECORD
$qry = "INSERT INTO tbl_trip_packages set
trip_name='".mysqli_real_escape_string($con, $trip_name)."',
trip_sku = '$trip_sku',
trip_thumb = '".mysqli_real_escape_string($con, $trip_thumb)."',
slug = '".mysqli_real_escape_string($con, $slug)."',
validity_start_date = '".mysqli_real_escape_string($con, $validity_start_date)."',
validity_end_date = '".mysqli_real_escape_string($con, $validity_end_date)."',
trip_days = '".mysqli_real_escape_string($con, $trip_days)."',
area = '".mysqli_real_escape_string($con, $area)."',
sub_area = '".mysqli_real_escape_string($con, $sub_area)."',
parent_dest = '".mysqli_real_escape_string($con, $parent_dest)."',
country_dest = '".mysqli_real_escape_string($con, $country_dest)."',
trip_themes = '".mysqli_real_escape_string($con, $trip_themes)."',
trip_category = '".mysqli_real_escape_string($con, $trip_category)."',
trip_type = '".mysqli_real_escape_string($con, $trip_type)."',
short_highlights = '".mysqli_real_escape_string($con, $short_highlights)."',
trip_inclusions = '".mysqli_real_escape_string($con, $trip_inclusions)."',
trip_highlights = '".mysqli_real_escape_string($con, $trip_highlights)."',
related_trip = '".mysqli_real_escape_string($con, $related_trip)."',
trip_map = '".mysqli_real_escape_string($con, $trip_map)."',
pre_trip_hotel = '".mysqli_real_escape_string($con, $pre_trip_hotel)."',
post_trip_hotel = '".mysqli_real_escape_string($con, $post_trip_hotel)."',
banner_id = '".mysqli_real_escape_string($con, $banner_id)."',
deal = '".mysqli_real_escape_string($con, $deal)."',
original_strike_twin_sharing = '".mysqli_real_escape_string($con, $original_strike_twin_sharing)."',
status='Inactive',
postIP='".$_SERVER['REMOTE_ADDR']."',
postDate=NOW()";
mysqli_query($con, $qry) or die(mysqli_connect_error());

$tripid = mysqli_insert_id($con);


// get itinerary
$qitenary=mysqli_query($con, "select * from tbl_trip_itineraries where trip_id = $trip_id AND cat_id = $trip_category ");
$iti_num = mysqli_num_rows($qitenary);
echo $iti_num;
if($iti_num > 0){
    $i=1;
    while( $ritenary=mysqli_fetch_assoc($qitenary)){
        
        $day[] = $ritenary['day'];
        $hotel[] = $ritenary['hotel'];
        $city_id[] = $ritenary['city_id'];
        
        // insert itinerary
        echo $itiqry = "INSERT INTO tbl_trip_itineraries SET
        trip_id=$tripid,
        cat_id=$trip_category, 
        day='".$ritenary['day']."',
        hotel='".$ritenary['hotel']."',
        meals='".$ritenary['meals']."',
        city_id='".$ritenary['city_id']."' ";
        mysqli_query($con, $itiqry) or die(mysqli_connect_error());
        $iti_id = mysqli_insert_id($con);
        
        // get itinerary day plan
        $dayplan=mysqli_query($con, "select * from tbl_trip_itinerary_day_plan where trip_id = $trip_id AND iti_id = '".$ritenary['itid']."' ");
        $plan_num = mysqli_num_rows($dayplan);
        if($plan_num > 0){
            $d=1;
            while($plan=mysqli_fetch_assoc($dayplan)){
                $plan_name = $plan['plan_name'];
                $flight_train_no = $plan['flight_train_no'];
                $departure_time = $plan['departure_time'];
                $arrival_time = $plan['arrival_time'];
                $departure_city_code = $plan['departure_city_code'];
                $arrival_city_code = $plan['arrival_city_code'];
                $departure_city = $plan['departure_city'];
                $arrival_city = $plan['arrival_city'];
                $departure_date = date('Y-m-d', strtotime($plan['departure_date']));
                $arrival_date = date('Y-m-d', strtotime($plan['arrival_date']));
                $duration = $plan['duration'];
                $transfer_details = $plan['transfer_details'];
                $transport_details = $plan['transport_details'];
                $distance = $plan['distance'];
                $sightseeing = $plan['sightseeing'];
                
                echo $plan = "INSERT INTO tbl_trip_itinerary_day_plan SET
                trip_id=$tripid,
                iti_id=$iti_id,
                plan_name='".mysqli_real_escape_string($con, $plan_name)."',
                flight_train_no='".mysqli_real_escape_string($con, $flight_train_no)."',
                departure_time='".mysqli_real_escape_string($con, $departure_time)."',
                arrival_time='".mysqli_real_escape_string($con, $arrival_time)."',
                departure_city_code='".mysqli_real_escape_string($con, $departure_city_code)."',
                arrival_city_code='".mysqli_real_escape_string($con, $arrival_city_code)."',
                departure_city='".mysqli_real_escape_string($con, $departure_city)."',
                arrival_city='".mysqli_real_escape_string($con, $arrival_city)."',
                departure_date='".mysqli_real_escape_string($con, $departure_date)."',
                arrival_date='".mysqli_real_escape_string($con, $arrival_date)."',
                duration='".mysqli_real_escape_string($con, $duration)."',
                transfer_details='".mysqli_real_escape_string($con, $transfer_details)."',
                transport_details='".mysqli_real_escape_string($con, $transport_details)."',
                distance='".mysqli_real_escape_string($con, $distance)."',
                sightseeing='".mysqli_real_escape_string($con, $sightseeing)."' ";
                mysqli_query($con, $plan) or die(mysqli_connect_error());
                
                $d++;
            }
            /*
            for($j=0; $j<$plan_num; $j++){
                echo ' Plan '.$plan_name[$j];
                echo ' Flight '.$flight_train_no[$j];
                echo ' Dep time '.$departure_time[$j];
                echo ' Arr. Time '.$arrival_time[$j];
                echo ' Dep city code '.$departure_city_code[$j];
                echo ' Arr city code '.$arrival_city_code[$j];
                echo ' Dep city '.$departure_city[$j];
                echo ' Arr city '.$arrival_city[$j];
                echo ' Dep date '.$departure_date[$j];
                echo ' Arr date '.$arrival_date[$j];
                echo ' Duration '.$duration[$j];
                echo ' Tranfer '.$transfer_details[$j];
                echo ' Transport '.$transport_details[$j];
                echo ' Distance '.$distance[$j];
                echo ' Sightseeing '.$sightseeing_views[$j];
                echo ' Meal '.$meal_name[$j];
                echo '<br>';
            }*/
        }
        echo '<br><br>';
        $i++;
    }
}

// get available date
$avail_date=mysqli_query($con, "select * from tbl_trip_prices where trip_id = $trip_id AND trip_categories = $trip_category ");
$avail_num = mysqli_num_rows($avail_date);
if($avail_num > 0){
    echo '<h3>Available Dates</h3><br>';
    $i=1;
    while( $date=mysqli_fetch_assoc($avail_date)){
        $trip_start_date[] = $date['trip_start_date'];
        $trip_end_date[] = $date['trip_end_date'];
        $trip_styles[] = $date['trip_styles'];
        
        // insert available date
        echo $a = "INSERT INTO tbl_trip_prices SET
        trip_id=$tripid,
        trip_categories=$trip_category,
        trip_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($date['trip_start_date'])))."',
        trip_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($date['trip_end_date'])))."',
        land_single_room='".mysqli_real_escape_string($con, $date['land_single_room'])."',
        land_twin_sharing='".mysqli_real_escape_string($con, $date['land_twin_sharing'])."',
        land_triple_sharing='".mysqli_real_escape_string($con, $date['land_triple_sharing'])."',
        airland_single_room='".mysqli_real_escape_string($con, $date['airland_single_room'])."',
        airland_twin_sharing='".mysqli_real_escape_string($con, $date['airland_twin_sharing'])."',
        airland_triple_sharing='".mysqli_real_escape_string($con, $date['airland_triple_sharing'])."',
        trip_styles='".mysqli_real_escape_string($con, $date['trip_styles'])."' ";
        mysqli_query($con, $a) or die(mysqli_connect_error());
    }
}

// get blackout date
$bl_date=mysqli_query($con, "select * from tbl_trip_blackout_dates where trip_id = $trip_id AND cat_id = $trip_category ");
$bl_num = mysqli_num_rows($bl_date);
if($bl_num > 0){
    echo '<h3>Blackout Dates</h3><br>';
    $i=1;
    while( $black=mysqli_fetch_assoc($bl_date)){
        $blackout_start_date[] = $black['blackout_start_date'];
        $blackout_end_date[] = $black['blackout_end_date'];
        
        //insert blackout date
        echo $bl = "INSERT INTO tbl_trip_blackout_dates SET
        trip_id=$tripid,
        cat_id=$trip_category,
        blackout_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($black['blackout_start_date'])))."',
        blackout_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($black['blackout_end_date'])))."' ";
        mysqli_query($con, $bl) or die(mysqli_connect_error());
    }
}

// get purchase date
$purchase_date=mysqli_query($con, "select * from tbl_trip_purchase_dates where trip_id = $trip_id AND cat_id = $trip_category ");
$purchase_num = mysqli_num_rows($purchase_date);
if($purchase_num > 0){
    echo '<h3>Purchase Dates</h3><br>';
    $i=1;
    while( $purchase=mysqli_fetch_assoc($purchase_date)){
        $purchase_start_date[] = $purchase['purchase_start_date'];
        $purchase_end_date[] = $purchase['purchase_end_date'];
        
        //insert purchase date
        echo $pd = "INSERT INTO tbl_trip_purchase_dates SET
        trip_id=$tripid,
        cat_id=$trip_category,
        purchase_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($purchase['purchase_start_date'])))."',
        purchase_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($purchase['purchase_end_date'])))."' ";
        mysqli_query($con, $pd) or die(mysqli_connect_error());
    }
}

// get season dates and room cost
/*$season_date=mysqli_query($con, "select * from tbl_trip_season_price where trip_id = $trip_id AND cat_id = $trip_category ");
$season_num = mysqli_num_rows($season_date);
if($season_num > 0){
    echo '<h3>Season Dates & Prices</h3><br>';
    $i=1;
    while( $season=mysqli_fetch_assoc($season_date)){
        $season_start_date[] = $season['season_start_date'];
        $season_end_date[] = $season['season_end_date'];
        $single_room[] = $season['single_room'];
        $twin_sharing[] = $season['twin_sharing'];
        $triple_sharing[] = $season['triple_sharing'];
        
        // insert season date with room cost
        echo $qry = "INSERT INTO tbl_trip_season_price SET
        trip_id=$tripid,
        cat_id=$trip_category,
        season_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($season['season_start_date'])))."',
        season_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($season['season_end_date'])))."',
        single_room='".mysqli_real_escape_string($con, $season['single_room'])."',
        twin_sharing='".mysqli_real_escape_string($con, $season['twin_sharing'])."',
        triple_sharing='".mysqli_real_escape_string($con, $season['triple_sharing'])."' ";
        mysqli_query($con, $qry) or die(mysqli_connect_error());
    }
}*/

header('location:trip-package-edit.php?id='.$tripid);
?>