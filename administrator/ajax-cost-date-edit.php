<?php
include('includes/config.inc.php');

$action = '';
$db_type = '';
$trip_cost = '';
$cost_start_date = '';
$cost_id = '';
$trip_id = '';
$cat_id = '';

//Manage Parent Destination for Menu
if( !empty($_POST['action']) ){
    $action = $_POST['action'];
    $db_type = $_POST['db_type'];
    $trip_cost = $_POST['trip_cost'];
    $cost_start_date = date('Y-m-d', strtotime($_POST['cost_date']));
    $cat_id = $_POST['trip_category'];
    
    $trip_id = $_POST['trip_id'];
    $cost_id = $_POST['cost_id'];
    
    //add cost in temporary table
    if($action == 'insert' && $db_type=='permanent' && !empty($trip_cost)){
        $query = "INSERT INTO tbl_trip_cost_datewise SET
        trip_id = $trip_id,
        trip_cost = $trip_cost,
        cost_date = '$cost_start_date',
        trip_cat_id = '$cat_id' ";
        mysqli_query($con, $query);
        
        echo mysqli_insert_id($con);
    }
    
    //delete from temporary table
    if($action == 'delete' && $db_type=='permanent' && $cost_id>0){
        mysqli_query($con, "DELETE from tbl_trip_cost_datewise WHERE cost_id = $cost_id ");
    }
}
?>