<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    $msg = '';

    if( !empty($_POST['CityName']) && !empty($_POST['CityName']) ){
        
        mysqli_query($con, "INSERT INTO tbl_cities SET 
        CityName = '".$_POST['CityName']."',
        CountryId = '".$_POST['CountryId']."',
        CityStatus = '1' ");
        
        $msg = '<p class="success text-center">City successfully inserted</p>';
        //header("location: hotels.php");
    } else {
        $msg = '<p class="error text-center">Fields should not be empty</p>';
    }
?>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-uppercase text-center">Add City</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <?php echo $msg; ?>     

        <div class="input-form">
            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="CityName">City Name*</label>
                            <input type="text" name="CityName" id="CityName" class="form-control form-control01" placeholder="City Name*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="CountryId">Select Country*</label>
                            <select name="CountryId" id="CountryId" class="form-control form-control01" required>
                                <option value="">Select Country*</option>
                                <?php
                                $cntry_qry = mysqli_query($con, "SELECT * FROM tbl_countries WHERE CountryStatus='1' ORDER BY CountryName ");
                                while($cntry_res = mysqli_fetch_assoc($cntry_qry)){
                                ?>
                                <option value="<?php echo $cntry_res['CountryId']; ?>"><?php echo $cntry_res['CountryName']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <button name="submit" class="btn btn03 btn-width-03">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<?php 
    include('elements/footer.php');
?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>