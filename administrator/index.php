<?php

session_start();



include('includes/config.inc.php');



$error = '';



if( isset($_POST['login']) ){

    //die();

    if( !empty($_POST['username']) && !empty($_POST['password']) ){

        

        $sql = mysqli_query( $con, "SELECT * FROM tbl_admin WHERE username = '".$_POST['username']."' AND password = '".$_POST['password']."' AND status='Active' " );

        $numrow = mysqli_num_rows($sql);

        

        if( $numrow > 0 ){

            $res = mysqli_fetch_assoc($sql);

            

            $_SESSION['LoGgEdInSuperAdminPerson'] = 'LoGgEdInSuperAdminPerson';

            $_SESSION['AdminID'] = $res['adminid'];

            $_SESSION['AdminUser'] = $res['username'];

            $_SESSION['AdminName'] = $res['fullname'];

            $_SESSION['AdminEmail'] = $res['email'];

            $_SESSION['AdminType'] = $res['usertype'];

            $_SESSION['AdminRole'] = $res['role_id'];

            $_SESSION['DestinationHandle'] = $res['handle_destination_enquiry'];

            

            mysqli_query($con, "UPDATE tbl_admin SET lastlogin = NOW(), lastloginip = '".$_SERVER['REMOTE_ADDR']."' WHERE adminid = '".$res['adminid']."' ");



            header('location: dashboard.php');

        } else {

            $error='<p class="error text-center">Incorrect username or password.</p>';

        }

    }else{

        $error='<p class="error text-center" style="color:#ff0000; text-align:center;">Username and Password should not be blank.</p>';

    }

}

?>

<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <!-- Favicon icon -->

    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">

    <title>Tavpdek Admin Login</title>

    

    <!-- page css -->

    <link href="dist/css/pages/login-register-lock.css" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="dist/css/style.css" rel="stylesheet">

    

    

    

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<![endif]-->

</head>



<body class="skin-default card-no-border">

    <!-- ============================================================== -->

    <!-- Preloader - style you can find in spinners.css -->

    <!-- ============================================================== -->

    <!-- <div class="preloader">

        <div class="loader">

            <div class="loader__figure"></div>

            <p class="loader__label">Elite admin</p>

        </div>

    </div> -->

    <!-- ============================================================== -->

    <!-- Main wrapper - style you can find in pages.scss -->

    <!-- ============================================================== -->

    <section id="wrapper">

        <div class="login-register" >

            <div class="login-box card">

                <div class="card-body">

                    

                    <form name="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        
                        <div class="text-center pb-4 pt-4"><img src="assets/images/logo.png" alt="TravDEk"></div>

                        <h3 class="text-center m-b-20"><span>Trav<span>Dek</span></span> Sign In</h3>

                        <div class="form-group ">

                            <div class="col-xs-12">

                                <input class="form-control" type="text" name="username" required="" placeholder="Username"> </div>

                        </div>

                        <div class="form-group">

                            <div class="col-xs-12">

                                <input class="form-control" type="password" name="password" required="" placeholder="Password"> </div>

                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">

                                <div class="d-flex no-block align-items-center">

                                    <div class="form-check">

                                        <input type="checkbox" class="form-check-input" id="customCheck1">

                                        <label class="form-check-label" for="customCheck1">Remember me</label>

                                    </div> 

                                    <div class="ms-auto">

                                        <a href="forgot-password.php" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Forgot pwd?</a> 

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="form-group text-center">

                            <div class="col-xs-12 p-b-20">

                                <button class="btn w-100 btn-lg btn-info btn-rounded text-white" name="login">Log In</button>

                            </div>

                        </div>

                        

                        

                    </form>

                    

                </div>

            </div>

        </div>

    </section>

    

    <!-- ============================================================== -->

    <!-- End Wrapper -->

    <!-- ============================================================== -->

    <!-- ============================================================== -->

    <!-- All Jquery -->

    <!-- ============================================================== -->

    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <!--Custom JavaScript -->

    <script type="text/javascript">

        $(function() {

            $(".preloader").fadeOut();

        });

        $(function() {

            $('[data-bs-toggle="tooltip"]').tooltip()

        });

        // ============================================================== 

        // Login and Recover Password 

        // ============================================================== 

        $('#to-recover').on("click", function() {

            $("#loginform").slideUp();

            $("#recoverform").fadeIn();

        });

    </script>

    

</body>



</html>