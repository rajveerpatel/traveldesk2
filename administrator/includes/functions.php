<?php
//image resize just for displaying

function imageResize($width, $height, $target) {

	//takes the larger size of the width and height and applies the formula accordingly...this is so this script will work dynamically with any size image
	
	if ($width > $height) {
	$percentage = ($target / $width);
	} else {
	$percentage = ($target / $height);
	}
	
	//gets the new value and applies the percentage, then rounds the value
	$width = round($width * $percentage);
	$height = round($height * $percentage);
	
	//returns the new sizes in html image tag format...this is so you can plug this function inside an image tag and just get the
	
	return "width=\"$width\" height=\"$height\"";
}


// Functions do not need to be inline with the rest of the code
  function getNonExistingFilename($uploadFilesTo, $name)
  {
      if (!file_exists($uploadFilesTo . '/' . $name))
          return $name;
     
      return getNonExistingFilename($uploadFilesTo, rand(100, 200) . '_' . $name);
  }
  
###################### FUNCTION TO UPLOAD FILES #########
function uploadfiles($filename, $tmp_name, $error, $size, $uploadFilesTo){
		// Assign the name to a variable
		 $thumbnail1 = $filename;
		// Assign the tmp_name to a variable
		$tmp_name = $tmp_name;
		// Assign the error to a variable
		$error = $error;
		// Assign the size to a variable
		$size = $size;
		// No trailing slash
		$uploadFilesTo = $uploadFilesTo;
		// Create safe filename
	   $thumbnail1 = preg_replace('/[^A-Za-z0-9.]/', '-', $thumbnail1);
		// Disallowed file extensions
		//what files you don't want upoad... leave this alone and you should be fine but you could add more
		$naughtyFileExtension = array("php", "php3", "asp", "inc", "wma","js", "exe", "jsp", "map", "obj", " ", "", "html", "mp3", "mpu", "wav", "cur", "ani","zip");    // Returns an array that includes the extension
		 $fileInfo = pathinfo($thumbnail1);
		// Check extension
		if (!in_array($fileInfo['extension'], $naughtyFileExtension))
		{
		  // Get filename
		  $thumbnail1 = getNonExistingFilename($uploadFilesTo, $thumbnail1);
		  // Upload the file
		  if (move_uploaded_file($tmp_name, $uploadFilesTo.$thumbnail1))
		  {
			  // Show success message
			  $msg = '<center><p>Your file uploaded successfully<br />'.$uploadFilesTo.$thumbnail1.'</p></center>';
			  return $thumbnail1;			 
		  }
		  else
		  {
			  // Show failure message
			  $msg = '<center><p>File failed to upload to '.$uploadFilesTo.$thumbnail1.'</p></center>';
			  //return $msg;			 
		  }
		}
		else
		{
			// Bad File type
			$msg='The file uses an extension we don\'t allow.';
			//return $msg;
		}
		 		
}  
  
//General query function
	function  tep_db_query($con, $sql)
	{
		$query=mysqli_query($con, $sql) or die(mysqli_error($con));
		return $query;
	}

//General fetch array function
	function tep_db_fetch_array($fetcharray)
	{
		$array = mysqli_fetch_array($fetcharray);
		return $array;
	}

//Fetch data from db
	function tep_db_fetch_all_data($con, $table,$where){
		$sql="select * from ".$table." where ".$where;
		$row=mysqli_fetch_assoc(mysqli_query($con,$sql));
		return $row;
	}

// function to check username exist in the table
	function check_username_exist($table_name, $columname, $username)
	{
		$checkusername=tep_db_query("select count(*) as cnt from ".$table_name." where ".$columname."='".$username."'");
		$usernamecounter=tep_db_fetch_array($checkusername);
		return $usernamecounter;
	}

//function to insert into database
	function tep_db_insert($con, $table_name, $sqlquery)
	{
		$sql="insert into ".$table_name." set ".$sqlquery." ";
		tep_db_query($con, $sql);
	}
	
//function to update data
	function tep_db_update($con, $table, $data, $parameters){  
	  $query = 'update ' . $table . ' set ';
		  while (list($columns, $value) = each($data)) {
			switch ((string)$value) {
			  case 'now()':
				$query .= $columns . ' = now(), ';
				break;
			  case 'null':
				$query .= $columns .= ' = null, ';
				break;
			  default:
				$query .= $columns . ' = \'' . $value . '\', ';
				break;
			}
		  }
		  $query = substr($query, 0, -2) . ' where ' . $parameters;
		 return tep_db_query($con, $query);
	  }

//function to delete from database
	function tep_db_delete($table_name, $parameter)
	{
		$sql="delete from ".$table_name." where ".$parameter;
		tep_db_query($sql);
	}
		
//sending HTML mail 
	function send_html_mail($to, $subject, $message,  $from_name, $from_email){	
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";		
		// Additional headers		
		$headers .= 'From: '.$from_name.'<'.$from_email.'>' . "\r\n";
		//mail it
		mail($to, $subject, $message, $headers);
	}	
	
//insert/update but need same field name
function tep_db_perform($table, $data, $action = 'insert', $parameters = '') {
    reset($data);
    if ($action == 'insert') {
      $query = 'insert into ' . $table . ' (';
      while (list($columns, ) = each($data)) {
        $query .= $columns . ', ';
      }
      $query = substr($query, 0, -2) . ') values (';
      reset($data);
      while (list(, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= 'now(), ';
            break;
          case 'null':
            $query .= 'null, ';
            break;
          default:
            $query .= '\'' . tep_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
      $query = 'update ' . $table . ' set ';
      while (list($columns, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= $columns . ' = now(), ';
            break;
          case 'null':
            $query .= $columns .= ' = null, ';
            break;
          default:
            $query .= $columns . ' = \'' . $_POST[$value] . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ' where ' . $parameters;
    }

    return tep_db_query($query);
  }
  
//login function  
	function check_login($table_name, $username, $password, $postusername, $postpassword)
	{
		$checklogin=tep_db_query("select count(*) as cnt from ".$table_name." where ".$username."='".$postusername."' and ".$password."='".$postpassword."'");
		$logincounter=tep_db_fetch_array($checklogin);
		return $logincounter;
	}

//SQL query to get all details of the table for one restaurant or customer
	function tep_db_fetch_alldetails($table_name, $columname, $wherefield){
		$sql=tep_db_query("select * from ".$table_name." where ".$columname."='".$wherefield."'");
		$row=tep_db_fetch_array($sql);
		return $row;
	}
	
//thumbnail creation code
// Given an image (fullfilename), create an icon of name $iconfilename of max width x maxheight
function CreateIconFromImage($fullfilename, $iconfilename, $max_width, $max_height)
{
	$quality = 75;  // JPG quality of icon
	$size = GetImageSize($fullfilename); // Read the size
	$width = $size[0];
	$height = $size[1];
	
	// Proportionally resize the image to the
	// max sizes specified above

	$x_ratio = $max_width / $width;
	$y_ratio = $max_height / $height;
	
	if( ($width <= $max_width) && ($height <= $max_height) )
	{
               $tn_width = $width;
               $tn_height = $height;
         }
         elseif (($x_ratio * $height) < $max_height)
         {
               $tn_height = ceil($x_ratio * $height);
               $tn_width = $max_width;
         }
         else
         {
               $tn_width = ceil($y_ratio * $width);
               $tn_height = $max_height;
         }
     // Increase memory limit to support larger files
     
     ini_set('memory_limit', '32M');
     
     // Create the new image!
     $src = ImageCreateFromJpeg($fullfilename);
     $dst = ImageCreateTrueColor($tn_width, $tn_height);
     ImageCopyResized($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
     ImageJpeg($dst, $iconfilename, $quality);
	// Destroy the images
	ImageDestroy($src);
	ImageDestroy($dst);

}	


//Function for pagination for simple query
function fnpagination($table, $wherequery, $tpage, $perpagelimit, $pagecnt){
	//Table name
	$tbl_name=$table;
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	/* First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.	*/	
	$where=$wherequery;
	$query = "SELECT COUNT(*) as num FROM $tbl_name ".$where."";
	$total_pages = mysqli_fetch_array(mysqli_query($con,$query));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = $tpage; 	//your file name  (the name of this file)
	$limit = $perpagelimit; 								//how many items to show per page
	$page = $pagecnt;
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;	
		
	/* Get data. */
	$sql = "SELECT * FROM $tbl_name ".$where." LIMIT $start, $limit";
	$result = mysqli_query($con,$sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"".$targetpage."page=$prev\" class=\"read_more\">&laquo;</a> &nbsp; ";
		else
			$pagination.= "<span class=\"h3\">&laquo; &nbsp; </span> ";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"h2\">$counter</span> ";
				else
					$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"h2\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\" class=\"read_more\">$lpm1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\" class=\"read_more\">$lastpage</a> ";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"".$targetpage."page=1\" class=\"read_more\">1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=2\" class=\"read_more\">2</a> ";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"h2\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\" class=\"read_more\">$lpm1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\" class=\"read_more\">$lastpage</a> ";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"".$targetpage."page=1\" class=\"read_more\">1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=2\" class=\"read_more\">2</a> ";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"read_more_active\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a> ";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= " &nbsp; <a href=\"".$targetpage."page=$next\" class=\"read_more\">&raquo;</a> &nbsp; ";
		else
			$pagination.= "<span class=\"h3\"> &nbsp; &raquo;</span>"; 
		$pagination.= "</div>\n";		
	}	
	
	return array($result,$pagination);

}

//Function for pagination for joined query
function fnpaginationjoined($joinedquery, $table, $wherequery, $tpage, $perpagelimit, $pagecnt){
	//Table name
	$tbl_name=$table;
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	/* First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.	*/	
	$where=$wherequery;
	$query = "SELECT COUNT(*) as num FROM $tbl_name ".$where."";
	$total_pages = mysqli_fetch_array(mysqli_query($con,$query));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = $tpage; 	//your file name  (the name of this file)
	$limit = $perpagelimit; 								//how many items to show per page
	$page = $pagecnt;
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;	
		
	/* Get data. */
	$sql = $joinedquery." LIMIT $start, $limit";
	$result = mysqli_query($con,$sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"".$targetpage."page=$prev\" class=\"read_more\">&laquo;</a> &nbsp; ";
		else
			$pagination.= "<span class=\"h3\">&laquo; &nbsp; </span> ";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"h2\">$counter</span> ";
				else
					$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"h2\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\" class=\"read_more\">$lpm1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\" class=\"read_more\">$lastpage</a> ";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"".$targetpage."page=1\" class=\"read_more\">1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=2\" class=\"read_more\">2</a> ";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"h2\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\" class=\"read_more\">$counter</a> ";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\" class=\"read_more\">$lpm1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\" class=\"read_more\">$lastpage</a> ";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"".$targetpage."page=1\" class=\"read_more\">1</a> ";
				$pagination.= "<a href=\"".$targetpage."page=2\" class=\"read_more\">2</a> ";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"h2\">$counter</span> ";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a> ";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= " &nbsp; <a href=\"".$targetpage."page=$next\" class=\"read_more\">&raquo;</a> &nbsp; ";
		else
			$pagination.= "<span class=\"h3\"> &nbsp; &raquo;</span>"; 
		$pagination.= "</div>\n";		
	}	
	
	return array($result,$pagination);

}

//Featured image size for home page
function featuredimage($filename, $maxwidth, $maxheight){		
		list($width, $height) = getimagesize($filename);
		if($width <= $maxwidth && $height <= $maxheight){
		echo '<img src="'.$filename.'" height="'.$maxheight.'" border="0" >';
		}else if($width > $maxwidth && $height > $maxheight){
		echo '<img src="'.$filename.'" height="'.$maxheight.'" border="0">';
		}else if($height > $maxheight){
		echo '<img src="'.$filename.'" height="'.$maxheight.'" border="0">';
		}else if($width > $maxwidth){
		echo '<img src="'.$filename.'" width="'.$maxwidth.'" border="0">';
		}
}

//Password generator
function generatePassword($l = 8, $c = 3, $n = 2, $s = 1) {
     // get count of all required minimum special chars
     $count = $c + $n + $s;
 
     // sanitize inputs; should be self-explanatory
     if(!is_int($l) || !is_int($c) || !is_int($n) || !is_int($s)) {
          trigger_error('Argument(s) not an integer', E_USER_WARNING);
          return false;
     }
     elseif($l < 0 || $l > 20 || $c < 0 || $n < 0 || $s < 0) {
          trigger_error('Argument(s) out of range', E_USER_WARNING);
          return false;
     }
     elseif($c > $l) {
          trigger_error('Number of password capitals required exceeds password length', E_USER_WARNING);
          return false;
     }
     elseif($n > $l) {
          trigger_error('Number of password numerals exceeds password length', E_USER_WARNING);
          return false;
     }
     elseif($s > $l) {
          trigger_error('Number of password capitals exceeds password length', E_USER_WARNING);
          return false;
     }
     elseif($count > $l) {
          trigger_error('Number of password special characters exceeds specified password length', E_USER_WARNING);
          return false;
     }
 
     // all inputs clean, proceed to build password
 
     // change these strings if you want to include or exclude possible password characters
     $chars = "abcdefghijklmnopqrstuvwxyz";
     $caps = strtoupper($chars);
     $nums = "0123456789";
     $syms = "!@#$%^&*()-+?";
 
     // build the base password of all lower-case letters
     for($i = 0; $i < $l; $i++) {
          $out .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
     }
 
     // create arrays if special character(s) required
     if($count) {
          // split base password to array; create special chars array
          $tmp1 = str_split($out);
          $tmp2 = array();
 
          // add required special character(s) to second array
          for($i = 0; $i < $c; $i++) {
               array_push($tmp2, substr($caps, mt_rand(0, strlen($caps) - 1), 1));
          }
          for($i = 0; $i < $n; $i++) {
               array_push($tmp2, substr($nums, mt_rand(0, strlen($nums) - 1), 1));
          }
          for($i = 0; $i < $s; $i++) {
               array_push($tmp2, substr($syms, mt_rand(0, strlen($syms) - 1), 1));
          }
 
          // hack off a chunk of the base password array that's as big as the special chars array
          $tmp1 = array_slice($tmp1, 0, $l - $count);
          // merge special character(s) array with base password array
          $tmp1 = array_merge($tmp1, $tmp2);
          // mix the characters up
          shuffle($tmp1);
          // convert to string for output
          $out = implode('', $tmp1);
     }
 
     return $out;
}

function remove_dir($dir)
  	 {
  	     $handle = opendir($dir);
  	     while (false!==($item = readdir($handle)))
  	     {
  	         if($item != '.' && $item != '..')
 	         {
 	             if(is_dir($dir.'/'.$item)) 
  	             {
  	                 remove_dir($dir.'/'.$item);
  	             }else{
  	                 unlink($dir.'/'.$item);
  	             }
  	         }
  	     }
  	     closedir($handle);
  	     if(rmdir($dir))
  	     {
  	         $success = true;
  	     }
  	     return $success;
 }
 
function datapagination($table, $wherequery, $tpage, $perpagelimit, $pagecnt){
	//Table name
	$tbl_name=$table;
	
	$adjacents = 3;
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$where=$wherequery;
	$query = "SELECT COUNT(*) as num FROM $tbl_name ".$where."";
	$total_pages = mysqli_fetch_array(mysqli_query($con,$query));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = $tpage; 	//your file name  (the name of this file)
	$limit = $perpagelimit; 								//how many items to show per page
	$page = $pagecnt;
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$sql = "SELECT * FROM $tbl_name ".$where." LIMIT $start, $limit";
	$result = mysqli_query($con,$sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"".$targetpage."page=$prev\">previous</a>";
		else
			$pagination.= "<span class=\"disabled\">previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"".$targetpage."page=1\">1</a>";
				$pagination.= "<a href=\"".$targetpage."page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"".$targetpage."page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"".$targetpage."page=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"".$targetpage."page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"".$targetpage."page=$counter\">$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"".$targetpage."page=$next\">next</a>";
		else
			$pagination.= "<span class=\"disabled\">next</span>";
		$pagination.= "</div>\n";		
	}
	return array($result,$pagination);
} 
?>