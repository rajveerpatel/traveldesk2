<?php 
    include('include.inc.php');
    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-delete', $_SESSION['AccessRights'])){
            $del_display = "";
        } else {
            $del_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('tailor-made-edit', $_SESSION['AccessRights'])){
            $edit_display = "";
        } else {
            $edit_display = "display: none;";
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Tailor Made Inquiries - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Tailor Made Inquiries</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Tailor Made Inquiries</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                                
                                <div class="table-responsive m-t-20">
                                    <table id="config-table" class="table display table-striped no-wrap m-t-20">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Trip Name</th>
                                                <th>Name</th>
                                                <th>Date of Birth</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>No. of Adults</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                        $query=mysqli_query($con, "SELECT * FROM  tbl_tailor_made ORDER BY tmid DESC");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                while( $row=mysqli_fetch_assoc($query) ){ ?>
                                            <tr>
                             <td><?php echo ++$i;?></td>
                             <td><?php echo $row['trip_name']; ?></td>
                             <td><?php echo $row['first_name'].' '.$row['last_name']; ?></td>
                             <td><?php if($row['dateofbirth']!='0000-00-00'){ echo date('d-m-Y', strtotime($row['dateofbirth'])); } ?></td>
                             <td><?php echo $row['email'];?></td>
                             <td><?php echo $row['countrycode'].'-'.$row['phoneno'];?></td> 
                             <td><?php echo $row['adults_count']; ?></td>
                             <td><div class="btn-group"><a href="tailor-made-edit.php?id=<?php echo $row['tmid'];?>" class="btn bg-red edit" style="<?php echo $edit_display; ?>" id="<?php echo $row['tmid'];?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" style="<?php echo $del_display; ?>" id="<?php echo $row['tmid'];?>" title="Delete"><i class="fa fa-trash"></i></a> <!--<a class="btn bg-red display" id="<?php echo $row['tmid'];?>" data-bs-toggle="modal" data-bs-target="#ShowUser<?php echo $row['tmid'];?>" title="Display"><i class="fa fa-eye"></i></a>--></div></td>
                        </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

        <?php
$qry=mysqli_query($con, "SELECT * FROM tbl_passenger_booking ORDER BY postdate DESC");
if( mysqli_num_rows($qry) > 0 ){
    $i=0;
    while( $res=mysqli_fetch_assoc($qry) ){
    ++$i;
    ?>
    
    <div class="modal bs-example-modal-lg" id="ShowUser<?php echo $res['booking_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title head04">Trip Booking Details</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
          </div>
          <div class="modal-body">
            <table id="example1" class="table table-striped">
                <tbody>
                    <tr>
                         <td colspan="2" width="100%" style="font-size: 20px; font-weight:500;">Trip Details: </td>
                    </tr>
                    <tr>
                         <td width="30%">Trip Name: </td>
                         <td width="70%"><?php echo $res['trip_name']; ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Trip Category: </td>
                         <?php $catname = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE cat_id = '".$res['trip_cat_id']."' ")); ?>
                         <td width="70%"><?php echo $catname['category_name']; ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Reference Code: </td>
                         <td width="70%"><?php echo $res['reference_code']; ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Total Cost: </td>
                         <td width="70%"><?php if(!empty($res['total_trip_cost'])){ echo '$'.number_format($res['total_trip_cost']); } ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Travel Date: </td>
                         <td width="70%"><?php if($res['travel_date']!='0000-00-00'){ echo date('dS M Y', strtotime($res['travel_date'])); } ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Passenger Booking Summary Link: </td>
                         <td width="70%"><?php echo '<a href="'.HOME_URL.'booking-summary/'.$res['reference_code'].'" target="_blank">'.HOME_URL.'booking-summary/'.$res['reference_code'].'</a>'; ?></td>
                    </tr>
                    <tr>
                         <td colspan="2" width="100%" style="font-size: 20px; font-weight:500;">Passenger Details: </td>
                    </tr>
                    <tr>
                         <td width="30%">Passenger 1 (Lead): </td>
                         <td width="70%">
                         <ul class="list-unstyled total-price-list-01 admin-passenger">
                                <?php if(!empty($res['lead_first_name']) || !empty($res['lead_last_name'])){ ?><li><?php echo 'Name: '.$res['lead_first_name'].' '.$res['lead_last_name']; ?></li><?php } ?>
                                <?php if(!empty($res['lead_gender'])){ ?><li><?php echo 'Gender: '.$res['lead_gender']; ?></li><?php } ?>
                                <?php if($res['lead_dob']!='0000-00-00'){ ?><li><?php echo 'DOB: '.date('dS M Y', strtotime($res['lead_dob'])); ?></li><?php } ?>
                                <?php if(!empty($res['lead_meal_pref'])){ ?><li><?php echo 'Meal Prefernce: '.$res['lead_meal_pref']; ?></li><?php } ?>
                                <?php if(!empty($res['lead_dietry_request'])){ ?><li><?php echo 'Dietry Request: '.$res['lead_dietry_request']; ?></li><?php } ?>
                                <?php if(!empty($res['room_type'])){ ?><li><?php echo 'Room Type: '.$res['room_type'].' ('.$res['room_no'].')'; ?></li><?php } ?>
                            </ul>
                         </td>
                    </tr>
                    <?php
                    $pass_q = mysqli_query($con, "SELECT * FROM tbl_passengers_info WHERE booking_id = '".$res['booking_id']."' ");
                    $p = 2;
                    while($pass_r = mysqli_fetch_assoc($pass_q)){
                    ?>
                    <tr>
                         <td width="30%">Passenger <?php echo $p; ?>: </td>
                         <td width="70%">
                         <ul class="list-unstyled total-price-list-01 admin-passenger">
                                <?php if(!empty($pass_r['first_name']) || !empty($pass_r['last_name'])){ ?><li><?php echo 'Name: '.$pass_r['first_name'].' '.$pass_r['last_name']; ?></li><?php } ?>
                                <?php if(!empty($pass_r['gender'])){ ?><li><?php echo 'Gender: '.$pass_r['gender']; ?></li><?php } ?>
                                <?php if($pass_r['date_of_birth']!='0000-00-00'){ ?><li><?php echo 'DOB: '.date('dS M Y', strtotime($pass_r['date_of_birth'])); ?></li><?php } ?>
                                <?php if(!empty($pass_r['meal_preference'])){ ?><li><?php echo 'Meal Prefernce: '.$pass_r['meal_preference']; ?></li><?php } ?>
                                <?php if(!empty($pass_r['special_dietry_request'])){ ?><li><?php echo 'Dietry Request: '.$pass_r['special_dietry_request']; ?></li><?php } ?>
                                <?php if(!empty($pass_r['room_type'])){ ?><li><?php echo 'Room Type: '.$pass_r['room_type'].' ('.$pass_r['room_no'].')'; ?></li><?php } ?>
                            </ul>
                         </td>
                    </tr>
                    <?php $p++; } ?>
                    <tr>
                         <td colspan="2" width="100%" style="font-size: 20px; font-weight:500;">Passenger Contact Details: </td>
                    </tr>
                    <?php if(!empty($res['lead_email'])){ ?>
                    <tr>
                         <td width="30%">Passenger Email: </td>
                         <td width="70%"><?php echo $res['lead_email'];?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['lead_alt_email'])){ ?>
                    <tr>
                         <td width="30%">Passenger Alternate Email: </td>
                         <td width="70%"><?php echo $res['lead_alt_email'];?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['lead_phone_no_1'])){ ?>
                    <tr>
                         <td width="30%">Phone no.: </td>
                         <td width="70%"><?php echo $res['lead_country_code_1'].'-'.$res['lead_phone_no_1'];?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['lead_country_code_2'])){ ?>
                    <tr>
                         <td width="30%" rowspan="2">Alternate Phone Details: </td>
                         <td width="70%">Alternate Country Code: <?php echo $res['lead_country_code_2'];?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['lead_phon_no_2'])){ ?>
                    <tr>
                         <td width="70%" colspan="2">Alternate Phone no.: <?php echo $res['lead_phon_no_2'];?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['lead_relation_with'])){ ?>
                    <tr>
                         <td width="30%">Relationship With Emergency Contact: </td>
                         <td width="70%"><?php echo $res['lead_relation_with']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['address_1'])){ ?>
                    <tr>
                         <td width="30%">Address: </td>
                         <td width="70%"><?php echo $res['address_1']; if(!empty($res['address_2'])){ echo '<br>'.$res['address_2']; } ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['city'])){ ?>
                    <tr>
                         <td width="30%">City: </td>
                         <td width="70%"><?php echo $res['city']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['zip_code'])){ ?>
                    <tr>
                         <td width="30%">Zip Code: </td>
                         <td width="70%"><?php echo $res['zip_code']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['state'])){ ?>
                    <tr>
                         <td width="30%">State: </td>
                         <td width="70%"><?php echo $res['state']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['country'])){ ?>
                    <tr>
                         <td width="30%">Country: </td>
                         <td width="70%"><?php echo $res['country']; ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                         <td colspan="2" width="100%" style="font-size: 20px; font-weight:500;">Price Breakdown: </td>
                    </tr>
                    <?php if(!empty($res['pre_ext_trip_id'])){ 
                    
                    $pre_ext_cat = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE cat_id = '".$res['pre_ext_trip_cat_id']."' "));

                    $pre_ext_trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = '".$res['pre_ext_trip_id']."' "));
                    ?>
                    <tr>
                         <td width="30%">Pre Trip Extension <!--(<?php /*echo $pre_ext_cat['category_name'];*/ ?>):--> </td>
                         <td width="70%">
                             <?php echo $pre_ext_trip['trip_name'].' : <br>';
                             $acc_qry = mysqli_query($con, "SELECT * FROM tbl_passenger_accomodation WHERE booking_id = '".$res['booking_id']."' ");
                            if(mysqli_num_rows($acc_qry) > 0){
                                while($acc_res = mysqli_fetch_assoc($acc_qry)){
                                    $r_type = explode(' ',$acc_res['room_type']);
                                    $room_type = implode('_',$r_type);
                                    $room_type = strtolower($room_type);
                                    
                                    $room_price = mysqli_fetch_assoc(mysqli_query($con, "SELECT $room_type FROM trip_catwise_room_price WHERE trip_id = '".$pre_ext_trip['trip_id']."' AND cat_id = '".$pre_ext_cat['cat_id']."' "));
                                    echo $acc_res['room_type'].' : '.$acc_res['no_of_adults'].' Passenger x $'.$room_price[$room_type].'<br>';
                                }
                            } ?>
                         </td>
                    </tr>
                    <?php } ?>
                    <?php if(!empty($res['post_ext_trip_id'])){ 
                    
                    $post_ext_cat = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE cat_id = '".$res['post_ext_trip_cat_id']."' "));

                    $post_ext_trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = '".$res['post_ext_trip_id']."' "));
                    ?>
                    <tr>
                         <td width="30%">Post Trip Extension : </td>
                         <td width="70%">
                             <?php echo $post_ext_trip['trip_name'].' : <br>';
                             $acc_qry = mysqli_query($con, "SELECT * FROM tbl_passenger_accomodation WHERE booking_id = '".$res['booking_id']."' ");
                            if(mysqli_num_rows($acc_qry) > 0){
                                while($acc_res = mysqli_fetch_assoc($acc_qry)){
                                    $r_type = explode(' ',$acc_res['room_type']);
                                    $room_type = implode('_',$r_type);
                                    $room_type = strtolower($room_type);
                                    
                                    $room_price = mysqli_fetch_assoc(mysqli_query($con, "SELECT $room_type FROM trip_catwise_room_price WHERE trip_id = '".$post_ext_trip['trip_id']."' AND cat_id = '".$post_ext_cat['cat_id']."' "));
                                    echo $acc_res['room_type'].' : '.$acc_res['no_of_adults'].' Passenger x $'.$room_price[$room_type].'<br>';
                                }
                            } ?>
                         </td>
                    </tr>
                    <?php } ?>
                    
                    <?php
                    $optqry = mysqli_query($con, "SELECT * FROM tbl_passenger_optionals WHERE booking_id = '".$res['booking_id']."' ");
                    if(mysqli_num_rows($optqry) > 0){
                        $op = 1;
                        while($optres = mysqli_fetch_assoc($optqry)){
                            if($optres['optional_no']=='optional_1'){
                                $opt_no = 'Optional 1';
                            } elseif($optres['optional_no']=='optional_2'){
                                $opt_no = 'Optional 2';
                            } elseif($optres['optional_no']=='optional_3'){
                                $opt_no = 'Optional 3';
                            }
                            $itires = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_itineraries WHERE trip_id = '".$res['trip_id']."' AND itid = '".$optres['itinerary_id']."' "));
                    ?>
                    <tr>
                         <td width="30%">Optional Service <?php echo $op ?>: </td>
                         <td width="70%"><?php echo $opt_no.' for Day '.$itires['day'].' : $'.$optres['optional_cost']; ?></td>
                    </tr>
                    <?php $op++; } } ?>
                    
                    <?php
                    $acc_qry = mysqli_query($con, "SELECT * FROM tbl_passenger_accomodation WHERE booking_id = '".$res['booking_id']."' ");
                    if(mysqli_num_rows($acc_qry) > 0){
                        while($acc_res = mysqli_fetch_assoc($acc_qry)){
                    ?>
                    <tr>
                         <td width="30%">Accomodation - (No. of Rooms): </td>
                         <td width="70%"><?php echo $acc_res['room_type'].' x ('.$acc_res['no_of_rooms'].') : $'.$acc_res['accomodation_cost']; ?></td>
                    </tr>
                    <?php } } ?>
                    <tr>
                         <td width="30%">Post Date: </td>
                         <td width="70%"><?php if($res['postdate']!='0000-00-00'){ echo date('d-m-Y', strtotime($res['postdate'])); } ?></td>
                    </tr>
                    <tr>
                         <td width="30%">Post IP: </td>
                         <td width="70%"><?php echo $res['postip'];?></td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php
    }
}
?>
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script>
   
$(document).ready(function(){

    // Delete 
    $('.delete').click(function(){
       var el = this;

       // Delete id
       var deleteid = $(this).attr('id');

       var confirmalert = confirm("Are you sure want to delete?");
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'delete-record.php',
            type: 'POST',
            data: { tailormade_id:deleteid },
            success: function(response){

            if(response == 1){
            // Remove row from HTML Table
            $(el).closest('tr').css('background','tomato');
            $(el).closest('tr').fadeOut(800,function(){
               $(this).remove();
            });
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
  

</script>
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 100,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary me-1');
        });

    </script>
</body>

</html>