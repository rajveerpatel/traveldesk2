<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('meals', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $hid = $_GET['id'];

    if( !empty($_POST['meal_name']) ){
        
        
        
        $qry = "UPDATE tbl_meals SET 
        meal_name = '".mysqli_real_escape_string($con, $_POST['meal_name'])."',
        meal_desc = '".mysqli_real_escape_string($con, $_POST['meal_desc'])."',
        meal_cost = '".$_POST['meal_cost']."',
        country_id = '".$_POST['country_id']."',
        city_id = '".$_POST['city_id']."',
        status = '".$_POST['status']."'
        WHERE meal_id = $hid ";
        mysqli_query($con, $qry);
        
        
        
       
        
        header("location: meals.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_meals WHERE meal_id = $hid "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Meal - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Meal</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="meals.php">Meals List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="activity_name">Meal Name*</label>
                            <input type="text" name="meal_name" id="meal_name" class="form-control form-control01" value="<?php echo $row['meal_name']; ?>" placeholder="Meal Name*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="activity_cost">Meal Cost($)*</label>
                            <input type="text" name="meal_cost" id="meal_cost" class="form-control form-control01" value="<?php echo $row['meal_cost']; ?>" placeholder="Meal Cost*" required>
                        </div>
                    </div>

                    

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="activity_desc">Meal Description*</label>
                            <textarea class="form-control form-control01" name="meal_desc" id="meal_desc" rows="3" cols="40"><?php echo $row['meal_desc']; ?></textarea>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_id">Select Country*</label>
                            <select name="country_id" id="country_id" class="form-control form-select" required>
                                <option value="">Select Country*</option>
                                <?php
                                $cntry_qry = mysqli_query($con, "SELECT * FROM tbl_countries WHERE CountryStatus='1' ORDER BY CountryName ");
                                while($cntry_res = mysqli_fetch_assoc($cntry_qry)){
                                ?>
                                <option value="<?php echo $cntry_res['CountryId']; ?>" <?php if($row['country_id']==$cntry_res['CountryId']){ echo 'selected'; } ?>><?php echo $cntry_res['CountryName']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                                </div> 
                        <div class="col-md-6">
                        <div class="form-group city_box">
                            <label class="user_login_label" for="city_id">Select City*</label>
                            <select name="city_id" id="city_id" class="form-control form-select" required>
                                <option value="">Select City*</option>
                                <?php
                                if($row['country_id'] > 0){
                                $city_qry = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityStatus='1' && CountryId = '".$row['country_id']."' ORDER BY CityName ");
                                while($city_res = mysqli_fetch_assoc($city_qry)){
                                ?>
                                <option value="<?php echo $city_res['CityId']; ?>" <?php if($row['city_id']==$city_res['CityId']){ echo 'selected'; } ?>><?php echo $city_res['CityName']; ?></option>
                                <?php } } ?>
                            </select>
                            <p class="img_note">Note : If searched city doesn't have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p>
                        </div>
                    </div>

                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    
    <script>
    
    $(document).ready(function(){
        $('#country_id').on('change', function(){
            var cid = $(this).val();
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country_id: cid },
                
                success: function(response){
                    $('#city_id').empty();
                    $('#city_id').append(response);
                    $('#city_id').select2();
                }
            });
        });
        
    });
                    
    $('#city_id').select2();
        
    </script>
    
</body>

</html>