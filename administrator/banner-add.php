<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('banners', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
    }

    if( !empty($_POST['pagename']) ){
        
        $qry = "insert into tbl_banners set
        pagename='".mysqli_real_escape_string($con, $_POST['pagename'])."',
        banner_for='".$_POST['banner_for']."',
        page_url='".$_POST['page_url']."',
        postdate=NOW(),
        postip='".$_SERVER['REMOTE_ADDR']."',
        status='".$_POST['status']."'
        ";
        mysqli_query($con, $qry) or die(mysqli_error($con));

       $bannerid=mysqli_insert_id($con);

        //add slides
        if( sizeof( $_FILES['slideimage']['name'] ) > 0 ){
        foreach( $_FILES['slideimage']['name']  as $key => $value ){
             if( !empty( $_FILES['slideimage']['name'][$key]  ) ){

                $slideimage = uploadfiles($_FILES['slideimage']['name'][$key], $_FILES['slideimage']['tmp_name'][$key], $_FILES['slideimage']['error'][$key], $_FILES['slideimage']['size'][$key], BANNER_IMG);

                $query = "insert into tbl_banners_slide set
                bannerid = '$bannerid',
                slideimage = '".$slideimage."',
                banner_title='".mysqli_real_escape_string($con, $_POST['banner_title'][$key])."',
                slide_button_title='".mysqli_real_escape_string($con, $_POST['slide_button_title'][$key])."',
                slide_button_link='".mysqli_real_escape_string($con, $_POST['slide_button_link'][$key])."',
                sliderorder = '".$_POST['order'][$key]."',
                status = '".$_POST['slidestatus'][$key]."'
                ";
                mysqli_query($con, $query)or die(mysqli_error($con)); 
             }
         }
    }
        
        header("location: banners.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Add Page Banner - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>.slide_box {
    padding: 10px;
    margin-bottom: 10px;
    border: 1px solid #2e6305;
    position: relative;
}</style>
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Page Banner</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="banners.php">Page Banner List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="pagename">Banner Page/Trip Name*</label>
                            <input type="text" name="pagename" id="pagename" class="form-control form-control01" placeholder="Banner Page/Trip Name*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3">Banner for*: </label>
                            <select name="banner_for" id="banner_for" class="form-control form-select" required>
                                <option value="">Please Select</option>
                                <option value="page">Page</option>
                                <option value="trip">Trip</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-12" id="page_url" style="display:none;">
                        <div class="form-group">
                            <label class="form-floating mb-3">Page URL: </label>
                            <input type="text" name="page_url" class="form-control form-control01" placeholder="Page URL"> 
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <p class="head07">Slides</p>
                    </div>



                    <div class="col-md-12">
                        <div id="text_boxes" style="width:100%;height:auto;">
                            <div id="txt_1" class="slide_box">
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="1" onclick="remove_slide(this.id)"></div>
                                <h3 class="user_login_label"><strong>Slide 1.</strong></h3>
                                <div class="form-group">
                                    <input type="file" name="slideimage[]" class="form-control form-control01" required>
                                </div>

                                <div class="row">
                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-floating mb-3">Slide Title: </label>
                                            <input type="text" name="banner_title[]" class="form-control form-control01" placeholder="Slide Title">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-floating mb-3">Slide Button Title: </label>
                                            <input type="text" name="slide_button_title[]" class="form-control form-control01" placeholder="Slide Button Title">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-floating mb-3">Slide Button Link: </label>
                                            <input type="text" name="slide_button_link[]" class="form-control form-control01" placeholder="Slide Button Link">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-floating mb-3">Order Slide</label>
                                            <input type="number" name="order[]" class="form-control form-control01" placeholder="Order in number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-floating mb-3">Slide Status</label>
                                            <select name="slidestatus[]" class="form-control form-control01">
                                                <option value="Show">Show </option>
                                                <option value="Hide">Hide</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="status_pro_variants_1" style="position:relative;"></div>
                            </div>	
                        </div> 

                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add" /></div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            var id = 1,max = 8,append_data;

            /*If the add icon was clicked*/
            $(".add").on('click',function(){
                if($("div[id^='txt_']").length <10){ //Don't add new textbox if max limit exceed
                //$(this).remove(); //remove the add icon from current text box
        id++;
        var append_data = '<div id="txt_'+id+'" class="slide_box"><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_slide(this.id)"></div><h3 class="user_login_label"><strong>Slide '+id+'.</strong></h3><div class="form-group"><input type="file" name="slideimage[]" class="form-control form-control01" required></div><div class="row"><div class="col-md-12"><div class="form-group"><label class="user_login_label">Slide Title: </label><input type="text" name="banner_title[]" class="form-control form-control01" placeholder="Slide Title"></div></div><div class="col-md-12"><div class="form-group"><label class="user_login_label">Slide Button Title: </label><input type="text" name="slide_button_title[]" class="form-control form-control01" placeholder="Slide Button Title"></div></div><div class="col-md-12"><div class="form-group"><label class="user_login_label">Slide Button Link: </label><input type="text" name="slide_button_link[]" class="form-control form-control01" placeholder="Slide Button Link"></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label">Slide Order</label><input type="number" name="order[]" class="form-control form-control01" placeholder="Order in number"></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label">Slide Status</label><select name="slidestatus[]" class="form-control form-control01"><option value="Show">Show </option><option value="Hide">Hide</option></select></div></div></div><div id="status_pro_variants_'+id+'" style="position:relative;"></div></div>';
                $("#text_boxes").append(append_data); //append new text box in main div
                $("#txt_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

                } else {
                    alert("Maximum 10 slides are allowed");
                }

            });
        });

        </script>

        <script>
        //If the cross icon was clicked
        function remove_slide(slide_no){
            //var slide_no = $(this).attr('id');
            $("#txt_"+slide_no).css('background','tomato');
            $("#txt_"+slide_no).fadeOut(800,function(){
               $("#txt_"+slide_no).remove();
            });
        }
        </script>

        <script>
        //url field show/hide on selection basis of banner use for page/product
        $(document).ready(function(){
            $('#banner_for').on('change', function(){
                var field_name = $(this).val();
                if(field_name=='page'){
                    $('#page_url').css('display', 'block');
                } else if(field_name=='trip'){
                    $('input[name=page_url]').val('');
                    $('#page_url').css('display', 'none');
                }
            });
        });
        </script>
    
</body>

</html>