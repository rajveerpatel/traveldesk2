<?php 
    include('include.inc.php');
    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-delete', $_SESSION['AccessRights'])){
            $del_display = "";
        } else {
            $del_display = "display: none;";
        }

        //Display Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-display', $_SESSION['AccessRights'])){
            $popup_display = "";
        } else {
            $popup_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('enquiry-edit', $_SESSION['AccessRights'])){
            $edit_display = "";
        } else {
            $edit_display = "display: none;";
        }

        //Enquiry Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('enquiry-edit-logs', $_SESSION['AccessRights'])){
            $inq_edit_display = "";
        } else {
            $inq_edit_display = "display: none;";
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Trip Enquiries - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>.passanger-image {
    height: 100%;
    background-color: #ddd;
}
.passanger-img-text {
    font-weight: bold;
    font-size: 18px;
    font-family: 'Calibri', sans-serif;
    color: #004a96;
    display: table;
    margin: auto;
}
</style>
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Trip Enquiries</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Trip Enquiries List</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                                                                
                                <div class="table-responsive m-t-20">
                                    <table id="config-table" class="table display table-striped no-wrap m-t-20">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Trip Name</th>
                                                <th>Customer Name</th>
                                                <th>Trip Date</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                                <th>&nbsp;</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                        $query=mysqli_query($con, "SELECT tp.country_dest, tl.* FROM tbl_tirp_lead tl LEFT JOIN tbl_trip_packages tp ON tp.trip_id = tl.trip_id WHERE selected = '1' ORDER BY post_date DESC");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                $dest_ids = explode(',',$_SESSION['DestinationHandle']);
                                while( $row=mysqli_fetch_assoc($query) ){
                                    $country = explode(':',$row['country_dest']);
                                    $country_id = $country[0];
                                    if(in_array($country_id,$dest_ids) || $_SESSION['AdminID']==1){
                        ?>
                             <tr>
                             <td><?php echo ++$i;?></td>
                             <td><?php echo $row['trip_name']; ?></td>
                             <td><?php echo $row['first_name'].' '.$row['last_name']; ?></td>
                             <td><?php if($row['trip_start_date']!='0000-00-00'){ echo date('M dS Y', strtotime($row['trip_start_date'])); } ?></td>
                             <td><?php echo $row['email'];?></td>
                             <td><?php echo $row['mobile_number_code'].' '.$row['mobile_number'];?></td>
                             <td><?php if($row['post_date']!='0000-00-00'){ echo date('M dS Y', strtotime($row['post_date'])); } ?></td>
                             <td><div class="btn-group"><a class="btn bg-navy" style="<?php echo $edit_display; ?>" href="enquiry-edit.php?id=<?php echo $row['lid'];?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" style="<?php echo $del_display; ?>" id="<?php echo $row['lid'];?>" title="Delete"><i class="fa fa-trash"></i></a> <a class="btn bg-red display" style="<?php echo $popup_display; ?>" id="<?php echo $row['lid'];?>" data-bs-toggle="modal" data-bs-target="#exampleModal<?php echo $row['lid'];?>" title="Display"><i class="fa fa-eye"></i></a> <a class="btn btn-update" style="<?php echo $inq_edit_display; ?>" href="enquiry-edit-logs.php?track_id=<?php echo $row['user_track_id'];?>" title="Edit Logs">Logs</a></div></td>
                             <td>&nbsp;</td>
                            </tr>
                                            <?php 
                                    }
                                }

                            } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <?php
$qry=mysqli_query($con, "SELECT c.category_name, tp.*, tl.* FROM tbl_tirp_lead tl 
LEFT JOIN tbl_trip_categories c ON c.cat_id = tl.cat_id
LEFT JOIN tbl_trip_packages tp ON tp.trip_id = tl.trip_id
WHERE tl.selected = '1' ORDER BY tl.post_date DESC");
//echo mysqli_num_rows($qry);
$i=0;
while( $res=mysqli_fetch_assoc($qry) ){
++$i;
?>
<!-- Inquiry details popup modal -->
<!-- <div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-xl"> -->
<div class="modal bs-example-modal-lg" id="exampleModal<?php echo $res['lid']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
<!-- <div class="modal fade inquiry" id="ShowUser<?php //echo $res['lid']; ?>" tabindex="-1" role="dialog"> -->
<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title" id="myLargeModalLabel">Trip Enquiry Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
          
        </button>
      </div>
      <div class="modal-body">
          <div class="box-gray-01">
            <h4 class="gt-head text-center text-uppercase"><?php echo $res['trip_name']; ?></h4>
            <p class="head09 text-center padtop03">Trip Category: <?php echo $res['category_name']; ?></p>
            <p class="head09 text-center padtop03">Trip Reference Code: <?php echo $res['trip_reference_code']; ?></p>
            <!--<p class="head09 text-center padtop03">User Track ID:  <?php //echo $res['user_track_id']; ?></p>-->
            <p class="head09 text-center padtop03">Travel Date: <?php echo date('M d, Y', strtotime($res['trip_start_date'])); ?></p>

            <div class="space1"></div>
            <hr class="hr09">
            <div class="space1"></div>

            <div class="row">
                <div class="col-md-3">
                    <p class="txt01">First Name: <?php echo $res['first_name']; ?></p>
                </div>
                <div class="col-md-3">
                    <p class="txt01">Last Name: <?php echo $res['last_name']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Mobile No.: <?php echo $res['mobile_number_code'].' '.$res['mobile_number']; ?></p>
                </div>
                <div class="col-md-3">
                    <p class="txt01">Email: <?php echo $res['email']; ?></p>
                </div>
            </div>

            <hr class="hr08">

            <div class="row">
                <div class="col-md-3">
                    <p class="txt01">Trip Duration: <?php if($res['trip_days'] < 10){ echo '0'.$res['trip_days'].' Days'; } else{ echo $res['trip_days'].' Days'; }  ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">No. of Passenger: <?php echo $res['no_of_passenger']; ?></p>
                </div>
                <div class="col-md-3">
                    <p class="txt01">Cost PP Twin Sharing: <?php echo '$'.$res['cost_pp_twin_sharing']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Total Trip Cost: <?php echo '$'.$res['total_trip_cost']; ?></p>
                </div>
            </div>

            <hr class="hr08">

            <div class="row">
                <div class="col-md-12">
                    <p class="txt01">Page URL: <a href="<?php echo $res['page_url']; ?>" target="_blank"><?php echo $res['page_url']; ?></a></p>
                </div>
            </div>

            <hr class="hr08">

            <div class="row">
                <div class="col-md-12">
                    <p class="txt01">Passenger Trip Inquiry Summary Link: <a href="<?php echo HOME_URL.'trip-inquiry-summary/'.$res['trip_reference_code']; ?>" target="_blank"><?php echo HOME_URL.'trip-inquiry-summary/'.$res['trip_reference_code']; ?></a></p>
                    <input type="hidden" id="user_track_id" value="<?php echo $res['user_track_id']; ?>">
                    <input type="hidden" id="adminid" value="<?php echo $_SESSION['AdminID']; ?>">
                    <input type="hidden" id="lead_id" value="<?php echo $res['lid']; ?>">
                </div>
            </div>

            <hr class="hr08">

            <?php if(!empty($res['pre_hotel_id'])){ ?>
            <p class="txt01"><strong>Pre-trip Hotel Details</strong></p>

            <div class="row">
                <div class="col-md-3">
                    <?php
                    $hotel = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_id = '".$res['pre_hotel_id']."' "));
                    ?>
                    <p class="txt01">Hotel Name: <?php echo $hotel['hotel_name']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Checkin Date: <?php if($res['pre_hotel_checkin_date']!='0000-00-00'){ echo date('F dS, Y', strtotime($res['pre_hotel_checkin_date'])); } ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">No. of Night: <?php echo $res['pre_hotel_no_of_night']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Hotel Total Cost: <?php echo '$'.$res['pre_hotel_total_cost']; ?></p>
                </div>
            </div>

            <hr class="hr08">
            <?php } ?>

            <?php if(!empty($res['post_hotel_id'])){ ?>
            <p class="txt01"><strong>Post-trip Hotel Details</strong></p>

            <div class="row">
                <div class="col-md-3">
                    <?php
                    $hotel = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_id = '".$res['post_hotel_id']."' "));
                    ?>
                    <p class="txt01">Hotel Name: <?php echo $hotel['hotel_name']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Checkin Date: <?php if($res['post_hotel_checkout_date']!='0000-00-00'){ echo date('F dS, Y', strtotime($res['post_hotel_checkout_date'])); } ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">No. of Night: <?php echo $res['post_hotel_no_of_night']; ?></p>
                </div>

                <div class="col-md-3">
                    <p class="txt01">Hotel Total Cost: <?php echo '$'.$res['post_hotel_total_cost']; ?></p>
                </div>
            </div>

            <hr class="hr08">
            <?php } ?>

            <div class="row">
                <div class="col-md-12">
                    <p class="txt01">Room Configuration: <?php echo $res['room_configuration']; ?></p>
                </div>  
            </div>

            <hr class="hr08">

            <div class="row">
                <div class="col-md-4">
                    <p class="txt01">Trip For: <?php if($res['trip_for']=='land'){ echo 'Land Only'; } elseif($res['trip_for']=='airland'){ echo 'Air & Land'; } ?></p>
                </div>

                <?php if($res['trip_for']=='airland'){ ?>
                <div class="col-md-4">
                    <?php
                    $dep_city = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_departure_cities WHERE status = 'Active' AND city_id = '".$res['dep_city_id']."' "));
                    ?>
                    <p class="txt01">Departure City: <?php echo $dep_city['departure_city']; ?></p>
                </div>
                <?php } ?>

                <?php if($res['trip_for']=='airland'){ ?>
                <div class="col-md-4">
                    <p class="txt01">Extra Flight Cost PP: <?php echo '$'.$res['extra_flight_cost']; ?></p>
                </div>
                <?php } ?>
            </div>

            <hr class="hr08">

            <?php if(!empty($res['message'])){ ?>
            <div class="row">
                <div class="col-md-12">
                    <p class="txt01">User Message: <?php echo $res['message']; ?></p>
                </div>  
            </div>

            <hr class="hr08">
            <?php } ?>

            <?php if(!empty($res['agent_id'])){
                if($res['requested_as']=="Registered Agent"){
                    $qry = mysqli_query($con, "SELECT * FROM tbl_agent_details WHERE agentid = '".$res['agent_id']."' AND status = 'Active' ");
                    $agent_name = mysqli_fetch_assoc($qry);
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="txt01">Requested From: <?php echo $agent_name['agent_name'].' ('.$res['requested_as'].')'; ?></p>
                        </div>  
                    </div>
                    <?php 
                    } else {
                    $agent_name = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_request_as_agent WHERE req_id = '".$res['agent_id']."' "));
                        ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="txt01">Requested From: <?php echo $agent_name['agent_first_name'].' '.$agent_name['agent_last_name'].' ('.$res['requested_as'].')'; ?></p>
                                </div>  
                            </div>
                        <?php
                    }  
                    echo '<hr class="hr08">';
                } else { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="txt01">Requested From: <?php echo $res['first_name'].' '.$res['last_name'].' ('.$res['requested_as'].')'; ?></p>
                        </div>  
                    </div>

                    <hr class="hr08">
              <?php } ?>

                <p class="space2"></p>
                <p class="destination_heading">Trip Summary</p>
                <hr class="redline">
                <p class="space2"></p>

                <p class="sub-title text-uppercase">SKU: <?php echo $res['trip_sku']; ?>  |  Trip Reference Code: <?php echo $res['trip_reference_code']; ?></p>
                <p class="space2"></p>

                <div class="summary-panel">
                    <div class="summary-body1">
                        <?php
                        $duration = $res['trip_days'] - 1;
                        $end_date = date('M dS, Y', strtotime("+$duration days", strtotime($res['trip_start_date'])));
                        ?>
                        <h3 class="sumary-title text-center">Trip Date: <?php echo date('M dS, Y', strtotime($res['trip_start_date'])); ?> to <?php echo $end_date; ?> | <?php echo $res['trip_name']; ?> | <?php echo $res['category_name']; ?></h3>
                        <hr class="hr-a mx-auto">
                        <h4 class="sumary-subtitle text-uppercase text-center">PREPARED FOR <?php echo $res['first_name'].' '.$res['last_name'].' And Group'; ?></h4>
                    </div>
                </div>

                <p class="space3"></p>

                <?php
                // Main itineraries
                    $iti_qry = mysqli_query($con, "SELECT * FROM tbl_tirp_lead_itineraries WHERE trip_id = '".$res['trip_id']."' ");
                    $total_days = mysqli_num_rows($iti_qry);
                    $trip_start_date = date('F dS, Y', strtotime($res['trip_start_date']));
                    $itinerary_day = '';
                    $cnt=1;
                    $i=0;
                    while($iti_res = mysqli_fetch_assoc($iti_qry)){
                    ?>

                    <?php
                    if($iti_res['city_id']!=$itinerary_day){
                        $qnight = mysqli_query($con, "SELECT * FROM tbl_tirp_lead_itineraries WHERE trip_id = '".$res['trip_id']."' ");
                        $n = 0;
                        $j=0;
                        $iti_cityid = '';
                        $iti_cityid = $iti_res['city_id'];
                        while($rnight = mysqli_fetch_assoc($qnight)){
                            if($i <= $j){
                                if($iti_cityid==$rnight['city_id']){
                                    $n++;
                                } elseif($iti_cityid!=$rnight['city_id']){
                                    break;
                                }
                            }
                            $j++;
                        }
                    $cityname = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityId = ".$iti_res['city_id']." AND CityStatus = '1' "));
                    ?>
                    <p class="destination_heading2"><?php echo $cityname['CityName']; ?> | <?php if($n < 2){ echo $n.' Night'; } else { echo $n.' Nights'; } ?>
                    <hr class="redline-full">
                    <p class="space2"></p>
                    <?php 
                    } 
                    $itinerary_day = $iti_res['city_id'];
                    $i++
                    ?>

                    <div class="passanger-box">
                        <h4 class="passanger-head inline-cal-img text-uppercase">DAY <?php if($cnt < 10){ echo '0'.$cnt; } else { echo $cnt; } ?> - <?php echo date('l dS F, Y', strtotime($trip_start_date)); ?></h4>
                        <p class="space1"></p>
                        <div class="row d-flex">
                            <div class="col-md-3">
                                <a href="#"><div class="passanger-image d-flex">
                                    <?php
                                    $cityname = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityId = ".$iti_res['city_id']." AND CityStatus = '1' "));
                                    ?>
                                    <span class="passanger-img-text text-uppercase"><?php echo $cityname['CityName']; ?></span>
                                </div></a>
                            </div>
                            <div class="col-md-9">
                                 <div class="table-passenger table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td width="24%"><a class="table-link text-uppercase" href="#"><span class="table-left-icons"><img src="<?php echo HOME_URL; ?>images/e1.png" class="img-responsive"></span>Hotel</a></td>
                                                <?php
                                                $qhotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_id = '".$iti_res['hotel']."' ");
                                                if(mysqli_num_rows($qhotel) > 0){
                                                    $hotel = mysqli_fetch_assoc($qhotel);

                                                    $city = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_cities WHERE CItyId = '".$hotel['city_id']."' "));

                                                    $hotelname = $hotel['hotel_name'].', '.$city['CityName'];
                                                } else {
                                                    $hotelname = '';
                                                }
                                                ?>
                                                <td width="76%" id="hotel-row-<?php echo $iti_res['itid']; ?>"><?php echo $hotelname; ?> <span class="edit-icon" id="edit-hotel-<?php echo $iti_res['itid']; ?>"><i class="fa fa-edit"></i></span></td>
                                            </tr>
                                            <tr>
                                                <td><a class="table-link text-uppercase" href="#"><span class="table-left-icons"><img src="<?php echo HOME_URL; ?>images/e2.png" class="img-responsive"></span>Meals</a></td>
                                                <td id="meal-row-<?php echo $iti_res['itid']; ?>"><?php if(!empty($iti_res['meals'])){ echo $iti_res['meals']; } ?> <span class="edit-icon" id="edit-meal-<?php echo $iti_res['itid']; ?>"><i class="fa fa-edit"></i></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                 </div>
                            </div>
                        </div>
                    </div>

                    <p class="space2"></p>
                    <?php if($total_days!=$cnt){ ?>
                    <img src="<?php echo HOME_URL; ?>images/shadow5.png" class="img-fluid mx-auto mt-4 mb-3">

                    <p class="space1"></p>
                    <?php } ?>

                <?php
                    $cnt++;
                    $trip_start_date = date('F dS, Y', strtotime("+1 days", strtotime($trip_start_date))); 
                    }
                ?>

                <hr class="redline-full">

                <p class="space2"></p>

                <div class="row d-flex">
                    <div class="col-md-5 offset-md-7">
                        <div class="ts-summary d-flex">
                            <div class="ts-left d-lg-flex align-items-lg-center">
                                <div class="ts-text">
                                    <?php
                                    $p = 1;
                                    $rooms = explode('|', $res['room_config_value']);
                                    for($i=0; $i<sizeof($rooms); $i++){
                                        $room_detail = explode('-', $rooms[$i]);
                                        $no_of_pax = $room_detail[0];
                                        $room_type = $room_detail[1];
                                        $room_name = explode('_', $room_type);
                                        echo '<strong>'.ucfirst($room_name[0]).' '.ucfirst($room_name[1]).'</strong><br>';

                                        $room_price = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_start_date = '".$res['trip_start_date']."' AND trip_id = '".$res['trip_id']."' "));
                                        $trip_for = $res['trip_for'];
                                        for($j=0; $j<$no_of_pax; $j++){
                                            echo 'Passenger '.$p.': $'.$room_price[$trip_for.'_'.$room_type].'<br>';
                                            $p++;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="ts-right">
                                <div class="ts-price">
                                    $<?php echo $res['total_trip_cost']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="space2"></p>

                <div class="text-center btn00">
                    <button class="btn btn-primary text-white" style="<?php echo $add_display; ?>" data-bs-toggle="modal" data-bs-target="#exampleModal" data-whatever="@mdo">Add Log Remark</button>
                </div>

                <p class="space2"></p>


                <div class="log-remark-<?php echo $res['lid']; ?>">
                <?php
                    $query=mysqli_query($con, "SELECT l.*, a.* FROM tbl_tirp_lead_logs l LEFT JOIN tbl_admin a ON a.adminid = l.updated_by WHERE l.user_track_id = '".$res['user_track_id']."' ORDER BY l.post_date DESC");
                    while($log_res = mysqli_fetch_assoc($query)){
                ?>
                    <div class="log-box">
                        <h3 class="log-time"><?php if($log_res['post_date']!='0000-00-00 00:00:00'){ echo date('m/d/Y, l | H:i', strtotime($log_res['post_date'])); } ?></h3>
                        <p class="remark-log"><?php echo $log_res['remark']; ?></p>
                        <p class="remark-bot">Posted by: <?php echo $log_res['agent_code']; ?> | Type of Remark: <?php echo $log_res['remark_type']; ?> | Added by: <?php echo $log_res['fullname']; ?></p>
                    </div>
                <?php } ?>
                </div>
          </div>
      </div>
    </div>
  </div>
</div>

<?php
}
?>

<!-- add logs remark  -->
<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title head04">Add Remark</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="user_login_label" for="add_remark">Add Remark*</label>
                    <input type="text" name="add_remark" id="add_remark" class="form-control form-control01" placeholder="Add Remark*" required>
                </div>
                
                <div class="form-group">
                    <label class="user_login_label" for="remark_type">Type of Remark*</label>
                    <select name="remark_type" id="remark_type" class="form-control form-control01" required>
                        <option value="">Please Select</option>
                        <option value="External">External</option>
                        <option value="Internal">Internal</option>
                    </select>
                </div>
                
                <div class="btn00 text-center">
                    <button name="submit" id="add_remark_btn" class="btn btn-primary text-white">Submit</button>
                </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    

    <script>
    $(document).ready(function(){
        $('#add_remark_btn').click(function(){
            var remark = $('#add_remark').val();
            var user_track_id = $('#user_track_id').val();
            //var trip_id = $('#trip_id').val();
            var adminid = $('#adminid').val();
            var lead_id = $('#lead_id').val();
            var remark_type = $('#remark_type option:selected').val();
            var action = 'add_remark';
            var popup_action = 'popup_action';
            
            $.ajax({
                url: 'ajax_logs_manage.php',
                type: 'POST',
                data: { remark: remark, user_track_id: user_track_id, adminid: adminid, remark_type: remark_type, action: action },
                success: function(response){
                    
                    setTimeout(function(){
                        $('#add_remark').val('');
                        $('#add-remark-modal').modal('hide');
                        //window.location.href = "trip-logs.php?trip_id="+trip_id+"&track_id="+user_track_id;
                    }, 100);
                    
                    $('.log-remark-'+lead_id).html(response);
                    
                    /*$.ajax({
                        url: 'ajax_logs_manage.php',
                        type: 'POST',
                        async: false,
                        data: { popup_action: popup_action, user_track_id: user_track_id },
                        success: function(response){
                            $('.edit-popup').html(response);    
                        }
                    });*/
                }
            });
        });
    });
</script>

<script>
   
$(document).ready(function(){

    // Delete 
    $('.delete').click(function(){
       var el = this;

       // Delete id
       var deleteid = $(this).attr('id');

       var confirmalert = confirm("Are you sure want to delete?");
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'delete-record.php',
            type: 'POST',
            data: { lid:deleteid },
            success: function(response){

            if(response == 1){
            // Remove row from HTML Table
            $(el).closest('tr').css('background','tomato');
            $(el).closest('tr').fadeOut(800,function(){
               $(this).remove();
            });
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
  
// Update hotel
$(document).on("click", ".edit-icon", function(e) {
    e.preventDefault();
    var icon_id = $(this).attr('id');
    var icon_id = icon_id.split('-');
    var field_name = icon_id[1];
    var field_id = icon_id[2];

    $.ajax({
        url: 'ajax_hotel_meal_update.php',
        type: 'POST',
        data: { itinerary_id: field_id, field_type: field_name },
        success: function(response){
            // get hotels for city
            if(field_name=='hotel'){
                $('#hotel-row-'+field_id).html(response);
            }
            
            //update hotel on change from hotel list
            $('.change-hotel').on('change', function(){
                var row_id = $(this).attr("id");
                var hotel_id = $(this).val();
                var action = 'hotel-update';

                $.ajax({
                    url: 'ajax_hotel_meal_update.php',
                    type: 'POST',
                    data: { itinerary_id: field_id, hotel_id: hotel_id, action: action },
                    success: function(response){
                        if(response){
                            if(field_name=='hotel'){
                                $('#hotel-row-'+field_id).html(response);
                            }
                        } else {
                            $('#hotel-row-'+field_id).html('<span class="edit-icon" id="edit-hotel-'+field_id+'"><i class="fa fa-edit"></i></span>');
                        }
                    } 
                });
            });
            
            //get meals for city
            if(field_name=='meal'){
                $('#meal-row-'+field_id).html(response);
                $('#meals-'+field_id).multiselect();
            }

            //update meal on select meal and click on submit button
            $('.meal-update').click(function(){
                var field_id = $(this).attr("id");

                var meals = $('#meals-'+field_id).val();
                
                $.ajax({
                    url: 'ajax_hotel_meal_update.php',
                    type: 'POST',
                    data: { itinerary_id: field_id, meals: meals },
                    success: function(response){
                        if(field_name=='meal'){
                            $('#meal-row-'+field_id).html(response);
                        }
                    }
                });
            });
        }
    });
});

</script>

<script>
    $( function() {
        $( ".date-type" ).datepicker({ dateFormat: 'M dd, yy' });
    });
</script>
    
</body>

</html>