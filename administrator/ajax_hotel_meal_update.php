<?php
include('includes/config.inc.php');

$itid = 0;
$city_id = 0;

//Get Hotels from selected City
if( !empty($_POST['itinerary_id']) && !empty($_POST['field_type']) && $_POST['field_type']=='hotel' ){
    
    $itid = $_POST['itinerary_id'];
    
    //Search City
    $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_tirp_lead_itineraries WHERE itid = $itid " ));
    $city_id = $res['city_id'];
    $hotel_id = $res['hotel'];
    ?>
    <select id="hotels-<?php echo $itid; ?>" class="form-control change-hotel form-select">
        <option value="">Please Select</option>
    
    <?php
    //Search Hotels
    $query = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE city_id = $city_id " );
    while($row = mysqli_fetch_assoc($query)){
    ?>
        <option value="<?php echo $row['hotel_id']; ?>" <?php if($row['hotel_id']==$hotel_id){ echo 'selected'; } ?>><?php echo $row['hotel_name']; ?></option>
    <?php } ?>
    </select>
    <?php
}

//Update Hotel of particular City
if( !empty($_POST['itinerary_id']) && !empty($_POST['action']) && $_POST['action']=='hotel-update' ){
    
    $itid = $_POST['itinerary_id'];
    $hotel_id = $_POST['hotel_id'];
    
    $sql = "UPDATE tbl_tirp_lead_itineraries SET
    hotel = '$hotel_id'
    WHERE itid = $itid ";
    mysqli_query($con, $sql);
    
    if($hotel_id!=''){
        $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT h.*, tl.hotel, c.CityName FROM tbl_hotels h
        LEFT JOIN tbl_tirp_lead_itineraries tl ON tl.hotel=h.hotel_id
        LEFT JOIN tbl_cities c ON c.CityId=h.city_id
        WHERE h.hotel_id = $hotel_id " ));

        echo $row['hotel_name'].', '.$row['CityName'].' <span class="edit-icon" id="edit-hotel-'.$itid.'"><i class="fa fa-edit"></i></span>';
    }
}

//Get Meals for selected City
if( !empty($_POST['itinerary_id']) && !empty($_POST['field_type']) && $_POST['field_type']=='meal' ){
    
    $itid = $_POST['itinerary_id'];
    
    //Search City
    $res = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_tirp_lead_itineraries WHERE itid = $itid " ));
    $city_id = $res['city_id'];
    $meals = explode(' | ', $res['meals']);
    ?>

    <select id="meals-<?php echo $itid; ?>" class="form-control change-meals form-control01" style="width: 60%" multiple>
        <option value="Breakfast" <?php if(in_array('Breakfast', $meals)){ echo 'selected'; } ?>>Breakfast</option>
        <option value="Lunch" <?php if(in_array('Lunch', $meals)){ echo 'selected'; } ?>>Lunch</option>
        <option value="Dinner" <?php if(in_array('Dinner', $meals)){ echo 'selected'; } ?>>Dinner</option>
    </select>
    <button type="button" class="btn btn-primary text-white" id="<?php echo $itid; ?>">Update</button>
    <?php
}

//Update Meals of particular City
if( !empty($_POST['itinerary_id']) && !empty($_POST['meals']) ){
    
    $itid = $_POST['itinerary_id'];
    $meals = implode(' | ', $_POST['meals']);
    
    $sql = "UPDATE tbl_tirp_lead_itineraries SET
    meals = '$meals'
    WHERE itid = $itid ";
    mysqli_query($con, $sql);
    
    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_tirp_lead_itineraries WHERE itid = $itid " ));
    
    echo $row['meals'].' <span class="edit-icon" id="edit-meal-'.$itid.'"><i class="fa fa-edit"></i></span>';
}
?>