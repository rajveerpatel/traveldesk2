<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }
        
    }

$msg = '';

if(isset($_FILES["niftycsv"]["name"])){
    
      function clean($string) {
           $string = str_replace(' ', '-', $string); 
          $string = str_replace("'", '`', $string); 
           $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string); 

           return preg_replace('/-+/', '-', $string); 
        }
       
    $filename=$_FILES["niftycsv"]["tmp_name"];  
    
    $row = 1;
  
  if($_FILES["niftycsv"]["size"] > 0)
     {
       $count=0;
      $file = fopen($filename, "r");
      $getData = fgetcsv($file, 10000, ",");
    //  var_dump($getData);
     
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
           {
                $sku = $getData[0];
                $tripname = $getData[1];
                $tripimage = $getData[2];
                $tripslider = $getData[3];
                $tripduration = $getData[4];
                $triparea = $getData[5];
                $tripsubarea = $getData[6];
        //Get destination id
                $parent_destination = $getData[7];
                $rparent_dest=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE parent_destination='$parent_destination' "));
                $parent_destination_id=$rparent_dest['parent_dest_id'];
        
                $country = $getData[8];
        
         //get trip category
        $trip_category = $getData[9];
        $rcat=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE category_name='$trip_category' "));
        $cat_id=$rcat['cat_id'];
        
        
        $trip_highlights = clean($getData[10]);
        $trip_short_highlights = clean($getData[11]);
        
        //this type id
        $trip_types = explode('|',$getData[12]);
          foreach($trip_types as $key=>$value){
            echo $value;
             $rtheme=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE trip_style ='$value' "));
             $trip_type_id=$rtheme['style_id'].',';
        }
        
        
        //get theme id
        $trip_theme = explode('|',$getData[13]);
        foreach($trip_theme as $key=>$value){
            echo $value;
             $rtheme=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE trip_type='$value' "));
             $trip_theme_id=$rtheme['trip_type_id'].',';
        }
        
        $trip_validity_from_date = date('Y-m-d', strtotime($getData[14])); 
        $trip_validity_to_date =  date('Y-m-d', strtotime($getData[15])); 
        
        $trip_map_iframe = $getData[16];
        
        $trip_pre_hotel = $getData[17];
        $trip_post_hotel = $getData[18];
        $trip_stop_over = $getData[19];
        $trip_extension = $getData[20];
        $trip_single_room_price = $getData[21];
        $trip_twin_sharing_price = $getData[22];
        $trip_tripple_sharing_price = $getData[23];
        $trip_quad_sharing_price = $getData[24];
        
       
        $pack_title = clean(trim($tripname));
        $pack_dest = $country;
        $pack_destination = clean($parent_destination);
      //  $duration =  (strtotime($trip_validity_to_date) - strtotime($trip_validity_from_date))/60/60/24;
       $duration =$tripduration;
        $slug=trim(strtolower($pack_title.'-'.$pack_destination.'-'.$duration.'-days'));
       
   
        
        $query="INSERT INTO tbl_trip_packages SET
                        trip_name='$tripname',
                        trip_sku='$sku',
                        trip_thumb='$tripimage',
                        slug='$slug',
                        validity_start_date='$trip_validity_from_date',
                        validity_end_date='$trip_validity_to_date',
                        trip_days='$tripduration',
                        area='$triparea',
                        sub_area='$tripsubarea',
                        parent_dest='$parent_destination_id',
                        country_dest='$country',
                        trip_themes='$trip_theme_id',
                        trip_category='$cat_id',
                        trip_type='$trip_type_id',
                        short_highlights='$trip_short_highlights',
                        trip_highlights_inclusions='$trip_highlights',
                        single_room='$trip_single_room_price',
                        twin_sharing='$trip_twin_sharing_price',
                        triple_sharing='$trip_tripple_sharing_price',
                        quad_sharing='$trip_quad_sharing_price',
                        trip_map='$trip_map_iframe',
                        status='Active',
                        postip='".$_SERVER['REMOTE_ADDR']."',
                        postdate=NOW()
                        ";
        
        mysqli_query($con, $query) or die(mysqli_error($con));
        $packageid=mysqli_insert_id($con);
        //exit();
        /*
        pre_trip_hotel='$trip_pre_hotel',
                        post_trip_hotel='$trip_post_hotel',
                        trip_stopover='$trip_stop_over',
                        trip_extension='$trip_extension',
        */
        
        //small group
        echo '<br>small grp <br>';
        for( $sg=25; $sg<43; ){
            $trip_small_grp_start_date = date('Y-m-d', strtotime($getData[$sg++]));  
              $trip_quad_grp_end_date = date('Y-m-d', strtotime($getData[$sg++]));  
              $trip_quad_grp_price = $getData[$sg++];
            if( !empty($trip_small_grp_start_date) && !empty($trip_quad_grp_end_date)  && !empty($trip_quad_grp_price) ){
            $query_small_grp="INSERT INTO tbl_trip_prices SET
                                            trip_id='$packageid',
                                            trip_start_date='$trip_small_grp_start_date',
                                            trip_end_date='$trip_quad_grp_end_date',
                                            trip_categories='$cat_id',
                                            trip_styles='1',
                                            trip_price='$trip_quad_grp_price'
                                            ";
            mysqli_query($con, $query_small_grp) or die(mysqli_error($con));
            }
        }   
        
        //private tour
        echo '<br>private tour <br>';
        for( $pt=43; $pt<52;  ){
              $trip_pvt_tour_start_date = date('Y-m-d', strtotime($getData[$pt++])); 
             $trip_pvt_tour_end_date = date('Y-m-d', strtotime($getData[$pt++])); 
             $trip_pvt_tour_price = $getData[$pt++];
            if( !empty($trip_pvt_tour_start_date) && !empty($trip_pvt_tour_end_date)  && !empty($trip_pvt_tour_price) ){
              $query_private_grp="INSERT INTO tbl_trip_prices SET
                                            trip_id='$packageid',
                                            trip_start_date='$trip_pvt_tour_start_date',
                                            trip_end_date='$trip_pvt_tour_end_date',
                                            trip_categories='$cat_id',
                                            trip_styles='2',
                                            trip_price='$trip_pvt_tour_price'
                                            ";
            mysqli_query($con, $query_private_grp) or die(mysqli_error($con));
            }    
        }  
        
    //    iteneraray
      echo '<br>itinerary <br>';
        $day=1;
        
        for( $it=52; $it<204;  ){
              $trip_itinerary_day_title = $getData[$it++];
            
            //get city id/country id
             $trip_itinerary_day_city = $getData[$it++];
            $rcity=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityName='$trip_itinerary_day_city' "));
            $cityid=$rcity['CityId'];
            $countryid=$rcity['CountryId'];
            
            //get hotel id
             $trip_itinerary_day_hotel = $getData[$it++];
            if($trip_itinerary_day_hotel){
              $rhotel=mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_name='$trip_itinerary_day_hotel' "));
                $hotel_id=$rhotel['hotel_id'];
            }else{
                $hotel_id=0;
            }
            
             $trip_itinerary_day_meal = $getData[$it++];
            
            //get activity id if exist else insert and get id
             $trip_itinerary_day_activities = $getData[$it++];
            if( $trip_itinerary_day_activities ){
             $qact=mysqli_query($con, "SELECT * FROM tbl_activities WHERE activity_name='$trip_itinerary_day_activities' ");
            if( mysqli_num_rows($qact) > 0 ){
                $ract=mysqli_fetch_assoc($qact);
                $activityid=$ract['act_id'];
            }else{
                mysqli_query($con, "INSERT INTO tbl_activities SET  activity_name='$trip_itinerary_day_activities', country_id='$countryid', city_id='$cityid', status='Active', postdate=NOW() ");
                $activityid=mysqli_insert_id($con);
            }
        }else{
               $activityid=0; 
            }        
               
            
             $trip_itinerary_day_description = mysqli_real_escape_string($con,$getData[$it++]);
             if( !empty($trip_itinerary_day_title) && !empty($trip_itinerary_day_city)  && !empty($trip_itinerary_day_hotel) ){ 
        echo   $query_itinerary="INSERT INTO tbl_trip_itineraries SET
                                            trip_id='$packageid',
                                            cat_id='$cat_id',
                                            day='$day',
                                            itenerary_title='$trip_itinerary_day_title',
                                            itenerary_description='$trip_itinerary_day_description',
                                            hotel='$hotel_id',
                                            activities='$activityid',
                                            meal='$trip_itinerary_day_meal',
                                            city_id='$cityid'
                                            ";
            mysqli_query($con, $query_itinerary) or die(mysqli_error($con));
                 ++$day;
             }
        }  
        
        
           }
   fclose($handle);
     
}
    
  
   // header('location:result-master.php');
  }    
?>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-center">Import CSV</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <?php echo $msg; ?>
        <p class="space2"></p>

        <div class="input-form">
          	<form  method="post" enctype="multipart/form-data" action="import-csv.php">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="file" name="niftycsv" id="niftycsv"  class="form-control form-control01"  required>
                        </div>
                    </div>

                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <button name="submit" class="btn btn03 btn-width-03">Import</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<?php 
    include('elements/footer.php');
?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>