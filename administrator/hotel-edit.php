<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('hotels', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $hid = $_GET['id'];

    if( !empty($_POST['hotel_name']) ){
        
        if(!empty($_FILES['hotel_image']['name'])){
            $hotel_image=uploadfiles($_FILES['hotel_image']['name'], $_FILES['hotel_image']['tmp_name'], $_FILES['hotel_image']['error'], $_FILES['hotel_image']['size'], HOTEL_GALLERY_IMG);
        }else{
            $hotel_image=$_POST['old_hotel_image'];
        }
        
        $qry = "UPDATE tbl_hotels SET 
        hotel_name = '".mysqli_real_escape_string($con, $_POST['hotel_name'])."',
        cost_pp_night = '".$_POST['cost_pp_night']."',
        hotel_desc = '".mysqli_real_escape_string($con, $_POST['hotel_desc'])."',
        country_id = '".$_POST['country_id']."',
        city_id = '".$_POST['city_id']."',
        category_id = '".$_POST['category_id']."',
        status = '".$_POST['status']."'
        WHERE hotel_id = $hid ";
        mysqli_query($con, $qry);
        
        //add hotel gallery images
        if( !empty($_FILES['gallery_image']['name']) ){
            mysqli_query($con, "DELETE FROM tbl_hotel_gallery WHERE hotel_id = $hid ");
            foreach($_FILES['gallery_image']['tmp_name'] as $keygpics => $valuesgpics){
                if( !empty($_FILES['gallery_image']['name'][$keygpics]) ){
                    
                    $gallery_img=uploadfiles($_FILES['gallery_image']['name'][$keygpics], $_FILES['gallery_image']['tmp_name'][$keygpics], $_FILES['gallery_image']['error'][$keygpics], $_FILES['gallery_image']['size'][$keygpics], HOTEL_GALLERY_IMG);
                    
                    echo $q = "INSERT INTO tbl_hotel_gallery set 
                    gallery_image='$gallery_img',
                    hotel_id=$hid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
       
        
        header("location: hotels.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_id = $hid "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Hotel - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Hotel</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="hotels.php">Hotel List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="hotel_name">Hotel Name*</label>
                            <input type="text" name="hotel_name" id="hotel_name" class="form-control form-control01" value="<?php echo $row['hotel_name']; ?>" placeholder="Hotel Name*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="cost_pp_night">Cost PP/Night($)*</label>
                            <input type="text" name="cost_pp_night" id="cost_pp_night" class="form-control form-control01" value="<?php echo $row['cost_pp_night']; ?>" placeholder="Cost PP/Night*" required>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="hotel_desc">Hotel Description*</label>
                            <textarea class="form-control form-control01" name="hotel_desc" id="hotel_desc" rows="3" cols="40"><?php echo $row['hotel_desc']; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <p class="head11">Hotel Room Type</p>
                        <div id="hotel_rooms" class="hotel_boxes">
                            <?php
                            $roomsql = mysqli_query($con, "SELECT * FROM tbl_hotel_room WHERE hotel_id = $hid ");
                            if(mysqli_num_rows($roomsql) > 0){
                                $i=0;
                                while($roomres = mysqli_fetch_assoc($roomsql)){
                                ++$i;
                            ?>
                            <div class="row" id="room_<?php echo $i; ?>">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="user_login_label">Room Type Name <?php echo $i; ?>:</label>
                                        <div class="hotel-box">
                                            <select name="room_type_id[]" id="room_type_id_<?php echo $i; ?>" class="form-control form-select">
                                                <option value="">Please Select</option>
                                                <?php
                                                $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' ");
                                                while($rroom = mysqli_fetch_assoc($qroom)){
                                                ?>
                                                <option value="<?php echo $rroom['room_type_id']; ?>" <?php if($rroom['room_type_id']==$roomres['room_type_id']){ echo 'selected'; } ?>><?php echo $rroom['room_type_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="user_login_label">Upgrade Room Cost <?php echo $i; ?>:</label>
                                        <div class="hotel-box">
                                            <input type="text" name="upgrade_room_cost[]" id="upgrade_room_cost_<?php echo $i; ?>" class="form-control form-control01" value="<?php echo $roomres['upgrade_room_cost']; ?>">
                                        </div>   
                                        <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $i; ?>" onclick="remove_room(this.id)" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php }
                            } else { ?>
                            <div class="row" id="room_1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="user_login_label">Room Type Name 1:</label>
                                        <div class="hotel-box">
                                            <select name="room_type_id[]" id="room_type_id_1" class="form-control form-select">
                                                <option value="">Please Select</option>
                                                <?php
                                                $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' ");
                                                while($rroom = mysqli_fetch_assoc($qroom)){
                                                ?>
                                                <option value="<?php echo $rroom['room_type_id']; ?>"><?php echo $rroom['room_type_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="user_login_label">Upgrade Room Cost 1:</label>
                                        <div class="hotel-box">
                                            <input type="text" name="upgrade_room_cost[]" id="upgrade_room_cost_1" class="form-control form-control01">
                                        </div>   
                                        <div class="remove-box"><img src="assets/images/remove-icon.png" id="1" onclick="remove_room(this.id)" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <input type="hidden" id="room_cnt" value="<?php echo $i; ?>">
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_room" /></div>
                    </div>
                    
                    <div class="col-md-12">
                        <p class="head11">Hotel Gallery Images</p>
                        <div id="hotel_images" class="hotel_boxes">
                            <div class="row">
                                <?php
                                $gellerysql = mysqli_query($con, "SELECT * FROM tbl_hotel_gallery WHERE hotel_id = $hid ");
                                if(mysqli_num_rows($gellerysql) > 0){
                                    $gh=0;
                                    while($galleryres = mysqli_fetch_assoc($gellerysql)){
                                    ++$gh;
                                ?>
                                <div class="col-md-6">
                                    <div id="image_<?php echo $gh; ?>">
                                        <div class="form-group">
                                            <label class="user_login_label">Gallery Image <?php echo $gh; ?>:</label>
                                            <div class="hotel-box">
                                                <input type="file" name="gallery_image[]" id="gallery_img_<?php echo $gh; ?>" class="form-control form-control01">
                                                <input type="hidden" name="old_gallery_img[]" value="<?php echo $galleryres['gallery_image']; ?>" class="form-control input-a">
                                            </div>   
                                            <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $gh; ?>" onclick="remove_image(this.id)" /></div>
                                        </div>
                                        <?php if(!empty($galleryres['gallery_image'])){
                                            echo '<img src="'.HOTEL_GALLERY_IMG.$galleryres['gallery_image'].'" class="img-responsive" width="100">';
                                        } ?>
                                        <p class="img_note">Note : Hotel Image size approx 650 x 350</p>
                                    </div>
                                </div>
                                <?php }
                                } else { ?>
                                <div class="col-md-6">
                                    <div id="image_1">
                                        <div class="form-group">
                                            <label class="user_login_label">Gallery Image 1:</label>
                                            <div class="hotel-box">
                                                <input type="file" name="gallery_image[]" id="gallery_img_1" class="form-control  form-control01">
                                            </div>   
                                            <div class="remove-box"><img src="assets/images/remove-icon.png" id="1" onclick="remove_image(this.id)" /></div>
                                        </div>
                                        <p class="img_note">Note : Hotel Image size approx 650 x 350</p>
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" id="gal_img_cnt" value="<?php echo $gh; ?>">
                            </div>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_gal_img" /></div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_id">Select Country*</label>
                            <select name="country_id" id="country_id" class="form-control form-select" required>
                                <option value="">Select Country*</option>
                                <?php
                                $cntry_qry = mysqli_query($con, "SELECT * FROM tbl_destinations WHERE status='Active' ORDER BY destination ");
                                while($cntry_res = mysqli_fetch_assoc($cntry_qry)){
                                ?>
                                <option value="<?php echo $cntry_res['dest_id']; ?>" <?php if($row['country_id']==$cntry_res['dest_id']){ echo 'selected'; } ?>><?php echo $cntry_res['destination']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                                </div> 
                        <div class="col-md-6">
                        <div class="form-group city_box">
                            <label class="user_login_label" for="city_id">Select City*</label>
                            <select name="city_id" id="city_id" class="form-control form-select" required>
                                <option value="">Select City*</option>
                                <?php
                                if($row['country_id'] > 0){
                                $city_qry = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityStatus='1' && CountryId = '".$row['country_id']."' ORDER BY CityName ");
                                while($city_res = mysqli_fetch_assoc($city_qry)){
                                ?>
                                <option value="<?php echo $city_res['CityId']; ?>" <?php if($row['city_id']==$city_res['CityId']){ echo 'selected'; } ?>><?php echo $city_res['CityName']; ?></option>
                                <?php } } ?>
                            </select>
                            <p class="img_note">Note : If searched city doesn't have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="category_id">Select Category*</label>
                            <select name="category_id" id="category_id" class="form-control form-select" required>
                                <option value="">Select Category*</option>
                                <?php
                                $cat_qry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ORDER BY order_no, order_no=0 ");
                                while($cat_res = mysqli_fetch_assoc($cat_qry)){
                                ?>
                                <option value="<?php echo $cat_res['cat_id']; ?>" <?php if($row['category_id']==$cat_res['cat_id']){ echo 'selected'; } ?>><?php echo $cat_res['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                    
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script type="text/javascript">
    // Add Multiple Gallery Images
    $(document).ready(function(){
        var hotel_img_cnt = $('#gal_img_cnt').val();
        if(hotel_img_cnt!=''){
            var id = hotel_img_cnt; 
        } else {
            var id = 1;
        }
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_gal_img").on('click',function(){
        if($("div[id^='image_']").length < 11){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="col-md-6"><div id="image_'+id+'"><div class="form-group"><label class="user_login_label">Gallery Image '+id+':</label><div class="hotel-box"><input type="file" name="gallery_image[]" id="gallery_img_'+id+'" class="form-control form-control01"></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div><p class="img_note">Note : Hotel Image size approx 650 x 350</p></div></div>';
            $("#hotel_images > .row").append(append_data); //append new text box in main div
            $("#image_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 10 hotel gallery images are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_image(imgnum){
        $("#image_"+imgnum).parent().css('background','tomato');
        $("#image_"+imgnum).parent().fadeOut(800,function(){
           $("#image_"+imgnum).parent().remove();
        });
    }
    </script>

    <script>    
    // Add Multiple Room Type
    $(document).ready(function(){
        var hotel_room = $('#room_cnt').val();
        if(hotel_room!=''){
            var id = hotel_room; 
        } else {
            var id = 1;
        }
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_room").on('click',function(){
        if($("div[id^='room_']").length < 11){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="row" id="room_'+id+'"><div class="col-md-6"><div class="form-group"><label class="user_login_label">Room Type Name '+id+':</label><div class="hotel-box"><select name="room_type_id[]" id="room_type_id_'+id+'" class="form-control form-control01"><option value="">Please Select</option><?php $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' "); while($roomres = mysqli_fetch_assoc($qroom)){ ?><option value="<?php echo $roomres['room_type_id']; ?>"><?php echo $roomres['room_type_name']; ?></option><?php } ?></select></div></div></div><div class="col-md-6"><div class="form-group"><label class="user_login_label">Upgrade Room Cost '+id+':</label><div class="hotel-box"><input type="text" name="upgrade_room_cost[]" id="upgrade_room_cost_'+id+'" class="form-control form-control01"></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_room(this.id)" /></div></div></div></div>';
            $("#hotel_rooms").append(append_data); //append new text box in main div
            $("#room_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 10 hotel room type are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_room(roomnum){
        $("#room_"+roomnum).css('background','tomato');
        $("#room_"+roomnum).fadeOut(800,function(){
           $("#room_"+roomnum).remove();
        });
    }
    </script>

    <script>
    
    $(document).ready(function(){
        $('#country_id').on('change', function(){
            var cid = $(this).val();
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country_id: cid },
                
                success: function(response){
                    $('#city_id').empty();
                    $('#city_id').append(response);
                    $('#city_id').select2();
                }
            });
        });
        
    });
                    
    $('#city_id').select2();
        
    </script>
    
</body>

</html>