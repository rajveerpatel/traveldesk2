<?php 
    include('include.inc.php');

    $id = $_GET['id'];

$curr_date = date('Y-m-d');

$res = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_tirp_lead WHERE lid = $id "));

$trip = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = '".$res['trip_id']."' "));

if(!empty($_POST['email']) && !empty($_POST['mobile_number'])){

    //Trip Reference Code
    function reference_code($length_of_string) 
    { 
        $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'; 
        return substr(str_shuffle($str_result), 0, $length_of_string); 
    } 
    
    $new_reference_code = reference_code(8);
    
    $reference_code = $res['trip_reference_code'];
    $user_track_id = $res['user_track_id'];
    $mobile_number_code = $res['mobile_number_code'];
    $message = $res['message'];
    $requested_as = $res['requested_as'];
    $agent_id = $res['agent_id'];
    $page_url = $res['page_url'];
    
    $cat_val = explode('|', strtotime($_POST['cat_id']));
    $cat_id = $cat_val[0];
    
    $qry = "UPDATE tbl_tirp_lead SET 
    selected = '0',
    trip_reference_code = '$new_reference_code' 
    WHERE lid = $id ";
    mysqli_query($con, $qry);
    
    $q = "INSERT INTO tbl_tirp_lead SET 
    cat_id = '".$_POST['cat_id']."',
    trip_id = '".$_POST['trip_id']."',
    trip_name = '".$_POST['trip_name']."',
    trip_days = '".$_POST['trip_days']."',
    user_track_id = '$user_track_id',
    trip_reference_code = '$reference_code',
    updated_by = '".$_SESSION['AdminID']."',
    selected = '1',
    first_name = '".$_POST['first_name']."',
    last_name = '".$_POST['last_name']."',
    email = '".$_POST['email']."',
    mobile_number = '".$_POST['mobile_number']."',
    mobile_number_code = '".$mobile_number_code."',
    message = '".$message."',
    no_of_passenger = '".$_POST['no_of_passenger']."',
    trip_start_date = '".date('Y-m-d', strtotime($_POST['available_date']))."',
    room_configuration = '".$_POST['room_config_text']."',
    room_config_value = '".$_POST['room_config_val']."',
    cost_pp_twin_sharing = '".$_POST['cost_pp_twin_sharing']."',
    total_trip_cost = '".$_POST['total_trip_cost']."',
    requested_as = '".$requested_as."',
    agent_id = '".$agent_id."',
    trip_for = '".$_POST['trip_for']."',
    extra_flight_cost = '".$_POST['extra_flight_cost']."',
    page_url = '".$page_url."',
    ip_address = '".$_SERVER['REMOTE_ADDR']."',
    post_date = NOW() ";
    mysqli_query($con, $q);
    
    $lead_id = mysqli_insert_id($con);
        
    header('location:trip-enquiries.php');
}

$pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Trip Logs
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-logs', $_SESSION['AccessRights'])){
            $logs_display = "";
        } else {
            $logs_display = "display: none;";
        }

        //Trip Enquiries
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-enquiries', $_SESSION['AccessRights'])){
            $enq_display = "";
        } else {
            $enq_display = "display: none;";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Trip Enquiry - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Trip Enquiry</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="trip-enquiries.php">Trip Enquiry List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="trip_name">Trip Name</label>
                            <input type="text" name="trip_name" id="trip_name" class="form-control form-control01" value="<?php echo $res['trip_name']; ?>" readonly>
                            <input type="hidden" name="trip_id" id="trip_id" value="<?php echo $res['trip_id']; ?>">
                            <input type="hidden" name="trip_for" id="trip_for" value="<?php echo $res['trip_for']; ?>">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="category_name">Trip Category</label>
                            <select class="form-control form-select trip_category" name="cat_id" id="cat_id" required="#">
                            <?php
                            $trip_cat_ids = array();
                            $trip_cat_q = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE slug = '".$trip['slug']."' ");
                            while($trip_cat_res = mysqli_fetch_assoc($trip_cat_q)){
                                $trip_cat_ids[] = $trip_cat_res['trip_category'];   
                            }
                            $trip_cat_ids = implode(',', $trip_cat_ids);

                            $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' AND FIND_IN_SET(cat_id, '$trip_cat_ids') ORDER BY order_no, order_no=0 ");
                            while($catres = mysqli_fetch_assoc($catqry)){
                            ?>
                                <option value="<?php echo $catres['cat_id'].'|'.$trip['slug']; ?>" <?php if($catres['cat_id']==$res['cat_id']){ echo 'selected'; } ?> ><?php echo $catres['category_name']; ?></option> 
                            <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="trip_reference_code">Trip Reference Code</label>
                            <input type="text" name="trip_reference_code" id="trip_reference_code" class="form-control form-control01" value="<?php echo $res['trip_reference_code']; ?>" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control form-control01" value="<?php echo $res['first_name']; ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control form-control01" value="<?php echo $res['last_name']; ?>">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control form-control01" value="<?php echo $res['email']; ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="mobile_number">Mobile Number</label>
                            <input type="text" name="mobile_number" id="mobile_number" class="form-control form-control01" value="<?php echo $res['mobile_number']; ?>">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="trip_days">Trip Duration</label>
                            <input type="text" name="trip_days" id="trip_days" class="form-control form-control01" value="<?php echo $res['trip_days']; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="no_of_passenger">No of Passenger</label>
                            <select name="no_of_passenger" id="no_of_adult" class="form-control form-control01">
                                <?php for($i=1; $i<=9; $i++){ ?>
                                <option value="<?php echo $i; ?>" <?php if($i==$res['no_of_passenger']){ echo 'selected'; } ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="available_date">Available Dates</label>
                            <select name="available_date" id="available_date" class="form-control form-select">
                                <?php
                                $date_qry = mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_styles = 1 AND trip_id = '".$res['trip_id']."' AND trip_categories = '".$res['cat_id']."' AND '$curr_date' < trip_start_date  ");
                                while($dateres = mysqli_fetch_assoc($date_qry)){
                                ?>
                                <option value="<?php echo $dateres['trip_start_date']; ?>" <?php if($dateres['trip_start_date']==$res['trip_start_date']){ echo 'selected'; } ?>><?php echo date('M d, Y', strtotime($dateres['trip_start_date'])).' &emsp; ($'.$dateres['land_twin_sharing'].')'; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3">Trip For</label>
                            <div>
                                <input type="radio" name="trip_package_for" class="price-type-radio" id="land" value="land" <?php if($res['trip_for']=='land'){ echo 'checked'; } ?>> <label class="user_login_label" for="land">Land Only</label> &nbsp;&nbsp;
                                <input type="radio" name="trip_package_for" class="price-type-radio" id="airland" value="airland" <?php if($res['trip_for']=='airland'){ echo 'checked'; } ?>> <label class="user_login_label" for="airland">Air & Land</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="room_config">Room Configuration</label>
                            <select class="form-control form-control01" id="room_config" name="room_config" required>
                                <option value="2-twin_sharing">1 Room, Twin Sharing</option>
                                <option value="2-single_room">2 Room, Single Room</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="cost_pp_twin_sharing">Cost PP Twin Sharing ($)</label>
                            <input type="text" name="cost_pp_twin_sharing" id="cost_pp_twin_sharing" class="form-control form-control01" value="<?php echo $res['cost_pp_twin_sharing']; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="total_trip_cost">Total Trip Cost ($)</label>
                            <input type="text" name="total_trip_cost" id="total_trip_cost" class="form-control form-control01" value="<?php echo $res['total_trip_cost']; ?>" readonly>
                        </div>
                    </div>
                    
                    <div id="rooms_price"></div>
                    
                    <input type="hidden" name="room_config_val" id="room_config_val" value="<?php echo $res['room_config_value']; ?>">
                    <input type="hidden" name="room_config_text" id="room_config_text" value="<?php echo $res['room_configuration']; ?>">
                    <input type="hidden" name="extra_flight_cost" id="extra_flight_cost" value="<?php echo $res['extra_flight_cost']; ?>">
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

    $('.trip_category').on('change', function(){
            
        var cat_val = $(this).val();
        var cat_val = cat_val.split('|');
        var cat_id = cat_val[0];
        var trip_slug = cat_val[1];
        var date_act = 'date_act';
        var get_trip_id = 'get_trip_id';
        var room_cost_act = 'room_cost_act';

        var no_of_adult = $('#no_of_adult').val();

        var available_date = $('#available_date option:selected').val();

        var room_config_val = $('#room_config option:selected').val();
        var room_config_text = $('#room_config option:selected').text();

        var trip_for = $('#trip_for').val();

        // get trip id on catgory change
        $.ajax({
            url:"ajax_get_trip_details.php",
            method:"POST",	
            async: false,
            data:{cat_id: cat_id, trip_slug: trip_slug, get_trip_id: get_trip_id},
            success:function(response){
                $('#trip_id').val(response);
            }
        });

        // get trip room price on category change
        $.ajax({
            url:"ajax_get_trip_details.php",
            method:"POST",	
            async: false,
            data:{cat_id: cat_id, trip_slug: trip_slug, available_date: available_date, room_cost_act: room_cost_act},
            success:function(response){
                $('#rooms_price').empty();
                $('#rooms_price').html(response);
            }
        });
        
        // get available dates on catgory change
        var prev_date = $('#available_date option:selected').val();
        $.ajax({
            url:"ajax_get_trip_details.php",
            method:"POST",	
            data:{cat_id: cat_id, trip_slug: trip_slug, date_act: date_act, prev_date: prev_date, price_type: trip_for},
            success:function(response){
                $('#available_date').empty();
                $('#available_date').html(response);

                var selected_date = $('#available_date').val();

                var format_selcted_date = new Date(selected_date);

                $('#trip_start_date').val(selected_date);
                $('#available_select_date').val(formatDate(format_selcted_date));
            }
        });
        
        var extra_cost_flight = $('#extra_flight_cost').val();
        
        // get Price on catgory change
        var room_arr = [];
        room_arr = room_config_val.split('|');
        if(room_arr.length > 0){
            var total_cost = 0;

            for(i=0; i<room_arr.length; i++){
                var room = room_arr[i];
                var room = room.split('-');
                var no_of_pax = room[0];
                var room_type = room[1];
                var room_type_name = room_type.split('_');
                var room_name = room_type_name[0]+' '+room_type_name[1];
                var room_price = $('#'+trip_for+'_'+room_type).val();
                
                var trip_cost = parseInt(no_of_pax) * parseInt(room_price);
                
                if(trip_for == 'airland'){
                    var total_extra_cost = parseInt(no_of_pax) * parseInt(extra_cost_flight);
                } else {
                    var total_extra_cost = 0;
                }
                var total_cost = parseInt(total_cost) + parseInt(trip_cost) + parseInt(total_extra_cost);
                
            }
            
            /*if(trip_for=='land'){
                var cost_pp = $('#'+trip_for+'_twin_sharing').val();
                $('#land_cost_pp').text('$'+cost_pp);
                $('#airland_cost_pp').text('');
            } else if(trip_for=="airland"){
                var cost_pp = $('#'+trip_for+'_twin_sharing').val();
                cost_pp = parseInt(cost_pp) + parseInt(extra_cost_flight);
                $('#airland_cost_pp').text('$'+cost_pp);
                $('#land_cost_pp').text('');
            }*/

            var land_cost_pp = $('#land_twin_sharing').val();
            $('#cost_pp_twin_sharing').val(land_cost_pp);
            if(trip_for == 'airland'){
                var airland_cost_pp = $('#airland_twin_sharing').val();
                airland_cost_pp = parseInt(airland_cost_pp) + parseInt(extra_cost_flight);
                $('#cost_pp_twin_sharing').val(airland_cost_pp);
            }
            
            $('#total_trip_cost').val(total_cost);
        }
    });
    </script>

    <script>        
    $(document).ready(function(){
        var available_date = $('#available_date option:selected').val();
        
        var cat_val = $('.trip_category').val();
        var cat_val = cat_val.split('|');
        var cat_id = cat_val[0];

        var trip_id = $('#trip_id').val();
        
        var room_acc = 'room_acc';
        
        $.ajax({
            type: 'POST',
            url: 'ajax_get_price_available_date.php',
            data: {trip_id: trip_id, cat_id: cat_id, available_date: available_date, room_acc: room_acc},
            async: false,
            success: function(response){
                $('#rooms_price').empty();
                $('#rooms_price').html(response);
            }
        });
        
        var total_pax = $('#no_of_adult option:selected').val();
        
        var room_config_val = $('#room_config_val').val();
        
        var room_option = '';
        //alert(total_pax);
        if(total_pax == 1){
            if(room_config_val=='1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="1-single_room" '+selected+'>1 Room, Single Room</option>';
        }

        if(total_pax == 2){
            if(room_config_val=='2-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="2-twin_sharing" '+selected+'>1 Room, Twin Sharing</option>';
            if(room_config_val=='2-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="2-single_room" '+selected+'>2 Room, Single Room</option>';
        }

        if(total_pax == 3){
            if(room_config_val=='3-triple_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing" '+selected+'>1 Room, Triple Sharing</option>';
            if(room_config_val=='2-twin_sharing|1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="2-twin_sharing|1-single_room" '+selected+'>1 Room, Twin Sharing & 1 Room, Single Room</option>';
            if(room_config_val=='3-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-single_room" '+selected+'>3 Room, Single Room</option>';
        }

        if(total_pax == 4){
            if(room_config_val=='4-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="4-twin_sharing" '+selected+'>2 Room, Twin Sharing</option>';
            if(room_config_val=='3-triple_sharing|1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing|1-single_room" '+selected+'>1 Room, Triple Sharing & 1 Room, Single Room</option>';
            if(room_config_val=='2-twin_sharing|2-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="2-twin_sharing|2-single_room" '+selected+'>1 Room, Twin Sharing & 2 Room, Single Room</option>';
        }

        if(total_pax == 5){
            if(room_config_val=='3-triple_sharing|2-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing|2-twin_sharing" '+selected+'>1 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
            if(room_config_val=='4-twin_sharing|1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="4-twin_sharing|1-single_room" '+selected+'>2 Room, Twin Sharing & 1 Room, Single Room</option>';
            if(room_config_val=='3-triple_sharing|2-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing|2-single_room" '+selected+'>1 Room, Triple Sharing & 2 Room, Single Room</option>';
        }

        if(total_pax == 6){
            if(room_config_val=='6-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="6-twin_sharing" '+selected+'>3 Room, Twin Sharing</option>';
            if(room_config_val=='6-triple_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="6-triple_sharing" '+selected+'>2 Room, Triple Sharing</option>';
            if(room_config_val=='3-triple_sharing|2-twin_sharing|1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing|2-twin_sharing|1-single_room" '+selected+'>1 Room, Triple Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
        }

        if(total_pax == 7){
            if(room_config_val=='3-triple_sharing|4-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="3-triple_sharing|4-twin_sharing" '+selected+'>1 Room, Triple Sharing & 2 Room, Twin Sharing</option>';
            if(room_config_val=='6-triple_sharing|1-single_room'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="6-triple_sharing|1-single_room" '+selected+'>2 Room, Triple Sharing & 1 Room, Single Room</option>';
        }

        if(total_pax == 8){
            if(room_config_val=='6-triple_sharing|2-twin_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="6-triple_sharing|2-twin_sharing" '+selected+'>2 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
        }

        if(total_pax == 9){
            if(room_config_val=='9-triple_sharing'){ var selected = 'selected'; } else { var selected = ''; }
            room_option += '<option value="9-triple_sharing" '+selected+'>3 Room, Triple Sharing</option>';
        }

        $('#room_config').empty();
        $('#room_config').append(room_option);
    });
    </script>

    <script>
        
    $('#available_date').on('change', function(){
        var selected_date = $(this).val();
        var selected_date_text = $("#available_date option:selected").text();
        
        var cat_val = $('.trip_category option:selected').val();
        var cat_val = cat_val.split('|');
        var cat_id = cat_val[0];
        var trip_slug = cat_val[1];
        var trip_slug = cat_val[1];

        var price_type = $('#trip_for').val();

        var extra_cost_flight = $('#extra_flight_cost').val();

        var trip_id = $('#trip_id').val();
        var room_acc = 'room_acc';
        $.ajax({
            type: 'POST',
            url: 'ajax_get_price_available_date.php',
            data: {trip_id: trip_id, cat_id: cat_id, available_date: selected_date, room_acc: room_acc},
            async: false,
            success: function(response){
                $('#rooms_price').empty();
                $('#rooms_price').html(response);
            }
        });


        var selected_room = $('#room_config option:selected').val();

        var room_arr = [];

        //var grand_total = $('#grand_total').val();

        room_arr = selected_room.split('|');            
        if(room_arr.length > 0){
            var total_cost = 0;

            for(i=0; i<room_arr.length; i++){                    
                var room = room_arr[i];
                var room = room.split('-');
                var no_of_pax = room[0];
                var room_type = room[1];
                var room_price = $('#'+price_type+'_'+room_type).val();

                var trip_cost = parseInt(no_of_pax) * parseInt(room_price);
                if(trip_for == 'airland'){
                    var total_extra_cost = parseInt(no_of_pax) * parseInt(extra_cost_flight);
                } else {
                    var total_extra_cost = 0;
                }
                var total_cost = parseInt(total_cost) + parseInt(trip_cost) + parseInt(total_extra_cost);
            }

            var land_cost_pp = $('#land_twin_sharing').val();
            $('#cost_pp_twin_sharing').val(land_cost_pp);
            if(extra_cost_flight > 0){
                var airland_cost_pp = $('#airland_twin_sharing').val();
                airland_cost_pp = parseInt(airland_cost_pp) + parseInt(extra_cost_flight);
                $('#cost_pp_twin_sharing').val(airland_cost_pp);
            }

            $('#total_trip_cost').val('');
            $('#total_trip_cost').val(total_cost);
        }
    });
    </script>

    <script>
    $('#room_config').change(function(){
        var selected_room = $(this).val();
        var selected_room_text = $('#room_config option:selected').text();

        //var grand_total = $('#grand_total').val();

        var price_type = $('#trip_for').val();

        var extra_cost_flight = $('#extra_flight_cost').val();

        // get trip price and break
        var room_arr = [];
        room_arr = selected_room.split('|');
        if(room_arr.length > 0){
            var total_cost = 0;

            for(i=0; i<room_arr.length; i++){
                var room = room_arr[i];
                var room = room.split('-');
                var no_of_pax = room[0];
                var room_type = room[1];
                var room_type_name = room_type.split('_');
                var room_name = room_type_name[0]+' '+room_type_name[1];
                var room_price = $('#'+price_type+'_'+room_type).val();

                var trip_cost = parseInt(no_of_pax) * parseInt(room_price);                    
                if(trip_for == 'airland'){
                    var total_extra_cost = parseInt(extra_cost_flight) * parseInt(no_of_pax);
                } else {
                    var total_extra_cost = 0;
                }
                var total_cost = parseInt(total_cost) + parseInt(trip_cost) + parseInt(total_extra_cost);
            }
            
            var land_cost_pp = $('#land_twin_sharing').val();
            $('#cost_pp_twin_sharing').val(land_cost_pp);
            if(extra_cost_flight > 0){
                var airland_cost_pp = $('#airland_twin_sharing').val();
                airland_cost_pp = parseInt(airland_cost_pp) + parseInt(extra_cost_flight);
                $('#cost_pp_twin_sharing').val(airland_cost_pp);
            }

            $('#total_trip_cost').val('');
            $('#total_trip_cost').val(total_cost);
        }
            
        $('#room_config_val').val(selected_room);
        $('#room_config_text').val(selected_room_text);

    });
    </script>

    <script>
    $( ".price-type-radio" ).click(function(){ 
        var trip_for = $(this).val();
        $('#trip_for').val(trip_for);
        
        var available_date = $('#available_date option:selected').val();
        var date_act = 'date_act';

        var no_of_adults = $('#no_of_adult option:selected').val();

        var selected_room = $('#room_config option:selected').val();
        var selected_room_text = $('#room_config option:selected').text();

        var trip_id = $('#trip_id').val();
        
        var cat_val = $('.trip_category option:selected').val();
        var cat_val = cat_val.split('|');
        var cat_id = cat_val[0];
        var trip_slug = cat_val[1];

        // get available dates on air & land cost
        $.ajax({
            url:"ajax_get_trip_details.php",
            method:"POST",	
            data:{cat_id: cat_id, trip_id: trip_id, date_act: date_act, prev_date: available_date, price_type: trip_for},
            success:function(response){
                $('#available_date').empty();
                $('#available_date').html(response);
            }
        });

        var extra_cost_flight = $('#extra_flight_cost').val();

        var room_acc = 'room_acc';            
        var room_arr = [];
        var room_price_pp = 0;
        var total_amount = 0;
        $.ajax({
            type: 'POST',
            url: 'ajax_get_price_available_date.php',
            data: {trip_id: trip_id, cat_id: cat_id, available_date: available_date, room_acc: room_acc},
            async: false,
            success: function(response){
                $('#rooms_price').empty();
                $('#rooms_price').html(response);
            }
        });

        room_arr = selected_room.split('|');

        for(i=0; i<room_arr.length; i++){
            var room_detail = room_arr[i].split('-');
            var passenger = room_detail[0];
            var room_price = $('#'+trip_for+'_'+room_detail[1]).val();
            room_price_pp = parseInt(room_price_pp) + parseInt(room_price);
            var total_cost = parseInt(room_price) * parseInt(passenger);
            
            if(trip_for == 'airland'){
                var total_extra_cost = parseInt(extra_cost_flight) * parseInt(passenger);
            } else {
                var total_extra_cost = 0;
            }
            total_amount = parseInt(total_amount) + parseInt(total_cost) + parseInt(total_extra_cost);
        }

        var land_cost_pp = $('#land_twin_sharing').val();
        
        $('#cost_pp_twin_sharing').val(land_cost_pp);
        if(trip_for == 'airland'){
            var airland_cost_pp = $('#airland_twin_sharing').val();
            airland_cost_pp = parseInt(airland_cost_pp) + parseInt(extra_cost_flight);
            $('#cost_pp_twin_sharing').val(airland_cost_pp);
        }

        $('#total_trip_cost').val('');
        $('#total_trip_cost').val(total_amount);
    });
    </script>
        
    <script>
    $( function() {
        $( ".date-type" ).datepicker({ dateFormat: 'M dd, yy' });
    });
    </script>

    <script>
    /*function calc_cost(){
        var no_of_adult = $('#no_of_adults').val();
        var no_of_child = $('#no_of_childrens').val();
        var adult_pp_cost = $('#adult_pp_cost').val();
        var children_pp_cost = $('#children_pp_cost').val();
        
        var total_opt_cost_pp = $('#total_opt_cost_pp').val();
        var total_opt_cost = $('#total_opt_cost').val();
        
        alert(total_opt_cost);
        
        var trip_actual_cost = $('#trip_actual_cost').val();
        var total_trip_cost = $('#total_trip_cost').val();
        
        var total_pre_ext_trip_cost = $('#total_pre_ext_trip_cost').val();
        var total_post_ext_trip_cost = $('#total_post_ext_trip_cost').val();
        
        var adult_total_cost = parseInt(no_of_adult) * parseInt(adult_pp_cost);
        var child_total_cost = parseInt(no_of_child) * parseInt(children_pp_cost);
        var total_passenger = parseInt(no_of_adult) + parseInt(no_of_child);
        var total_pax_opt_cost = parseInt(total_opt_cost_pp) * parseInt(total_passenger);
        
        var pre_ext_trip_cost = $('#pre_ext_trip_cost').val();
        var post_ext_trip_cost = $('#post_ext_trip_cost').val();
        
        var trip_pre_ext_child_cost = parseInt(pre_ext_trip_cost) * parseInt(75) / parseInt(100);
        var adult_total_pre_ext_cost = parseInt(pre_ext_trip_cost) * parseInt(no_of_adult);
        var child_total_pre_ext_cost = parseInt(trip_pre_ext_child_cost) * parseInt(no_of_child);
        var total_trip_pre_ext_cost = parseInt(adult_total_pre_ext_cost) + parseInt(child_total_pre_ext_cost);
        
        var trip_post_ext_child_cost = parseInt(post_ext_trip_cost) * parseInt(75) / parseInt(100);
        var adult_total_post_ext_cost = parseInt(post_ext_trip_cost) * parseInt(no_of_adult);
        var child_total_post_ext_cost = parseInt(trip_post_ext_child_cost) * parseInt(no_of_child);
        var total_trip_post_ext_cost = parseInt(adult_total_post_ext_cost) + parseInt(child_total_post_ext_cost);
        
        var sum_trip_cost = parseInt(adult_total_cost) + parseInt(child_total_cost);
        var trip_cost_without_basic = parseInt(total_trip_cost) - parseInt(trip_actual_cost) - parseInt(total_opt_cost) - parseInt(total_pre_ext_trip_cost) - parseInt(total_post_ext_trip_cost);
        var trip_cost_incl = parseInt(trip_cost_without_basic) + parseInt(sum_trip_cost) + parseInt(total_pax_opt_cost) + parseInt(total_trip_pre_ext_cost) + parseInt(total_trip_post_ext_cost);
        
        $('#total_trip_cost').val(trip_cost_incl);
        $('.total_trip_cost').html(trip_cost_incl);
        $('#trip_actual_cost').val(sum_trip_cost);
        
        $('#total_opt_cost').val(total_pax_opt_cost);
        
        $('#total_pre_ext_trip_cost').val(total_trip_pre_ext_cost);
        $('#total_post_ext_trip_cost').val(total_trip_post_ext_cost);
    }*/
    </script>
    
</body>

</html>