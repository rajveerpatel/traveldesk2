<?php 
    include('include.inc.php');
    $pagename = explode('.',$page);
    $page_name = $pagename[0];
    
    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Deals Trip Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-deals-packages', $_SESSION['AccessRights'])){
            $deal_display = "";
        } else {
            $deal_display = "display: none;";
        }

        //Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-delete', $_SESSION['AccessRights'])){
            $del_display = "";
        } else {
            $del_display = "display: none;";
        }

        //Duplicate Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-duplicate', $_SESSION['AccessRights'])){
            $dup_display = "";
        } else {
            $dup_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-package-add', $_SESSION['AccessRights'])){
            $add_display = "";
        } else {
            $add_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-package-edit', $_SESSION['AccessRights'])){
            $edit_display = "";
        } else {
            $edit_display = "display: none;";
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Trip Packages - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Trip Packages</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Trip Packages</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                                <p align="right"><a class="btn btn-primary text-white" href="trip-package-add.php" role="button">Add Trip Packages</a></p>
                                
                                <div class="table-responsive m-t-20">
                                    <table id="config-table" class="table display table-striped no-wrap m-t-20">
                                        <thead>
                                            <tr>
                                            <th>Order</th>
                                            <<th>Package</th>
                                            <th>Duration</th>
                                            <th>Destination</th>
                                            <th>Trip Category</th>
                                            <th>Trip Types</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            <th>&nbsp;</th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                        $query=mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE deal = '' ORDER BY order_no=0, order_no ");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                while( $row=mysqli_fetch_assoc($query) ){ 
                                    $trip_theme = $row['trip_themes'];
                                    $trip_cat = $row['trip_category'];
                        ?>
                                            <tr>
                             <td><input type="text" class="order_no" name="<?php echo $row['trip_id'];?>" size="3" value="<?php echo $row['order_no'];?>"></td>
                             <td><?php echo $row['trip_name']; ?></td>
                             <td><?php echo $row['trip_days'].' '.'Days'; ?></td>
                             <td><?php $destination = explode(':', $row['country_dest']);  echo $destination[1]; ?></td>
                             <td>
                                <?php
                                $catres = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' AND cat_id = $trip_cat "));
                                echo $catres['category_name'];
                                ?>
                             </td>
                             <td>
                                <?php
                                $theme = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'Active' AND trip_type_id = $trip_theme "));
                                echo $theme['trip_type'];
                                ?>
                             </td>
                             <td class="dropdown-listing">
                                <select class="form-control select-a" id="status_<?php echo $row['trip_id']; ?>">
                                    <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                    <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                                </select>
                             </td>
                             <td><div class="btn-group"><a class="btn bg-navy" style="<?php echo $edit_display; ?>" href="trip-package-edit.php?id=<?php echo $row['trip_id'];?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-navy" style="<?php echo $dup_display; ?>" href="trip-duplicate.php?id=<?php echo $row['trip_id'];?>" title="Duplicate"><i class="fa fa-clone"></i></a> <a class="btn bg-red delete" style="<?php echo $del_display; ?>" id="<?php echo $row['trip_id'];?>" title="Delete"><i class="fa fa-trash"></i></a> <button class="btn btn-primary text-white" id="<?php echo $row['trip_id']; ?>">Update</button></div></td>
                             <td>&nbsp;</td> 
                            </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script>
   
$(document).ready(function(){

    // Delete 
    $('.delete').click(function(){
       var el = this;

       // Delete id
       var deleteid = $(this).attr('id');

       var confirmalert = confirm("Are you sure want to delete?");
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'delete-record.php',
            type: 'POST',
            data: { trip_id:deleteid },
            success: function(response){

            if(response == 1){
            // Remove row from HTML Table
            $(el).closest('tr').css('background','tomato');
            $(el).closest('tr').fadeOut(800,function(){
               $(this).remove();
            });
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
    

// Update Status    
$(document).ready(function(){

    // Delete 
    $('.change_status').click(function(){
       var el = this;

       // Delete id
       var statusid = $(this).attr('id');
       var status_title = $(this).attr('title');

       if( $(this).attr('title')=='Active' ){
            var confirmalert = confirm("Are you sure want to Active?");
       } else if( $(this).attr('title')=='Inactive' ){
            var confirmalert = confirm("Are you sure want to Inactive?");
       }
        
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'status-update.php',
            type: 'POST',
            data: { trip_id:statusid, trip_title:status_title },
            success: function(response){

            if(response){
                $('#status_'+statusid).empty();
                $('#status_'+statusid).html(response);
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
</script>

<script>
    $(document).ready(function(){
        $('.btn-update').click(function(){
           var el = this;
           var packid = $(this).attr("id");
           /*var catid = $("#trip_categories_"+packid).val();
           var types_id = $("#trip_types_"+packid).val();*/
           var status = $("#status_"+packid).val();
            
           var confirmalert = confirm("Are you sure want to update record?");
           if (confirmalert == true) {
              // AJAX Request
              $.ajax({
                url: 'ajax-update-trip.php',
                type: 'POST',
                data: { trip_id:packid, status:status },
                success: function(response){

                    if(response){
                        $(el).closest('tr > td').css('border-bottom','2px solid green');
                        setTimeout(function () {
                            alert('Record Updated');
                            $(el).closest('tr > td').css('border-bottom', 'none');
                        },1000);
                    }else{
                        alert('Invalid ID.');
                    }

                }
              });
           }
        });
    });
</script>
  
<script>
    jQuery(document).ready(function(){
        $('.order_no').blur(function(){
            //$.each($('.order_no'), function(){
                var el = this;
                var tripid = $(el).attr('name');
                var orderno = $(el).val();
                
                $.ajax({
                    url: 'ajax_order_update.php' ,
                    type: 'POST',
                    data: {trip_id: tripid, order_no: orderno},
                    success: function(response){
                        $(el).closest('tr > td').css('border-bottom','2px solid green');
                        setTimeout(function () {
                            $(el).closest('tr > td').css('border-bottom', 'none');
                        },300);
                    }
                });
            //});
        }) 
    });
</script>

<script>
    $(function () {
        $('.trip_types').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            buttonText: function (options) {
            if (options.length == 0) {
                 return 'None selected';
                } else {
                 var selected = 0;
                 options.each(function () {
                     selected += 1;
                 });
                 return selected +  ' Selected';
                }
            }
        });
    });
    
    $(function () {
        $('.trip_categories').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            buttonText: function (options) {
            if (options.length == 0) {
                 return 'None selected';
                } else {
                 var selected = 0;
                 options.each(function () {
                     selected += 1;
                 });
                 return selected +  ' Selected';
                }
            }
        });
    });
</script>
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 100,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary me-1');
        });

    </script>
</body>

</html>