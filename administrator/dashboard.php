<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <!-- Favicon icon -->

    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">

    <title>Dashboard - TravDek</title>

    <!-- This page CSS -->

    <!-- chartist CSS -->

    <link href="assets/node_modules/morrisjs/morris.css" rel="stylesheet">

    <!--Toaster Popup message CSS -->

    <link href="assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="dist/css/style.css" rel="stylesheet">

    <!-- Dashboard 1 Page CSS -->

    <link href="dist/css/pages/dashboard1.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<![endif]-->

</head>



<body class="skin-blue fixed-layout">

    <!-- ============================================================== -->

    <!-- Preloader - style you can find in spinners.css -->

    <!-- ============================================================== -->

    <div class="preloader">

        <div class="loader">

            <div class="loader__figure"></div>

            <p class="loader__label">Trav Dek</p>

        </div>

    </div>

    <!-- ============================================================== -->

    <!-- Main wrapper - style you can find in pages.scss -->

    <!-- ============================================================== -->

    <div id="main-wrapper">

        <!-- ============================================================== -->

        <!-- Topbar header - style you can find in pages.scss -->

        <!-- ============================================================== -->

        <header class="topbar">

            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <!-- ============================================================== -->

                <!-- Logo -->

                <!-- ============================================================== -->

                <div class="navbar-header">

                <?php include("elements/lefttop.php"); ?>

                </div>

                <!-- ============================================================== -->

                <!-- End Logo -->

                <!-- ============================================================== -->

                <div class="navbar-collapse">

                <?php include("elements/header.php"); ?>

                </div>

            </nav>

        </header>

        <!-- ============================================================== -->

        <!-- End Topbar header -->

        <!-- ============================================================== -->

        <!-- ============================================================== -->

        <!-- Left Sidebar - style you can find in sidebar.scss  -->

        <!-- ============================================================== -->

        <aside class="left-sidebar">

            <!-- Sidebar scroll-->

            <?php include("elements/head.php"); ?>

            <!-- End Sidebar scroll-->

        </aside>

        <!-- ============================================================== -->

        <!-- End Left Sidebar - style you can find in sidebar.scss  -->

        <!-- ============================================================== -->

        <!-- ============================================================== -->

        <!-- Page wrapper  -->

        <!-- ============================================================== -->

        <div class="page-wrapper">

            <!-- ============================================================== -->

            <!-- Container fluid  -->

            <!-- ============================================================== -->

            <div class="container-fluid">

                <!-- ============================================================== -->

                <!-- Bread crumb and right sidebar toggle -->

                <!-- ============================================================== -->

                <div class="row page-titles">

                    <div class="col-md-5 align-self-center">

                        <h4 class="text-themecolor">Dashboard </h4>

                    </div>

                    <div class="col-md-7 align-self-center text-end">

                        <div class="d-flex justify-content-end align-items-center">

                            <ol class="breadcrumb justify-content-end">

                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>

                                <li class="breadcrumb-item active">Dashboard </li>

                            </ol>

                            

                        </div>

                    </div>

                </div>

                <!-- ============================================================== -->

                <!-- End Bread crumb and right sidebar toggle -->

                <!-- ============================================================== -->

                <!-- ============================================================== -->

                <!-- Info box -->

                <!-- ============================================================== -->

                <div class="row g-25">

					<div class="col-lg-3 col-md-6">

						<div class="card booking">

							<div class="card-body">

								<div class="row">

									<div class="col-md-12">

										<div class="d-flex no-block align-items-center">

											<div>

												<h3><i class="icon-screen-desktop"></i></h3>

												<p class="text-muted">TOTAL INQUIRY</p>

											</div>

											<div class="ms-auto">

												<h2 class="counter text-primary">23</h2>

											</div>

										</div>

									</div>

									<div class="col-12">

										<div class="progress">

											<div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>



					<div class="col-lg-3 col-md-6">

						<div class="card booking">

							<div class="card-body">

								<div class="row">

									<div class="col-md-12">

										<div class="d-flex no-block align-items-center">

											<div>

												<h3><i class="icon-note"></i></h3>

												<p class="text-muted">TOUR PACKAGES</p>

											</div>

											<div class="ms-auto">

												<h2 class="counter text-cyan">169</h2>

											</div>

										</div>

									</div>

									<div class="col-12">

										<div class="progress">

											<div class="progress-bar bg-cyan" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>



					<div class="col-lg-3 col-md-6">

						<div class="card booking">

							<div class="card-body">

								<div class="row">

									<div class="col-md-12">

										<div class="d-flex no-block align-items-center">

											<div>

												<h3><i class="icon-doc"></i></h3>

												<p class="text-muted">TOTAL AGENTS</p>

											</div>

											<div class="ms-auto">

												<h2 class="counter text-purple">157</h2>

											</div>

										</div>

									</div>

									<div class="col-12">

										<div class="progress">

											<div class="progress-bar bg-purple" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>



					<div class="col-lg-3 col-md-6">

						<div class="card booking">

							<div class="card-body">

								<div class="row">

									<div class="col-md-12">

										<div class="d-flex no-block align-items-center">

											<div>

												<h3><i class="icon-bag"></i></h3>

												<p class="text-muted">TOTAL USERS</p>

											</div>

											<div class="ms-auto">

												<h2 class="counter text-success">431</h2>

											</div>

										</div>

									</div>

									<div class="col-12">

										<div class="progress">

											<div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

                <!-- ============================================================== -->

                <!-- End Info box -->

                <!-- ============================================================== -->

                <!-- ============================================================== -->

                <!-- Over Visitor, Our income , slaes different and  sales prediction -->

                <!-- ============================================================== -->

                <div class="row g-20" style="margin-top:20px;">

                    <!-- Column -->

                    <div class="col-lg-8 col-md-12">

                        <div class="card">

                            <div class="card-body">

                                <div class="d-flex m-b-40 align-items-center no-block">

                                    <h5 class="card-title ">YEARLY SALES</h5>

                                    <div class="ms-auto">

                                        <ul class="list-inline font-12">

                                            <li><i class="fa fa-circle text-cyan"></i> Iphone</li>

                                            <li><i class="fa fa-circle text-primary"></i> Ipad</li>

                                            <li><i class="fa fa-circle text-purple"></i> Ipod</li>

                                        </ul>

                                    </div>

                                </div>

                                <div id="morris-area-chart" style="height: 340px;"></div>

                            </div>

                        </div>

                    </div>

                    <!-- Column -->

                    <div class="col-lg-4 col-md-12">

                        <div class="row">

                            <!-- Column -->

                            <div class="col-md-12">

                                <div class="card bg-cyan text-white">

                                    <div class="card-body ">

                                        <div class="row weather">

                                            <div class="col-6 m-t-40">

                                                <h3>&nbsp;</h3>

                                                <div class="display-4">73<sup>°F</sup></div>

                                                <p class="text-white">AHMEDABAD, INDIA</p>

                                            </div>

                                            <div class="col-6 text-end">

                                                <h1 class="m-b-"><i class="wi wi-day-cloudy-high"></i></h1>

                                                <b class="text-white">SUNNEY DAY</b>

                                                <p class="op-5">April 14</p>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!-- Column -->

                            <div class="col-md-12">

                                <div class="card bg-primary text-white">

                                    <div class="card-body">

                                        <div id="myCarouse2" class="carousel slide" data-bs-ride="carousel">

                                            <!-- Carousel items -->

                                            <div class="carousel-inner">

                                                <div class="carousel-item active">

                                                    <h4 class="cmin-height">Amazing Experienace <span class="font-medium">Travelling</span> over the world.<br/>happy to join TravDek</h4>

                                                    <div class="d-flex no-block">

                                                        <span><img src="assets/images/users/2.jpg" alt="user" width="50" class="img-circle"></span>

                                                        <span class="m-l-10">

                                                    <h4 class="text-white m-b-0">John</h4>

                                                    <p class="text-white">Actor</p>

                                                    </span>

                                                    </div>

                                                </div>

                                                <div class="carousel-item">

                                                    <h4 class="cmin-height">TravDek <span class="font-medium">Favourite</span> place to book <br/>the ticket.</h4>

                                                    <div class="d-flex no-block">

                                                        <span><img src="assets/images/users/3.jpg" alt="user" width="50" class="img-circle"></span>

                                                        <span class="m-l-10">

                                                    <h4 class="text-white m-b-0">Sandy</h4>

                                                    <p class="text-white">Actor</p>

                                                    </span>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!-- Column -->

                        </div>

                    </div>

                </div>

                <!-- ============================================================== -->

                <!-- Comment - table -->

                <!-- ============================================================== -->

                

                

                

               

                

                

                

               

            </div>

            <!-- ============================================================== -->

            <!-- End Container fluid  -->

            <!-- ============================================================== -->

        </div>

        <!-- ============================================================== -->

        <!-- End Page wrapper  -->

        <!-- ============================================================== -->

        <!-- ============================================================== -->

        <!-- footer -->

        <!-- ============================================================== -->

        <footer class="footer">

        <?php include("elements/footer.php"); ?>

        </footer>

        <!-- ============================================================== -->

        <!-- End footer -->

        <!-- ============================================================== -->

    </div>

    <!-- ============================================================== -->

    <!-- End Wrapper -->

    <!-- ============================================================== -->

    <!-- ============================================================== -->

    <!-- All Jquery -->

    <!-- ============================================================== -->

    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <!-- slimscrollbar scrollbar JavaScript -->

    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>

    <!--Wave Effects -->

    <script src="dist/js/waves.js"></script>

    <!--Menu sidebar -->

    <script src="dist/js/sidebarmenu.js"></script>

    <!--Custom JavaScript -->

    <script src="dist/js/custom.min.js"></script>

    <!-- ============================================================== -->

    <!-- This page plugins -->

    <!-- ============================================================== -->

    <!--morris JavaScript -->

    <script src="assets/node_modules/raphael/raphael-min.js"></script>

    <script src="assets/node_modules/morrisjs/morris.min.js"></script>

    <script src="assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>

    <!-- Popup message jquery -->

    <script src="assets/node_modules/toast-master/js/jquery.toast.js"></script>

    <!-- Chart JS -->

    <script src="dist/js/dashboard1.js"></script>

    <script src="assets/node_modules/toast-master/js/jquery.toast.js"></script>

</body>



</html>