<?php error_reporting(0);
    include('include.inc.php');

    $id = $_GET['id'];

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('book-air-plus', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    if( !empty($_POST['first_name']) && !empty($_POST['last_name'])  && !empty($_POST['dateofbirth']) && !empty($_POST['email']) && !empty($_POST['mobile_number']) && !empty($_POST['mobile_number_code'])  ){
    $message='';
    //var_dump($_POST);
         $query="UPDATE  tbl_book_air_bus SET
                        first_name='".$_POST['first_name']."',
                        last_name='".$_POST['last_name']."',
                        email='".$_POST['email']."',
                        countrycode='".$_POST['mobile_number_code']."',
                        phoneno='".$_POST['mobile_number']."',
                        address01='".$_POST['address01']."',
                        address02='".$_POST['address02']."',
                        city='".$_POST['city']."',
                        province='".$_POST['province']."',
                        zip='".$_POST['zip']."',
                        country='".$_POST['country']."',
                        postdate=NOW(),
                        postip='".$_SERVER['REMOTE_ADDR']."',
                        status='Pending'
                         WHERE tmid= $id
                     ";
    mysqli_query($con, $query);
     //   var_dump($_POST['destination_city']);
    foreach($_POST['fromcity'] as $key=>$value){
         $querydest="UPDATE tbl_book_air_bus_details SET
                                            fromcity='".$_POST['fromcity'][$key]."',
                                            tocity='".$_POST['tocity'][$key]."',
                                            departingdate='".date('Y-m-d', strtotime($_POST['departingdate'][$key]))."',
                                            returningdate='".date('Y-m-d', strtotime($_POST['returningdate'][$key]))."'
                                            WHERE busid='$key'
                                            ";
            mysqli_query($con, $querydest);
    }
        
        
     /*   
    for($i=1; $i<=15; $i++){
        if( !empty($_POST['destination_city_'.$i])){
            $querydest="INSERT INTO tbl_tailor_made_itinerary SET
                                            city='".$_POST['destination_city_'.$i]."',
                                            departure_date='".date('Y-m-d', strtotime($_POST['departure_date_'.$i]))."',
                                            arrival_date='".date('Y-m-d', strtotime($_POST['arrival_date_'.$i]))."',
                                            tmid='$tmid'
                                            ";
            mysqli_query($con, $querydest);
            $tmitid=mysqli_insert_id($con);
            if(sizeof($_POST['travel_date_'.$i]) > 0){
            foreach( $_POST['travel_date_'.$i] as $key=>$value ){
             //  echo $_POST['travel_date_'.$i][$key];
                if($_POST['travel_date_'.$i][$key] ){
                    $queryitbreak= "INSERT INTO tbl_tailor_made_itinerary_break SET
                                             tmid='$tmid',
                                             tmitid='$tmid',
                                            itdate='".date('Y-m-d', strtotime($_POST['travel_date_'.$i][$key]))."',
                                            ithotel='".$_POST['add_hotel_'.$i][$key]."',
                                            ittransport='".$_POST['transport_option_'.$i][$key]."',
                                            itactivities='".$_POST['activities_'.$i][$key] ."',
                                            it_meals='".$_POST['meals_'.$i][$key] ."'
                                            ";
                    mysqli_query($con, $queryitbreak);
                    
                }
            }
            }    
            
        }
    }  */
    
      
 
                //$headers .= 'Cc:demo@TravDek.com'."\r\n";
  //      mail($tocustomer, $subject, $thankyoumail, $headers);
        
        
  header('location:book-air-plus.php');
}

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_book_air_bus WHERE tmid= $id "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Air Plus Booking Details - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Air Plus Booking Details</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="book-air-plus.php">Air Plus Booking Details</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="category" action="" method="post" enctype="multipart/form-data">
          
                 <div class="head09 text-center">
                            <strong>Trip Plan</strong>
                            <div class="space0"></div>
                        </div>
						<hr class="redline-full">
						<p class="py-3 mb-0"></p>
						
						
                
                
                        
						
					
                        
						<div class="head09">
                            <strong>Passenger Details</strong>
                            <div class="space0"></div>
                        </div>
                      
                        Full name(s) as shown on the passport
						<hr class="redline-full">
						<p class="py-3 mb-0"></p>
                       
						<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										
										<input class="form-control form-control01" name="first_name" placeholder="First Name" value="<?php if(isset($row['first_name'])){echo $row['first_name'];}?>" required>
									</div>
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
										<input class="form-control form-control01" name="last_name" placeholder="Last Name/Surname" value="<?php if(isset($row['last_name'])){echo $row['last_name'];}?>" required>
									</div>
								</div>
                                <!--<div class="col-lg-4">
									<div class="form-group">
										<input class="form-control form-control01 form-date" name="dateofbirth" placeholder="Date of Birth" value="<?php if(isset($row['dateofbirth'])){echo $row['dateofbirth'];}?>" required>
									</div>
								</div>
                                -->
							</div>
                
            
                        
                        <div class="head09">
                            <strong> Passenger Contact Details</strong>
                            <div class="space0"></div>
                        </div>
						
                  
						<hr class="redline-full">
						<p class="py-3 mb-0"></p>
						<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="user_login_label">Email*</label>
										<input class="form-control form-control01" type="email" name="email" placeholder="name@mail.com" value="<?php if(isset($row['email'])){echo $row['email'];}?>" required>
									</div>
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
                                        <label class="user_login_label">Phone Number*</label>
										<div class="row">
                                            <div class="col-lg-4 agent-su">
                                                <input class="form-control form-control01" id="mobile_number_code" name="mobile_number_code" value="<?php if(isset($row['countrycode'])){echo $row['countrycode'];}?>" required>
                                            </div>

                                            <div class="col-lg-8">
                                                <input class="form-control form-control01" id="mobile_number" name="mobile_number" placeholder="Please enter" value="<?php if(isset($row['phoneno'])){echo $row['phoneno'];}?>" required>
                                            </div>
                                        </div>
									</div>
								</div>
                                
							</div>
                  
                        <div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="user_login_label">Passenger's Address*:</label>
										<input class="form-control form-control01" name="address01" placeholder="Address line 01" value="<?php if(isset($row['address01'])){echo $row['address01'];}?>">
									</div>
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
                                        <label class="user_login_label">.</label>
										<input class="form-control form-control01" name="address02" placeholder="Address line 02" value="<?php if(isset($row['address02'])){echo $row['address02'];}?>">
									</div>
								</div>
                                
							</div>
                        <div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="user_login_label">City*:</label>
										<input class="form-control form-control01" name="city" placeholder="Please Enter" value="<?php if(isset($row['city'])){echo $row['city'];}?>">
									</div>
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
                                        <label class="user_login_label">State/Province/County*:</label>
										<input class="form-control form-control01" name="province" placeholder="Please Enter" value="<?php if(isset($row['province'])){echo $row['province'];}?>">
									</div>
								</div>
                                
							</div>
                        <div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="user_login_label">Zip/PIN Code*:</label>
										<input class="form-control form-control01" name="zip" placeholder="Please Enter" value="<?php if(isset($row['zip'])){echo $row['zip'];}?>">
									</div>
								</div>
                                <div class="col-lg-6">
									<div class="form-group">
                                        <label class="user_login_label">Country*:</label>
										<input class="form-control form-control01" name="country" placeholder="Please Enter" value="<?php if(isset($row['country'])){echo $row['country'];}?>">
									</div>
								</div>
                                
							</div>
                        
                        
						<div class="provence-form">
						
                  <?php
                    $qdest=mysqli_query($con, "SELECT * FROM tbl_book_air_bus_details WHERE airbusid=$id");
                            if(mysqli_num_rows($qdest) > 0){
                                ?>
                            <div class="row">
								<div class="col-lg-4">
									<div class="form-group">
                                        <label class="user_login_label">Leaving From</label>
									</div>
								</div>
                                <div class="col-lg-4">
									<div class="form-group">
                                        <label class="user_login_label">Going to</label>
										
									</div>
								</div>
								
                                <div class="col-lg-2">
									<div class="form-group">
                                        <label class="user_login_label">Departing</label>
									</div>
								</div>
                                
								<div class="col-lg-2">
									<div class="form-group">
                                        <label class="user_login_label">Returning</label>
									</div>
								</div>
								
							</div>
                        <?php        while( $rdest=mysqli_fetch_assoc($qdest)){
                            ?>   
                            
                            <div class="row">
								<div class="col-lg-4">
									<div class="form-group">
                                        <input class="form-control form-control01" type="text"  name="fromcity[<?php echo $rdest['busid'];?>]"  id="fromcity_1" placeholder="Leaving From"   value="<?php if(isset($rdest['fromcity'])){echo $rdest['fromcity'];}?>"  >
										
									</div>
								</div>
                                <div class="col-lg-4">
									<div class="form-group">
                                        <input class="form-control form-control01" type="text"  name="tocity[<?php echo $rdest['busid'];?>]"  id="tocity_1"  value="<?php if(isset($rdest['tocity'])){echo $rdest['tocity'];}?>" placeholder="Going to"   >
										
									</div>
								</div>
								
                                <div class="col-lg-2">
									<div class="form-group">
										<input class="form-control form-control01 form-date" type="text" name="departingdate[<?php echo $rdest['busid'];?>]" id="departingdate_1" value="<?php if(isset($rdest['departingdate'])){echo date('d-m-Y', strtotime($rdest['departingdate']));}?>"     placeholder="Departing">
									</div>
								</div>
                                
								<div class="col-lg-2">
									<div class="form-group">
										<input class="form-control form-control01 form-date" type="text"  name="returningdate[<?php echo $rdest['busid'];?>]" id="returningdate_1" value="<?php if(isset($rdest['returningdate'])){  echo date('d-m-Y', strtotime($rdest['returningdate'])); }?>"  placeholder="Returning">
									</div>
								</div>
							</div>
                            
							
                            
						<?php
                            }
                            }
                            ?>	
							
        <div id="price_boxes" class="price_boxes"></div>
     <!--   <a href="javascript:void(0)" class="add_price">  Add More [ + ]</a> -->
</div>
                        
                        
						
						
						
						
						<p class="py-2"></p>
						
						<div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                        </div>
                        
                        
                
                
            </form>	
						
						
						
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script type="text/javascript">

    $(document).ready(function(){
        var i = 1;
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_price").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            i++;
            var append_data = '<img src="<?php echo HOME_URL; ?>images/shadow5.png" class="img-fluid my-4"><div class="row"><div class="col-lg-4"><div class="form-group"><label class="user_login_label">Destination/City</label><input class="form-control form-control01" type="text"  name="destination_city_'+i+'"  id="destination_city_'+i+'"  value=""   required="#" ></div></div><div class="col-lg-4"><div class="form-group"><label class="user_login_label">Arrival Date*</label><input class="form-control form-control01 form-date" type="text"  name="arrival_date_'+i+'" id="arrival_date_'+i+'" value=""   required="#" placeholder="mm/dd/yyyy"></div></div><div class="col-lg-4"><div class="form-group"><label class="user_login_label">Departure Date*</label><input class="form-control form-control01 form-date" type="departure_date_'+i+'" name="departure_date_'+i+'" value=""   required="#" placeholder="mm/dd/yyyy"></div></div></div><div class="row"><div class="col-lg-4"><div class="form-group"><input class="form-control form-control01  form-date" name="travel_date_'+i+'[]" id="travel_date_'+i+'" value=""  required="#" placeholder="Enter Date (mm/dd/yyyy)"></div></div></div><div class="row"><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Hotel*</label><select class="form-control form-control01" name="add_hotel_'+i+'[]" id="add_hotel_'+i+'"><option value="">Please select a Hotel</option><option value="Simple">Simple</option><option value="Classic">Classic</option><option value="Premium">Premium</option><option value="Delux">Delux</option><option value="Luxury">Luxury</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Transport*</label><select name="transport_option_'+i+'[]" id="transport_option_'+i+'" class="form-control form-control01"><option value="">Please select Transport</option><option value="Airport Transfer">Airport Transfer</option><option value="Half Day Use">Half Day Use</option><option value="Full Day Use">Full Day Use</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Activities*</label><select name="activities_'+i+'[]" id="activities_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Full Day Sightseeing">Full Day Sightseeing</option><option value="Half Day Sightseeing">Half Day Sightseeing</option><option value="Special Activities">Special Activities</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Meals*</label><select name="meals_'+i+'[]" id="meals_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Breakfast">Breakfast</option><option value="Lunch">Lunch</option><option value="Dinner">Dinner</option></select></div></div></div><div class="row"><div class="col-lg-4"><div class="form-group"><input class="form-control form-control01  form-date" name="travel_date_'+i+'[]" id="travel_date_'+i+'" value=""  required="#" placeholder="Enter Date (mm/dd/yyyy)"></div></div></div><div class="row"><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Hotel*</label><select class="form-control form-control01" name="add_hotel_'+i+'[]" id="add_hotel_'+i+'"><option value="">Please select a Hotel</option><option value="Simple">Simple</option><option value="Classic">Classic</option><option value="Premium">Premium</option><option value="Delux">Delux</option><option value="Luxury">Luxury</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Transport*</label><select name="transport_option_'+i+'[]" id="transport_option_'+i+'" class="form-control form-control01"><option value="">Please select Transport</option><option value="Airport Transfer">Airport Transfer</option><option value="Half Day Use">Half Day Use</option><option value="Full Day Use">Full Day Use</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Activities*</label><select name="activities_'+i+'[]" id="activities_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Full Day Sightseeing">Full Day Sightseeing</option><option value="Half Day Sightseeing">Half Day Sightseeing</option><option value="Special Activities">Special Activities</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Meals*</label><select name="meals_'+i+'[]" id="meals_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Breakfast">Breakfast</option><option value="Lunch">Lunch</option><option value="Dinner">Dinner</option></select></div></div></div><div class="row"><div class="col-lg-4"><div class="form-group"><input class="form-control form-control01  form-date" name="travel_date_'+i+'[]" id="travel_date_'+i+'" value=""  required="#" placeholder="Enter Date (mm/dd/yyyy)"></div></div></div><div class="row"><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Hotel*</label><select class="form-control form-control01" name="add_hotel_'+i+'[]" id="add_hotel_'+i+'"><option value="">Please select a Hotel</option><option value="Simple">Simple</option><option value="Classic">Classic</option><option value="Premium">Premium</option><option value="Delux">Delux</option><option value="Luxury">Luxury</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Transport*</label><select name="transport_option_'+i+'[]" id="transport_option_'+i+'" class="form-control form-control01"><option value="">Please select Transport</option><option value="Airport Transfer">Airport Transfer</option><option value="Half Day Use">Half Day Use</option><option value="Full Day Use">Full Day Use</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Activities*</label><select name="activities_'+i+'[]" id="activities_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Full Day Sightseeing">Full Day Sightseeing</option><option value="Half Day Sightseeing">Half Day Sightseeing</option><option value="Special Activities">Special Activities</option></select></div></div><div class="col-lg-3"><div class="form-group"><label class="user_login_label">Select Meals*</label><select name="meals_'+i+'[]" id="meals_'+i+'" class="form-control form-control01"><option value="">Please select Activities</option><option value="Breakfast">Breakfast</option><option value="Lunch">Lunch</option><option value="Dinner">Dinner</option></select></div></div></div>';
            $("#price_boxes").append(append_data); //append new text box in main div
            //$("#price_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".form-date" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
        
    });

    //If the cross icon was clicked
    function remove_image(pricenum){
        $("#price_box_"+pricenum).css('background','tomato');
        $("#price_box_"+pricenum).fadeOut(800,function(){
           $("#price_box_"+pricenum).remove();
        });
    }
    
    function getpassengers(total_pax){
       // alert(total_pax);
          if(total_pax >1 ){
                var count= parseInt(total_pax -1);
                 var append_data='';
       //         alert(count);
              var i;
              var passengercnt=1;
                 for(i=1; i<= count; i++ ){
                 passengercnt=parseInt(passengercnt+1);    
             append_data =append_data +'Passenger '+passengercnt+' (Lead/Adult)* <div class="row"><div class="col-lg-4"><div class="form-group"><input class="form-control form-control01" name="other_first_name[]" placeholder="First Name"></div></div><div class="col-lg-4"><div class="form-group"><input class="form-control form-control01" name="other_last_name[]" placeholder="Last Name/Surname" ></div></div> <div class="col-lg-4"><div class="form-group"><input class="form-control form-control01 form-date" name="other_dateofbirth[]" placeholder="Date of Birth"></div></div></div>';
                    } 
            }
            $("#passenters_details_id").append(append_data); 
    }
    </script>

<script src="<?php echo HOME_URL; ?>js/room_config.js" type="text/javascript"></script>

    <script>
        $(window).load(function(){
            var total_pax = $('#no_of_adult option:selected').val();
            var room_option = '';
            var selected_room = '<?php echo $_POST['selected_room_val']; ?>';
            
            if(total_pax == 1){
                room_option += '<option value="1-single_room">1 Room, Single Room</option>';
            }

            if(total_pax == 2){
                room_option += '<option value="2-twin_sharing">1 Room, Twin Sharing</option>';
                room_option += '<option value="2-single_room">2 Room, Single Room</option>';
            }

            if(total_pax == 3){
                room_option += '<option value="3-triple_sharing">1 Room, Triple Sharing</option>';
                room_option += '<option value="2-twin_sharing|1-single_room">1 Room, Twin Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="3-single_room">3 Room, Single Room</option>';
            }

            if(total_pax == 4){
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing">1 Room, Quad Sharing</option>';
                }
                room_option += '<option value="4-twin_sharing">2 Room, Twin Sharing</option>';
                room_option += '<option value="3-triple_sharing|1-single_room">1 Room, Triple Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|2-single_room">1 Room, Twin Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="4-single_room">4 Room, Single Room</option>';
            }

            if(total_pax == 5){
                room_option += '<option value="3-triple_sharing|2-twin_sharing">1 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|1-single_room">1 Room, Quad Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|1-single_room">2 Room, Twin Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-single_room">1 Room, Triple Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|3-single_room">1 Room, Twin Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="5-single_room">5 Room, Single Room</option>';
            }

            if(total_pax == 6){
                room_option += '<option value="6-triple_sharing">2 Room, Triple Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing">1 Room, Quad Sharing & 1 Room, Twin Sharing</option>';
                }
                room_option += '<option value="6-twin_sharing">3 Room, Twin Sharing</option>';
                room_option += '<option value="3-triple_sharing|2-single_room">1 Room, Triple Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|1-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-single_room">1 Room, Quad Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|3-single_room">1 Room, Triple Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|4-single_room">1 Room, Twin Sharing & 4 Room, Single Room</option>';
                room_option += '<option value="6-single_room">6 Room, Single Room</option>';
            }

            if(total_pax == 7){
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing">1 Room, Quad Sharing & 1 Room, Triple Sharing</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing">1 Room, Triple Sharing & 2 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing,1-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|1-single_room">2 Room, Triple Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="6-twin_sharing|1-single_room">3 Room, Twin Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|2-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 2 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-single_room">1 Room, Quad Sharing & 3 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|3-single_room">2 Room, Twin Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|4-single_room">1 Room, Triple Sharing & 4 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|5-single_room">1 Room, Twin Sharing & 5 Room, Single Room</option>';
                room_option += '<option value="7-single_room">7 Room, Single Room</option>';
            }

            if(total_pax == 8){
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing">2 Room, Quad Sharing</option>';
                }
                room_option += '<option value="6-triple_sharing|2-twin_sharing">2 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|4-twin_sharing">1 Room, Quad Sharing & 2 Room, Twin Sharing</option>';
                }
                room_option += '<option value="8-twin_sharing">4 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|1-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing|1-single_room">1 Room, Triple Sharing & 2 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing|2-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|2-single_room">2 Room, Triple Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="6-twin_sharing|2-single_room">3 Room, Twin Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|3-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 3 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|4-single_room">1 Room, Quad Sharing & 4 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|4-single_room">2 Room, Twin Sharing & 4 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|5-single_room">1 Room, Triple Sharing & 5 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|6-single_room">1 Room, Twin Sharing & 6 Room, Single Room</option>';
                room_option += '<option value="8-single_room">8 Room, Single Room</option>';
            }

            if(total_pax == 9){
                room_option += '<option value="9-triple_sharing">3 Room, Triple Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|2-twin_sharing">1 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
                }
                room_option += '<option value="3-triple_sharing|6-twin_sharing">1 Room, Triple Sharing & 3 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|1-single_room">2 Room, Quad Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="8-twin_sharing|1-single_room">4 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|4-twin_sharing|1-single_room">1 Room, Quad Sharing & 2 Room, Twin Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|2-twin_sharing|1-single_room">2 Room, Triple Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|2-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing|2-single_room">1 Room, Triple Sharing & 2 Room, Twin Sharing & 2 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing|3-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 3 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|3-single_room">2 Room, Triple Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="6-twin_sharing|3-single_room">3 Room, Twin Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|4-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 4 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|5-single_room">1 Room, Quad Sharing & 5 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|5-single_room">2 Room, Twin Sharing & 5 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|6-single_room">1 Room, Triple Sharing & 6 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|7-single_room">1 Room, Twin Sharing & 7 Room, Single Room</option>';
                room_option += '<option value="9-single_room">9 Room, Single Room</option>';
            }

            if(total_pax == 10){
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|2-twin_sharing">2 Room, Quad Sharing & 1 Room, Twin Sharing</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-triple_sharing">1 Room, Quad Sharing & 2 Room, Triple Sharing</option>';
                }
                room_option += '<option value="6-triple_sharing|4-twin_sharing">2 Room, Triple Sharing & 2 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-twin_sharing">1 Room, Quad Sharing & 3 Room, Twin Sharing</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|2-twin_sharing|1-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="9-triple_sharing|1-single_room">3 Room, Triple Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|2-single_room">2 Room, Quad Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="10-twin_sharing">5 Room, Twin Sharing</option>';
                room_option += '<option value="8-twin_sharing|2-single_room">4 Room, Twin Sharing & 2 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|3-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 3 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing|3-single_room">1 Room, Triple Sharing & 2 Room, Twin Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="6-triple_sharing|4-single_room">2 Room, Triple Sharing & 4 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing|4-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 4 Room, Single Room</option>';
                }
                room_option += '<option value="6-twin_sharing|4-single_room">3 Room, Twin Sharing & 4 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|5-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 5 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-single_room">1 Room, Quad Sharing & 6 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|6-single_room">2 Room, Twin Sharing & 6 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|7-single_room">1 Room, Triple Sharing & 7 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|8-single_room">1 Room, Twin Sharing & 8 Room, Single Room</option>';
                room_option += '<option value="10-single_room">10 Room, Single Room</option>';
            }

            if(total_pax == 11){
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|3-triple_sharing">2 Room, Quad Sharing & 1 Room, Triple Sharing</option>';
                }
                room_option += '<option value="9-triple_sharing|2-twin_sharing">3 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
                room_option += '<option value="3-triple_sharing|8-twin_sharing">1 Room, Triple Sharing & 4 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|4-twin_sharing">1 Room, Quad Sharing & 1 Room, Triple Sharing & 2 Room, Twin Sharing</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-triple_sharing|1-single_room">1 Room, Quad Sharing & 2 Room, Triple Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|4-twin_sharing|1-single_room">2 Room, Triple Sharing & 2 Room, Twin Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="10-twin_sharing|1-single_room">5 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|2-twin_sharing|2-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Twin Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="9-triple_sharing|2-single_room">3 Room, Triple Sharing & 2 Room, Single Room</option>';
                room_option += '<option value="6-triple_sharing|2-twin_sharing|3-single_room">2 Room, Triple Sharing & 1 Room, Twin Sharing & 3 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|3-single_room">2 Room, Quad Sharing & 3 Room, Single Room</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|4-twin_sharing|3-single_room">1 Room, Quad Sharing & 2 Room, Twin Sharing & 3 Room, Single Room</option>';
                }
                room_option += '<option value="8-twin_sharing|3-single_room">4 Room, Twin Sharing & 3 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|4-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 4 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing|4-single_room">1 Room, Triple Sharing & 2 Room, Twin Sharing & 4 Room, Single Room</option>';
                room_option += '<option value="6-triple_sharing|5-single_room">2 Room, Triple Sharing & 5 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing|5-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 5 Room, Single Room</option>';
                }
                room_option += '<option value="6-twin_sharing|5-single_room">3 Room, Twin Sharing & 5 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|6-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 6 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|7-single_room">1 Room, Quad Sharing & 7 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|7-single_room">2 Room, Twin Sharing & 7 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|8-single_room">1 Room, Triple Sharing & 8 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|9-single_room">1 Room, Twin Sharing & 9 Room, Single Room</option>';
                room_option += '<option value="11-single_room">11 Room, Single Room</option>';
            }

            if(total_pax == 12){
                if($('#destination').val()=='USA'){
                room_option += '<option value="12-quad_sharing">3 Room, Quad Sharing</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|4-twin_sharing">2 Room, Quad Sharing & 2 Room, Twin Sharing</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-triple_sharing|2-twin_sharing">1 Room, Quad Sharing & 2 Room, Triple Sharing & 1 Room, Twin Sharing</option>';
                }
                room_option += '<option value="12-triple_sharing">4 Room, Triple Sharing</option>';
                room_option += '<option value="6-triple_sharing|6-twin_sharing">2 Room, Triple Sharing & 3 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|8-twin_sharing">1 Room, Quad Sharing & 4 Room, Twin Sharing</option>';
                }
                room_option += '<option value="12-twin_sharing">6 Room, Twin Sharing</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|3-triple_sharing|1-single_room">2 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Single Room</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|4-twin_sharing|1-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 2 Room, Twin Sharing & 1 Room, Single Room</option>';
                }
                room_option += '<option value="9-triple_sharing|2-twin_sharing|1-single_room">3 Room, Triple Sharing & 1 Room, Twin Sharing & 1 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|8-twin_sharing|1-single_room">1 Room, Triple Sharing & 4 Room, Twin Sharing & 1 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|2-twin_sharing|2-single_room">2 Room, Quad Sharing & 1 Room, Twin Sharing & 2 Room, Single Room</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-triple_sharing|2-single_room">1 Room, Quad Sharing & 2 Room, Triple Sharing & 2 Room, Single Room</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|6-twin_sharing|2-single_room">1 Room, Quad Sharing & 3 Room, Twin Sharing & 2 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|4-twin_sharing|2-single_room">2 Room, Triple Sharing & 2 Room, Twin Sharing & 2 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|2-twin_sharing|3-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 1 Room, Twin Sharing & 3 Room, Single Room</option>';
                }
                room_option += '<option value="9-triple_sharing|3-single_room">3 Room, Triple Sharing & 3 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|6-twin_sharing|3-single_room">1 Room, Triple Sharing & 3 Room, Twin Sharing & 3 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="8-quad_sharing|4-single_room">2 Room, Quad Sharing & 4 Room, Single Room</option>';
                }
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|4-twin_sharing|4-single_room">1 Room, Quad Sharing & 2 Room, Twin Sharing & 4 Room, Single Room</option>';
                }
                room_option += '<option value="8-twin_sharing|4-single_room">4 Room, Twin Sharing & 4 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|3-triple_sharing|5-single_room">1 Room, Quad Sharing & 1 Room, Triple Sharing & 5 Room, Single Room</option>';
                }
                room_option += '<option value="3-triple_sharing|4-twin_sharing|5-single_room">1 Room, Triple Sharing & 2 Room, Twin Sharing & 5 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|2-twin_sharing|6-single_room">1 Room, Quad Sharing & 1 Room, Twin Sharing & 6 Room, Single Room</option>';
                }
                room_option += '<option value="6-triple_sharing|6-single_room">2 Room, Triple Sharing & 6 Room, Single Room</option>';
                room_option += '<option value="6-twin_sharing|6-single_room">3 Room, Twin Sharing & 6 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|2-twin_sharing|7-single_room">1 Room, Triple Sharing & 1 Room, Twin Sharing & 7 Room, Single Room</option>';
                if($('#destination').val()=='USA'){
                room_option += '<option value="4-quad_sharing|8-single_room">1 Room, Quad Sharing & 8 Room, Single Room</option>';
                }
                room_option += '<option value="4-twin_sharing|8-single_room">2 Room, Twin Sharing & 8 Room, Single Room</option>';
                room_option += '<option value="3-triple_sharing|9-single_room">1 Room, Triple Sharing & 9 Room, Single Room</option>';
                room_option += '<option value="2-twin_sharing|10-single_room">1 Room, Twin Sharing & 10 Room, Single Room</option>';
                room_option += '<option value="10-single_room">10 Room, Single Room</option>';
            }
            
            $('#room_config').empty();
            $('#room_config').append(room_option);
            
            $('#room_config option[value="'+selected_room+'"]').prop("selected", true);
            
           
            
        });
    </script>


      <script>
        if(location.hostname=='localhost:8888'){
            var home_url=document.location.origin+'/travdek_new/'; 
        }else{
            var home_url=document.location.origin+'/travdek_new/'; 
        }
      //    alert(home_url);
       
        var input_2 = document.querySelector("#mobile_number_code");
        window.intlTelInput(input_2, {
          onlyCountries: ['us', 'in'],
          preferredCountries: [],
          utilsScript: home_url+"assets/country_code/utils.js",
        });
      </script>

    <script>
        $(window).load(function(){
            $('.iti__selected-flag').each(function(){
                var country_code = $(this).attr('title');
                var country = country_code.split(':');
                var inputid = $(this).parent().siblings().attr('id');
                $('#'+inputid).val(country[1]);
            });
        });

        $('.iti__country').click(function(){
            var country_code = $(this).attr('data-dial-code');
            var inputid = $(this).parent().parent().siblings().attr('id');
            $('#'+inputid).val('+'+country_code);
        });
    </script>

 <script>
  /*  $('.add_services').multiselect({
        inheritClass: true,
        buttonWidth: '100%',
        buttonText: function (options) {
        if (options.length == 0) {
             return 'Select Activities';
            } else {
             var selected = 0;
             options.each(function () {
                 selected += 1;
             });
             return selected +  ' Selected';
            }
        }
    }); */
    </script>

 <script>
 /*   $('.add_meals').multiselect({
        inheritClass: true,
        buttonWidth: '100%',
        buttonText: function (options) {
        if (options.length == 0) {
             return 'Select Meals';
            } else {
             var selected = 0;
             options.each(function () {
                 selected += 1;
             });
             return selected +  ' Selected';
            }
        }
    }); */
    </script>

    <script>
    $( function() {
        $( ".form-date" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
    });
    </script>
    
</body>

</html>