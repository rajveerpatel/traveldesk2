<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-types', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $id = $_GET['id'];
    
    if( !empty($_POST['trip_type']) ){
        
        if(!empty($_FILES['trip_type_img']['name'])){
            $trip_type_img=uploadfiles($_FILES['trip_type_img']['name'], $_FILES['trip_type_img']['tmp_name'], $_FILES['trip_type_img']['error'], $_FILES['trip_type_img']['size'], TRIP_TYPES_IMG);
        }else{
            $trip_type_img=$_POST['old_thumb'];
        }
        
        if(!empty($_FILES['trip_theme_icon']['name'])){
            $trip_theme_icon=uploadfiles($_FILES['trip_theme_icon']['name'], $_FILES['trip_theme_icon']['tmp_name'], $_FILES['trip_theme_icon']['error'], $_FILES['trip_theme_icon']['size'], TRIP_TYPES_IMG);
        }else{
            $trip_theme_icon=$_POST['old_icon'];
        }
        
        if(!empty($_FILES['trip_type_banner']['name'])){
            $trip_type_banner=uploadfiles($_FILES['trip_type_banner']['name'], $_FILES['trip_type_banner']['tmp_name'], $_FILES['trip_type_banner']['error'], $_FILES['trip_type_banner']['size'], BANNER_IMG);
        }else{
            $trip_type_banner=$_POST['old_banner'];
        }
        
        mysqli_query($con, "UPDATE tbl_trip_types SET 
        trip_type = '".$_POST['trip_type']."',
        trip_type_img = '".$trip_type_img."',
        trip_theme_icon = '".$trip_theme_icon."',
        trip_type_banner = '".$trip_type_banner."',
        status = '".$_POST['status']."'
        WHERE trip_type_id = $id ");
        
        header("location: trip-types.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE trip_type_id = $id "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Trip Theme - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Trip Theme</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="trip-types.php">Trip Theme List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            <label class="user_login_label" for="trip_type">Trip Theme*</label>
                            <input type="text" name="trip_type" class="form-control form-control01" value="<?php echo $row['trip_type']; ?>" placeholder="Trip Theme*" required>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            <label class="user_login_label">Trip Theme Thumb</label>
                            <input type="file" name="trip_type_img" class="form-control form-control01" placeholder="Trip Theme Image*">
                            <input type="hidden" name="old_thumb" value="<?php echo $row['trip_type_img']; ?>" >
                            <?php 
                            if(!empty($row['trip_type_img'])){
                                echo '<img src="'.TRIP_TYPES_IMG.$row['trip_type_img'].'" class="img-responsive" width="80px">';
                            }
                            ?>
                            <p class="img_note">Note : Trip Theme thumb image size should be 271 x 297</p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            <label class="user_login_label">Trip Theme Icon</label>
                            <input type="file" name="trip_theme_icon" class="form-control form-control01" placeholder="Trip Icon*">
                            <input type="hidden" name="old_icon" value="<?php echo $row['trip_theme_icon']; ?>" >
                            <?php 
                            if(!empty($row['trip_theme_icon'])){
                                echo '<img src="'.TRIP_TYPES_IMG.$row['trip_theme_icon'].'" class="img-responsive" width="30px">';
                            }
                            ?>
                            <p class="img_note">Note : Trip Theme icon size should be 42 x 42</p>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            <label class="user_login_label">Trip Theme Banner</label>
                            <input type="file" name="trip_type_banner" class="form-control form-control01" placeholder="Trip Theme Banner*">
                            <input type="hidden" name="old_banner" value="<?php echo $row['trip_type_banner']; ?>" >
                            <?php 
                            if(!empty($row['trip_type_banner'])){
                                echo '<img src="'.BANNER_IMG.$row['trip_type_banner'].'" class="img-responsive" width="80px">';
                            }
                            ?>
                            <p class="img_note">Note : Trip Theme banner image size should be 1349 x 630</p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            <label class="user_login_label" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
   
    
</body>

</html>