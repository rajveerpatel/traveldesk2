<?php
include('includes/config.inc.php');

//add trip booking log remark
if( !empty($_POST['remark']) && !empty($_POST['user_track_id']) && !empty($_POST['action']) && $_POST['action']=='add_remark' ){
    $remark = $_POST['remark'];
    $user_track_id = $_POST['user_track_id'];
    $adminid = $_POST['adminid'];
    $remark_type = $_POST['remark_type'];
    
    //User track id
    function log_track($length_of_string) 
    { 
        $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'; 
        return substr(str_shuffle($str_result), 0, $length_of_string); 
    }
    
    $log_track = log_track(5);
    
    $max_order = mysqli_fetch_assoc(mysqli_query($con, "SELECT MAX(order_no) as max_order FROM tbl_tirp_lead_logs WHERE user_track_id = '$user_track_id' AND selected = '1' "));
    $new_order = $max_order['max_order'] + 1;
    
    mysqli_query($con, "INSERT INTO tbl_tirp_lead_logs SET
    user_track_id = '$user_track_id',
    remark = '$remark',
    log_track = '$log_track',
    updated_by = $adminid,
    remark_type = '$remark_type',
    selected = '1',
    order_no = $new_order,
    ip_address = '".$_SERVER['REMOTE_ADDR']."',
    post_date = NOW() ");
    
    $adminuser = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_admin WHERE adminid = $adminid "));
    
    if($remark_type=="External"){
        $adminemail = $adminuser['email'];

        $subject = "TravDek Enquiry Booking Logs";

        $message = '<br><br>Hi<br> Below are the Enquiry Booking Log details :<br><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
        <td width="19%" align="left" valign="top" class="textmatter_01">Log Remark</td>
        <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
        <td width="80%" align="left" valign="top">'.$remark.'</td>
        </tr>
        <tr>
        <td width="19%" align="left" valign="top" class="textmatter_01">Log Added By</td>
        <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
        <td width="80%" align="left" valign="top">'.$adminuser["fullname"].'</td>
        </tr>
        <tr>
        <td width="19%" align="left" valign="top" class="textmatter_01">IP Address</td>
        <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
        <td width="80%" align="left" valign="top">'.$_SERVER["REMOTE_ADDR"].'</td>
        </tr>
        <tr>
        <td align="left" valign="top" class="textmatter_01">Post Date</td>
        <td align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
        <td align="left" valign="top">'.date('M dS, Y').'</td>
        </tr>
        </table>';

        //mail

        $to  = $adminemail;

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From:TravDek<NoReply@travdek.com>'."\r\n";
        //$headers .= 'Bcc:demo@travdek.com'."\r\n";

        mail($to, $subject, $message, $headers);
    }
    
    $query=mysqli_query($con, "SELECT l.*, a.* FROM tbl_tirp_lead_logs l LEFT JOIN tbl_admin a ON a.adminid = l.updated_by WHERE l.user_track_id = '$user_track_id' ORDER BY l.post_date DESC");
        if( mysqli_num_rows($query) > 0 ){
            $i=0;
            while( $row=mysqli_fetch_assoc($query) ){ ?>
            <div class="log-box">
                <h3 class="log-time"><?php if($row['post_date']!='0000-00-00 00:00:00'){ echo date('m/d/Y, l | H:i', strtotime($row['post_date'])); } ?></h3>
                <p class="remark-log"><?php echo $row['remark']; ?></p>
                <p class="remark-bot">Posted by: <?php echo $row['agent_code']; ?> | Type of Remark: <?php echo $row['remark_type']; ?> | Added by: <?php echo $row['fullname']; ?></p>
            </div>
   <?php    }

        }
}

//edit trip booking log remark
if( !empty($_POST['remark']) && !empty($_POST['user_track_id']) && !empty($_POST['log_id']) && !empty($_POST['action']) && $_POST['action']=='edit_remark' ){
    $log_id = $_POST['log_id'];
    $remark = $_POST['remark'];
    $order_no = $_POST['order_no'];
    $user_track_id = $_POST['user_track_id'];
    $log_track = $_POST['log_track'];
    $adminid = $_POST['adminid'];
    
    // edited log move to archive
    mysqli_query($con, "UPDATE tbl_tirp_lead_logs SET selected = '0' WHERE log_id = $log_id ");
    
    // add new log
    $sql = "INSERT INTO tbl_tirp_lead_logs SET
    user_track_id = '$user_track_id',
    log_track = '$log_track',
    remark = '$remark',
    updated_by = $adminid,
    selected = '1',
    order_no = $order_no,
    ip_address = '".$_SERVER['REMOTE_ADDR']."',
    post_date = NOW() ";
    mysqli_query($con, $sql);
    
    $adminuser = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_admin WHERE adminid = $adminid "));
    $adminemail = $adminuser['email'];
    
    $subject = "TravDek Enquiry Booking Logs";

    $message = '<br><br>Hi<br> Below are the Enquiry Booking Log details :<br><table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
    <td width="19%" align="left" valign="top" class="textmatter_01">Log Remark</td>
    <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
    <td width="80%" align="left" valign="top">'.$remark.'</td>
    </tr>
    <tr>
    <td width="19%" align="left" valign="top" class="textmatter_01">Log Added By</td>
    <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
    <td width="80%" align="left" valign="top">'.$adminuser["fullname"].'</td>
    </tr>
    <tr>
    <td width="19%" align="left" valign="top" class="textmatter_01">IP Address</td>
    <td width="1%" align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
    <td width="80%" align="left" valign="top">'.$_SERVER["REMOTE_ADDR"].'</td>
    </tr>
    <tr>
    <td align="left" valign="top" class="textmatter_01">Post Date</td>
    <td align="center" valign="top" class="textmatter_01"><strong>:</strong></td>
    <td align="left" valign="top">'.date('M dS, Y').'</td>
    </tr>
    </table>';

    //mail

    $to  = $adminemail;

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:TravDek<NoReply@travdek.com>'."\r\n";
    //$headers .= 'Bcc:demo@travdek.com'."\r\n";

    mail($to, $subject, $message, $headers);
    
    
    $query=mysqli_query($con, "SELECT l.*, a.fullname FROM tbl_tirp_lead_logs l LEFT JOIN tbl_admin a ON a.adminid = l.updated_by WHERE l.user_track_id = '$user_track_id' AND selected = '1' ORDER BY l.order_no ASC");
        if( mysqli_num_rows($query) > 0 ){
            $i=0;
            while( $row=mysqli_fetch_assoc($query) ){ ?>
     <tr>
         <td><input type="text" class="order_no" name="<?php echo $row['log_id'];?>" size="3" value="<?php echo $row['order_no'];?>"></td>
         <td><?php echo $row['remark']; ?></td>
         <td><?php echo $row['fullname']; ?></td>
         <td><?php if($row['post_date']!='0000-00-00'){ echo date('M dS Y', strtotime($row['post_date'])); } ?></td>
         <td><div class="btn-group"><a class="btn bg-navy" data-toggle="modal" href="#edit-remark-modal-<?php echo $row['log_id']; ?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" id="<?php echo $row['log_id'];?>" title="Delete"><i class="fa fa-trash"></i></a> </div></td>
    </tr>

   <?php    }

        }
}

//add popup for edit booking log remark
/*if( !empty($_POST['popup_action']) && $_POST['popup_action']=='popup_action' ){
    $user_track_id = $_POST['user_track_id'];
    $query=mysqli_query($con, "SELECT * FROM tbl_tirp_lead_logs WHERE user_track_id = '$user_track_id' AND selected = '1'  ORDER BY order_no ASC");
    if( mysqli_num_rows($query) > 0 ){
        $i=0;
        while( $row=mysqli_fetch_assoc($query) ){ ?>
            <div class="modal fade" id="edit-remark-modal-<?php echo $row['log_id']; ?>" tabindex="-1" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title head04">Edit Remark</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="user_login_label" for="edit_remark">Edit Log Remark*</label>
                                <input type="text" name="edit_remark-<?php echo $row['log_id']; ?>" id="edit_remark-<?php echo $row['log_id']; ?>" class="form-control form-control01" value="<?php echo $row['remark']; ?>" placeholder="Edit Log Remark*" required>
                            </div>

                            <div class="btn00 text-center">
                                <button name="submit" aria-label="<?php echo $row['log_id']; ?>" class="edit_remark_btn btn btn03 btn-width-03">Submit</button>
                            </div>

                            <input type="hidden" id="order_no-<?php echo $row['log_id']; ?>" value="<?php echo $row['order_no']; ?>">
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        <?php
        }
    }
}*/
?>