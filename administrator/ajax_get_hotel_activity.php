<?php
include('includes/config.inc.php');

$id = 0;
$act_arr = array();
$cat_id = 0;
$city_id = 0;

//Get Hotels from selected City
if( !empty($_POST['hotel_city_id']) ){
    
    $city_id = $_POST['hotel_city_id'];
    //$cat_id = $_POST['hotel_cat_id'];
    
    $query = mysqli_query($con, "SELECT h.*, c.category_name FROM tbl_hotels h LEFT JOIN tbl_trip_categories c ON c.cat_id = h.category_id WHERE h.status= 'Active' AND h.city_id = $city_id ORDER BY c.cat_id " );
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="">Select Hotel</option>';
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['hotel_id'].'">'.$row['hotel_name'].' ('.$row['category_name'].')</option>';
        }
    } else {
        echo '<option value="">Hotel Not Found</option>';
    }
    
}


//Get Activities from selected City
if( !empty($_POST['activities_city_id']) ){
    
    $city_id = $_POST['activities_city_id'];
    
    
    $query = mysqli_query($con, "SELECT *  FROM tbl_activities WHERE city_id = $city_id ORDER BY city_id " );
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="">Select Activities</option>';
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['act_id'].'">'.$row['activity_name'].' ($'.$row['activity_cost'].')</option>';
        }
    } else {
        echo '<option value="">Activities Not Found</option>';
    }
    
}




//Get Meals from selected City
if( !empty($_POST['meal_city_id']) ){
    
    $city_id = $_POST['meal_city_id'];
    
    
    $query = mysqli_query($con, "SELECT *  FROM tbl_meals WHERE city_id = $city_id ORDER BY city_id " );
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="Breakfast">Breakfast</option>';
        echo '<option value="Lunch">Lunch</option>';
        echo '<option value="Dinner">Dinner</option>';
       
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['meal_id'].'">'.$row['meal_name'].' ($'.$row['meal_cost'].')</option>';
        }
    } else {
        echo '<option value="">Meals Not Found</option>';
    }
    
}

//Get Activities from selected City
if( !empty($_POST['activity_city_id']) ){
    
    $city_id = $_POST['activity_city_id'];
    
    $query = mysqli_query($con, "SELECT * FROM tbl_activities WHERE city_id = $city_id ORDER BY activity_name " );
    if( mysqli_num_rows($query) > 0 ){
        while( $row = mysqli_fetch_assoc($query) ){
            $id = $row['act_id'];
            $activity_name = $row['activity_name'];

            $act_arr[] = array('label' => $activity_name,
                            'title' => $activity_name,
                            'value' => $id);
        }
        echo json_encode($act_arr);
    } else {
        echo '<option value="">None</option>';
    }
    
}

//Get Hotels from selected City and Category
if( !empty($_POST['cat_id']) ){
    
    $cat_id = $_POST['cat_id'];
    $city_id = $_POST['city_id'];
    
    if($cat_id > 0 && $city_id == 0){
        $query = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE category_id = $cat_id ORDER BY hotel_name " );
    } elseif($cat_id > 0 && $city_id > 0){
        $query = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE category_id = $cat_id AND city_id = $city_id ORDER BY hotel_name " );
    }
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="">Select Hotel</option>';
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['hotel_id'].'">'.$row['hotel_name'].'</option>';
        }
    } else {
        echo '<option value="">None</option>';
    }
    
}
?>