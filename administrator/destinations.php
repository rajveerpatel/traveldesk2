<?php 
    include('include.inc.php');
    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array($page_name.'-delete', $_SESSION['AccessRights'])){
            $del_display = "";
        } else {
            $del_display = "display: none;";
        }

        //Add Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('destination-add', $_SESSION['AccessRights'])){
            $add_display = "";
        } else {
            $add_display = "display: none;";
        }

        //Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('destination-edit', $_SESSION['AccessRights'])){
            $edit_display = "";
        } else {
            $edit_display = "display: none;";
        }

        //Parent Destination Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('parent-destinations', $_SESSION['AccessRights'])){
            $parent_display = "";
        } else {
            $parent_display = "display: none;";
        }

        //Trip Delete Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-packages-delete', $_SESSION['AccessRights'])){
            $trip_del_display = "";
        } else {
            $trip_del_display = "display: none;";
        }

        //Trip Edit Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('trip-package-edit', $_SESSION['AccessRights'])){
            $trip_edit_display = "";
        } else {
            $trip_edit_display = "display: none;";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Destinations - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Destinations List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Destinations List</li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                                <p align="right"><a class="btn btn-primary text-white" href="destination-add.php" role="button">Add Destinations</a></p>
                                
                                <div class="table-responsive m-t-20">
                                    <table id="config-table" class="table display table-striped no-wrap m-t-20">
                                        <thead>
                                            <tr>
                                            <th>Order</th>
                                            <th>Destination</th>
                                            <th>Menu</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                        $query=mysqli_query($con, "SELECT * FROM tbl_destinations ORDER BY order_no=0, order_no ");
                            if( mysqli_num_rows($query) > 0 ){
                                $i=0;
                                while( $row=mysqli_fetch_assoc($query) ){ ?>
                                            <tr>
                            <td width="5.5%"><input type="text" class="order_no" name="<?php echo $row['dest_id'];?>" size="3" value="<?php echo $row['order_no'];?>"></td>
                            <td colspan="4">
                                <table class="table inner-table">
                                    <tbody>
                                        <tr>
                                             <td width="42.3%%"><span class="country_dest click-text" id="<?php echo $row['dest_id']; ?>"><?php echo $row['destination']; ?></span></td>
                                             <td width="16.2%"><input type="checkbox" class="manage_home_menu" id="menu_dest-<?php echo $row['dest_id']; ?>" value="<?php echo $row['show_menu'];?>" <?php if($row['show_menu']==1){ echo 'checked'; } ?>></td>
                                             <td width="16.3%" id="status_<?php echo $row['dest_id']; ?>"><?php echo $row['status'];?></td>
                                             <td width="30%"><div class="btn-group"><a class="btn bg-red change_status" id="<?php echo $row['dest_id'];?>" title="Active"><i class="fa fa-check"></i></a> <a class="btn bg-red change_status" id="<?php echo $row['dest_id'];?>" title="Inactive"><i class="fa fa-ban"></i></a> <a class="btn bg-navy" style="<?php echo $edit_display; ?>" href="destination-edit.php?id=<?php echo $row['dest_id'];?>" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" style="<?php echo $del_display; ?>" id="<?php echo $row['dest_id'];?>" title="Delete"><i class="fa fa-trash"></i></a> </div></td>
                                        </tr>
                                        <tr id="itinerary_block_<?php echo $row['dest_id']; ?>" style="display:none;">
                                            <td colspan="5">
                                                <table class="table inner-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="40px">Order</th>
                                                            <th>Trip Packages</th>
                                                            <th>Duration</th>
                                                            <th>Trip Category</th>
                                                            <th>Trip Types</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $tripqry=mysqli_query($con, "SELECT * FROM tbl_trip_packages ORDER BY order_no=0, order_no ");
                                                            if( mysqli_num_rows($tripqry) > 0 ){
                                                                $k=0;
                                                                while( $tripres=mysqli_fetch_assoc($tripqry) ){ 
                                                                    $dest = explode(':', $tripres['country_dest']);
                                                                    $destid = $dest[0];
                                                                    //echo '<br>';
                                                                    if($destid == $row['dest_id']){
                                                                    $k++;
                                                                    $trip_types = explode(', ', $tripres['trip_types']);
                                                        ?>
                                                         <tr>
                                                             <td><input type="text" class="trip_order_no" name="<?php echo $tripres['trip_id'];?>" size="3" value="<?php echo $tripres['order_no'];?>"></td>
                                                             <td><?php echo $tripres['trip_name']; ?></td>
                                                             <td><?php echo $tripres['trip_days'].' '.'Days'; ?></td>
                                                             <td class="dropdown-listing">
                                                                <select class="form-control select-a" id="trip_categories_<?php echo $tripres['trip_id']; ?>">
                                                                    <?php
                                                                    $catsql = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
                                                                    while( $catres = mysqli_fetch_assoc($catsql) ){
                                                                    ?> 
                                                                    <option value="<?php echo $catres['cat_id']; ?>" <?php if($catres['cat_id']==$tripres['trip_category']){ echo 'selected'; } ?>><?php echo $catres['category_name']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                             </td>
                                                             <td class="dropdown-listing">
                                                                <select class="form-control select-a trip_types" id="trip_types_<?php echo $tripres['trip_id']; ?>" multiple>
                                                                    <?php
                                                                    $catsql = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'Active' ");
                                                                    while( $catres = mysqli_fetch_assoc($catsql) ){
                                                                    ?> 
                                                                    <option value="<?php echo $catres['trip_type_id']; ?>" <?php if(in_array($catres['trip_type_id'], $trip_types)){ echo 'selected'; } ?>><?php echo $catres['trip_type']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                             </td>
                                                             <td class="dropdown-listing">
                                                                <select class="form-control select-a" id="trip_status_<?php echo $tripres['trip_id']; ?>">
                                                                    <option value="Active" <?php if($tripres['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                                                    <option value="Inactive" <?php if($tripres['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                                                                </select>
                                                             </td>
                                                             <td><div class="btn-group btn00"><!--<a class="btn bg-red change_status" id="< ?php echo $tripres['trip_id'];?>" title="Active"><i class="fa fa-check"></i></a> <a class="btn bg-red change_status" id="< ?php echo $tripres['trip_id'];?>" title="Inactive"><i class="fa fa-ban"></i></a>--> <a class="btn bg-navy" style="<?php echo $trip_edit_display; ?>" href="trip-package-edit.php?id=<?php echo $tripres['trip_id'];?>" target="_blank" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn bg-red delete" style="<?php echo $trip_del_display; ?>" id="<?php echo $tripres['trip_id'];?>" title="Delete"><i class="fa fa-trash"></i></a> <button class="btn btn-primary text-white" id="<?php echo $tripres['trip_id']; ?>">Update</button></div></td>
                                                        </tr>
                                                        <?php } } }
                                                        if($k==0){
                                                            echo '<tr><td colspan="6">Itineraries Not Found...</td></tr>';
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="display:none;">&nbsp;</td>
                            <td style="display:none;">&nbsp;</td>
                            <td style="display:none;">&nbsp;</td>
                         </tr>

                       <?php         }

                            }
                        ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- This is data table -->
    <script src="assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script>
$(document).ready(function(){
    
    //Display Itinerary under Country
    $('.country_dest').click(function(){
        var el = this;
        
        var destid = $(el).attr('id');
        
        $('#itinerary_block_'+destid).toggle();
    });
    
});
   
$(document).ready(function(){

    // Delete 
    $('.delete').click(function(){
       var el = this;

       // Delete id
       var deleteid = $(this).attr('id');

       var confirmalert = confirm("Are you sure want to delete?");
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'delete-record.php',
            type: 'POST',
            data: { dest_id:deleteid },
            success: function(response){

            if(response == 1){
            // Remove row from HTML Table
            $(el).closest('tr').css('background','tomato');
            $(el).closest('tr').fadeOut(800,function(){
               $(this).remove();
            });
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
    

// Update Status    
$(document).ready(function(){

    // Delete 
    $('.change_status').click(function(){
       var el = this;

       // Delete id
       var statusid = $(this).attr('id');
       var status_title = $(this).attr('title');

       if( $(this).attr('title')=='Active' ){
            var confirmalert = confirm("Are you sure want to Active?");
       } else if( $(this).attr('title')=='Inactive' ){
            var confirmalert = confirm("Are you sure want to Inactive?");
       }
        
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'status-update.php',
            type: 'POST',
            data: { dest_id:statusid, dest_title:status_title },
            success: function(response){

            if(response){
                $('#status_'+statusid).css('border-bottom','2px solid green');
                setTimeout(function () {
                    //alert(response);
                    $('#status_'+statusid).empty();
                    $('#status_'+statusid).html(response);
                    $('#status_'+statusid).css('border-bottom', 'none');
                },500);
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
    

// Manage Destination on Menu & Homepage    
$(document).ready(function(){

    $('.manage_home_menu').click(function(){
       var el = this;

       // Checkbox value
       var checkval = $(this).val();
       var destid = $(this).attr('id');
        
       var confirmalert = confirm("Are you sure want to Update?");
        
       if (confirmalert == true) {
          // AJAX Request
          $.ajax({
            url: 'ajax-manage-home-menu-item.php',
            type: 'POST',
            data: { dest_id:destid, check_val:checkval },
            success: function(response){

            if(response){
                $(el).closest('tr > td').css('border-bottom','2px solid green');
                setTimeout(function () {
                    alert('Record Updated');
                    var checkid = response.split("|");
                    if(checkid[0]==destid){
                        $('#'+checkid[0]).val(checkid[1]);   
                    }
                    $(el).closest('tr > td').css('border-bottom', 'none');
                },500);
            }else{
                alert('Invalid ID.');
            }

            }
          });
       }

    });

});
</script>
  
<script>
    jQuery(document).ready(function(){
        $('.order_no').blur(function(){
            //$.each($('.order_no'), function(){
                var el = this;
                var destid = $(el).attr('name');
                var orderno = $(el).val();
                
                $.ajax({
                    url: 'ajax_order_update.php' ,
                    type: 'POST',
                    data: {dest_id: destid, order_no: orderno},
                    success: function(response){
                        $(el).closest('tr > td').css('border-bottom','2px solid green');
                        setTimeout(function () {
                            $(el).closest('tr > td').css('border-bottom', 'none');
                        },300);
                    }
                });
            //});
        }) 
    });
</script>
  
<script>
    //Update order for Trip Packages
    jQuery(document).ready(function(){
        $('.trip_order_no').blur(function(){
            //$.each($('.trip_order_no'), function(){
                var el = this;
                var tripid = $(el).attr('name');
                var orderno = $(el).val();
                
                $.ajax({
                    url: 'ajax_order_update.php' ,
                    type: 'POST',
                    data: {trip_id: tripid, order_no: orderno},
                    success: function(response){
                        $(el).closest('tr > td').css('border-bottom','2px solid green');
                        setTimeout(function () {
                            $(el).closest('tr > td').css('border-bottom', 'none');
                        },300);
                    }
                });
            //});
        }) 
    });
</script>
  
<script>
    $(function () {
        $('.trip_types').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            buttonText: function (options) {
            if (options.length == 0) {
                 return 'None selected';
                } else {
                 var selected = 0;
                 options.each(function () {
                     selected += 1;
                 });
                 return selected +  ' Selected';
                }
            }
        });
    });
</script>
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 100,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary me-1');
        });

    </script>
</body>

</html>