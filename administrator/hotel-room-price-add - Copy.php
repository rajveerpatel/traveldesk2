<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('hotels', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $hid = $_REQUEST['hid'];

    if( !empty($_POST['dFromDate']) )
    {
       $dFromDate = date('Y-m-d', strtotime($_POST['dFromDate']));
       $dToDate = date('Y-m-d', strtotime($_POST['dToDate']));  
       if( sizeof( $_POST['single_room_cost'] ) > 0 || sizeof( $_POST['double_room_cost'] ) > 0 || sizeof( $_POST['triple_room_cost'] ) > 0 || sizeof( $_POST['qad_room_cost'] ) > 0 )
       {
            //mysqli_query($con, "DELETE FROM tblroomprice WHERE iRoomPriceId = $hid ");
            foreach($_POST['room_type_id'] as $key => $values)
            {
                if( !empty($_POST['room_type_id'][$key]) )
                {
                    
                    $q = "INSERT INTO tblroomprice set 
                    iHotelId='".$hid."',
                    dFromDate='".$dFromDate."',
                    dToDate='".$dToDate."',
                    room_type_id='".$_POST['room_type_id'][$key]."',
                    single_room_cost='".$_POST['single_room_cost'][$key]."',
                    double_room_cost='".$_POST['double_room_cost'][$key]."',
                    triple_room_cost='".$_POST['triple_room_cost'][$key]."',
                    qad_room_cost='".$_POST['qad_room_cost'][$key]."'";
                    //die();
                    mysqli_query($con, $q);
                }
            }
        }
        
        
        
        header("location: hotel-room-price.php?id=$hid");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Add Hotel Room Price - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Hotel Room Price</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="hotel-room-price.php">Hotels Room Price List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            
                            <label class="user_login_label" for="dFromDate">Date From*</label>
                            <input type="text" name="dFromDate" id="dFromDate" class="form-control form-control01 date-type" placeholder="Date From*" required>

                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                        <label class="user_login_label" for="dToDate">Date To*</label>
                            <input type="text" name="dToDate" id="dToDate" class="form-control form-control01 date-type"  placeholder="To Date*" required>

                            
                        </div>
                    </div>
                    <div class="col-md-12">
                    <p class="head11">Hotel Room Type</p>
                        <div id="hotel_rooms" class="hotel_boxes">
                            <div class="row" id="room_1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Room Type Name 1:</label>
                                        <div class="hotel-box">
                                            <select name="room_type_id[]" id="room_type_id_1" class="form-control form-select">
                                                <option value="">Please Select</option>
                                                <?php
                                                $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' ");
                                                while($roomres = mysqli_fetch_assoc($qroom)){
                                                ?>
                                                <option value="<?php echo $roomres['room_type_id']; ?>"><?php echo $roomres['room_type_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                       
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Single Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="single_room_cost[]" id="single_room_cost_1" class="form-control form-control01">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Double Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="double_room_cost[]" id="double_room_cost_1" class="form-control form-control01">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Triple Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="triple_room_cost[]" id="triple_room_cost_1" class="form-control form-control01">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">QAD Bed Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="qad_room_cost[]" id="qad_room_cost_1" class="form-control form-control01">
                                        </div>   
                                        <div class="remove-box"><img src="assets/images/remove-icon.png" id="1" onclick="remove_room(this.id)" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_room" /></div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
       <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">

    $(document).ready(function(){
        var id = 1,max = 20,append_data;

        //If the add icon was clicked
        $(".add_room").on('click',function(){
        if($("div[id^='room_']").length < 11){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="row" id="room_'+id+'"><div class="col-md-6"><div class="form-group"><label class="form-floating mb-3">Room Type Name '+id+':</label><div class="hotel-box"><select name="room_type_id[]" id="room_type_id_'+id+'" class="form-control form-select"> <option value="">Please Select</option><?php $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' ");  while($roomres = mysqli_fetch_assoc($qroom)){  ?> <option value="<?php echo $roomres['room_type_id']; ?>"><?php echo $roomres['room_type_name']; ?></option><?php } ?></select></div></div></div><div class="col-md-6"></div><div class="col-md-6"><div class="form-group"><label class="form-floating mb-3">Single Room Cost '+id+':</label><div class="hotel-box"><input type="text" name="single_room_cost[]" id="single_room_cost_'+id+'" class="form-control form-control01"></div></div></div><div class="col-md-6"><div class="form-group"><label class="form-floating mb-3">Double Room Cost '+id+' :</label><div class="hotel-box"><input type="text" name="double_room_cost[]" id="double_room_cost_'+id+'" class="form-control form-control01"></div></div></div><div class="col-md-6"><div class="form-group"><label class="form-floating mb-3">Triple Room Cost '+id+':</label><div class="hotel-box"><input type="text" name="triple_room_cost[]" id="triple_room_cost_'+id+'" class="form-control form-select"></div></div></div><div class="col-md-6"><div class="form-group"><label class="form-floating mb-3">QAD Bed Cost '+id+':</label><div class="hotel-box"><input type="text" name="qad_room_cost[]" id="qad_room_cost_'+id+'" class="form-control form-control01"></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="1" onclick="remove_room(this.id)" /></div></div></div></div>';
            $("#hotel_rooms").append(append_data); //append new text box in main div
            $("#room_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 10 hotel room type are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_room(roomnum){
        $("#room_"+roomnum).css('background','tomato');
        $("#room_"+roomnum).fadeOut(800,function(){
           $("#room_"+roomnum).remove();
        });
    }
    </script>
      
    <script>
    $( function() {
        $( "#dFromDate" ).datepicker({ 
            dateFormat: 'mm/dd/yy',
            minDate: 0 
        });/*.on('change', function(selected){
            //var min_date = new Date(selected.timeStamp);
            alert(new Date(selected.timeStamp));
            //$('#validity_end_date').datepicker({minDate: min_date});
        });*/
        $( "#dToDate" ).datepicker({ dateFormat: 'mm/dd/yy', maxDate: "+2Y" });
        $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });
    });
    </script>

    
    
</body>

</html>