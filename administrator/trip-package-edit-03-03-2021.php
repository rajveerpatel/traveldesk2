<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    $tripid = $_GET['id'];

    if( !empty($_POST['trip_name']) ){
        
        if(!empty($_FILES['trip_thumb']['name'])){
            $trip_thumb=uploadfiles($_FILES['trip_thumb']['name'], $_FILES['trip_thumb']['tmp_name'], $_FILES['trip_thumb']['error'], $_FILES['trip_thumb']['size'], PACKAGE_IMG);
        }else{
            $trip_thumb=$_POST['old_thumb'];
        }
        
        if(!empty($_FILES['banner_img']['name'])){
            $banner_img=uploadfiles($_FILES['banner_img']['name'], $_FILES['banner_img']['tmp_name'], $_FILES['banner_img']['error'], $_FILES['banner_img']['size'], BANNER_IMG);
        }else{
            $banner_img=$_POST['old_banner'];
        }
        
        if(!empty($_POST['trip_types'])){
            $trip_type = implode(',', $_POST['trip_types']);
        } else {
            $trip_type = '';
        }
        
        if(!empty($_POST['trip_category'])){
            $trip_cat = implode(',', $_POST['trip_category']);
        } else {
            $trip_cat = '';
        }
        
        if(!empty($_POST['trip_provides'])){
            $trip_provides = implode(',', $_POST['trip_provides']);
        } else {
            $trip_provides = '';
        }
        
        $tripdays = $_POST['trip_days'] + $_POST['more_days'] ;
        
        echo $tsql = "UPDATE tbl_trip_packages SET 
        trip_name = '".$_POST['trip_name']."',
        trip_thumb = '".$trip_thumb."',
        banner_img = '".$banner_img."',
        classic_price = '".$_POST['classic_price']."',
        premium_price = '".$_POST['premium_price']."',
        deluxe_price = '".$_POST['deluxe_price']."',
        travel_start_date = '".date('Y-m-d', strtotime($_POST['travel_start_date']))."',
        travel_end_date = '".date('Y-m-d', strtotime($_POST['travel_end_date']))."',
        trip_days = '".$tripdays."',
        parent_dest = '".$_POST['parent_dest']."',
        country_dest = '".$_POST['country_dest']."',
        trip_types = '".$trip_type."',
        trip_category = '".$trip_cat."',
        short_details = '".mysqli_real_escape_string($con, $_POST['short_details'])."',
        trip_highlights = '".mysqli_real_escape_string($con, $_POST['trip_highlights'])."',
        trip_provides = '".$trip_provides."',
        trip_map = '".mysqli_real_escape_string($con, $_POST['trip_map'])."',
        status = '".$_POST['status']."'
        WHERE trip_id = $tripid ";
        mysqli_query($con, $tsql);
        
        //add itenerary
        if( sizeof( $_POST['itenerary_title'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
            mysqli_query($con, "DELETE FROM tbl_itineraries_catwise WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['itenerary_title'] as $key => $value ){
                 if( !empty( $_POST['itenerary_title'][$key]  ) ){
                    
                    if(!empty($_FILES['itinerary_image']['name'][$key])){
                        echo $itinerary_image=uploadfiles($_FILES['itinerary_image']['name'][$key], $_FILES['itinerary_image']['tmp_name'][$key], $_FILES['itinerary_image']['error'][$key], $_FILES['itinerary_image']['size'][$key], ITINERARY_IMG);
                    }else{
                        echo $itinerary_image=$_POST['old_itinerary_image'][$key];
                    }
                    
                    echo $isql = "insert into tbl_trip_itineraries set
                    trip_id = '$tripid',
                    day = '".$_POST['day'][$key]."',
                    itenerary_title = '".mysqli_real_escape_string($con, $_POST['itenerary_title'][$key])."',
                    itenerary_description = '".mysqli_real_escape_string($con, $_POST['itenerary_description'][$key])."',
                    itinerary_image = '".mysqli_real_escape_string($con, $itinerary_image)."',
                    city_id = '".mysqli_real_escape_string($con, $_POST['itinerary_city'][$key])."' ";
                    mysqli_query($con, $isql);
                     
                    $itinerary_id = mysqli_insert_id($con);
                                     
                    $catcnt = sizeof( $_POST['hotel_category'.$i]);
                    
                    if( $catcnt > 0 ){
                        $j=1;
                        foreach( $_POST['hotel'.$i] as $more => $val ){
                            if( !empty( $_POST['hotel'.$i][$more]  ) ){
                                
                                $catid = $_POST['hotel_category'.$i][$more];
                                $hotel_id = $_POST['hotel'.$i][$more];
                    
                                $activities = $_POST['activities'.$i.'-'.$j];
                                if(!empty($activities)){
                                    $activity_ids = implode(',', $activities);
                                } else {
                                    $activity_ids = '';
                                }

                                $transport = $_POST['transport_input'.$i][$more];

                                $breakfast = $_POST['breakfast_input'.$i][$more];
                                $lunch = $_POST['lunch_input'.$i][$more];
                                $dinner = $_POST['dinner_input'.$i][$more];

                                $travel_by = $_POST['travel_by'.$i.'-'.$j];
                                $travel_by = implode(',',$travel_by);
                                
                                $optional_1 = $_POST['optional_1-'.$i][$more];
                                $optional_price_1 = $_POST['optional_price_1-'.$i][$more];
                                $optional_2 = $_POST['optional_2-'.$i][$more];
                                $optional_price_2 = $_POST['optional_price_2-'.$i][$more];
                                $optional_3 = $_POST['optional_3-'.$i][$more];
                                $optional_price_3 = $_POST['optional_price_3-'.$i][$more];
                                
                                echo $sql = "insert into tbl_itineraries_catwise set
                                trip_id = '$tripid',
                                itinerary_id = '$itinerary_id',
                                cat_id = '$catid',
                                hotel_id = '$hotel_id',
                                activity_ids = '".mysqli_real_escape_string($con, $activity_ids)."',
                                breakfast = '".mysqli_real_escape_string($con, $breakfast)."',
                                lunch = '".mysqli_real_escape_string($con, $lunch)."',
                                dinner = '".mysqli_real_escape_string($con, $dinner)."',
                                transport = '".mysqli_real_escape_string($con, $transport)."',
                                travel_by = '".mysqli_real_escape_string($con, $travel_by)."',
                                optional_1_desc = '".mysqli_real_escape_string($con, $optional_1)."',
                                optional_1_cost = '".mysqli_real_escape_string($con, $optional_price_1)."',
                                optional_2_desc = '".mysqli_real_escape_string($con, $optional_2)."',
                                optional_2_cost = '".mysqli_real_escape_string($con, $optional_price_2)."',
                                optional_3_desc = '".mysqli_real_escape_string($con, $optional_3)."',
                                optional_3_cost = '".mysqli_real_escape_string($con, $optional_price_3)."' ";
                                mysqli_query($con, $sql);
                            }
                            $j++;
                        }
                    }
                     
                    $i++;
                 }
             }
         }
        
        //add trip summary
        if( sizeof( $_POST['summary_text'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_summaries WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['summary_text'] as $key => $value ){
                 if( !empty( $_POST['summary_text'][$key]  ) ){
                    
                    echo $q = "INSERT INTO tbl_trip_summaries set 
                    summary_text='".mysqli_real_escape_string($con, $_POST['summary_text'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
        //add trip inclusions and exclusions
        if( sizeof( $_POST['inclusions'] ) > 0 ){
            mysqli_query($con, "DELETE FROM trip_catwise_inc_exc WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['inclusions'] as $key => $value ){
                 if( !empty( $_POST['inclusions'][$key]  ) ){
                    
                    echo $q = "INSERT INTO trip_catwise_inc_exc set 
                    cat_id='".mysqli_real_escape_string($con, $_POST['inc_exc_cat'][$key])."',
                    inclusions='".mysqli_real_escape_string($con, $_POST['inclusions'][$key])."',
                    exclusions='".mysqli_real_escape_string($con, $_POST['exclusions'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
        //add trip rooms price
        if( sizeof( $_POST['single_room'] ) > 0 ){
            mysqli_query($con, "DELETE FROM trip_catwise_room_price WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['inclusions'] as $key => $value ){
                 if( !empty( $_POST['inclusions'][$key]  ) ){
                    
                    echo $q = "INSERT INTO trip_catwise_room_price set 
                    cat_id='".mysqli_real_escape_string($con, $_POST['inc_exc_cat'][$key])."',
                    single_room='".mysqli_real_escape_string($con, $_POST['single_room'][$key])."',
                    double_room='".mysqli_real_escape_string($con, $_POST['double_room'][$key])."',
                    twin_sharing='".mysqli_real_escape_string($con, $_POST['twin_sharing'][$key])."',
                    triple_sharing='".mysqli_real_escape_string($con, $_POST['triple_sharing'][$key])."',
                    quad_sharing='".mysqli_real_escape_string($con, $_POST['quad_sharing'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
        header("location: trip-packages.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = $tripid "));

    function cost_data(){
        global $con, $tripid;
        $sql = mysqli_query($con, "SELECT tc.*, c.category_name FROM tbl_trip_cost_datewise tc LEFT JOIN tbl_trip_categories c ON c.cat_id=tc.trip_cat_id WHERE tc.trip_id=$tripid ");
        $cost = array();
        while ($row = mysqli_fetch_assoc($sql)) {
            $id = $row['cost_id'];
            $start = $row['cost_date'];
            $title = $row['trip_cost'];
            $trip_cat = $row['category_name'];
            $costArray['id'] = $id;
            $costArray['title'] = '$'.$title.' ('.$trip_cat.')';
            $costArray['start'] = $start;
            $cost[] = $costArray;
        }
        echo json_encode($cost);
    }

?>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-center">Edit Trip Package</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <div class="text-right btn00">
            <a class="btn btn03 btn-width-03" href="trip-packages.php">Trip Packages</a>
        </div>
        <p class="space2"></p>        

        <div class="input-form">
            <form name="category" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_name">Trip Name*</label>
                            <input type="text" name="trip_name" id="trip_name" class="form-control form-control01" value="<?php echo $row['trip_name']; ?>" placeholder="Trip Name*" required>
                            <input type="hidden" id="trip_id" value="<?php echo $row['trip_id']; ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_thumb">Trip Image*</label>
                            <input type="file" name="trip_thumb" class="form-control form-control01" placeholder="Trip Thumb*">
                            <input type="hidden" name="old_thumb" value="<?php echo $row['trip_thumb']; ?>" >
                            <?php
                            if(!empty($row['trip_thumb'])){
                                echo '<img src="'.PACKAGE_IMG.$row['trip_thumb'].'" class="img-responsive" width="80">';
                            } 
                            ?>
                            <p class="img_note">Note : Trip Image size should be 400 x 279</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="classic_price">Classic Price($)</label>
                            <input type="text" name="classic_price" id="classic_price" class="form-control form-control01" value="<?php echo $row['classic_price']; ?>" placeholder="Classic Price($)">
                        </div>
                        
                        <div class="form-group">
                            <label class="user_login_label" for="premium_price">Premium Price($)</label>
                            <input type="text" name="premium_price" id="premium_price" class="form-control form-control01" value="<?php echo $row['premium_price']; ?>"  placeholder="Premium Price($)">
                        </div>
                        
                        <div class="form-group">
                            <label class="user_login_label" for="deluxe_price">Deluxe Price($)</label>
                            <input type="text" name="deluxe_price" id="deluxe_price" class="form-control form-control01" value="<?php echo $row['deluxe_price']; ?>" placeholder="Deluxe Price($)">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="luxury_price">Price in Calendar($)</label>
                            <div id="calendar"></div>
                            <textarea hidden id="costs"><?php cost_data(); ?></textarea>
                            <input type="hidden" id="tomorrow_date" value="<?php echo date('Y-m-d', strtotime('+1 day')); ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="travel_start_date">Travel Start Date*</label>
                            <input type="text" name="travel_start_date" id="travel_start_date" class="form-control form-control01 date-type" value="<?php if($row['travel_start_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($row['travel_start_date'])); } else { echo ''; } ?>" placeholder="Travel Start Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="travel_end_date">Travel End Date*</label>
                            <input type="text" name="travel_end_date" id="travel_end_date" class="form-control form-control01 date-type" value="<?php if($row['travel_end_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($row['travel_end_date'])); } else { echo ''; } ?>" placeholder="Travel End Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_dest">Parent Destination*</label>
                            <select name="parent_dest" id="parent_dest" class="form-control form-control01" onchange="return select_dest(this.value);" required>
                                <option value="">Select Parent Destination*</option>
                                <?php
                                $qry_pd = mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE status = 'active' ");
                                while($res_pd = mysqli_fetch_assoc($qry_pd)){
                                ?>
                                <option value="<?php echo $res_pd['parent_dest_id']; ?>" <?php if($res_pd['parent_dest_id']==$row['parent_dest']){ echo 'selected'; } ?>><?php echo $res_pd['parent_destination']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_dest">Country Destination*</label>
                            <select name="country_dest" id="country_dest" class="form-control form-control01" required>
                                <option value="">Select Country Destination*</option>
                                <?php
                                if(!empty($row['parent_dest'])){
                                    $destid = explode(':', $row['country_dest']);
                                    $destsql = mysqli_query($con, "SELECT * FROM tbl_destinations WHERE parent_dest = '".$row['parent_dest']."' ");
                                    while($destres = mysqli_fetch_assoc($destsql)){ ?>
                                        <option value="<?php echo $destres['dest_id'].':'.$destres['destination']; ?>" <?php if($destid[0]==$destres['dest_id']){ echo 'selected'; } ?>><?php echo $destres['destination']; ?></option>';
                                    <?php } } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_category">Select Trip Categories*</label>
                            <select name="trip_category[]" id="trip_category" class="form-control form-control01" multiple required>
                                <?php
                                $qry_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'active' ");
                                while($res_cat = mysqli_fetch_assoc($qry_cat)){
                                    $cat_ids = explode(',', $row['trip_category']);
                                ?>
                                <option value="<?php echo $res_cat['cat_id']; ?>" <?php if(in_array($res_cat['cat_id'], $cat_ids)){ echo 'selected'; } ?>><?php echo $res_cat['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_type">Select Trip Types*</label>
                            <select name="trip_types[]" class="form-control form-control01" id="trip_type" multiple required>
                            <?php
                                $qry_type = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'active' ");
                                while($res_type = mysqli_fetch_assoc($qry_type)){
                                    $types_id = explode(',', $row['trip_types']);
                            ?>
                                <option value="<?php echo $res_type['trip_type_id']; ?>" <?php if(in_array($res_type['trip_type_id'], $types_id)){ echo 'selected'; } ?>><?php echo $res_type['trip_type']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                
                <?php if(!empty($row['trip_days'])){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">Trip Days</label>
                            <?php if($row['trip_days'] < 10){ $zero = '0'; } else { $zero = ''; } ?>
                            <input type="text" class="form-control form-control01" id="trip_days" name="trip_days" value="<?php if($row['trip_days'] > 1 ){ echo $zero.$row['trip_days'].' Days'; } else { echo $zero.$row['trip_days'].' Day'; } ?>" readonly>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
                <?php
                $itinerary = mysqli_query($con, "SELECT * FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
                if(mysqli_num_rows($itinerary) > 0){ ?>
                    <div class="row">
                        <div class="col-md-12"><p class="head11">Itinerary Days</p></div>
                        <?php
                        $i=0;
                        while($result = mysqli_fetch_assoc($itinerary)){
                            ++$i;
                        ?>
                        <div class="itinerary_box">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="user_login_label">DAY <?php if($i < 10){ echo '0'.$i; } else { echo $i; } ?></label>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="day[]" value="<?php echo $i; ?>" >
                                    <input type="text" name="itenerary_title[]" class="form-control form-control01" value="<?php echo $result['itenerary_title']; ?>" placeholder="Itinerary Title">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="file" name="itinerary_image[]" class="form-control form-control01">
                                    <input type="hidden" name="old_itinerary_image[]" value="<?php echo $result['itinerary_image']; ?>">
                                    <?php
                                    if(!empty($result['itinerary_image'])){
                                        echo '<img src="'.ITINERARY_IMG.$result['itinerary_image'].'" class="img-responsive" width="80">';
                                    }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-<?php echo $i; ?>" placeholder="Itinerary Description"><?php echo $result['itenerary_description']; ?></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group city_box">
                                    <select name="itinerary_city[]" id="itinerary_city-<?php echo $i; ?>" class="cty_drpdwn form-control form-control01">
                                        <option value="">Select Itinerary City</option>
                                        <?php
                                        $qcity = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CountryId = '".$destid[0]."' AND CityStatus = '1' ");
                                        while($rcity = mysqli_fetch_assoc($qcity)){ ?>
                                        <option value="<?php echo $rcity['CityId']; ?>" <?php if($rcity['CityId']==$result['city_id']){ echo 'selected'; } ?>><?php echo $rcity['CityName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <p class="img_note">Note : If searched city doesn't have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="itinerary-sc">
                                    <div class="itinerary-tabs explore-tabs">
                                        <ul class="nav nav-tabs clearfix" role="tablist">
                                            <?php
                                            $j=1;
                                            $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                            while($catres = mysqli_fetch_assoc($catqry)){
                                            ?>
                                            <li <?php if($j==1) echo 'class="active"'; ?>><a data-toggle="tab" href="#<?php echo $catres['category_name'].$i; ?>"><?php echo $catres['category_name']; ?></a></li>
                                            <?php $j++; } ?>
                                        </ul>

                                        <div class="tab-content">
                                            <?php
                                            $j=1;
                                            $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                            while($catres = mysqli_fetch_assoc($catqry)){
                                                
                                                $cat_iti = mysqli_query($con, "SELECT * FROM tbl_itineraries_catwise WHERE trip_id = $tripid AND itinerary_id = '".$result['itid']."' AND cat_id = '".$catres['cat_id']."' ");
                                                $iti_res = mysqli_fetch_assoc($cat_iti);
                                            ?>
                                            <div id="<?php echo $catres['category_name'].$i; ?>" class="tab-pane fade <?php if($j==1){ echo 'in active'; } ?>">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="hotel_category<?php echo $i; ?>[]" id="hotel_category-<?php echo $i.'-'.$j; ?>" class="cat_drpdwn form-control form-control01" readonly>
                                                                        <?php 
                                                                        $rcat = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE cat_id='".$catres['cat_id']."' AND status= 'Active' "));
                                                                        ?>
                                                                        <option value="<?php echo $rcat['cat_id']; ?>"><?php echo $rcat['category_name']; ?></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="hotel<?php echo $i; ?>[]" id="hotel-<?php echo $i.'-'.$j; ?>" class="hotel_dropdown form-control form-control01">
                                                                        <option value="">Select Hotel</option>
                                                                        <?php 
                                                                        if( $catres['cat_id'] > 0 ){
                                                                            $cat_clause = ' AND category_id = '.$catres['cat_id'];
                                                                        }
                                                                        if( $result['city_id'] > 0 ){
                                                                            $city_clause = ' AND city_id = '.$result['city_id'];
                                                                        }
                                                                        $qhotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status= 'Active' $city_clause $cat_clause ");
                                                                        while($rhotel = mysqli_fetch_assoc($qhotel)){
                                                                        ?>
                                                                        <option value="<?php echo $rhotel['hotel_id']; ?>" <?php if($rhotel['hotel_id']=$iti_res['hotel_id']){ echo 'selected'; } ?>><?php echo $rhotel['hotel_name']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <select name="activities<?php echo $i.'-'.$j; ?>[]" id="activities-<?php echo $i.'-'.$j; ?>" class="act_drpdwn form-control form-control01" multiple>
                                                                        <?php 
                                                                        if( $result['city_id'] > 0 ){
                                                                            $city_clause = ' AND city_id = '.$result['city_id'];
                                                                        }
                                                                        $act_ids = explode(',', $iti_res['activity_ids']);
                                                                        $qact = mysqli_query($con, "SELECT * FROM tbl_activities WHERE status= 'Active' $city_clause ORDER BY activity_name");
                                                                        while($ract = mysqli_fetch_assoc($qact)){
                                                                        ?>
                                                                        <option value="<?php echo $ract['act_id']; ?>" <?php if(in_array($ract['act_id'], $act_ids)){ echo 'selected'; } ?>><?php echo $ract['activity_name']; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="user_login_label">Meal:</label>&nbsp;&nbsp;

                                                                    <input type="checkbox" class="check-input" name="meal<?php echo $i.'-'.$j; ?>[]" id="breakfast-<?php echo $i.'-'.$j; ?>" value="1" <?php if($iti_res['breakfast']==1){ echo 'checked'; } ?>>
                                                                    <input type="hidden" name="breakfast_input<?php echo $i; ?>[]" id="breakfast_input_<?php echo $i.'-'.$j; ?>" value="<?php echo $iti_res['breakfast']; ?>"> 
                                                                    <label class="user_login_label" for="breakfast-<?php echo $i.'-'.$j; ?>">Breakfast</label>&nbsp;&nbsp;

                                                                    <input type="checkbox" class="check-input" name="meal<?php echo $i.'-'.$j; ?>[]" id="lunch-<?php echo $i.'-'.$j; ?>" value="1" <?php if($iti_res['lunch']==1){ echo 'checked'; } ?>>
                                                                    <input type="hidden" name="lunch_input<?php echo $i; ?>[]" id="lunch_input_<?php echo $i.'-'.$j; ?>" value="<?php echo $iti_res['lunch']; ?>"> 
                                                                    <label class="user_login_label" for="lunch-<?php echo $i.'-'.$j; ?>">Lunch</label>&nbsp;&nbsp;

                                                                    <input type="checkbox" class="check-input" name="meal<?php echo $i.'-'.$j; ?>[]" id="dinner-<?php echo $i.'-'.$j; ?>" value="1" <?php if($iti_res['dinner']==1){ echo 'checked'; } ?>>
                                                                    <input type="hidden" name="dinner_input<?php echo $i; ?>[]" id="dinner_input_<?php echo $i.'-'.$j; ?>" value="<?php echo $iti_res['dinner']; ?>"> 
                                                                    <label class="user_login_label" for="dinner-<?php echo $i.'-'.$j; ?>">Dinner</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="user_login_label">Transport:</label>&nbsp;&nbsp;
                                                                    <input type="checkbox" class="check-input" name="transport<?php echo $i.'-'.$j; ?>[]" id="transport-<?php echo $i.'-'.$j; ?>" value="1" <?php if($iti_res['transport']==1){ echo 'checked'; } ?>>
                                                                    <input type="hidden" name="transport_input<?php echo $i; ?>[]" id="transport_input_<?php echo $i.'-'.$j; ?>" value="<?php echo $iti_res['transport']; ?>"> 
                                                                    <label class="user_login_label" for="transport-<?php echo $i.'-'.$j; ?>">Yes</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="user_login_label">Travel By:</label>&nbsp;&nbsp;
                                                                    <input type="radio" class="check-input" name="travel_by<?php echo $i.'-'.$j; ?>[]" id="flight-<?php echo $i.'-'.$j; ?>" value="1" <?php if($iti_res['travel_by']==1){ echo 'checked'; } ?>> 
                                                                    <label class="user_login_label" for="flight-<?php echo $i.'-'.$j; ?>">Flight</label>&nbsp;&nbsp;

                                                                    <input type="radio" class="check-input" name="travel_by<?php echo $i.'-'.$j; ?>[]" id="train-<?php echo $i.'-'.$j; ?>" value="2" <?php if($iti_res['travel_by']==2){ echo 'checked'; } ?>> 
                                                                    <label class="user_login_label" for="train-<?php echo $i.'-'.$j; ?>">Rail</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <textarea class="form-control form-control01" id="optional_1-<?php echo $i.'-'.$j; ?>" name="optional_1-<?php echo $i; ?>[]" rows="3" cols="40" placeholder="Optional 1"><?php echo $iti_res['optional_1_desc']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="optional_price_1-<?php echo $i.'-'.$j; ?>" name="optional_price_1-<?php echo $i; ?>[]" class="form-control form-control01" value="<?php echo $iti_res['optional_1_cost']; ?>" placeholder="Price for Optional 1 ($)">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <textarea class="form-control form-control01" id="optional_2-<?php echo $i.'-'.$j; ?>" name="optional_2-<?php echo $i; ?>[]" rows="3" cols="40" placeholder="Optional 2"><?php echo $iti_res['optional_2_desc']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="optional_price_2<?php echo $i.'-'.$j; ?>" name="optional_price_2-<?php echo $i; ?>[]" class="form-control form-control01" value="<?php echo $iti_res['optional_2_cost']; ?>" placeholder="Price for Optional 2 ($)">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <textarea class="form-control form-control01" id="optional_3-<?php echo $i.'-'.$j; ?>" name="optional_3-<?php echo $i; ?>[]" rows="3" cols="40" placeholder="Optional 3"><?php echo $iti_res['optional_3_desc']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="optional_price_3<?php echo $i.'-'.$j; ?>" name="optional_price_3-<?php echo $i; ?>[]" class="form-control form-control01" value="<?php echo $iti_res['optional_3_cost']; ?>" placeholder="Price for Optional 3 ($)">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- -->
                                            <?php $j++; } ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="more_days" class="form-control form-control01" onchange="fnitenerary(this.value)">
                                <option value="">Add More Itineraries</option>
                            <?php
                                $count = 30 - $row['trip_days'];
                                for($i=1; $i<=$count; $i++){
                                    if( $i < 10 ){
                                        $i = '0'.$i;
                                    }
                            ?>
                                <option value="<?php echo $i; ?>" ><?php echo $i; if($i < 2){ echo ' Day'; } else { echo ' Days'; } ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="itinerary"></div>
                    
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Trip Summary</p>
                        <div id="summary_boxes" class="summary_boxes">
                            <div class="row">
                                <?php
                                $sumsql = mysqli_query($con, "SELECT * FROM tbl_trip_summaries WHERE trip_id = $tripid ");
                                if(mysqli_num_rows($sumsql) > 0){
                                    $s=0;
                                    while($sumres = mysqli_fetch_assoc($sumsql)){
                                    ++$s;
                                ?>
                                <div class="col-md-6">
                                    <div id="box_<?php echo $s; ?>">
                                        <div class="form-group">
                                            <label class="user_login_label">Trip Summary <?php echo $s; ?>:</label>
                                            <div class="hotel-box">
                                                <input type="text" name="summary_text[]" id="summary_text_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $sumres['summary_text']; ?>">
                                            </div>   
                                            <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $s; ?>" onclick="remove_image(this.id)" /></div>
                                        </div>
                                    </div>
                                </div>
                                <?php }
                                } else { ?>
                                <div class="col-md-6">
                                    <div id="box_1">
                                        <div class="form-group">
                                            <label class="user_login_label">Trip Summary 1:</label>
                                            <div class="hotel-box">
                                                <input type="text" name="summary_text[]" id="summary_text_1" class="form-control  form-control01">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" id="summary_cnt" value="<?php echo $s; ?>">
                            </div>
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_summary" /></div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="short_details">Trip Short Details*</label>
                            <textarea id="short_details" name="short_details" class="form-control form-control01" rows="3"><?php echo $row['short_details']; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_highlights">Trip Highlights</label>
                            <textarea id="trip_highlights" name="trip_highlights" rows="10"><?php echo $row['trip_highlights']; ?></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'trip_highlights' );</script>
                        </div>
                    </div>
                            
                    <div class="col-md-12">
                        <div class="itinerary-sc">
                            <div class="itinerary-tabs">
                                <ul class="nav nav-tabs clearfix" role="tablist">
                                    <?php
                                    $j=1;
                                    $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                    while($catres = mysqli_fetch_assoc($catqry)){
                                    ?>
                                    <li <?php if($j==1) echo 'class="active"'; ?>><a data-toggle="tab" href="#<?php echo $catres['category_name']; ?>"><?php echo $catres['category_name']; ?></a></li>
                                    <?php $j++; } ?>
                                </ul>

                                <div class="tab-content">
                                    <?php
                                    $j=1;
                                    $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                    while($catres = mysqli_fetch_assoc($catqry)){
                                    ?>
                                    <div id="<?php echo $catres['category_name']; ?>" class="tab-pane fade <?php if($j==1){ echo 'in active'; } ?>">
                                        <div class="row">
                                            <?php
                                            $inc_exc_qry = mysqli_query($con, "SELECT * FROM trip_catwise_inc_exc WHERE trip_id = $tripid AND cat_id = '".$catres['cat_id']."' ");
                                            $trip_cat_inc_exc = mysqli_fetch_assoc($inc_exc_qry);
                                            ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="inclusions-<?php echo $j; ?>">Trip Inclusions (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="hidden" name="inc_exc_cat[]" value="<?php echo $catres['cat_id']; ?>">
                                                    <textarea cols="80" rows="4" name="inclusions[]" id="inclusions-<?php echo $j; ?>" class="inclusions form-control form-control01"><?php echo $trip_cat_inc_exc['inclusions']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="exclusions-<?php echo $j; ?>">Trip Exclusions (<?php echo $catres['category_name']; ?>)</label>
                                                    <textarea cols="80" rows="4" name="exclusions[]" id="exclusions-<?php echo $j; ?>" class="exclusions form-control form-control01"><?php echo $trip_cat_inc_exc['inclusions']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <?php
                                            $rooms_qry = mysqli_query($con, "SELECT * FROM trip_catwise_room_price WHERE trip_id = $tripid AND cat_id = '".$catres['cat_id']."' ");
                                            $trip_cat_rooms = mysqli_fetch_assoc($rooms_qry);
                                            ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="single_room-<?php echo $j; ?>">Single Room Price (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="number" min="0" name="single_room[]" id="single_room-<?php echo $j; ?>" class="form-control form-control01" value="<?php echo $trip_cat_rooms['single_room']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="double_room-<?php echo $j; ?>">Double Room Price (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="number" min="0" name="double_room[]" id="double_room-<?php echo $j; ?>" class="form-control form-control01" value="<?php echo $trip_cat_rooms['double_room']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="twin_sharing-<?php echo $j; ?>">Twin Sharing Price (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="number" min="0" name="twin_sharing[]" id="twin_sharing-<?php echo $j; ?>" class="form-control form-control01" value="<?php echo $trip_cat_rooms['twin_sharing']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="triple_sharing-<?php echo $j; ?>">Triple Sharing Price (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="number" min="0" name="triple_sharing[]" id="triple_sharing-<?php echo $j; ?>" class="form-control form-control01" value="<?php echo $trip_cat_rooms['triple_sharing']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="user_login_label" for="quad_sharing-<?php echo $j; ?>">Quad Sharing Price (<?php echo $catres['category_name']; ?>)</label>
                                                    <input type="number" min="0" name="quad_sharing[]" id="quad_sharing-<?php echo $j; ?>" class="form-control form-control01" value="<?php echo $trip_cat_rooms['quad_sharing']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div><!-- -->
                                    <?php $j++; } ?>

                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Map*</label>
                            <textarea cols="80" id="trip_map" class="form-control form-control01 map-area" name="trip_map" rows="3" required><?php echo $row['trip_map']; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Provides</label>
                            <div class="row">
                                <?php $provides = explode(',', $row['trip_provides']); ?>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Flights" id="flight" <?php if(in_array('Flights', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="flight"><img src="assets/images/e6.png" class="img-responsive provide_icons"> Flights</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Hotel" id="hotel" <?php if(in_array('Hotel', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="hotel"><img src="assets/images/e1.png" class="img-responsive provide_icons"> Hotel</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Meal" id="meal" <?php if(in_array('Meal', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="meal"><img src="assets/images/e2.png" class="img-responsive provide_icons"> Meal</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Activities" id="activities" <?php if(in_array('Activities', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="activities"><img src="assets/images/e4.png" class="img-responsive provide_icons"> Activities</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Transport" id="transport" <?php if(in_array('Transport', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="transport"><img src="assets/images/e3.png" class="img-responsive provide_icons"> Transport</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="checkbox" name="trip_provides[]" value="Rail" id="rail" <?php if(in_array('Rail', $provides)){ echo 'checked'; } ?>> 
                                    <label class="user_login_label" for="rail"><img src="assets/images/inn_city.png" class="img-responsive provide_icons"> Rail</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Banner Image</label>
                            <input type="file" name="banner_img" class="form-control form-control01" placeholder="Banner Image">
                            <input type="hidden" name="old_banner" value="<?php echo $row['banner_img']; ?>">
                            <?php
                            if(!empty($row['banner_img'])){
                                echo '<img src="'.BANNER_IMG.$row['banner_img'].'" class="img-responsive" width="150">';
                            }
                            ?>
                            <p class="img_note">Note : Banner Image size should be 1349 x 351</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Status*</label>
                            <select name="status" class="form-control form-control01" required>
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <button name="submit" class="btn btn03 btn-width-03">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<?php 
    include('elements/footer.php');
?>

<div class="trdek-pp">  
    <!-- Modal HTML -->
    <div id="calendar_price" class="modal fade">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title inline-img-t6">Add Trip Price</h4>
                </div>
                <div class="modal-body">
					<div class="space2"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="user_login_label" id="trip_date_show"></div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_price">Trip Price($)*</label>
                                <input type="text" class="form-control form-control01" id="trip_price" placeholder="Trip Price($)*" >
                            </div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_cat_price">Trip Category*</label>
                                <select class="form-control form-control01" id="trip_cat_price" >
                                    <option value="">Please Select</option>
                                    <?php
                                    $q_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                    while($r_cat = mysqli_fetch_assoc($q_cat)){
                                    ?>
                                    <option value="<?php echo $r_cat['cat_id']; ?>"><?php echo $r_cat['category_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="modal-footer btn00">
                    <button type="button" id="save_btn" class="btn btn03 btn-width-03" data-dismiss="modal">Save changes</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
</div> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
      
    <script src="assets/fullcalendar/lib/main.min.js"></script>
    <script src="assets/js/calendar_edit.js"></script>
      
    <script src="assets/select2/select2.min.js"></script>

    <!-- Multi Select JS -->
    <script src="assets/js/bootstrap-multiselect.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(function () {
            $('#trip_type').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Types*'
            });
        });
        
        $(function () {
            $('#trip_category').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Categories*'
            });
        });
    </script>

    <script>
    function select_dest(parent_id){
        $.ajax({
            url: 'get-destination.php',
            type: 'POST',
            data: { parentid:parent_id },
            success: function(response){

                if(response){
                    $('#country_dest').empty();
                    $('#country_dest').append(response);
                }
            }
          });
    }
    </script>
<?php
$catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
while($catres = mysqli_fetch_assoc($catqry)){
    $catoption .= '<option value="'.$catres['cat_id'].'">'.$catres['category_name'].'</option>';
}
?>

    <script>
    function fnitenerary(val){
        var dest = $("#country_dest").val();
        dest = dest.split(':');
        var dest_id = dest[0];
        
        var daynum = parseInt($("#trip_days").val())
        var cnt = parseInt(val)
        itinerary = '';
        //alert(cnt);
        itinerary='<div class="col-md-12"><p class="head11">Itinerary Days</p></div>';
        for( i=1; i<=cnt; ++i){
        if(daynum>0){
            daynum = parseInt(daynum)+parseInt(1);
        } else {
            daynum = 1;
        }
        num = daynum;
        if( daynum < 10 ){ daynum = '0'+daynum; } else { daynum = daynum; }
        itinerary+='<div class="itinerary_box"><div class="col-md-12"><div class="form-group"><label class="user_login_label">DAY '+daynum+'</label></div></div><div class="col-md-6"><div class="form-group"><input type="hidden" name="day[]" value="'+daynum+'" ><input type="text" name="itenerary_title[]" class="form-control form-control01" placeholder="Itinerary Title"></div></div><div class="col-md-6"><div class="form-group"><input type="file" name="itinerary_image[]" class="form-control form-control01"></div></div><div class="col-md-12"><div class="form-group"><textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-'+num+'" placeholder="Itinerary Description"></textarea></div></div><div class="col-md-12"><div class="form-group city_box"><select name="itinerary_city[]" id="itinerary_city-'+num+'" class="city_drpdwn form-control form-control01"><option value="">Select Itinerary City</option></select><p class="img_note">Note : If searched city does not have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p></div></div><div class="col-md-12"><div class="itinerary-sc"><div class="itinerary-tabs"><ul class="nav nav-tabs clearfix" role="tablist"><?php $j=1; $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' "); while($catres = mysqli_fetch_assoc($catqry)){ ?><li <?php if($j==1) echo 'class="active"'; ?>><a data-toggle="tab" href="#<?php echo $catres['category_name']; ?>'+num+'"><?php echo $catres['category_name']; ?></a></li><?php $j++; } ?></ul><div class="tab-content"><?php $j=1; $catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' "); while($catres = mysqli_fetch_assoc($catqry)){ ?><div id="<?php echo $catres['category_name']; ?>'+num+'" class="tab-pane fade <?php if($j==1){ echo 'in active'; } ?>"><div class="row"><div class="col-md-12"><div class="row"><div class="col-md-4"><div class="form-group"><select name="hotel_category'+num+'[]" id="hotel_category-'+num+'-<?php echo $j; ?>" class="cat_drpdwn form-control form-control01" readonly><option value="<?php echo $catres['cat_id']; ?>"><?php echo $catres['category_name']; ?></option></select></div></div><div class="col-md-4"><div class="form-group"><select name="hotel'+num+'[]" id="hotel-'+num+'-<?php echo $j; ?>" class="hotel_drpdwn form-control form-control01"><option value="">Select Hotel</option></select></div></div><div class="col-md-4"><div class="form-group"><select name="activities'+num+'-<?php echo $j; ?>[]" id="activities-'+num+'-<?php echo $j; ?>" class="activity_drpdwn form-control form-control01" multiple><option value="">Select Activities</option></select></div></div></div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><div class="form-group"><label class="user_login_label">Meal:</label>&nbsp;&nbsp;<input type="checkbox" class="check-input" name="meal'+num+'-<?php echo $j; ?>[]" id="breakfast-'+num+'-<?php echo $j; ?>" value="1"><input type="hidden" name="breakfast_input'+num+'[]" id="breakfast_input_'+num+'-<?php echo $j; ?>" value="0"> <label class="user_login_label" for="breakfast-'+num+'-<?php echo $j; ?>">Breakfast</label>&nbsp;&nbsp;<input type="checkbox" class="check-input" name="meal'+num+'-<?php echo $j; ?>[]" id="lunch-'+num+'-<?php echo $j; ?>" value="1"><input type="hidden" name="lunch_input'+num+'[]" id="lunch_input_'+num+'-<?php echo $j; ?>" value="0"> <label class="user_login_label" for="lunch-'+num+'-<?php echo $j; ?>">Lunch</label>&nbsp;&nbsp;<input type="checkbox" class="check-input" name="meal'+num+'-<?php echo $j; ?>[]" id="dinner-'+num+'-<?php echo $j; ?>" value="1"><input type="hidden" name="dinner_input'+num+'[]" id="dinner_input_'+num+'-<?php echo $j; ?>" value="0"> <label class="user_login_label" for="dinner-'+num+'-<?php echo $j; ?>">Dinner</label></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label">Transport:</label>&nbsp;&nbsp;<input type="checkbox" class="check-input" name="transport'+num+'-<?php echo $j; ?>[]" id="transport-'+num+'-<?php echo $j; ?>" value="1"><input type="hidden" name="transport_input'+num+'[]" id="transport_input_'+num+'-<?php echo $j; ?>" value="0"> <label class="user_login_label" for="transport-'+num+'-<?php echo $j; ?>">Yes</label></div></div><div class="col-md-4"><div class="form-group"><label class="user_login_label">Travel By:</label>&nbsp;&nbsp;<input type="radio" class="check-input" name="travel_by'+num+'-<?php echo $j; ?>[]" id="flight-'+num+'-<?php echo $j; ?>" value="1"> <label class="user_login_label" for="flight-'+num+'-<?php echo $j; ?>">Flight</label>&nbsp;&nbsp;<input type="radio" class="check-input" name="travel_by'+num+'-<?php echo $j; ?>[]" id="train-'+num+'-<?php echo $j; ?>" value="2"> <label class="user_login_label" for="train-'+num+'-<?php echo $j; ?>">Rail</label></div></div></div><div class="row"><div class="col-md-8"><div class="form-group"><textarea class="form-control form-control01" id="optional_1-'+num+'-<?php echo $j; ?>" name="optional_1-'+num+'[]" rows="3" cols="40" placeholder="Optional 1"></textarea></div></div><div class="col-md-4"><div class="form-group"><input type="text" id="optional_price_1'+num+'-<?php echo $j; ?>" name="optional_price_1-'+num+'[]" class="form-control form-control01" placeholder="Price for Optional 1 ($)"></div></div><div class="col-md-8"><div class="form-group"><textarea class="form-control form-control01" id="optional_2-'+num+'-<?php echo $j; ?>" name="optional_2-'+num+'[]" rows="3" cols="40" placeholder="Optional 2"></textarea></div></div><div class="col-md-4"><div class="form-group"><input type="text" id="optional_price_2'+num+'-<?php echo $j; ?>" name="optional_price_2-'+num+'[]" class="form-control form-control01" placeholder="Price for Optional 2 ($)"></div></div><div class="col-md-8"><div class="form-group"><textarea class="form-control form-control01" id="optional_3-'+num+'-<?php echo $j; ?>" name="optional_3-'+num+'[]" rows="3" cols="40" placeholder="Optional 3"></textarea></div></div><div class="col-md-4"><div class="form-group"><input type="text" id="optional_price_3-'+num+'-<?php echo $j; ?>" name="optional_price_3-'+num+'[]" class="form-control form-control01" placeholder="Price for Optional 3 ($)"></div></div></div></div></div></div><!-- --><?php $j++; } ?></div></div></div></div></div>';
        }       
        $("#itinerary").html(itinerary);
        
        //Display cities according to selected destination
        $.ajax({
            url: 'ajax_get_city.php',
            type: 'post',
            data: { country: dest_id },
            
            success: function(response){
                $('.city_drpdwn').empty();
                $('.city_drpdwn').append(response);
                $('.city_drpdwn').select2().on("select2:select", function (e) {
                    var city_element = $(e.currentTarget);
                    var serial_id = city_element.attr('id');
                    var city_id = city_element.val();
                    
                    serial_id = serial_id.split('-');
                    serial_no = serial_id[1];
                    
                    var catcnt = $('[id^="hotel_category-'+serial_no+'"]').length;
                    
                    for(var i=1; i<=catcnt; i++){
                        var catid = $('#hotel_category-'+serial_no+'-'+i).val();
                        var hotel_id = '#hotel-'+serial_no+'-'+i;

                        //get hotels of selected city
                        $.ajax({
                            url: 'ajax_get_hotel_activity.php',
                            async: false,
                            type: 'post',
                            data: { hotel_city_id: city_id, hotel_cat_id: catid },

                            success: function(response){
                                $(hotel_id).empty();
                                $(hotel_id).append(response);
                            }
                        });
                    }

                    //get activities of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        type: 'post',
                        data: { activity_city_id: city_id },

                        success: function(response){
                            $('[id^="activities-'+serial_no+'"]').multiselect();
                            var options = jQuery.parseJSON(response);
                            $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                        }
                    });
                });
            }
        });
        
        //$('.cat_drpdwn').append('<?php echo $catoption; ?>');
        
        //CKEDITOR.replaceAll( 'itinerary_detail' );
        
        $(function () {
            $('.activity_drpdwn').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Activities'
            });
        });
        
        $('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });
    }
    </script>

    <script>        
        $('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });     
        
        
        $('.cty_drpdwn').select2().on("select2:select", function (e) {
            var city_element = $(e.currentTarget);
            var serial_id = city_element.attr('id');
            var city_id = city_element.val();
            
            serial_id = serial_id.split('-');
            serial_no = serial_id[1];
                    
            var catcnt = $('[id^="hotel_category-'+serial_no+'"]').length;
            
            for(var i=1; i<=catcnt; i++){
                var catid = $('#hotel_category-'+serial_no+'-'+i).val();
                var hotel_id = '#hotel-'+serial_no+'-'+i;
            
                //get hotels of selected city
                $.ajax({
                    url: 'ajax_get_hotel_activity.php',
                    async: false,
                    type: 'post',
                    data: { hotel_city_id: city_id, hotel_cat_id: catid },

                    success: function(response){
                        $(hotel_id).empty();
                        $(hotel_id).append(response);
                    }
                });
            }

            //get activities of selected city
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { activity_city_id: city_id },

                success: function(response){
                    $('[id^="activities-'+serial_no+'"]').multiselect();
                    var options = jQuery.parseJSON(response);
                    $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                }
            });
        });
    </script>

    <script>
    //Display cities according to selected destination
    $(document).ready( function() {
        $('#country_dest').on('change', function(){
            var dest = $(this).val();
            var dest = dest.split(':');
            var dest_id = dest[0];
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country: dest_id },

                success: function(response){
                    $('.city_drpdwn').empty();
                    $('.city_drpdwn').append(response);
                    $('.city_drpdwn').select2();
                    
                    $('.cty_drpdwn').empty();
                    $('.cty_drpdwn').append(response);
                    $('.cty_drpdwn').select2();
                    
                    $('.hotel_dropdown').empty();
                    $('.hotel_dropdown').append('<option value="">Select Hotel</option>');
                    
                    /*$('.activity_drpdwn option').remove();
                    $('.activity_drpdwn').multiselect('rebuild');
                    $('.activity_drpdwn').multiselect('refresh');*/
                }
            });
        });
    } );
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var summary_cnt = $('#summary_cnt').val();
        if(summary_cnt!=''){
            var id = summary_cnt; 
        } else {
            var id = 1;
        }
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_summary").on('click',function(){
        if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="col-md-6"><div id="box_'+id+'"><div class="form-group"><label class="user_login_label">Trip Summary '+id+':</label><div class="hotel-box"><input type="text" name="summary_text[]" id="summary_text_'+id+'" class="form-control  form-control01"></div> <div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div></div></div>';
            $("#summary_boxes > .row").append(append_data); //append new text box in main div
            $("#box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 20 summary text are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_image(sumnum){
        $("#box_"+sumnum).parent().css('background','tomato');
        $("#box_"+sumnum).parent().fadeOut(800,function(){
           $("#box_"+sumnum).parent().remove();
        });
    }
    </script>

    <script>
        $('.cty_drpdwn').select2();
        $('.act_drpdwn').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            nonSelectedText: 'Select Activities'
        });
        CKEDITOR.replaceAll( 'itinerary_detail' );
        CKEDITOR.replaceAll( 'inclusions' );
        CKEDITOR.replaceAll( 'exclusions' );
    </script>

    <script>
    $( function() {
        $( "#travel_start_date" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
        $( "#travel_end_date" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
    } );
    </script>

  </body>
</html>
-----------------------------900514716342012417444176002
Content-Disposition: form-data; name="overwrite"

0