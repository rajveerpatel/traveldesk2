<?php
include('includes/config.inc.php');

$id = 0;
$checkval = '';
$checkfor = '';

//Manage Parent Destination for Menu
if( !empty($_POST['parent_dest_id']) ){
    $parent_dest = explode('-', $_POST['parent_dest_id']);
    $checkfor = $parent_dest[0];
    $id = $parent_dest[1];
    $checkval = $_POST['check_val'];
    
    if($checkval==0){
        $checkval = 1;
    } else {
        $checkval = 0;
    }
    
    if( $id > 0  && $checkfor == 'parent_menu_dest' ){
        mysqli_query($con, "UPDATE tbl_parent_destinations SET
        show_menu = '".$checkval."'
        WHERE parent_dest_id = $id " );
        
        echo $_POST['parent_dest_id'].'|'.$checkval;
    } elseif( $id > 0  && $checkfor == 'parent_home_dest' ){
        mysqli_query($con, "UPDATE tbl_parent_destinations SET
        show_home = '".$checkval."'
        WHERE parent_dest_id = $id " );
        
        echo $_POST['parent_dest_id'].'|'.$checkval;
    }
}

//Manage Destination for Menu
if( !empty($_POST['dest_id']) ){
    $destination = explode('-', $_POST['dest_id']);
    $checkfor = $destination[0];
    $id = $destination[1];
    $checkval = $_POST['check_val'];
    
    if($checkval==0){
        $checkval = 1;
    } else {
        $checkval = 0;
    }
    
    if( $id > 0  && $checkfor == 'menu_dest' ){
        mysqli_query($con, "UPDATE tbl_destinations SET
        show_menu = '".$checkval."'
        WHERE dest_id = $id " );
        
        echo $_POST['dest_id'].'|'.$checkval;
    }
}

//Manage Trip Types for Menu and Homepage 
if( !empty($_POST['trip_type_id']) ){
    $trip_type = explode('-', $_POST['trip_type_id']);
    $checkfor = $trip_type[0];
    $id = $trip_type[1];
    $checkval = $_POST['check_val'];
    
    if($checkval==0){
        $checkval = 1;
    } else {
        $checkval = 0;
    }
    
    if( $id > 0  && $checkfor == 'menu_dest' ){
        mysqli_query($con, "UPDATE tbl_trip_types SET
        show_menu = '".$checkval."'
        WHERE trip_type_id = $id " );
        
        echo $_POST['trip_type_id'].'|'.$checkval;
    } elseif( $id > 0  && $checkfor == 'home_dest' ){
        mysqli_query($con, "UPDATE tbl_trip_types SET
        show_home = '".$checkval."'
        WHERE trip_type_id = $id " );
        
        echo $_POST['trip_type_id'].'|'.$checkval;
    }
}

//Manage Trip Categories for Menu and Homepage 
if( !empty($_POST['cat_id']) ){
    $trip_cat = explode('-', $_POST['cat_id']);
    $checkfor = $trip_cat[0];
    $id = $trip_cat[1];
    $checkval = $_POST['check_val'];
    
    if($checkval==0){
        $checkval = 1;
    } else {
        $checkval = 0;
    }
    
    if( $id > 0  && $checkfor == 'menu_dest' ){
        mysqli_query($con, "UPDATE tbl_trip_categories SET
        show_menu = '".$checkval."'
        WHERE cat_id = $id " );
        
        echo $_POST['cat_id'].'|'.$checkval;
    } elseif( $id > 0  && $checkfor == 'home_dest' ){
        mysqli_query($con, "UPDATE tbl_trip_categories SET
        show_home = '".$checkval."'
        WHERE cat_id = $id " );
        
        echo $_POST['cat_id'].'|'.$checkval;
    }
}

//Manage Trip Styles for Menu
if( !empty($_POST['style_id']) ){
    $trip_style = explode('-', $_POST['style_id']);
    $checkfor = $trip_style[0];
    $id = $trip_style[1];
    $checkval = $_POST['check_val'];
    
    if($checkval==0){
        $checkval = 1;
    } else {
        $checkval = 0;
    }
    
    if( $id > 0  && $checkfor == 'menu_style' ){
        mysqli_query($con, "UPDATE tbl_trip_style SET
        show_menu = '".$checkval."'
        WHERE style_id = $id " );
        
        echo $_POST['style_id'].'|'.$checkval;
    }
}
?>