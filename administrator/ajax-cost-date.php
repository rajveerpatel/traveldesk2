<?php
include('includes/config.inc.php');

$action = '';
$db_type = '';
$trip_cost = '';
$cost_start_date = '';
$cost_id = '';
$cat_id = '';

$query = "SELECT tc.*, c.category_name FROM tbl_temp_trip_cost tc LEFT JOIN tbl_trip_categories c ON c.cat_id=tc.trip_cat_id";
$res = mysqli_query($con, $query) or die(mysqli_error($con));
$cost = array();
while ($row = mysqli_fetch_assoc($res)) {
    $id = $row['cost_id'];
    $start = $row['cost_date'];
    $title = $row['trip_cost'];
    $trip_cat = $row['category_name'];
    $costArray['id'] = $id;
    $costArray['title'] = '$'.$title.' ('.$trip_cat.')';
    $costArray['start'] = $start;
    $cost[] = $costArray;
}
echo json_encode($cost);

//Manage Parent Destination for Menu
if( !empty($_POST['action']) ){
    $action = $_POST['action'];
    $db_type = $_POST['db_type'];
    $trip_cost = $_POST['trip_cost'];
    $cost_start_date = date('Y-m-d', strtotime($_POST['cost_date']));
    $cat_id = $_POST['trip_category'];
    
    $cost_id = $_POST['cost_id'];
    
    //add cost in temporary table
    if($action == 'insert' && $db_type=='temp' && !empty($trip_cost)){
        echo $query = "INSERT INTO tbl_temp_trip_cost SET
        trip_cost = $trip_cost,
        cost_date = '$cost_start_date',
        trip_cat_id = '$cat_id' ";
        mysqli_query($con, $query);
    }
    
    //delete from temporary table
    if($action == 'delete' && $db_type=='temp' && $cost_id>0){
        mysqli_query($con, "DELETE from tbl_temp_trip_cost WHERE cost_id = $cost_id ");
    }
}
?>