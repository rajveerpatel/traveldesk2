<?php
include('includes/config.inc.php');

$cat_id = 0;
$first_city = '';
$last_city = '';

//get first city and display its hotels in pre trip hotel dropdown
if( !empty($_POST['first_city']) ){
    $first_city = $_POST['first_city'];
    $cat_id = $_POST['cat_id'];
    
    echo '<option value="">Please Select</option>';
    
    $q_pre_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= $first_city AND category_id = $cat_id ");
    while($pre_hotel = mysqli_fetch_assoc($q_pre_hotel)){
?>
    <option value="<?php echo $pre_hotel['hotel_id']; ?>"><?php echo $pre_hotel['hotel_name']; ?></option>
<?php  
    }
}

//get last city and display its hotels in pre trip hotel dropdown
if( !empty($_POST['last_city']) ){
    $last_city = $_POST['last_city'];
    $cat_id = $_POST['cat_id'];
    
    echo '<option value="">Please Select</option>';
    
    $q_pre_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= $last_city AND category_id = $cat_id ");
    while($pre_hotel = mysqli_fetch_assoc($q_pre_hotel)){
?>
    <option value="<?php echo $pre_hotel['hotel_id']; ?>"><?php echo $pre_hotel['hotel_name']; ?></option>
<?php  
    }
}

//get all trip according to selected category
if( !empty($_POST['action']) && $_POST['action']=='all_trip' ){
    $cat_id = $_POST['cat_id'];
    $destination = $_POST['destination'];
    
    if(isset($_POST['trip_id'])){
        $cond = " AND trip_id != '".$_POST['trip_id']."' ";
    } else {
        $cond = "";
    }
    
    $qry = "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND trip_category = $cat_id AND country_dest like '%".$destination."%' $cond ";
    $query = mysqli_query($con, $qry);
    if( mysqli_num_rows($query) > 0 ){
        while( $row = mysqli_fetch_assoc($query) ){
            $id = $row['trip_id'];
            $trip_name = $row['trip_name'];

            $trip_arr[] = array('label' => $trip_name,
                            'title' => $trip_name,
                            'value' => $id);
        }
        echo json_encode($trip_arr);
    } else {
        echo '<option value="">None</option>';
    }
    
}
