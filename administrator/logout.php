<?php 
ini_set("error_reporting", E_ALL & ~E_NOTICE);
ob_start();
session_start();
unset($_SESSION['LoGgEdInSuperAdminPerson']);
unset($_SESSION['AdminID']);
unset($_SESSION['AdminUser']);
unset($_SESSION['AdminName']);
unset($_SESSION['AdminEmail']);
unset($_SESSION['AdminType']);
header('Location:index.php');
?>
