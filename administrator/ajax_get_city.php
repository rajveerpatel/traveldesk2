<?php
include('includes/config.inc.php');

$id = 0;
$name = '';

//Get City from selected Country for hotel and activity master
if( !empty($_POST['country_id']) ){
    
    $id = $_POST['country_id'];
    
    $query = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CountryId = $id ORDER BY CityName " );
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="">Select City*</option>';
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['CityId'].'">'.$row['CityName'].'</option>';
        }
    }
    
}

//Get City from selected Country for itinerary
if( !empty($_POST['country']) ){
    
    $id = $_POST['country'];
    
    $query = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CountryId = $id ORDER BY CityName " );
    if( mysqli_num_rows($query) > 0 ){
        echo '<option value="">Select Itinerary City</option>';
        while( $row = mysqli_fetch_assoc($query) ){
            echo '<option value="'.$row['CityId'].'">'.$row['CityName'].'</option>';
        }
    }
    
}
?>