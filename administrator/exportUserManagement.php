<?php
include('include.inc.php');


// fetch the data
		
$csv='';
//Enter the headings of the excel columns
$csv="FULL NAME, EMAIL, PHONE, POST DATE, POST IP\n";
//Mysql query to get records from datanbase
//You can customize the query to filter from particular date and month etc...Which will depends your database structure.
// loop over the rows, outputting them
$sql1="SELECT * FROM tbl_user_management ORDER BY postdate DESC";
$qry1=mysqli_query($con, $sql1) or die(mysqli_connect_error());

if(mysqli_num_rows($qry1)>0){
while($row=mysqli_fetch_assoc($qry1)){
    
    $phone_no = mysqli_real_escape_string($con, $row['country_code'].'-'.$row['phone_no']);
    if($row['postdate']!='0000-00-00 00:00:00'){
        $postdate = date('d-m-Y', strtotime($row['postdate']));
    }
    $csv .= $row['first_name'].' '.$row['last_name'].','.$row['email'].','.$phone_no.','.$row['postdate'].','.$row['postip']."\n";	
}

}
// remove html and php tags etc.
$csv = strip_tags($csv); 

//header to make force download the file
header("Content-type: text/x-csv");
header("Content-type:text/octect-stream");
header("Content-Disposition: attachment; filename=UserManagement_".date('d-m-Y').".csv");
print $csv;



//For more examples related PHP visit www.webinfoipedia.com and enjoy demo and free download..
?>