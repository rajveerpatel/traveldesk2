<?php
include('includes/config.inc.php');

$id = 0;
$offer_price = '';
$offer_buy_date = '';

//Update Offer price of checked deals trips  
if( !empty($_POST['trip_id']) ){
    $id = $_POST['trip_id'];
    $offer_price = $_POST['offer_price'];
    if(!empty($_POST['buy_date'])){
        $offer_buy_date = date('Y-m-d', strtotime($_POST['buy_date']));
    } else {
        $offer_buy_date = '';
    }
    
    if(!empty($_POST['start_date'])){
        $start_date = date('Y-m-d', strtotime($_POST['start_date']));
    } else {
        $start_date = '';
    }
    
    if(!empty($_POST['end_date'])){
        $end_date = date('Y-m-d', strtotime($_POST['end_date']));
    } else {
        $end_date = '';
    }
    
   mysqli_query($con, "UPDATE tbl_trip_packages SET offer_price = $offer_price, offer_buy_date = '$offer_buy_date', travel_start_date = '$start_date', travel_end_date = '$end_date' WHERE trip_id = $id ");
    
}

//Update Offer price as blank of unchecked deals trips  
if( !empty($_POST['tripid']) ){
    $id = $_POST['tripid'];
    
    $q = "UPDATE tbl_trip_packages SET offer_price = '', offer_buy_date = '' WHERE trip_id = $id ";
    mysqli_query($con, $q);
    
}

//Get details of checked element  
if( !empty($_POST['getid']) ){
    $id = $_POST['getid'];
    
    $sql = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = $id ");
    $row = mysqli_fetch_assoc($sql);
    
    if($row['offer_buy_date']!='0000-00-00'){
        $offer_buy_date = date('d-m-Y', strtotime($row['offer_buy_date']));
    } else {
        $offer_buy_date = '';
    }
    if($row['travel_start_date']!='0000-00-00'){
        $travel_start_date = date('d-m-Y', strtotime($row['travel_start_date']));
    } else {
        $travel_start_date = '';
    }
    if($row['travel_end_date']!='0000-00-00'){
        $travel_end_date = date('d-m-Y', strtotime($row['travel_end_date']));
    } else {
        $travel_end_date = '';
    }
    echo $tripdata = $row['offer_price'].'|'.$offer_buy_date.'|'.$travel_start_date.'|'.$travel_end_date;
}