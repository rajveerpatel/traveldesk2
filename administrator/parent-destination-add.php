<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('parent-destinations', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    if( !empty($_POST['parent_destination']) ){
        
        if(!empty($_FILES['thumb_img']['name'])){
            $thumb_img=uploadfiles($_FILES['thumb_img']['name'], $_FILES['thumb_img']['tmp_name'], $_FILES['thumb_img']['error'], $_FILES['thumb_img']['size'], DESTINATION_IMG);
        }else{
            $thumb_img='';
        }
        
        if(!empty($_FILES['banner_img']['name'])){
            $banner_img=uploadfiles($_FILES['banner_img']['name'], $_FILES['banner_img']['tmp_name'], $_FILES['banner_img']['error'], $_FILES['banner_img']['size'], BANNER_IMG);
        }else{
            $banner_img='';
        }

        $replacecharacters = array("+", "&", "#", "@", "#", "*", "(", ")", "!", "%", "$", "^", "=", " ", "'");		$slug=str_replace($replacecharacters,'-', trim($_POST['parent_destination']));
        
        mysqli_query($con, "INSERT INTO tbl_parent_destinations SET 
        parent_destination = '".$_POST['parent_destination']."',
        slug = '".strtolower($slug)."',
        thumb_img = '".$thumb_img."',
        banner_img = '".$banner_img."',
        status = '".$_POST['status']."',
        postdate=NOW() ");
        
        header("location: parent-destinations.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Add Parent Destination - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Parent Destination</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="parent-destinations.php">Parent Destination List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_destination">Parent Destination Name*</label>
                            <input type="text" id="parent_destination" name="parent_destination" class="form-control form-control01" placeholder="Parent Destination*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="thumb_img">Thumb Image*</label>
                            <input type="file" name="thumb_img" id="thumb_img" class="form-control form-control01" required>
                            <p class="img_note">Note : Parent Destination image should be any size</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="banner_img">Banner Image*</label>
                            <input type="file" name="banner_img" id="banner_img" class="form-control form-control01" placeholder="Banner Image*" required>
                            <p class="img_note">Note : Banner Image size should be 1349 x 629</p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="status" class="form-control form-select">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    
    
</body>

</html>