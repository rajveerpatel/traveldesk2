<?php 
    include('include.inc.php');
    include('elements/head.php');
    include('elements/header.php');

    $tripid = $_GET['id'];

    if( !empty($_POST['trip_name']) ){
        
        if(!empty($_FILES['trip_thumb']['name'])){
            $trip_thumb=uploadfiles($_FILES['trip_thumb']['name'], $_FILES['trip_thumb']['tmp_name'], $_FILES['trip_thumb']['error'], $_FILES['trip_thumb']['size'], PACKAGE_IMG);
        }else{
            $trip_thumb=$_POST['old_thumb'];
        }
        
        if(!empty($_POST['trip_themes'])){
            $trip_theme = implode(',', $_POST['trip_themes']);
        } else {
            $trip_theme = '';
        }
        
        /*if(!empty($_POST['trip_category'])){
            $trip_cat = implode(',', $_POST['trip_category']);
        } else {
            $trip_cat = '';
        }*/
        
        if(!empty($_POST['trip_category'])){
            $trip_cat = $_POST['trip_category'];
        } else {
            $trip_cat = '';
        }
        
        if(!empty($_POST['trip_type'])){
            $trip_type = implode(',', $_POST['trip_type']);
        } else {
            $trip_type = '';
        }
        
        if(!empty($_POST['validity_start_date'])){
            $validity_start_date = date('Y-m-d', strtotime($_POST['validity_start_date']));
        } else {
            $validity_start_date = '';
        }
        
        if(!empty($_POST['validity_end_date'])){
            $validity_end_date = date('Y-m-d', strtotime($_POST['validity_end_date']));
        } else {
            $validity_end_date = '';
        }
        
        /*if(!empty($_POST['trip_provides'])){
            $trip_provides = implode(',', $_POST['trip_provides']);
        } else {
            $trip_provides = '';
        }*/
        
        $tripdays = $_POST['trip_days'] + $_POST['more_days'] ;
        
        $tsql = "UPDATE tbl_trip_packages SET 
        trip_name = '".mysqli_real_escape_string($con, $_POST['trip_name'])."',
        trip_thumb = '".$trip_thumb."',
        banner_id = '".$_POST['banner_id']."',
        trip_days = '".$tripdays."',
        parent_dest = '".$_POST['parent_dest']."',
        country_dest = '".$_POST['country_dest']."',
        trip_themes = '".$trip_theme."',
        trip_category = '".$trip_cat."',
        trip_type = '".$trip_type."',
        validity_start_date = '".$validity_start_date."',
        validity_end_date = '".$validity_end_date."',
        short_highlights = '".mysqli_real_escape_string($con, $_POST['short_highlights'])."',
        trip_highlights_inclusions = '".mysqli_real_escape_string($con, $_POST['trip_highlights_inclusions'])."',
        single_room = '".mysqli_real_escape_string($con, $_POST['single_room'])."',
        twin_sharing = '".mysqli_real_escape_string($con, $_POST['twin_sharing'])."',
        triple_sharing = '".mysqli_real_escape_string($con, $_POST['triple_sharing'])."',
        quad_sharing = '".mysqli_real_escape_string($con, $_POST['quad_sharing'])."',
        trip_map = '".mysqli_real_escape_string($con, $_POST['trip_map'])."',
        trip_stopover = '".$_POST['trip_stopover']."',
        trip_extension = '".$_POST['trip_extension']."',
        pre_trip_hotel = '".$_POST['pre_trip_hotel']."',
        post_trip_hotel = '".$_POST['post_trip_hotel']."',
        check_ext_st = '".$_POST['check_ext_st']."',
        status = '".$_POST['status']."'
        WHERE trip_id = $tripid ";
        mysqli_query($con, $tsql);
        
        //add itenerary
        if( sizeof( $_POST['itenerary_title'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['itenerary_title'] as $key => $value ){
                 if( !empty( $_POST['itenerary_title'][$key]  ) ){
                    
                    if(!empty($_FILES['itinerary_image']['name'][$key])){
                        $itinerary_image=uploadfiles($_FILES['itinerary_image']['name'][$key], $_FILES['itinerary_image']['tmp_name'][$key], $_FILES['itinerary_image']['error'][$key], $_FILES['itinerary_image']['size'][$key], ITINERARY_IMG);
                    }else{
                        $itinerary_image=$_POST['old_itinerary_image'][$key];
                    }
                    
                    $activities = $_POST["activities$i"];
                    if(!empty($activities)){
                        echo $activity_ids = implode(',', $activities);
                    } else {
                        echo $activity_ids = '';
                    }
                    
                    $meal = $_POST['meal'.$i];
                    if(!empty($meal)){
                        echo $meals = implode(',', $meal);
                    } else {
                        echo $meals = '';
                    }
                    
                    $isql = "insert into tbl_trip_itineraries set
                    trip_id = '$tripid',
                    cat_id = '$trip_cat',
                    day = '".$_POST['day'][$key]."',
                    itenerary_title = '".mysqli_real_escape_string($con, $_POST['itenerary_title'][$key])."',
                    itenerary_description = '".mysqli_real_escape_string($con, $_POST['itenerary_description'][$key])."',
                    itinerary_image = '".mysqli_real_escape_string($con, $itinerary_image)."',
                    hotel = '".mysqli_real_escape_string($con, $_POST['hotel'][$key])."',
                    activities = '".mysqli_real_escape_string($con, $activity_ids)."',
                    meal = '".mysqli_real_escape_string($con, $meals)."',
                    city_id = '".mysqli_real_escape_string($con, $_POST['itinerary_city'][$key])."' ";
                    mysqli_query($con, $isql);
                     
                    $itinerary_id = mysqli_insert_id($con);
                     
                    $i++;
                 }
             }
         }
        
        //add trip prices
        if( sizeof( $_POST['trip_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['trip_start_date'] as $key => $value ){
                 if( !empty( $_POST['trip_start_date'][$key]  ) ){
                    
                    $q = "INSERT INTO tbl_trip_prices set 
                    trip_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_start_date'][$key])))."',
                    trip_end_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_end_date'][$key])))."',
                    trip_categories='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_styles='".mysqli_real_escape_string($con, $_POST['trip_styles'][$key])."',
                    trip_price='".mysqli_real_escape_string($con, $_POST['trip_price'][$key])."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        }
        
        header("location: trip-packages.php");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE trip_id = $tripid "));
    
    $cat_id = $row['trip_category'];

?>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<section class="con-a">
    <div class="container">
        <h2 class="head04 text-center">Edit Trip Package</h2>
        <hr class="hr09">
        <p class="space2"></p>
        <div class="text-right btn00">
            <a class="btn btn03 btn-width-03" href="trip-packages.php">Trip Packages</a>
        </div>
        <p class="space2"></p>        

        <div class="input-form">
            <form name="category" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_name">Trip Name*</label>
                            <input type="text" name="trip_name" id="trip_name" class="form-control form-control01" value="<?php echo $row['trip_name']; ?>" placeholder="Trip Name*" required>
                            <input type="hidden" id="trip_id" value="<?php echo $row['trip_id']; ?>">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_thumb">Trip Image*</label>
                            <input type="file" name="trip_thumb" class="form-control form-control01" placeholder="Trip Thumb*">
                            <input type="hidden" name="old_thumb" value="<?php echo $row['trip_thumb']; ?>" >
                            <?php
                            if(!empty($row['trip_thumb'])){
                                echo '<img src="'.PACKAGE_IMG.$row['trip_thumb'].'" class="img-responsive" width="80">';
                            } 
                            ?>
                            <p class="img_note">Note : Trip Image size should be 400 x 279</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_sku">Trip SKU</label>
                            <input type="text" name="trip_sku" id="trip_sku" class="form-control form-control01" value="<?php echo $row['trip_sku']; ?>" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_type">Trip Type</label>
                            <select name="trip_type[]" id="trip_type" class="form-control form-control01" multiple required>
                                <?php
                                $qry_style = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                while($res_style = mysqli_fetch_assoc($qry_style)){
                                    $style_ids = explode(',', $row['trip_type']);
                                ?>
                                <option value="<?php echo $res_style['style_id']; ?>" <?php if(in_array($res_style['style_id'], $style_ids)){ echo 'selected'; } ?>><?php echo $res_style['trip_style']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Trip Price (trip type and date wise)</p>
                        <div id="price_boxes" class="price_boxes">
                            <?php
                            $pricesql = mysqli_query($con, "SELECT * FROM tbl_trip_prices WHERE trip_id = $tripid AND trip_categories = $cat_id ");
                            if(mysqli_num_rows($pricesql) > 0){
                                $s=0;
                                while($priceres = mysqli_fetch_assoc($pricesql)){
                                ++$s;
                            ?>
                            <div id="price_box_<?php echo $s; ?>" class="price_box">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_start_date_<?php echo $s; ?>">Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_start_date[]" id="trip_start_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($priceres['trip_start_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($priceres['trip_start_date'])); } ?>">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_end_date_<?php echo $s; ?>">End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_end_date[]" id="trip_end_date_<?php echo $s; ?>" class="form-control form-control01 date-type" value="<?php if($priceres['trip_end_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($priceres['trip_end_date'])); } ?>">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_styles_<?php echo $s; ?>">Trip Type</label>
                                            <div class="hotel-box">
                                                <select name="trip_styles[]" id="trip_styles_<?php echo $s; ?>" class="form-control form-control01">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                                    while($style_res = mysqli_fetch_assoc($style_qry)){
                                                    ?>
                                                    <option value="<?php echo $style_res['style_id']; ?>" <?php if($style_res['style_id']==$priceres['trip_styles']){ echo 'selected'; } ?>><?php echo $style_res['trip_style']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_price_<?php echo $s; ?>">Trip Price</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_price[]" id="trip_price_<?php echo $s; ?>" class="form-control form-control01" value="<?php echo $priceres['trip_price']; ?>">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="remove-box"><img src="assets/images/remove-icon.png" id="<?php echo $s; ?>" onclick="remove_image(this.id)" /></div>
                            </div>
                            <?php }
                            } else { ?>
                            <div id="price_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_start_date_1">Start Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_start_date[]" id="trip_start_date_1" class="form-control form-control01 date-type" placeholder="Start Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_end_date_1">End Date</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_end_date[]" id="trip_end_date_1" class="form-control form-control01 date-type" placeholder="End Date">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_styles_1">Trip Type</label>
                                            <div class="hotel-box">
                                                <select name="trip_styles[]" id="trip_styles_1" class="form-control form-control01">
                                                    <option value="">Please Select</option>
                                                    <?php
                                                    $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' ");
                                                    while($style_res = mysqli_fetch_assoc($style_qry)){
                                                    ?>
                                                    <option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="user_login_label" for="trip_price_1">Trip Price ($)</label>
                                            <div class="hotel-box">
                                                <input type="text" name="trip_price[]" id="trip_price_1" class="form-control form-control01" placeholder="Trip Price ($)">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <input type="hidden" id="prices_cnt" value="<?php echo $s; ?>">
                        </div>
                        <div align="right"><img src="assets/images/plus_icon.png" class="plus_icon22 add_price" /></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_start_date">Trip Validity Start Date*</label>
                            <input type="text" name="validity_start_date" id="validity_start_date" class="form-control form-control01 date-type" value="<?php if($row['validity_start_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($row['validity_start_date'])); } else { echo ''; } ?>" placeholder="Travel Start Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_end_date">Trip Validity End Date*</label>
                            <input type="text" name="validity_end_date" id="validity_end_date" class="form-control form-control01 date-type" value="<?php if($row['validity_end_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($row['validity_end_date'])); } else { echo ''; } ?>" placeholder="Travel End Date*" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_dest">Parent Destination*</label>
                            <select name="parent_dest" id="parent_dest" class="form-control form-control01" onchange="return select_dest(this.value);" required>
                                <option value="">Select Parent Destination*</option>
                                <?php
                                $qry_pd = mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE status = 'active' ");
                                while($res_pd = mysqli_fetch_assoc($qry_pd)){
                                ?>
                                <option value="<?php echo $res_pd['parent_dest_id']; ?>" <?php if($res_pd['parent_dest_id']==$row['parent_dest']){ echo 'selected'; } ?>><?php echo $res_pd['parent_destination']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_dest">Country Destination*</label>
                            <select name="country_dest" id="country_dest" class="form-control form-control01" required>
                                <option value="">Select Country Destination*</option>
                                <?php
                                if(!empty($row['parent_dest'])){
                                    $destid = explode(':', $row['country_dest']);
                                    $destsql = mysqli_query($con, "SELECT * FROM tbl_destinations WHERE parent_dest = '".$row['parent_dest']."' ");
                                    while($destres = mysqli_fetch_assoc($destsql)){ ?>
                                        <option value="<?php echo $destres['dest_id'].':'.$destres['destination']; ?>" <?php if($destid[0]==$destres['dest_id']){ echo 'selected'; } ?>><?php echo $destres['destination']; ?></option>';
                                    <?php } } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_category">Select Trip Category*</label>
                            <select name="trip_category" id="trip_category" class="form-control form-control01" required>
                                <option value="">Please Select</option>
                                <?php
                                $qry_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'active' ORDER BY order_no, order_no=0 ");
                                while($res_cat = mysqli_fetch_assoc($qry_cat)){
                                    $cat_id = $row['trip_category'];
                                ?>
                                <option value="<?php echo $res_cat['cat_id']; ?>" <?php if($res_cat['cat_id']==$cat_id){ echo 'selected'; } ?>><?php echo $res_cat['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_theme">Select Trip Themes*</label>
                            <select name="trip_themes[]" class="form-control form-control01" id="trip_theme" multiple required>
                            <?php
                                $qry_type = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE status = 'active' ");
                                while($res_type = mysqli_fetch_assoc($qry_type)){
                                    $types_id = explode(',', $row['trip_themes']);
                            ?>
                                <option value="<?php echo $res_type['trip_type_id']; ?>" <?php if(in_array($res_type['trip_type_id'], $types_id)){ echo 'selected'; } ?>><?php echo $res_type['trip_type']; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                
                <?php if(!empty($row['trip_days'])){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">Trip Days</label>
                            <?php if($row['trip_days'] < 10){ $zero = '0'; } else { $zero = ''; } ?>
                            <input type="text" class="form-control form-control01" id="trip_days" name="trip_days" value="<?php if($row['trip_days'] > 1 ){ echo $zero.$row['trip_days'].' Days'; } else { echo $zero.$row['trip_days'].' Day'; } ?>" readonly>
                        </div>
                    </div>
                <?php } ?>
                </div>
                
                <?php
                $itinerary = mysqli_query($con, "SELECT * FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
                if(mysqli_num_rows($itinerary) > 0){ ?>
                    <div class="row">
                        <div class="col-md-12"><p class="head11">Itinerary Days</p></div>
                        <?php
                        $i=0;
                        while($result = mysqli_fetch_assoc($itinerary)){
                            ++$i;
                            if($i==1){ $first_city = $result['city_id']; }
                            $last_city = $result['city_id'];
                        ?>
                        <div class="itinerary_box clearfix">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="user_login_label">DAY <?php if($i < 10){ echo '0'.$i; } else { echo $i; } ?></label>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="day[]" value="<?php echo $i; ?>" >
                                    <input type="text" name="itenerary_title[]" class="form-control form-control01" value="<?php echo $result['itenerary_title']; ?>" placeholder="Itinerary Title">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="file" name="itinerary_image[]" class="form-control form-control01">
                                    <input type="hidden" name="old_itinerary_image[]" value="<?php echo $result['itinerary_image']; ?>">
                                    <?php
                                    if(!empty($result['itinerary_image'])){
                                        echo '<img src="'.ITINERARY_IMG.$result['itinerary_image'].'" class="img-responsive" width="80">';
                                    }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-<?php echo $i; ?>" placeholder="Itinerary Description"><?php echo $result['itenerary_description']; ?></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group city_box">
                                    <select name="itinerary_city[]" id="itinerary_city-<?php echo $i; ?>" class="cty_drpdwn form-control form-control01">
                                        <option value="">Select Itinerary City</option>
                                        <?php
                                        $qcity = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CountryId = '".$destid[0]."' AND CityStatus = '1' ");
                                        while($rcity = mysqli_fetch_assoc($qcity)){ ?>
                                        <option value="<?php echo $rcity['CityId']; ?>" <?php if($rcity['CityId']==$result['city_id']){ echo 'selected'; } ?>><?php echo $rcity['CityName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <p class="img_note">Note : If searched city doesn't have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="hotel[]" id="hotel-<?php echo $i; ?>" class="hotel_dropdown form-control form-control01">
                                                <option value="">Select Hotel</option>
                                                <?php 
                                                if( $result['cat_id'] > 0 ){
                                                    $cat_clause = ' AND category_id = '.$result['cat_id'];
                                                }
                                                if( $result['city_id'] > 0 ){
                                                    $city_clause = ' AND city_id = '.$result['city_id'];
                                                }
                                                $qhotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status= 'Active' $city_clause $cat_clause ");
                                                while($rhotel = mysqli_fetch_assoc($qhotel)){
                                                ?>
                                                <option value="<?php echo $rhotel['hotel_id']; ?>" <?php if($rhotel['hotel_id']=$result['hotel']){ echo 'selected'; } ?>><?php echo $rhotel['hotel_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="activities<?php echo $i; ?>[]" id="activities-<?php echo $i; ?>" class="act_drpdwn form-control form-control01" multiple>
                                                <?php 
                                                if( $result['city_id'] > 0 ){
                                                    $city_clause = ' AND city_id = '.$result['city_id'];
                                                }
                                                $act_ids = explode(',', $result['activities']);
                                                $qact = mysqli_query($con, "SELECT * FROM tbl_activities WHERE status= 'Active' $city_clause ORDER BY activity_name");
                                                while($ract = mysqli_fetch_assoc($qact)){
                                                ?>
                                                <option value="<?php echo $ract['act_id']; ?>" <?php if(in_array($ract['act_id'], $act_ids)){ echo 'selected'; } ?>><?php echo $ract['activity_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="user_login_label">Meal:</label>&nbsp;&nbsp;
                                            <?php $meal = explode(',',$result['meal']); ?>
                                            <input type="checkbox" class="check-input" name="meal<?php echo $i; ?>[]" id="breakfast-<?php echo $i; ?>" value="Breakfast" <?php if(in_array('Breakfast', $meal)){ echo 'checked'; } ?>>
                                            <label class="user_login_label" for="breakfast-<?php echo $i; ?>">Breakfast</label>&nbsp;&nbsp;

                                            <input type="checkbox" class="check-input" name="meal<?php echo $i; ?>[]" id="lunch-<?php echo $i; ?>" value="Lunch" <?php if(in_array('Lunch', $meal)){ echo 'checked'; } ?>>
                                            <label class="user_login_label" for="lunch-<?php echo $i; ?>">Lunch</label>&nbsp;&nbsp;

                                            <input type="checkbox" class="check-input" name="meal<?php echo $i; ?>[]" id="dinner-<?php echo $i; ?>" value="Dinner" <?php if(in_array('Dinner', $meal)){ echo 'checked'; } ?>>
                                            <label class="user_login_label" for="dinner-<?php echo $i; ?>">Dinner</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="more_days" class="form-control form-control01" onchange="fnitenerary(this.value)">
                                <option value="">Add More Itineraries</option>
                            <?php
                                $count = 30 - $row['trip_days'];
                                for($i=1; $i<=$count; $i++){
                                    if( $i < 10 ){
                                        $i = '0'.$i;
                                    }
                            ?>
                                <option value="<?php echo $i; ?>" ><?php echo $i; if($i < 2){ echo ' Day'; } else { echo ' Days'; } ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="itinerary"></div>
                    
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="short_highlights">Trip Short Highlight*</label>
                            <textarea id="short_highlights" name="short_highlights" class="form-control form-control01" rows="3"><?php echo $row['short_highlights']; ?></textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_highlights_inclusions">Trip Highlights & Inclusions</label>
                            <textarea id="trip_highlights_inclusions" name="trip_highlights_inclusions" rows="10"><?php echo $row['trip_highlights_inclusions']; ?></textarea>
				            <script type="text/javascript">CKEDITOR.replace( 'trip_highlights_inclusions' );</script>
                        </div>
                    </div>
                            
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="single_room">Single Room</label>
                                    <input type="number" min="0" name="single_room" id="single_room" class="form-control form-control01" value="<?php echo $row['single_room']; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="twin_sharing">Twin Sharing</label>
                                    <input type="number" min="0" name="twin_sharing" id="twin_sharing" class="form-control form-control01" value="<?php echo $row['twin_sharing']; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="triple_sharing">Triple Sharing</label>
                                    <input type="number" min="0" name="triple_sharing" id="triple_sharing" class="form-control form-control01" value="<?php echo $row['triple_sharing']; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="user_login_label" for="quad_sharing">Quad Sharing</label>
                                    <input type="number" min="0" name="quad_sharing" id="quad_sharing" class="form-control form-control01" value="<?php echo $row['quad_sharing']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Map*</label>
                            <textarea cols="80" id="trip_map" class="form-control form-control01 map-area" name="trip_map" rows="3" required><?php echo $row['trip_map']; ?></textarea>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Pre-Trip Hotel</label>
                            <select name="pre_trip_hotel" id="pre_trip_hotel" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $q_pre_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= '".$first_city."' AND category_id = '".$row['trip_category']."' ");
                                while($pre_hotel = mysqli_fetch_assoc($q_pre_hotel)){
                                ?>
                                <option value="<?php echo $pre_hotel['hotel_id']; ?>" <?php if($pre_hotel['hotel_id']==$row['pre_trip_hotel']){ echo 'selected'; } ?>><?php echo $pre_hotel['hotel_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Post-Trip Hotel</label>
                            <select name="post_trip_hotel" id="post_trip_hotel" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $q_post_hotel = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE status = 'Active' AND city_id= '".$last_city."' AND category_id = '".$row['trip_category']."' ");
                                while($post_hotel = mysqli_fetch_assoc($q_post_hotel)){
                                ?>
                                <option value="<?php echo $post_hotel['hotel_id']; ?>" <?php if($post_hotel['hotel_id']==$row['post_trip_hotel']){ echo 'selected'; } ?>><?php echo $post_hotel['hotel_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label">This Trip as Stopover or Extension: <input type="checkbox" name="check_ext_st" id="check_ext_st" value="Yes" <?php if($row['check_ext_st']=="Yes"){ echo 'checked'; } ?>> Yes</label>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="stop_ext" <?php if($row['check_ext_st']=="Yes"){ echo 'style="display: none;"'; } ?>>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Trip Stopover</label>
                            <select name="trip_stopover" id="trip_stopover" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ts = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND trip_id!= '".$row['trip_id']."' AND trip_category = '".$row['trip_category']."' AND check_ext_st = 'Yes' ");
                                while($ts_res = mysqli_fetch_assoc($ts)){
                                ?>
                                <option value="<?php echo $ts_res['trip_id']; ?>" <?php if($ts_res['trip_id']==$row['trip_stopover']){ echo 'selected'; } ?>><?php echo $ts_res['trip_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Trip Extension</label>
                            <select name="trip_extension" id="trip_extension" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ts = mysqli_query($con, "SELECT * FROM tbl_trip_packages WHERE status = 'Active' AND trip_id!= '".$row['trip_id']."' AND trip_category = '".$row['trip_category']."' AND check_ext_st = 'Yes' ");
                                while($ts_res = mysqli_fetch_assoc($ts)){
                                ?>
                                <option value="<?php echo $ts_res['trip_id']; ?>" <?php if($ts_res['trip_id']==$row['trip_extension']){ echo 'selected'; } ?>><?php echo $ts_res['trip_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Banner Slider</label>
                            <select name="banner_id" id="banner_id" class="form-control form-control01">
                                <option value="">Please Select</option>
                                <?php
                                $ban = mysqli_query($con, "SELECT * FROM tbl_banners WHERE status = 'Active' and banner_for = 'trip' ");
                                while($banres = mysqli_fetch_assoc($ban)){
                                ?>
                                <option value="<?php echo $banres['pbid']; ?>" <?php if($banres['pbid']==$row['banner_id']){ echo 'selected'; } ?>><?php echo $banres['pagename']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Status*</label>
                            <select name="status" class="form-control form-control01" required>
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="btn00 text-center">
                            <button name="submit" class="btn btn03 btn-width-03">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>

<?php 
    include('elements/footer.php');
?>

<div class="trdek-pp">  
    <!-- Modal HTML -->
    <div id="calendar_price" class="modal fade">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title inline-img-t6">Add Trip Price</h4>
                </div>
                <div class="modal-body">
					<div class="space2"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="user_login_label" id="trip_date_show"></div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_price">Trip Price($)*</label>
                                <input type="text" class="form-control form-control01" id="trip_price" placeholder="Trip Price($)*" >
                            </div>
                            <div class="form-group">
                                <label class="user_login_label" for="trip_cat_price">Trip Category*</label>
                                <select class="form-control form-control01" id="trip_cat_price" >
                                    <option value="">Please Select</option>
                                    <?php
                                    $q_cat = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status='Active' ");
                                    while($r_cat = mysqli_fetch_assoc($q_cat)){
                                    ?>
                                    <option value="<?php echo $r_cat['cat_id']; ?>"><?php echo $r_cat['category_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
				</div>
                <div class="modal-footer btn00">
                    <button type="button" id="save_btn" class="btn btn03 btn-width-03" data-dismiss="modal">Save changes</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
</div> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
      
    <script src="assets/fullcalendar/lib/main.min.js"></script>
    <script src="assets/js/calendar_edit.js"></script>
      
    <script src="assets/select2/select2.min.js"></script>

    <!-- Multi Select JS -->
    <script src="assets/js/bootstrap-multiselect.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $('#check_ext_st').click(function(){
            if($(this).prop('checked')==true){
                $('#trip_stopover').prop('disabled', 'disabled');
                $('#trip_extension').prop('disabled', 'disabled');
                $('#stop_ext').css('display', 'none');
            } else {
                $('#stop_ext').css('display', 'block');
                $('#trip_stopover').removeAttr('disabled');
                $('#trip_extension').removeAttr('disabled');
            }
        });
    </script>

    <script>
        $(function () {
            $('#trip_theme').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Types*'
            });
        });
        
        /*$(function () {
            $('#trip_category').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Categories*'
            });
        });*/
        
        $(function () {
            $('#trip_type').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Trip Type*'
            });
        });
    </script>

    <script>
    function select_dest(parent_id){
        $.ajax({
            url: 'get-destination.php',
            type: 'POST',
            data: { parentid:parent_id },
            success: function(response){

                if(response){
                    $('#country_dest').empty();
                    $('#country_dest').append(response);
                }
            }
          });
    }
    </script>
<?php
$catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
while($catres = mysqli_fetch_assoc($catqry)){
    $catoption .= '<option value="'.$catres['cat_id'].'">'.$catres['category_name'].'</option>';
}
?>

    <script>
    function fnitenerary(val){
        var dest = $("#country_dest").val();
        dest = dest.split(':');
        var dest_id = dest[0];
        
        var daynum = parseInt($("#trip_days").val())
        var cnt = parseInt(val)
        itinerary = '';
        //alert(cnt);
        itinerary='<div class="col-md-12"><p class="head11">Itinerary Days</p></div>';
        for( i=1; i<=cnt; ++i){
        if(daynum>0){
            daynum = parseInt(daynum)+parseInt(1);
        } else {
            daynum = 1;
        }
        num = daynum;
        if( daynum < 10 ){ daynum = '0'+daynum; } else { daynum = daynum; }
        itinerary+='<div class="itinerary_box clearfix"><div class="col-md-12"><div class="form-group"><label class="user_login_label">DAY '+daynum+'</label></div></div><div class="col-md-6"><div class="form-group"><input type="hidden" name="day[]" value="'+daynum+'" ><input type="text" name="itenerary_title[]" class="form-control form-control01" placeholder="Itinerary Title"></div></div><div class="col-md-6"><div class="form-group"><input type="file" name="itinerary_image[]" class="form-control form-control01"></div></div><div class="col-md-12"><div class="form-group"><textarea name="itenerary_description[]" class="itinerary_detail form-control form-control01" id="itinerary_detail-'+num+'" placeholder="Itinerary Description"><?php echo $result['itenerary_description']; ?></textarea></div></div><div class="col-md-12"><div class="form-group city_box"><select name="itinerary_city[]" id="itinerary_city-'+num+'" class="city_drpdwn form-control form-control01"><option value="">Select Itinerary City</option></select><p class="img_note">Note : If searched city doesn\'t have in above dropdown please click on link to <a href="city-add.php" target="_blank"><strong>add city</strong></a></p></div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><div class="form-group"><select name="hotel[]" id="hotel-'+num+'" class="hotel_dropdown form-control form-control01"><option value="">Select Hotel</option></select></div></div><div class="col-md-4"><div class="form-group"><select name="activities'+num+'[]" id="activities-'+num+'" class="activity_drpdwn form-control form-control01" multiple></select></div></div><div class="col-md- 4"><div class="form-group"><label class="user_login_label">Meal:</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+num+'[]" id="breakfast-'+num+'" value="Breakfast"> <label class="user_login_label" for="breakfast-'+num+'">Breakfast</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+num+'[]" id="lunch-'+num+'" value="Lunch"> <label class="user_login_label" for="lunch-'+num+'">Lunch</label>&nbsp;&nbsp; <input type="checkbox" class="check-input" name="meal'+num+'[]" id="dinner-'+num+'" value="Dinner"> <label class="user_login_label" for="dinner-'+num+'">Dinner</label></div></div></div></div></div>';
        }       
        $("#itinerary").html(itinerary);
        
        //Display cities according to selected destination
        $.ajax({
            url: 'ajax_get_city.php',
            type: 'post',
            data: { country: dest_id },
            
            success: function(response){
                $('.city_drpdwn').empty();
                $('.city_drpdwn').append(response);
                $('.city_drpdwn').select2().on("select2:select", function (e) {
                    var city_element = $(e.currentTarget);
                    var serial_id = city_element.attr('id');
                    var city_id = city_element.val();
                    
                    serial_id = serial_id.split('-');
                    serial_no = serial_id[1];
                    
                    var catid = $('#trip_category option:selected').val();

                    //get hotels of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        async: false,
                        type: 'post',
                        data: { hotel_city_id: city_id, hotel_cat_id: catid },

                        success: function(response){
                            $('#hotel-'+serial_no).empty();
                            $('#hotel-'+serial_no).append(response);
                        }
                    });

                    //get activities of selected city
                    $.ajax({
                        url: 'ajax_get_hotel_activity.php',
                        type: 'post',
                        data: { activity_city_id: city_id },

                        success: function(response){
                            $('[id^="activities-'+serial_no+'"]').multiselect();
                            var options = jQuery.parseJSON(response);
                            $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                        }
                    });
                    
                    
                    //get first and last city and display its hotels in pre trip hotel dropdown
                    var day_length = $('select[name="itinerary_city[]"]').length;
                    alert(day_length);
                    alert(serial_no);
                    for(var i = 1; i <= day_length; i++){
                        if(i==1 && serial_no==i){
                            var first_city = $('#itinerary_city-'+i).val();

                            //get first city and display its hotels in pre trip hotel dropdown    
                            $.ajax({
                                url: 'ajax_get_dynamic_data.php',
                                type: 'post',
                                data: { first_city: first_city, cat_id: catid },

                                success: function(response){
                                    $('#pre_trip_hotel').empty();
                                    $('#pre_trip_hotel').append(response);
                                }
                            });
                        } else if(serial_no==day_length){
                            var last_city = $('#itinerary_city-'+i).val();

                            //get last city and display its hotels in post trip hotel dropdown
                            $.ajax({
                                url: 'ajax_get_dynamic_data.php',
                                type: 'post',
                                data: { last_city: last_city, cat_id: catid },

                                success: function(response){
                                    $('#post_trip_hotel').empty();
                                    $('#post_trip_hotel').append(response);
                                }
                            });
                        }
                    }
                });
            }
        });
        
        //$('.cat_drpdwn').append('<?php echo $catoption; ?>');
        
        //CKEDITOR.replaceAll( 'itinerary_detail' );
        
        $(function () {
            $('.activity_drpdwn').multiselect({
                inheritClass: true,
                buttonWidth: '100%',
                nonSelectedText: 'Select Activities'
            });
        });
        
        /*$('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });*/
    }
    </script>

    <script>        
        $('.check-input').click(function(){
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_name = fld_id[0];
            var fld_num = fld_id[1];
            var fld_cat_num = fld_id[2];
            
            if ( $(this).prop('checked') == true ) {
                var fld_val = 1;
            } else {
                var fld_val = 0;
            }
            
            $('#'+fld_name+'_input_'+fld_num+'-'+fld_cat_num).val(fld_val);
        });
        
        
        $('.cat_drpdwn').on('change', function(){
            var catid = $(this).val();
            var fld_id = $(this).attr('id');
            var fld_id = fld_id.split('-');
            var fld_num = fld_id[1];
            
            var cityid = $("#itinerary_city-"+fld_num).select2().val();
            
            //get hotels of selected city and category
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { cat_id: catid, city_id: cityid },

                success: function(response){
                    $('#hotel-'+fld_num).empty();
                    $('#hotel-'+fld_num).append(response);
                }
            });
        });     
        
        
        $('.cty_drpdwn').select2().on("select2:select", function (e) {
            var city_element = $(e.currentTarget);
            var serial_id = city_element.attr('id');
            var city_id = city_element.val();
            serial_id = serial_id.split('-');
            serial_no = serial_id[1];
                    
            var catid = $('#trip_category option:selected').val();
            
            //get hotels of selected city
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                async: false,
                type: 'post',
                data: { hotel_city_id: city_id, hotel_cat_id: catid },

                success: function(response){
                    $('#hotel-'+serial_no).empty();
                    $('#hotel-'+serial_no).append(response);
                }
            });

            //get activities of selected city
            $.ajax({
                url: 'ajax_get_hotel_activity.php',
                type: 'post',
                data: { activity_city_id: city_id },

                success: function(response){
                    $('[id^="activities-'+serial_no+'"]').multiselect();
                    var options = jQuery.parseJSON(response);
                    $('[id^="activities-'+serial_no+'"]').multiselect('dataprovider', options);
                }
            });
                    
                    
            //get first and last city and display its hotels in pre trip hotel dropdown
            var day_length = $('select[name="itinerary_city[]"]').length;
            for(var i = 1; i <= day_length; i++){
                if(i==1 && serial_no==i){
                    var first_city = $('#itinerary_city-'+i).val();
                    
                    //get first city and display its hotels in pre trip hotel dropdown    
                    $.ajax({
                        url: 'ajax_get_dynamic_data.php',
                        type: 'post',
                        data: { first_city: first_city, cat_id: catid },

                        success: function(response){
                            $('#pre_trip_hotel').empty();
                            $('#pre_trip_hotel').append(response);
                        }
                    });
                } else if(serial_no==day_length){
                    var last_city = $('#itinerary_city-'+i).val();
                    
                    //get last city and display its hotels in post trip hotel dropdown
                    $.ajax({
                        url: 'ajax_get_dynamic_data.php',
                        type: 'post',
                        data: { last_city: last_city, cat_id: catid },

                        success: function(response){
                            $('#post_trip_hotel').empty();
                            $('#post_trip_hotel').append(response);
                        }
                    });
                }
            }

        });
    </script>

    <script>
    //Display cities according to selected destination
    $(document).ready( function() {
        $('#country_dest').on('change', function(){
            var dest = $(this).val();
            var dest = dest.split(':');
            var dest_id = dest[0];
            
            $.ajax({
                url: 'ajax_get_city.php',
                type: 'post',
                data: { country: dest_id },

                success: function(response){
                    $('.city_drpdwn').empty();
                    $('.city_drpdwn').append(response);
                    $('.city_drpdwn').select2();
                    
                    $('.cty_drpdwn').empty();
                    $('.cty_drpdwn').append(response);
                    $('.cty_drpdwn').select2();
                    
                    $('.hotel_dropdown').empty();
                    $('.hotel_dropdown').append('<option value="">Select Hotel</option>');
                    
                    /*$('.activity_drpdwn option').remove();
                    $('.activity_drpdwn').multiselect('rebuild');
                    $('.activity_drpdwn').multiselect('refresh');*/
                }
            });
        });
    } );
    </script>

    <script>
    //Get Trip Stopovers and Extensions according to selected category
    $('#trip_category').change(function(){
        var cat_id  = $(this).val();
        var action = 'st_ext';
        
        $.ajax({
            url: 'ajax_get_dynamic_data.php',
            type: 'post',
            data: { cat_id: cat_id, action: action },

            success: function(response){
                $('#trip_stopover').empty();
                $('#trip_stopover').append(response);
                $('#trip_extension').empty();
                $('#trip_extension').append(response);
            }
        });
    });
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var summary_cnt = $('#summary_cnt').val();
        if(summary_cnt!=''){
            var id = summary_cnt; 
        } else {
            var id = 1;
        }
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_summary").on('click',function(){
        if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div class="col-md-6"><div id="box_'+id+'"><div class="form-group"><label class="user_login_label">Trip Summary '+id+':</label><div class="hotel-box"><input type="text" name="summary_text[]" id="summary_text_'+id+'" class="form-control  form-control01"></div> <div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div></div></div>';
            $("#summary_boxes > .row").append(append_data); //append new text box in main div
            $("#box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            } else {
                alert("Maximum 20 summary text are allowed");
            }
        });
    });

    //If the cross icon was clicked
    function remove_image(sumnum){
        $("#box_"+sumnum).parent().css('background','tomato');
        $("#box_"+sumnum).parent().fadeOut(800,function(){
           $("#box_"+sumnum).parent().remove();
        });
    }
    </script>
      
    <script type="text/javascript">

    $(document).ready(function(){
        var prices_cnt = $('#prices_cnt').val();
        if(prices_cnt!=''){
            var id = prices_cnt; 
        } else {
            var id = 1;
        }
        
        var max = 20,append_data;

        //If the add icon was clicked
        $(".add_price").on('click',function(){
        //if($("div[id^='box_']").length < max){ //Don't add new textbox if max limit exceed
            //$(this).remove(); //remove the add icon from current text box
            id++;
            var append_data = '<div id="price_box_'+id+'" class="price_box"><div class="row"><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_start_date_'+id+'">Start Date</label><div class="hotel-box"><input type="text" name="trip_start_date[]" id="trip_start_date_'+id+'" class="form-control form-control01 date-type" placeholder="Start Date"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_end_date_'+id+'">End Date</label><div class="hotel-box"><input type="text" name="trip_end_date[]" id="trip_end_date_'+id+'" class="form-control form-control01 date-type" placeholder="End Date"></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_styles_'+id+'">Trip Type</label><div class="hotel-box"><select name="trip_styles[]" id="trip_styles_'+id+'" class="form-control form-control01"><option value="">Please Select</option> <?php $style_qry = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE status = 'Active' "); while($style_res = mysqli_fetch_assoc($style_qry)){ ?><option value="<?php echo $style_res['style_id']; ?>"><?php echo $style_res['trip_style']; ?></option><?php } ?></select></div></div></div><div class="col-md-3"><div class="form-group"><label class="user_login_label" for="trip_price_'+id+'">Trip Price ($)</label><div class="hotel-box"><input type="text" name="trip_price[]" id="trip_price_'+id+'" class="form-control form-control01" placeholder="Trip Price ($)"></div></div></div></div><div class="remove-box"><img src="assets/images/remove-icon.png" id="'+id+'" onclick="remove_image(this.id)" /></div></div>';
            $("#price_boxes").append(append_data); //append new text box in main div
            $("#price_box_"+id).effect("bounce", { times:3 }, 300); //display block appended text box with silde down

            $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'dd-mm-yy' });
            
            /*} else {
                alert("Maximum 20 summary text are allowed");
            }*/
        });
    });

    //If the cross icon was clicked
    function remove_image(pricenum){
        $("#price_box_"+pricenum).css('background','tomato');
        $("#price_box_"+pricenum).fadeOut(800,function(){
           $("#price_box_"+pricenum).remove();
        });
    }
    </script>

    <script>
        $('.cty_drpdwn').select2();
        $('.act_drpdwn').multiselect({
            inheritClass: true,
            buttonWidth: '100%',
            nonSelectedText: 'Select Activities'
        });
        CKEDITOR.replaceAll( 'itinerary_detail' );
        CKEDITOR.replaceAll( 'inclusions' );
        CKEDITOR.replaceAll( 'exclusions' );
    </script>

    <script>
    $( function() {
        $( ".date-type" ).datepicker({ dateFormat: 'dd-mm-yy' });
    });
    </script>

  </body>
</html>
