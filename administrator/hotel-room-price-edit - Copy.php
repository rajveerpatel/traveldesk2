<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];

    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }

        //List Access
        if($_SESSION['AdminType']=='SuperAdmin' || in_array('hotels', $_SESSION['AccessRights'])){
            $list_display = "";
        } else {
            $list_display = "display: none;";
        }
        
    }

    $id = $_REQUEST['id'];
    $hid = $_REQUEST['hid'];

    if( !empty($_POST['dFromDate']) )
    {
       $dFromDate = date('Y-m-d', strtotime($_POST['dFromDate']));
       $dToDate = date('Y-m-d', strtotime($_POST['dToDate']));  
       if( sizeof( $_POST['single_room_cost'] ) > 0 || sizeof( $_POST['double_room_cost'] ) > 0 || sizeof( $_POST['triple_room_cost'] ) > 0 || sizeof( $_POST['qad_room_cost'] ) > 0 )
       {
           

           $qry = "UPDATE tblroomprice SET 
                    dFromDate='".$dFromDate."',
                    dToDate='".$dToDate."',
                    room_type_id='".$_POST['room_type_id']."',
                    single_room_cost='".$_POST['single_room_cost']."',
                    double_room_cost='".$_POST['double_room_cost']."',
                    triple_room_cost='".$_POST['triple_room_cost']."',
                    qad_room_cost='".$_POST['qad_room_cost']."',
                    status = '".$_POST['status']."'
                    WHERE iRoomPriceId = $id ";

                    mysqli_query($con, $qry);
        }
        
        
        
        header("location: hotel-room-price.php?id=$hid");
    }

    $row = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM tblroomprice WHERE iRoomPriceId = $id "));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Edit Hotel Room Price - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Edit Hotel Room Price</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="hotel-room-price.php?id=<?php $_REQUEST['id'];?>">Hotels Room Price List</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    
                    <div class="col-md-6">
                    <div class="form-group">
                            
                            <label class="user_login_label" for="dFromDate">Date From*</label>
                            <input type="text" name="dFromDate" id="dFromDate" class="form-control form-control01 date-type" placeholder="Date From*" value="<?php if($row['dFromDate']!='0000-00-00' && $row['dFromDate']!='1970-01-01'){ echo date('m/d/Y', strtotime($row['dFromDate'])); } else { echo ''; } ?>" required>

                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                        <label class="user_login_label" for="dToDate">Date To*</label>
                            <input type="text" name="dToDate" id="dToDate" class="form-control form-control01 date-type"  placeholder="To Date*" value="<?php if($row['dToDate']!='0000-00-00' && $row['dToDate']!='1970-01-01'){ echo date('m/d/Y', strtotime($row['dToDate'])); } else { echo ''; } ?>" required>

                            
                        </div>
                    </div>
                    <div class="col-md-12">
                    <p class="head11">Hotel Room Type</p>
                        <div id="hotel_rooms" class="hotel_boxes">
                            <div class="row" id="room_1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Room Type Name :</label>
                                        <div class="hotel-box">
                                            <select name="room_type_id" class="form-control form-select">
                                                <option value="">Please Select</option>
                                                <?php
                                                $qroom = mysqli_query($con, "SELECT * FROM tbl_room_type WHERE status = 'Active' ");
                                                while($rroom = mysqli_fetch_assoc($qroom)){
                                                ?>
                                                <option value="<?php echo $rroom['room_type_id']; ?>" <?php if($rroom['room_type_id']==$row['room_type_id']){ echo 'selected'; } ?>><?php echo $rroom['room_type_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                       
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Single Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="single_room_cost" id="single_room_cost_1" class="form-control form-control01" value="<?php echo $row['single_room_cost']; ?>">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Double Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="double_room_cost" id="double_room_cost_1" class="form-control form-control01" value="<?php echo $row['double_room_cost']; ?>">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">Triple Room Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="triple_room_cost" id="triple_room_cost_1" class="form-control form-control01" value="<?php echo $row['triple_room_cost']; ?>">
                                        </div>   
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-floating mb-3">QAD Bed Cost :</label>
                                        <div class="hotel-box">
                                            <input type="text" name="qad_room_cost" id="qad_room_cost_1" class="form-control form-control01" value="<?php echo $row['qad_room_cost']; ?>">
                                        </div>   
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-floating mb-3" for="status">Status*</label>
                            <select name="status" id="status" class="form-control form-select">
                                <option value="Active" <?php if($row['status']=='Active'){ echo 'selected'; } ?>>Active</option>
                                <option value="Inactive" <?php if($row['status']=='Inactive'){ echo 'selected'; } ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    

                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
       <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
      
    <script>
    $( function() {
        $( "#dFromDate" ).datepicker({ 
            dateFormat: 'mm/dd/yy',
            minDate: 0 
        });/*.on('change', function(selected){
            //var min_date = new Date(selected.timeStamp);
            alert(new Date(selected.timeStamp));
            //$('#validity_end_date').datepicker({minDate: min_date});
        });*/
        $( "#dToDate" ).datepicker({ dateFormat: 'mm/dd/yy', maxDate: "+2Y" });
        $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });
    });
    </script>

    
    
</body>

</html>