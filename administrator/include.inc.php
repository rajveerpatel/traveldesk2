<?php
ini_set('display_errors', 0);
//date_default_timezone_set('Asia/Kolkata'); // CDT
include('includes/config.inc.php');
include("checkAdmin.inc.php");
include("includes/functions.php");


//file uploading folders
define('UPLOADS', '../uploads/');
define('BANNER_IMG', UPLOADS.'banner_img/');
define('BANNER_VID', UPLOADS.'banner_video/');
define('PACKAGE_IMG', UPLOADS.'trip_package/');
define('DESTINATION_IMG', UPLOADS.'destination/');
define('CATEGORY_IMG', UPLOADS.'trip_category/');
define('TRIP_TYPES_IMG', UPLOADS.'trip_types/');
define('PAGE_CONTENT_IMG', UPLOADS.'page_content_img/');
define('HOTEL_GALLERY_IMG', UPLOADS.'hotel_gallery_img/');
define('ITINERARY_IMG', UPLOADS.'itinerary_img/');
define('ACTIVITY_IMG', UPLOADS.'activities_img/');
?>