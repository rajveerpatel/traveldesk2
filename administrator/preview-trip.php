<?php 
    include('include.inc.php');

    $pagename = explode('.',$page);
    $page_name = $pagename[0];
    
    if(!empty($_SESSION['AccessRights'])){
        if($_SESSION['AdminType']!='SuperAdmin' && !in_array($page_name, $_SESSION['AccessRights'])){
            header('location:index.php');
        }        
    }

    //print_r($_POST);
    //die();
    // $_SESSION['trip_name'] = $_POST['trip_name'];
    // $_SESSION['trip_thumb'] = $_FILES['trip_thumb']['name'];
    // $_SESSION['trip_themes'] = $_POST['trip_themes'];
    // $_SESSION['trip_category'] = $_POST['trip_category'];
    // $_SESSION['trip_type'] = $_POST['trip_type'];
    // $_SESSION['validity_start_date'] = $_POST['validity_start_date'];
    // $_SESSION['validity_end_date'] = $_POST['validity_end_date'];
    // $_SESSION['related_trip'] = $_POST['related_trip'];
    // $_SESSION['country_dest'] = $_POST['country_dest'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['fixed_price_deal'] = $_POST['fixed_price_deal'];
    // $_SESSION['percentge_price_deal'] = $_POST['percentge_price_deal'];
    // $_SESSION['short_highlights'] = $_POST['short_highlights'];
    // $_SESSION['trip_inclusions'] = $_POST['trip_inclusions'];
    // $_SESSION['trip_highlights'] = $_POST['trip_highlights'];


    // $_SESSION['itinerary_city'] = $_POST['itinerary_city'];
    // $_SESSION['purchase_start_date'] = $_POST['purchase_start_date'];
    // $_SESSION['purchase_end_date'] = $_POST['purchase_end_date'];
    // $_SESSION['blackout_start_date'] = $_POST['blackout_start_date'];
    // $_SESSION['blackout_end_date'] = $_POST['blackout_end_date'];
    // $_SESSION['vehicle_type'] = $_POST['vehicle_type'];
    // $_SESSION['vehicle_cost'] = $_POST['vehicle_cost'];


    // $_SESSION['trip_days'] = $_POST['trip_days'];
    // $_SESSION['plan_frm_cnt'] = $_POST['plan_frm_cnt'];

    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];
    // $_SESSION['deal'] = $_POST['deal'];






    if( !empty($_POST['trip_name']) ){
        
        if(!empty($_FILES['trip_thumb']['name'])){
            $trip_thumb=uploadfiles($_FILES['trip_thumb']['name'], $_FILES['trip_thumb']['tmp_name'], $_FILES['trip_thumb']['error'], $_FILES['trip_thumb']['size'], PACKAGE_IMG);
        }else{
            $trip_thumb='';
        }
        
        /*if(isset($_POST['thumb_name'])){
            $trip_thumb = $_POST['thumb_name'];
        } else {
            $trip_thumb = '';
        }*/
        
        if(!empty($_POST['trip_themes'])){
            $trip_theme = $_POST['trip_themes'];
        } else {
            $trip_theme = '';
        }
        
        if(!empty($_POST['trip_category'])){
            $trip_cat = $_POST['trip_category'];
        } else {
            $trip_cat = '';
        }
        
        if(!empty($_POST['trip_type'])){
            $trip_type = implode(',', $_POST['trip_type']);
        } else {
            $trip_type = '';
        }
        
        if(!empty($_POST['validity_start_date'])){
            $validity_start_date = date('Y-m-d', strtotime($_POST['validity_start_date']));
        } else {
            $validity_start_date = '';
        }
        
        if(!empty($_POST['validity_end_date'])){
            $validity_end_date = date('Y-m-d', strtotime($_POST['validity_end_date']));
        } else {
            $validity_end_date = '';
        }
        
        if(!empty($_POST['related_trip'])){
            $related_trip = implode(',', $_POST['related_trip']);
        } else {
            $related_trip = '';
        }
        
        /*if(!empty($_POST['trip_provides'])){
            $trip_provides = implode(',', $_POST['trip_provides']);
        } else {
            $trip_provides = '';
        }*/
        
        function clean($string) {
           $string = str_replace(' ', '-', $string); 
           $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string); 

           return preg_replace('/-+/', '-', $string); 
        }
        /*$replacecharacters = '/[^A-Za-z0-9\-]/';*/
        $pack_title = clean(trim($_POST['trip_name']));
        $pack_dest = explode(':', $_POST['country_dest']);
        $pack_destination = clean(trim($pack_dest[1]));
        $duration = clean(trim($_POST['trip_days']));
        $slug=$pack_title.'-'.$pack_destination.'-'.$duration.'-days';

        /*$qslug = mysqli_query($con, "select slug from tbl_trip_packages ");
        if(mysqli_num_rows($qslug) > 0){
            for($i=1; $i<=30; $i++){
                while($rslug = mysqli_fetch_assoc($qslug)){
                    $slug_arr[] = $rslug['slug'];
                }
                if($i!=1){ $slugend = '-'.$i; } else { $slugend = ''; }
                $new_slug = strtolower($slug.$slugend);
                if(!in_array($new_slug, $slug_arr)){
                    $slug= $new_slug;
                    break;
                }
            }
        }*/
        
        $tsql = "INSERT INTO tbl_trip_packages SET 
        trip_sku = '".$_POST['trip_sku']."',
        trip_name = '".mysqli_real_escape_string($con, $_POST['trip_name'])."',
        trip_thumb = '".$trip_thumb."',
        banner_id = '".$_POST['banner_id']."',
        slug = '".trim(strtolower($slug))."',
        trip_days = '".$_POST['trip_days']."',
        parent_dest = '".$_POST['parent_dest']."',
        country_dest = '".$_POST['country_dest']."',
        trip_themes = '".$trip_theme."',
        trip_category = '".$trip_cat."',
        trip_type = '".$trip_type."',
        validity_start_date = '".$validity_start_date."',
        validity_end_date = '".$validity_end_date."',
        short_highlights = '".mysqli_real_escape_string($con, $_POST['short_highlights'])."',
        trip_inclusions = '".mysqli_real_escape_string($con, $_POST['trip_inclusions'])."',
        trip_highlights = '".mysqli_real_escape_string($con, $_POST['trip_highlights'])."',
        trip_map = '".mysqli_real_escape_string($con, $_POST['trip_map'])."',
        related_trip = '".$related_trip."',
        deal = '".$_POST['deal']."',
        original_strike_twin_sharing = '".$_POST['original_strike_twin_sharing']."',
        status = '".$_POST['status']."',
        postip = '".$_SERVER['REMOTE_ADDR']."',
        postdate=NOW() ";
        mysqli_query($con, $tsql);
        
        $tripid = mysqli_insert_id($con);
        
        //add itenerary
        if( sizeof( $_POST['itinerary_city'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_itineraries WHERE trip_id = $tripid ");
            mysqli_query($con, "DELETE FROM tbl_trip_itinerary_day_plan WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['itinerary_city'] as $key => $value ){
                 if( !empty( $_POST['itinerary_city'][$key]  ) ){
                    $meals = implode(' | ', $_POST['meals_'.$i]);
                    $isql = "INSERT INTO tbl_trip_itineraries set
                    trip_id = '$tripid',
                    cat_id = '$trip_cat',
                    day = '".$_POST['day'][$key]."',
                    hotel = '".mysqli_real_escape_string($con, $_POST['hotel'][$key])."',
                    meals = '".mysqli_real_escape_string($con, $meals)."',
                    city_id = '".mysqli_real_escape_string($con, $_POST['itinerary_city'][$key])."' ";
                    mysqli_query($con, $isql);

                    $itinerary_id = mysqli_insert_id($con);
                    
                    if( $_POST['plan_frm_cnt'][$key] > 0 ){
                        $form_cnt = $_POST['plan_frm_cnt'][$key];
                        for( $cnt = 1; $cnt <= $form_cnt; $cnt++ ){
                            if(isset($_POST['day_act_type_'.$i.'_'.$cnt])){
                                 $day_plan = $_POST['day_act_type_'.$i.'_'.$cnt];
                            } else {
                                 $day_plan = '';
                            }
                            if(isset($_POST['flight_train_no_'.$i.'_'.$cnt])){
                                 $flight_train_no = $_POST['flight_train_no_'.$i.'_'.$cnt];
                            } else {
                                 $flight_train_no = '';
                            }
                            if(isset($_POST['flight_train_no_'.$i.'_'.$cnt])){
                                 $flight_train_no = $_POST['flight_train_no_'.$i.'_'.$cnt];
                            } else {
                                 $flight_train_no = '';
                            }
                            if(isset($_POST['departure_time_'.$i.'_'.$cnt])){
                                 $departure_time = $_POST['departure_time_'.$i.'_'.$cnt];
                            } else {
                                 $departure_time = '';
                            }
                            if(isset($_POST['arrival_time_'.$i.'_'.$cnt])){
                                 $arrival_time = $_POST['arrival_time_'.$i.'_'.$cnt];
                            } else {
                                 $arrival_time = '';
                            }
                            if(isset($_POST['dep_city_code_'.$i.'_'.$cnt])){
                                 $dep_city_code = $_POST['dep_city_code_'.$i.'_'.$cnt];
                            } else {
                                 $dep_city_code = '';
                            }
                            if(isset($_POST['arr_city_code_'.$i.'_'.$cnt])){
                                 $arr_city_code = $_POST['arr_city_code_'.$i.'_'.$cnt];
                            } else {
                                 $arr_city_code = '';
                            }
                            if(isset($_POST['departure_city_'.$i.'_'.$cnt])){
                                 $departure_city = $_POST['departure_city_'.$i.'_'.$cnt];
                            } else {
                                 $departure_city = '';
                            }
                            if(isset($_POST['arrival_city_'.$i.'_'.$cnt])){
                                 $arrival_city = $_POST['arrival_city_'.$i.'_'.$cnt];
                            } else {
                                 $arrival_city = '';
                            }
                            if(isset($_POST['departure_date_'.$i.'_'.$cnt])){
                                 $departure_date = date('Y-m-d', strtotime($_POST['departure_date_'.$i.'_'.$cnt]));
                            } else {
                                 $departure_date = '';
                            }
                            if(isset($_POST['arrival_date_'.$i.'_'.$cnt])){
                                 $arrival_date = date('Y-m-d', strtotime($_POST['arrival_date_'.$i.'_'.$cnt]));
                            } else {
                                 $arrival_date = '';
                            }
                            if(isset($_POST['duration_'.$i.'_'.$cnt])){
                                 $duration = $_POST['duration_'.$i.'_'.$cnt];
                            } else {
                                 $duration = '';
                            }
                            if(isset($_POST['transfer_details_'.$i.'_'.$cnt])){
                                 $transfer_details = $_POST['transfer_details_'.$i.'_'.$cnt];
                            } else {
                                 $transfer_details = '';
                            }
                            if(isset($_POST['transport_details_'.$i.'_'.$cnt])){
                                 $transport_details = $_POST['transport_details_'.$i.'_'.$cnt];
                            } else {
                                 $transport_details = '';
                            }
                            if(isset($_POST['distance_'.$i.'_'.$cnt])){
                                 $distance = $_POST['distance_'.$i.'_'.$cnt];
                            } else {
                                 $distance = '';
                            }
                            if(isset($_POST['sightseeing_'.$i.'_'.$cnt])){
                                 $sightseeing = $_POST['sightseeing_'.$i.'_'.$cnt];
                            } else {
                                 $sightseeing = '';
                            }
                            
                            $plan_q = "INSERT INTO tbl_trip_itinerary_day_plan set
                            trip_id = $tripid,
                            iti_id = '$itinerary_id',
                            plan_name = '".mysqli_real_escape_string($con, $day_plan)."',
                            flight_train_no = '".mysqli_real_escape_string($con, $flight_train_no)."',
                            departure_time = '".mysqli_real_escape_string($con, $departure_time)."',
                            arrival_time = '".mysqli_real_escape_string($con, $arrival_time)."',
                            departure_city_code = '".mysqli_real_escape_string($con, $dep_city_code)."',
                            arrival_city_code = '".mysqli_real_escape_string($con, $arr_city_code)."',
                            departure_city = '".mysqli_real_escape_string($con, $departure_city)."',
                            arrival_city = '".mysqli_real_escape_string($con, $arrival_city)."',
                            departure_date = '".mysqli_real_escape_string($con, $departure_date)."',
                            arrival_date = '".mysqli_real_escape_string($con, $arrival_date)."',
                            duration = '".mysqli_real_escape_string($con, $duration)."',
                            transfer_details = '".mysqli_real_escape_string($con, $transfer_details)."',
                            transport_details = '".mysqli_real_escape_string($con, $transport_details)."',
                            distance = '".mysqli_real_escape_string($con, $distance)."',
                            sightseeing = '".mysqli_real_escape_string($con, $sightseeing)."' ";
                            mysqli_query($con, $plan_q);
                        }
                        //echo '<br>';
                    }
                    $i++;
                 }
             }
         }
        
        //add trip prices
        if( sizeof( $_POST['trip_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['trip_start_date'] as $key => $value ){
                 if( !empty( $_POST['trip_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['trip_end_date'][$key])){
                         $trip_end_date = date('Y-m-d', strtotime($_POST['trip_end_date'][$key]));
                     } else {
                         $trip_end_date = '';
                     }
                    
                    $q = "INSERT INTO tbl_trip_prices set 
                    trip_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['trip_start_date'][$key])))."',
                    land_single_room='".mysqli_real_escape_string($con, $_POST['land_single_room'][$key])."',
                    land_twin_sharing='".mysqli_real_escape_string($con, $_POST['land_twin_sharing'][$key])."',
                    land_triple_sharing='".mysqli_real_escape_string($con, $_POST['land_triple_sharing'][$key])."',
                    airland_single_room='".mysqli_real_escape_string($con, $_POST['airland_single_room'][$key])."',
                    airland_twin_sharing='".mysqli_real_escape_string($con, $_POST['airland_twin_sharing'][$key])."',
                    airland_triple_sharing='".mysqli_real_escape_string($con, $_POST['airland_triple_sharing'][$key])."',
                    trip_categories='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_styles=1,
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_prices WHERE trip_id = $tripid ");
        }
        
        //add trip blackout dates
        if( sizeof( $_POST['blackout_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_blackout_dates WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['blackout_start_date'] as $key => $value ){
                 if( !empty( $_POST['blackout_start_date'][$key] ) ){
                     
                     if(!empty($_POST['blackout_end_date'][$key])){
                         $blackout_end_date = date('Y-m-d', strtotime($_POST['blackout_end_date'][$key]));
                     } else {
                         $blackout_end_date = '';
                     }
                    
                     $q = "INSERT INTO tbl_trip_blackout_dates set 
                    blackout_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['blackout_start_date'][$key])))."',
                    blackout_end_date='".mysqli_real_escape_string($con, $blackout_end_date)."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_blackout_dates WHERE trip_id = $tripid ");
        }
        
        //add trip purchase dates
        if( sizeof( $_POST['purchase_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_purchase_dates WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['purchase_start_date'] as $key => $value ){
                 if( !empty( $_POST['purchase_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['purchase_end_date'][$key])){
                         $purchase_end_date = date('Y-m-d', strtotime($_POST['purchase_end_date'][$key]));
                     } else {
                         $purchase_end_date = '';
                     }
                    
                    $q = "INSERT INTO tbl_trip_purchase_dates set 
                    purchase_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['purchase_start_date'][$key])))."',
                    purchase_end_date='".mysqli_real_escape_string($con, $purchase_end_date)."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else{
            mysqli_query($con, "DELETE FROM tbl_trip_purchase_dates WHERE trip_id = $tripid ");
        }
        
        //add trip purchase dates
        /*if( sizeof( $_POST['season_start_date'] ) > 0 ){
            mysqli_query($con, "DELETE FROM tbl_trip_season_price WHERE trip_id = $tripid ");
            $i=1;
             foreach( $_POST['season_start_date'] as $key => $value ){
                 if( !empty( $_POST['season_start_date'][$key]  ) ){
                     
                     if(!empty($_POST['season_end_date'][$key])){
                         $season_end_date = date('Y-m-d', strtotime($_POST['season_end_date'][$key]));
                     } else {
                         $season_end_date = '';
                     }
                    
                    echo $q = "INSERT INTO tbl_trip_season_price set 
                    season_start_date='".mysqli_real_escape_string($con, date('Y-m-d', strtotime($_POST['season_start_date'][$key])))."',
                    season_end_date='".mysqli_real_escape_string($con, $season_end_date)."',
                    single_room='".mysqli_real_escape_string($con, $_POST['single_room'][$key])."',
                    twin_sharing='".mysqli_real_escape_string($con, $_POST['twin_sharing'][$key])."',
                    triple_sharing='".mysqli_real_escape_string($con, $_POST['triple_sharing'][$key])."',
                    cat_id='".mysqli_real_escape_string($con, $trip_cat)."',
                    trip_id=$tripid ";
                    mysqli_query($con, $q);
                }
            }
        } else {
            mysqli_query($con, "DELETE FROM tbl_trip_season_price WHERE trip_id = $tripid ");
        }*/
        
       // header("location: trip-packages.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Add Trip Package - TRAVDEK</title>
    
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    
    <link href="dist/css/pages/icon-page.css" rel="stylesheet">
    <link href="dist/css/pages/file-upload.css" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue fixed-layout">
    
    <div id="main-wrapper">
        
    <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <?php include("elements/lefttop.php"); ?>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                   <?php include("elements/header.php"); ?>
                </div>             
            </nav>
        </header>
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <?php include("elements/head.php"); ?>
            <!-- End Sidebar scroll-->
        </aside>
        
        <div class="page-wrapper">
            
            <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Trip Package</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-end">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"><a href="trip-packages.php">Trip Package</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        
                      <div class="card">
                            <div class="card-body">
                            <form name="change_pass" action="" method="post" enctype="multipart/form-data">
              <div class="row">
                    
                <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_name">Trip Name</label>
                            <?php echo $_POST['trip_name']; ?>
                            <input type="hidden" name="trip_name" value="<?php echo $_POST['trip_name'] ?>">
                        </div>
                </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_thumb">Trip Image*</label>
                            <!-- <input type="file" name="trip_thumb" id="trip_thumb" class="form-control form-control01" placeholder="Trip Thumb*" required> -->
                            <input type="hidden" name="trip_thumb" id="trip_thumb" value="<?php echo $_POST['trip_thumb'];?>" class="form-control form-control01">
                            <!-- <div id="uploaded"></div>
                            <p class="img_note">Note : Trip Image size should be 690 x 480</p> -->
                        </div>
                    </div>

                    <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_sku">Trip SKU</label>
                             <?php echo $_POST['trip_sku']; ?>
                            <input type="hidden" name="trip_sku" id="trip_sku" value="<?php echo $_POST['trip_sku'];?>"  class="form-control form-control01">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Banner Slider</label>
                            
                            
                                <?php
                                $ban = mysqli_query($con, "SELECT * FROM tbl_banners WHERE pbid = '".$_POST['banner_id']."' and status = 'Active' and banner_for = 'trip' ");
                                while($banres = mysqli_fetch_assoc($ban))
                                {
                                ?>
                               <?php echo $banres['pagename']; ?>
                                <?php } ?>
                                <input type="hidden" name="banner_id" value="<?php echo $_POST['banner_id'];?>" >
                            </select>
                            
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_start_date">Trip Validity Start Date*</label>
                            <?php echo $_POST['trip_sku']; ?>
                            <input type="hidden" name="validity_start_date" value="<?php echo $_POST['validity_start_date'] ?>">
                            
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="validity_end_date">Trip Validity End Date*</label>
                            <?php echo $_POST['trip_sku']; ?>
                            <input type="hidden" name="validity_end_date" value="<?php echo $_POST['validity_end_date'] ?>">
                            
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label">Status*</label>
                            <?php echo $_POST['status']; ?>
                            <input type="hidden" name="status" value="<?php echo $_POST['status'] ?>">

                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_style">Trip Type</label>
                            <?php 
                            $trip_type = $_REQUEST['trip_type'];

                                     if($trip_type)
                                        $trip_type_value = implode(",", $_POST["trip_type"]);
{
    foreach ($trip_type as $value)
    {
        $id = $value;
        $trip_type_inserts .=$id.",";
        //$sublist="SELECT * FROM `tblacservices` where iAcServiceId = '$parseid' order by iAcServiceId desc";
        $qry_style = mysqli_query($con, "SELECT * FROM tbl_trip_style WHERE style_id = '".$id."' and status = 'Active' ");
        while($res_style = mysqli_fetch_assoc($qry_style))
        {
           //$acparts1 += GetACPartsPrice($id);
            echo $res_style['trip_style'].", ";
        }    
    }
}
                             ?>
                             <input type="hidden" name="trip_type" value="<?php echo $trip_type_value; ?>">
                            
                        </div>
                    </div>
                    
                    
                </div>
                    <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Transportation Cost</p>
                        <div id="transportation_boxes" class="transportation_boxes">
                            <div id="transportation_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="vehicle_type_1">Vehicle Type</label>

                                            <div class="hotel-box">
                                             <?php 
                                                $vehicle_type = $_REQUEST['vehicle_type'];
                                                $vehicle_type_values = implode(",", $_POST["vehicle_type"]);
                                                if($vehicle_type){
                                                foreach ($vehicle_type as $value)
                                                {
                                                   $id = $value;
                                                   $vehicle_type_inserts .=$id.",";
                                                   

                                                   
        $vhe_type_data = mysqli_query($con, "SELECT * FROM tblvehicle WHERE vehicleid = '".$id."' and vehiclestatus = '1' ");
        while($res_style = mysqli_fetch_assoc($vhe_type_data))
        {
           //$acparts1 += GetACPartsPrice($id);
            echo $vhecle_type_dt = $res_style['vehicletype'].", ";
        } 
                                                }
                                            }
                                             ?>
         <input type="hidden" name="vehicle_type" value="<?php echo $vehicle_type_values ?>">


                                               
                                                
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="vehicle_cost_1">Vehicle Cost</label>
                                             <?php 
                                                $vehicle_cost = $_REQUEST['vehicle_cost'];
                                                echo  $vehicle_cost_values = implode(",", $_POST["vehicle_cost"]);
                                                
                                             ?>
                                           <input type="hidden" name="vehicle_cost" value="<?php echo $vehicle_cost_values ?>">
                                            
                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Available Dates</p>
                        <div id="price_boxes" class="price_boxes">
                            <div id="price_box_1" class="price_box col-md-2 col-1-8" style="display: inline-flex;">
                              <div class="row">  
                                    
                                        <div class="form-group">
                                            <label class="user_login_label">&nbsp;</label>
                                            <div class="form-group">
                                             <?php 
                                                $trip_start_date = $_REQUEST['trip_start_date'];
                                                $trip_start_date_values = implode(",", $_POST["trip_start_date"]);
                                                echo $trip_start_date_values1 = implode(",<br>", $_POST["trip_start_date"]);
                                                
                                             ?> <input type="hidden" name="trip_start_date" value="<?php echo $trip_start_date_values ?>">
                                                   
                                            </div>
                                        </div>
                                   </div>
                                                                    
                            </div>
                        </div>
                       
                    
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="user_login_label">Make a Deal: <input type="checkbox" <?php if($_POST['deal']=='Yes'){?> checked <? } ?> name="deal" id="deal" value="Yes"> Yes</label>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-4 deal_box" <?php if($_POST['deal']=='Yes'){ ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                <div class="form-group">
                                    <p class="ftl_txt02">Above price will be deals price and original price twin sharing will be struck off</p>
                                </div>
                            </div>
                            <div class="col-md-4 deal_box" <?php if($_POST['deal']=='Yes'){ ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                <div class="form-group">
                                    <label class="user_login_label" for="fixed_price_deal">Fixed Amount for Package</label>
                                    <?php echo $_POST['fixed_price_deal']; ?>
                                    <input type="hidden" name="fixed_price_deal" value="<?php echo $fixed_price_deal ?>">
                               </div>
                            </div>
                            <div class="col-md-4 deal_box" <?php if($_POST['deal']=='Yes'){ ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
                                <div class="form-group">
                                    <label class="user_login_label" for="percentge_price_deal">Percentage for Package %</label>
                                      <?php echo $_POST['percentge_price_deal']; ?>
                                    <input type="hidden" name="percentge_price_deal" value="<?php echo $percentge_price_deal ?>">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Blackout Dates</p>
                        <div id="blackout_boxes" class="blackout_boxes">
                            <div id="blackout_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_start_date_1"><strong>Blackout Start Date</strong></label>
                                            <?php 
                                                $blackout_start_date = $_REQUEST['blackout_start_date'];
                                                $blackout_start_date_values = implode(",", $_POST["blackout_start_date"]);
                                                echo $blackout_start_date_values1 = implode(",<br>", $_POST["blackout_start_date"]);
                                                
                                             ?> <input type="hidden" name="blackout_start_date" value="<?php echo $blackout_start_date_values ?>">
                                               
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="blackout_end_date_1">Blackout End Date</label>
                                            <?php 
                                                $blackout_end_date = $_REQUEST['blackout_end_date'];
                                                $blackout_end_date_values = implode(",", $_POST["blackout_end_date"]);
                                                echo $blackout_end_date_values1 = implode(",<br>", $_POST["blackout_end_date"]);
                                                
                                             ?> <input type="hidden" name="blackout_start_date" value="<?php echo $blackout_end_date_values ?>">



                                              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p class="head11">Purchase Dates</p>
                        <div id="purchase_boxes" class="purchase_boxes">
                            <div id="purchase_box_1" class="price_box">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_start_date_1">Purchase Start Date</label>
                                            
                                            <?php 
                                                $purchase_start_date = $_REQUEST['purchase_start_date'];
                                                $purchase_start_date_values = implode(",", $_POST["purchase_start_date"]);
                                                echo $purchase_start_date_values1 = implode(",<br>", $_POST["purchase_start_date"]);
                                                
                                             ?> <input type="hidden" name="purchase_start_date" value="<?php echo $purchase_start_date_values ?>">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="user_login_label" for="purchase_end_date_1">Purchase End Date</label>
                                            <?php 
                                                $purchase_end_date = $_REQUEST['purchase_end_date'];
                                                $purchase_end_date_values = implode(",", $_POST["purchase_end_date"]);
                                                echo $purchase_end_date_values1 = implode(",<br>", $_POST["purchase_end_date"]);
                                                
                                             ?> <input type="hidden" name="purchase_end_date" value="<?php echo $purchase_end_date_values ?>">
                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_map">Trip Map</label>
                            <?php echo $_REQUEST['trip_map']; ?>
                            <input type="hidden" name="trip_map" value="<?php echo $_REQUEST['trip_map']; ?>">
                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="parent_dest">Select Parent Destination</label>
                            <?php
                                $destinationres = mysqli_query($con, "SELECT * FROM tbl_parent_destinations WHERE parent_dest_id = '".$_POST['parent_dest']."' and status = 'Active'");
                                while($desres = mysqli_fetch_assoc($destinationres))
                                {
                                ?>
                               <?php echo $desres['parent_destination']; ?>
                                <?php } ?>
                                <input type="hidden" name="parent_dest" value="<?php echo $_POST['parent_dest'];?>">
                            
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="country_dest">Select Country Destination*</label>
                            <?php
                                $coudestinationres = mysqli_query($con, "SELECT * FROM tbl_destinations WHERE dest_id = '".$_POST['country_dest']."' and status = 'Active'");
                                while($coudesres = mysqli_fetch_assoc($coudestinationres))
                                {
                                ?>
                               <?php echo $coudesres['destination']; ?>
                                <?php } ?>
                                <input type="hidden" name="country_dest" value="<?php echo $_POST['parent_dest'];?>">





                         
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_category">Select Trip Categories</label>
                            <?php
                                $tripcategories = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE cat_id = '".$_POST['trip_category']."' and status = 'active'");
                                while($tripcats = mysqli_fetch_assoc($tripcategories))
                                {
                                ?>
                               <?php echo $tripcats['category_name']; ?>
                                <?php } ?>
                                <input type="hidden" name="trip_category" value="<?php echo $_POST['trip_category'];?>">
                           
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_type">Select Trip Themes</label>

                            <?php
                                $tripstyles = mysqli_query($con, "SELECT * FROM tbl_trip_types WHERE trip_type_id = '".$_POST['trip_themes']."' and status = 'active'");
                                while($tripsty = mysqli_fetch_assoc($tripstyles))
                                {
                                ?>
                               <?php echo $tripsty['trip_type']; ?>
                                <?php } ?>
                                <input type="hidden" name="trip_themes" value="<?php echo $_POST['trip_themes'];?>">

                            
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_inclusions">Trip Inclusions</label>
                            <?php echo $_REQUEST['trip_inclusions']; ?>
                            <input type="hidden" name="trip_inclusions" value="<?php echo $_REQUEST['trip_inclusions']; ?>">
                            
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_highlights">Trip Highlights</label>
                            <?php echo $_REQUEST['trip_highlights']; ?>
                            <input type="hidden" name="trip_highlights" value="<?php echo $_REQUEST['trip_highlights']; ?>">
                            
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="trip_days">Trip Duration</label>
                            <?php echo $_REQUEST['trip_days']; ?>
                            <input type="hidden" name="trip_days" value="<?php echo $_REQUEST['trip_days']; ?>">
                            
                        </div>
                    </div>
                </div>

                <?php   
                    
                     $i=0;
                     
                     $num = 1;
                      for($i=1, $j=0; $i<=$_REQUEST['trip_days']; $i++, $j++)
                      {
                         //$j++;     
                 ?>
               <div class="col-md-12">
                     <div class="form-group">
                        <label class="user_login_label">DAY <? echo $i; ?></label></div>
                    </div>
                    <input type="hidden" name="day[]" class="day" value="<? echo $i; ?>" >


   <?php             $itinerary_city = $_REQUEST['itinerary_city'];
                     $itinerary_city_values = implode(",", $_POST["itinerary_city"]);
                     $str_arr = preg_split ("/\,/", $itinerary_city_values); 


                    $q_itenary_city = mysqli_query($con, "SELECT * FROM tbl_cities WHERE CityId= '".$str_arr[$j]."' "); 
                    $rowcount_city = mysqli_num_rows( $q_itenary_city );
                    if($rowcount_city)
                    {
                    
    ?>



                    <div class="col-md-12">
                        <div class="form-group city_box">
                            <label class="user_login_label" for="trip_days">Itinerary City</label>
                            
                           <?php 
                       $res_style_city = mysqli_fetch_assoc($q_itenary_city);    
                       echo $res_style_city['CityName'];                                       
        
                                               
                                            
                                             ?>
                                    <input type="hidden" name="itinerary_city[]"  value="<? echo $str_arr[$j]; ?>" >
                            
                         </div>
                     </div>

                     <?  } ?>



                     <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="row">
                                    <div class="col-md-12 optional_'+i+'">
                                        <div class="form-group">
                                            <div class="user_login_label">Flight/Train Details </div>
                                            <div id="flight_train_box" class="price_box flight_train_details">
                        <div class="row">


                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="flight_train_no_<? echo $i;?>_<?php echo $num; ?>'">Flight/Train No.</label>
                                    <div class="hotel-box">
                                        <?php $flightno =  flight_train_no_.$i."_".$num; 
                                    echo $_REQUEST[$flightno]; ?>
                                        <input type="hidden" name="flight_train_no_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$flightno]; ?>" >

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="departure_time_'+id+'_'+num+'">Departure Time</label>
                                    <div class="hotel-box">
                                            <?php $departure_time =  departure_time_.$i."_".$num; 
                                    echo $_REQUEST[$departure_time]; ?>
                                        <input type="hidden" name="departure_time_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$departure_time]; ?>" >

                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="arrival_time_'+id+'_'+num+'">Arrival Time</label>
                                    <div class="hotel-box">
                                        <?php $arrival_time =  arrival_time_.$i."_".$num; 
                                    echo $_REQUEST[$arrival_time]; ?>
                                        <input type="hidden" name="arrival_time_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$arrival_time]; ?>" >

                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="dep_city_code_'+id+'_'+num+'">Departure City Code</label>
                                    <div class="hotel-box">
                                            <?php $dep_city_code =  dep_city_code_.$i."_".$num; 
                                    echo $_REQUEST[$dep_city_code]; ?>
                                        <input type="hidden" name="dep_city_code_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$dep_city_code]; ?>" >

                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="arr_city_code_'+id+'_'+num+'">Arrival City Code:</label>
                                    <div class="hotel-box">
                                        <?php $arr_city_code =  arr_city_code_.$i."_".$num; 
                                    echo $_REQUEST[$arr_city_code]; ?>
                                        <input type="hidden" name="arr_city_code_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$arr_city_code]; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-2-5">
                                <div class="form-group"><label class="user_login_label" for="departure_city_'+id+'_'+num+'">Departure City:</label>
                                    <div class="hotel-box">
                                        <?php $departure_city =  departure_city_.$i."_".$num; 
                                    echo $_REQUEST[$departure_city]; ?>
                                        <input type="hidden" name="departure_city_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$departure_city]; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="arrival_city_'+id+'_'+num+'">Arrival City:</label>
                                <div class="hotel-box">
                                    <?php $arrival_city =  arrival_city_.$i."_".$num; 
                                    echo $_REQUEST[$arrival_city]; ?>
                                        <input type="hidden" name="arrival_city_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$arrival_city]; ?>" >
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-2-5"><div class="form-group"><label class="user_login_label" for="departure_date_'+id+'_'+num+'">Departure Date:</label>
                            <div class="hotel-box">
                                <?php $departure_date =  departure_date_.$i."_".$num; 
                                    echo $_REQUEST[$departure_date]; ?>
                                        <input type="hidden" name="departure_date_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$departure_date]; ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-2-5">
                        <div class="form-group"><label class="user_login_label" for="arrival_date_'+id+'_'+num+'">Arrival Date:</label>
                            <div class="hotel-box">
                                <?php $arrival_date =  arrival_date_.$i."_".$num; 
                                    echo $_REQUEST[$arrival_date]; ?>
                                        <input type="hidden" name="arrival_date_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$arrival_date]; ?>" >

                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-2-5">
                        <div class="form-group"><label class="user_login_label" for="duration_'+id+'_'+num+'">Duration:</label>
                            <div class="hotel-box">
                                <?php $duration =  duration_.$i."_".$num; 
                                    echo $_REQUEST[$duration]; ?>
                                        <input type="hidden" name="duration_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$duration]; ?>" >

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 optional_'+i+'">
                                        <div class="form-group">
                                            <div class="user_login_label">Airport/Station Transfer </div>
                                            <div id="transfer_box_'+id+'_'+num+'" class="price_box flight_train_details">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="hotel-box">

                                                                <?php $transfer_details =  transfer_details_.$i."_".$num; 
                                    echo $_REQUEST[$transfer_details]; ?>
                                        <input type="hidden" name="transfer_details_<? echo $i;?>_<?php echo $num; ?>" 
                                        value="<?php echo $_REQUEST[$transfer_details]; ?>" >    
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                    <?php 
                                         

                                             
                                             $sightseeing_city = $_REQUEST['day_act_type'];
                                             $sightseeing_values = implode(",", $_POST["day_act_type"]);
                                             $array=array_map('intval', explode(',', $sightseeing_values));
                                             $array = implode("','",$array);
                   
                        
                        $qry_sightseen = mysqli_query($con, "SELECT * FROM tbl_activities WHERE act_id in ('".$array."') and city_id = '".$str_arr[$j]."' ");
                            $rowcount = mysqli_num_rows( $qry_sightseen );
                            
                        if($rowcount) 
                        { 
                            
                            ?>

                            <div class="col-md-12 ">
                                        <div class="form-group">
                                            <div class="user_login_label">Sightseeing </div>

                                             <div id="sightseeing_box_'+num+'" class="price_box flight_train_details"><div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                        <?php  while($res_style = mysqli_fetch_assoc($qry_sightseen))
                        {
                                             // if($sightseeing_city)
                                             // {
                                                 
                                                 ?>

                                    
                                           





                           <?                        
                           
                            echo $res_style['activity_name'].", ";
                        
                                                    ?>
                                                    
                                                <input type="hidden" name="day_act_type[]" value="<?php echo $sightseeing_values; ?>">
                                                        
                            <? } ?>
                            </div>
                                            </div>
                                        </div>
                                    </div>
                                        </div>
                                    </div>                       
                                                
                                <? //}    
                    } ?>
                                    
                             <?php 
                                         

                                             
                                             
                                              $meal_details =  meals_.$i; 
                                             $mealvaules =  $_REQUEST[$meal_details];
                                             $meal_values = implode(",", $mealvaules);
                                             $meal_array=array_map('intval', explode(',', $meal_values));
                                             $meal_array = implode("','",$meal_array);
                   
                        
                        $qry_meals_sightseen = mysqli_query($con, "SELECT * FROM tbl_meals WHERE meal_id in ('".$meal_array."') and city_id = '".$str_arr[$j]."' ");
                        $rowcount_meal = mysqli_num_rows( $qry_meals_sightseen );
                            
                         if($rowcount_meal) 
                        { 
                            
                            ?>
                                    <div class="col-md-12 "><div class="form-group">
                                        <div class="user_login_label">Meal </div>
                                        <div id="meal_box_'+num+'" class="price_box flight_train_details">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                         <?php  while($res_style = mysqli_fetch_assoc($qry_meals_sightseen))
                        {
                                             // if($sightseeing_city)
                                             // {
                                                 
                                                 ?>

                                    
                                           





                           <?                        
                           
                            echo $res_style['meal_name'].", ";
                        
                                                    ?>
                                                    
                                                <input type="hidden" name="<?php echo $meal_details."[]";?>" value="<?php echo $meal_values; ?>">
                                                        
                            <? } ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                                

                                                    <?php 
                                             $hotel_city = $_REQUEST['hotel'];
                                             $hotel_values = implode(",", $_POST["hotel"]);
                                             $hotel_array=array_map('intval', explode(',', $hotel_values));
                                             $hotel_array = implode("','",$hotel_array);
                                             $qry_hotel_sightseen = mysqli_query($con, "SELECT * FROM tbl_hotels WHERE hotel_id in ('".$hotel_array."') and city_id = '".$str_arr[$j]."' ");
                        $rowcount_hotel = mysqli_num_rows( $qry_hotel_sightseen );
                                                if($rowcount_hotel)
                                                { ?>
<div class="col-md-12">
                                    <div class="form-group">
                                        <div class="user_login_label">Hotel </div>
                                        <div id="hotel_box_'+num+'" class="price_box flight_train_details">
                                            <div class="row">
                                                <div class="col-md-12">                                                
                                                   

        <?php  while($res_style = mysqli_fetch_assoc($qry_hotel_sightseen))
                        {
                                                                 
                           
                            echo $res_style['hotel_name'].", ";
                        
                                                    
                                            
                                             ?>
                                             <input type="hidden" name="hotel[]"  value="<? echo $hotel_values; ?>" >
                            
                        <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            
                            </div>
                            <?php } ?>
                        </div>
                        <?
                            
                            $plan_frm_cnt = $_REQUEST['plan_frm_cnt'];
                            $planfrm_values = implode(",", $_POST["plan_frm_cnt"]);
                            
                        ?>
                        <input type="hidden" name="plan_frm_cnt[]" value="<? echo $planfrm_values; ?>">
                    </div>
                </div>
              <?php } ?>
              

      
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="user_login_label" for="short_highlights">Trip Short Highlightfv</label>
                            <?php echo $_REQUEST['short_highlights']; ?>
                            <input type="hidden" name="short_highlights" value="<?php echo $_REQUEST['short_highlights']; ?>">
                            
                        </div>
                    </div>
                </div>
                    
                    <div class="col-md-12">
                        <div class="btn00 text-center">
                        <button type="submit" class="btn btn-primary text-white">Submit</button>
                            
                        </div>
                    </div>
                </div>
            </form>
                                
                                

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <div id="ImgCrop" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                          <div id="image" style="width:250px; margin-top:20px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success crop_image">Crop & Upload Image</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
        <?php include("elements/footer.php"); ?>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/node_modules/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="dist/js/pages/jasny-bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#deal').click(function(){
            if($(this).prop('checked')==true){
                $('.deal_box').css('display', 'block');
            } else {
                $('.deal_box').css('display', 'none');
            }
        });
    </script>

   
    
<?php
$catqry = mysqli_query($con, "SELECT * FROM tbl_trip_categories WHERE status = 'Active' ");
while($catres = mysqli_fetch_assoc($catqry)){
    $catoption .= '<option value="'.$catres['cat_id'].'">'.$catres['category_name'].'</option>';
}
?>
    
<script>
    $( function() {
        $( "#validity_start_date" ).datepicker({ 
            dateFormat: 'mm/dd/yy',
            minDate: 0 
        });/*.on('change', function(selected){
            //var min_date = new Date(selected.timeStamp);
            alert(new Date(selected.timeStamp));
            //$('#validity_end_date').datepicker({minDate: min_date});
        });*/
        $( "#validity_end_date" ).datepicker({ dateFormat: 'mm/dd/yy', maxDate: "+2Y" });
        $( ".date-type" ).datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' });
    });
    </script>
    
</body>

</html>